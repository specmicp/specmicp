#! /usr/bin/env sh

# This script run the PGO (Profile guided optimization) for Reactmicp
# It accepts one argument that should be passed to make (typically -jx)

# author : fabien georget <fabieng@princeton.edu>

make clean
cmake . -DSPECMICP_PROFILE_GENERATE:BOOL=TRUE -DSPECMICP_PROFILE_USE:BOOL=FALSE -DSPECMICP_USE_OPENMP=FALSE
make $1
make install
make $1 ex_reactmicp_carbo
cd examples/ && ./ex_reactmicp_carbo
#make $1 ex_reactmicp_carbo_static # only make what we need
#cd examples/ && ./ex_reactmicp_carbo_static
cd ..
cmake . -DSPECMICP_PROFILE_GENERATE:BOOL=FALSE -DSPECMICP_PROFILE_USE:BOOL=TRUE -DSPECMICP_USE_OPENMP=TRUE
make $1
make install
