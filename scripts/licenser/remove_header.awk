BEGIN { is_file_start = 1 ; is_in_comment=0; }
{
    if (is_file_start == 1)
    {
        if (is_in_comment == 0)
        {
            if (/^\/\*/)
                is_in_comment = 1;
            else
            {
                print $0;
                is_file_start = 0;
            }
        }
        else
        {
            if (/\*\//)
            {
                is_in_comment = 0;
            }
        }
    }
    else
    {
        print $0;
    }
}
