#! /usr/bin/env bash

SRC_DIR=$(dirname $0)

tmp1=$(tempfile)
tmp2=$(tempfile)

# copy file
cp $1 ${tmp1}
# remove old header
awk -f ${SRC_DIR}/remove_header.awk ${tmp1} > ${tmp2}
# copy new header
cat $2 ${tmp2} > ${tmp1}

rm ${tmp2}
# finally mv the temporary
mv ${tmp1} $1
