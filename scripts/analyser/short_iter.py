import pandas
import numpy as np

def analyse_file(iter_file, out_file):
    df = open_file(iter_file)
    with open(out_file, 'w') as out_file:
        out_file.write("\n")
        analyse_timestep(df, out_file)
        analyse_iter(df, out_file)
        analyse_return_code(df, out_file)
        analyse_cpu_time(df, out_file)
        analyse_cpu_transport_time(df, out_file)
        analyse_cpu_chemistry_time(df, out_file)
        out_file.write("\n")

def open_file(iter_file):
    df = pandas.read_csv(iter_file,
                header=1, sep="\t\s*",
                skiprows=8, skipfooter=14,
                skip_blank_lines=False, usecols=range(1,11), index_col=False)
    return df
                          
             
def basic_print(serie, out_file):
    out_file.write("mean : {0:.4f}\n".format(serie.mean()))
    out_file.write("std : {0:.4f}\n".format(serie.std()))
    out_file.write("median : {0:.4f}\n".format(serie.median()))
    out_file.write("min : {0:.4f}\n".format(serie.min()))
    out_file.write("max : {0:.4f}\n".format(serie.max()))
    
    
def analyse_iter(df, out_file):
    try:
        c_iter = df["Iterations"]
    except KeyError:
        c_iter = df["Iterations "] # ... format ...
                
    out_file.write("\nIterations\n=========\n")
    basic_print(c_iter, out_file)
    hist_n, hist_range = np.histogram(c_iter)
    out_file.write("\nHistogram : \n")
    for i, n in enumerate(hist_n):
        out_file.write("{0}\t{1}\n".format(hist_range[i], n))
    out_file.write("{0}\t{1}\n".format(hist_range[-1], 0))

def analyse_timestep(df, out_file):
    dt_time = df["dt"]
    
    out_file.write("Nb timesteps : {0}\n".format(dt_time.count()))  

    t_time = df["T"] 
    ret_code = df["Return_code"]
    
    out_file.write("Last timestep : {0} with retcode {1}\n".format(t_time[t_time.last_valid_index()],ret_code[ret_code.last_valid_index()]))
    
    out_file.write("\nTimestep\n=========\n")
    basic_print(dt_time, out_file)

def analyse_return_code(df, out_file):
    ret_code = df["Return_code"]

    out_file.write("\nReturn codes\n=========\n")

    hist_n, hist_range = np.histogram(ret_code, bins=[-3,-2,-1,0,1,2,3,4,5])
    for i, n in enumerate(hist_n):
        out_file.write("{0}\t{1}\n".format(hist_range[i], n))
    out_file.write("{0}\t{1}\n".format(hist_range[-1], 0))
    
    
def analyse_cpu_time(df, out_file):
    t_time = df["Time"]
    out_file.write("\nIteration CPU Time\n=========\n")
    basic_print(t_time, out_file)
    
def analyse_cpu_transport_time(df, out_file):
    t_time = df["Transport_time"]
    out_file.write("\nIteration CPU Transport Time\n=========\n")
    basic_print(t_time, out_file)
        
def analyse_cpu_chemistry_time(df, out_file):
    t_time = df["Chemistry_time"]
    out_file.write("\nIteration CPU Chemistry Time\n=========\n")
    basic_print(t_time, out_file)
        
    
if __name__ == "__main__":
    import sys
    analyse_file(sys.argv[1], sys.argv[2])
        
    
                
                
