// this file is a micro-benchmark of the maths functions
//

// It uses the google/benchmark framework

#include <benchmark/benchmark.h>

#include "specmicp_common/types.hpp"
#include "dfpm/mesh.hpp"


static void uniform_mesh_creation(benchmark::State& state)
{
    while (state.KeepRunning()) {
        auto the_mesh = specmicp::mesh::uniform_mesh1d({0.1, 50, 5.0});
    }
}
BENCHMARK(uniform_mesh_creation);

static void ramp_mesh_creation(benchmark::State& state)
{
    while (state.KeepRunning()) {
        specmicp::mesh::Ramp1DMeshGeometry ramp_mesh_geom;
        ramp_mesh_geom.dx_min         = 0.05;
        ramp_mesh_geom.dx_max         = 0.5;
        ramp_mesh_geom.length_ramp    = 2;
        ramp_mesh_geom.length_plateau = 10;
        ramp_mesh_geom.section        = 5;
        auto the_mesh = specmicp::mesh::ramp_mesh1d(ramp_mesh_geom);
    }
}
BENCHMARK(ramp_mesh_creation);

static void nb_nodes_50(benchmark::State& state)
{
    auto the_mesh = specmicp::mesh::uniform_mesh1d({0.1, 50, 5.0});
    while (state.KeepRunning()) {
        for (int i=0; i<50; ++i)
        {
            auto n = the_mesh->nb_nodes();
            benchmark::DoNotOptimize(n);
        }
    }
}
BENCHMARK(nb_nodes_50);

static void nb_elements_50(benchmark::State& state)
{
    auto the_mesh = specmicp::mesh::uniform_mesh1d({0.1, 50, 5.0});
    while (state.KeepRunning()) {
        for (int i=0; i<50; ++i)
        {
            auto n = the_mesh->nb_elements();
            benchmark::DoNotOptimize(n);
        }
    }
}
BENCHMARK(nb_elements_50);

static void get_position_50(benchmark::State& state)
{
    auto the_mesh = specmicp::mesh::uniform_mesh1d({0.1, 50, 5.0});
    while (state.KeepRunning()) {
        for (int i=0; i<50; ++i)
        {
            auto x = the_mesh->get_position(i);
            benchmark::DoNotOptimize(x);
        }
    }
}
BENCHMARK(get_position_50);

static void get_dx_50(benchmark::State& state)
{
    auto the_mesh = specmicp::mesh::uniform_mesh1d({0.1, 50, 5.0});
    while (state.KeepRunning()) {
        for (auto n: the_mesh->range_nodes())
        {
            auto x = the_mesh->get_dx(n);
            benchmark::DoNotOptimize(x);
        }
    }
}
BENCHMARK(get_dx_50);

static void get_volume_cell_element_50(benchmark::State& state)
{
    auto the_mesh = specmicp::mesh::uniform_mesh1d({0.1, 50, 5.0});
    while (state.KeepRunning()) {
        for (auto n: the_mesh->range_elements())
        {
            auto x = the_mesh->get_volume_cell_element(n, 0);
            benchmark::DoNotOptimize(x);
        }
    }
}
BENCHMARK(get_volume_cell_element_50);

static void get_volume_element_50(benchmark::State& state)
{
    auto the_mesh = specmicp::mesh::uniform_mesh1d({0.1, 50, 5.0});
    while (state.KeepRunning()) {
        for (auto n:the_mesh->range_elements())
        {
            auto x = the_mesh->get_volume_element(n);
            benchmark::DoNotOptimize(x);
        }
    }
}
BENCHMARK(get_volume_element_50);



BENCHMARK_MAIN()
