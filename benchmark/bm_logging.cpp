// this file is a micro-benchmark of the logging capacity
//
// right now, this is mainly a test of the data_container.

// It uses the google/benchmark framework


#include <benchmark/benchmark.h>

#include "specmicp_common/log.hpp"

#include <iostream>
#include <fstream>

using namespace specmicp;

static void InitLogger(benchmark::State& state) {
    while (state.KeepRunning()) {
        std::ofstream stream("/dev/null");
        specmicp::init_logger(&stream, specmicp::logger::Info);
    }
}

BENCHMARK(InitLogger);


static void DebugMessage(benchmark::State& state) {
    std::ofstream stream("/dev/null");
    specmicp::init_logger(&stream, specmicp::logger::Info);
    while (state.KeepRunning()) {
        DEBUG << "debug message";
    }
}
// Register the function as a benchmark
BENCHMARK(DebugMessage);

static void DisregardedWarningMessage(benchmark::State& state) {
    std::ofstream stream("/dev/null");
    specmicp::init_logger(&stream, specmicp::logger::Error);
    while (state.KeepRunning()) {
        WARNING << "warning message";
    }
}
// Register the function as a benchmark
BENCHMARK(DisregardedWarningMessage);

static void WarningMessage(benchmark::State& state) {
    std::ofstream stream("/dev/null");
    specmicp::init_logger(&stream, specmicp::logger::Info);
    while (state.KeepRunning()) {
        WARNING << "warning message";
    }
}
// Register the function as a benchmark
BENCHMARK(WarningMessage);

static void ErrorMessage(benchmark::State& state) {
    std::ofstream stream("/dev/null");
    specmicp::init_logger(&stream, specmicp::logger::Info);
    while (state.KeepRunning()) {
        WARNING << "error message";
    }
}
// Register the function as a benchmark
BENCHMARK(ErrorMessage);

BENCHMARK_MAIN()
