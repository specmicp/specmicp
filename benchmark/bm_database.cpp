// this file is a micro-benchmark of the database
//
// right now, this is mainly a test of the data_container.

// It uses the google/benchmark framework

#include <benchmark/benchmark.h>

#include "specmicp_database/database.hpp"

using namespace specmicp;

static database::RawDatabasePtr get_database()
{
    database::Database the_database(TEST_CEMDATA_PATH);
    return the_database.get_database();
}

static void read_nb_components(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    scalar_t x = 0.0;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(x = the_db->nb_component());
    }
}

BENCHMARK(read_nb_components);

static void loop_range_components(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    while (state.KeepRunning()) {
        for (auto i: the_db->range_component())
        {
            benchmark::DoNotOptimize(i);
        }
    }
}

BENCHMARK(loop_range_components);

static void loop_standard_components(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    while (state.KeepRunning()) {
        for (index_t i=0; i<the_db->nb_component();++i)
        {
            benchmark::DoNotOptimize(i);
        }
    }
}

BENCHMARK(loop_standard_components);

static void loop_range_aqueous(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    while (state.KeepRunning()) {
        for (auto i: the_db->range_aqueous())
        {
            benchmark::DoNotOptimize(i);
        }
    }
}

BENCHMARK(loop_range_aqueous);

static void loop_standard_aqueous(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    while (state.KeepRunning()) {
        for (index_t i=0; i<the_db->nb_aqueous();++i)
        {
            benchmark::DoNotOptimize(i);
        }
    }
}

BENCHMARK(loop_standard_aqueous);

static void read_logk_aqueous(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    index_t aqueous = 3;
    scalar_t x = 0.0;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(x = the_db->logk_aqueous(aqueous));
    }
}

BENCHMARK(read_logk_aqueous);

static void read_nuji_aqueous(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    index_t aqueous = 3;
    index_t component = 3;
    scalar_t x = 0.0;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(x = the_db->nu_aqueous(aqueous, component));
    }
}

BENCHMARK(read_nuji_aqueous);

static void get_id_component_h2o(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    scalar_t id;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(id = the_db->get_id_component("H2O"));
    }
}

BENCHMARK(get_id_component_h2o);

static void get_id_component_mg(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    scalar_t id;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(id = the_db->get_id_component("Mg[2+]"));
    }
}

BENCHMARK(get_id_component_mg);

static void get_id_component_mg_from_element(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    scalar_t id;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(id = the_db->get_id_component_from_element("Mg"));
    }
}

BENCHMARK(get_id_component_mg_from_element);

static void get_label_component_0(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    std::string label;
    while (state.KeepRunning()) {
        label = the_db->get_label_component(0);
        benchmark::DoNotOptimize(label);
    }
}

BENCHMARK(get_label_component_0);


static void get_label_component_5(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    std::string label;
    while (state.KeepRunning()) {
       label = the_db->get_label_component(5);
       benchmark::DoNotOptimize(label);
    }
}

BENCHMARK(get_label_component_5);

static void molar_mass_basis_si_direct_scaling(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    scalar_t x;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(x = the_db->molar_mass_basis(2));
    }
}

BENCHMARK(molar_mass_basis_si_direct_scaling);

static void molar_mass_basis_si_scaling(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    scalar_t x;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(x = the_db->molar_mass_basis(2, units::SI_units));
    }
}

BENCHMARK(molar_mass_basis_si_scaling);

static void molar_mass_basis_no_scaling(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    scalar_t x;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(x = the_db->molar_mass_basis(2, units::CMG_units));
    }
}

BENCHMARK(molar_mass_basis_no_scaling);

static void scaling_molar_volume_m(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    scalar_t x;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(x = the_db->scaling_molar_volume(units::SI_units));
    }
}

BENCHMARK(scaling_molar_volume_m);

static void scaling_molar_volume_dm(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    scalar_t x;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(x = the_db->scaling_molar_volume(units::CMG_units));
    }
}

BENCHMARK(scaling_molar_volume_dm);

static void is_valid(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    the_db->freeze_db();
    bool x;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(x = the_db->is_valid());
    }
}

BENCHMARK(is_valid);

static void is_valid_nofreeze(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    bool x;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(x = the_db->is_valid());
    }
}

BENCHMARK(is_valid_nofreeze);


static void get_hash(benchmark::State& state) {
    database::RawDatabasePtr the_db = get_database();
    size_t x;
    while (state.KeepRunning()) {
        benchmark::DoNotOptimize(x = the_db->get_hash());
    }
}

BENCHMARK(get_hash);

BENCHMARK_MAIN()
