/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSOLVER_HPP
#define SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSOLVER_HPP

#include "adimensional_system_solver_structs.hpp"

#include "specmicp_database/database.hpp"
#include "specmicp_common/options_handler.hpp"

#include <unordered_map>
#include <map>

/*!
\file adimensional_system_solver.hpp
\brief Solve a reduced system

\code{cpp}
std::shared_ptr<database::DataContainer> raw_data;
AdimensionalSystemConstraints constraints;    // ...
AdimensionalSystemSolution previous_solution; // ...
AdimensionalSystemSolverOptions options;      // ...

AdimensionalSystemSolver solver(raw_data, constraints, previous_solution, options);
Vector variables;

micpsolver::MiCPPerformance perf = solver.solve(variables, true);

if (perf.return_code <= micpsolver::MiCPSolverReturnCode::NotConvergedYet) {
    throw std::runtime_error("Failed to solve the problem");
}
AdimensionalSystemSolution solution = solver.get_raw_solution(variables);
\endcode
*/

namespace specmicp {

// forward declaration
class AdimensionalSystem;
struct AdimensionalSystemSolution;

//! \brief Solve an adimensional system
//!
//! Take care of non-existing component in the system
//! and restart the computation if necessary
//!
//! \ingroup specmicp_api
class SPECMICP_DLL_PUBLIC AdimensionalSystemSolver:
        public OptionsHandler<AdimensionalSystemSolverOptions>
{
public:
    //! \brief Pointer to the system
    using SystemPtr = std::shared_ptr<AdimensionalSystem>;

    //! Default constructor
    //!
    //! Another constructor is necessary
    AdimensionalSystemSolver() {}

    //! \brief Initialise a solver from scratch
    //!
    //! \param data A raw database
    //! \param constraints The system to solver
    AdimensionalSystemSolver(RawDatabasePtr data,
                        const AdimensionalSystemConstraints& constraints
                        );
    //! \brief Initialise a solver from scratch
    //!
    //! \param data A raw database
    //! \param constraints The system to solver
    //! \param options (optional) customize the behavior of the solver
    AdimensionalSystemSolver(RawDatabasePtr data,
                        const AdimensionalSystemConstraints& constraints,
                        const AdimensionalSystemSolverOptions& options
                        );
    //! \brief Initialise a solver from a previous solution
    //!
    //! \param data A raw database
    //! \param constraints The system to solver
    //! \param previous_solution A solution of a similar system that will be used to initialize the system
    AdimensionalSystemSolver(RawDatabasePtr data,
                        const AdimensionalSystemConstraints& constraints,
                        const AdimensionalSystemSolution& previous_solution
                        );
    //! \brief Initialise a solver from a previous solution
    //!
    //! \param data A raw database
    //! \param constraints The system to solver
    //! \param previous_solution A solution of a similar system that will be used to initialize the system
    //! \param options customize the behavior of the solver
    AdimensionalSystemSolver(RawDatabasePtr data,
                        const AdimensionalSystemConstraints& constraints,
                        const AdimensionalSystemSolution& previous_solution,
                        const AdimensionalSystemSolverOptions& options
                        );
    //! \brief solve the problem using initial guess x
    //!
    //! \param[in,out] x   in -> initial guess, out -> solution
    //! \param init if true, the algorithm guess a starting point
    micpsolver::MiCPPerformance solve(Vector& x, bool init=false);

    //! \brief Return the system used for the computation
    SystemPtr get_system() {return m_system;}

    //! \brief Return the solution in a manageable form
    //!
    //! \param x The solution (complete set of the main variables)
    AdimensionalSystemSolution get_raw_solution(Vector& x);
    //! \brief Return the solution in a manageable form
    //!
    //! \param x The solution (complete set of the main variables)
    AdimensionalSystemSolution unsafe_get_raw_solution(Vector& x);

    //! \brief Initialize the problem using the Positive continuous fraction method
    //!
    //! \sa specmicp::AdimensionalSystemPCFM
    void run_pcfm(Vector& x);

    //! \brief Custom initialisation of variables
    //!
    //! The amount compon
    //! \param[out] x The initial guess (complete set of the main variables)
    //! \param volume_fraction_water volume fraction of water
    //! \param log_molalities log_10 of the molalities for chosen aqueous component
    //! \param volume_fraction_minerals volume fraction of the minerals
    //! \param log_free_sorption_site_concentration concentration of free sorption site
    void initialize_variables(Vector& x,
                              scalar_t volume_fraction_water,
                              std::unordered_map<std::string, scalar_t> log_molalities,
                              std::unordered_map<std::string, scalar_t> volume_fraction_minerals = std::unordered_map<std::string, scalar_t>(),
                              scalar_t log_free_sorption_site_concentration = 0
                              );
    //! \brief Custom initialisation of variables
    //!
    //! The amount compon
    //! \param[out] x The initial guess (complete set of the main variables)
    //! \param volume_fraction_water volume fraction of water
    //! \param log_molalities log_10 of the molalities for all aqueous components
    void initialize_variables(Vector& x,
                              scalar_t volume_fraction_water,
                              scalar_t log_molalities
                              );
    //! \brief Custom initialisation of variables
    //!
    //! The amount compon
    //! \param[out] x The initial guess (complete set of the main variables)
    //! \param volume_fraction_water volume fraction of water
    //! \param log_molalities log_10 of the molalities for all aqueous components
    //! \param log_free_sorption_site_concentration concentration of free sorption site
    void initialize_variables(Vector& x,
                              scalar_t volume_fraction_water,
                              scalar_t log_molalities,
                              scalar_t log_free_sorption_site_concentration
                              );
private:
    //! \brief set up the true variable vector
    //!
    //! \param x The solution (complete set of the main variables)
    void SPECMICP_DLL_LOCAL set_true_variable_vector(const Vector& x);
    //! \brief set up the true solution vector
    //!
    //! add zero components
    //! \param x The solution (complete set of the main variables)
    void SPECMICP_DLL_LOCAL set_return_vector(Vector& x);
    //! \brief solve the problem
    micpsolver::MiCPPerformance SPECMICP_DLL_LOCAL solve_system(Vector& x);

    RawDatabasePtr m_data; //! The raw database
    SystemPtr m_system; //! The system to solve
    Vector m_var; //! Copy of the solution vector (necessary in case of failing)
};

//! \brief Solve a reduced system, function provided for convenience
//!
//! \param data_ptr the database
//! \param constraints Constraints applied to the system
//! \param options Options for the solver
AdimensionalSystemSolution SPECMICP_DLL_PUBLIC solve_equilibrium(
        RawDatabasePtr data_ptr,
        const AdimensionalSystemConstraints& constraints,
        const AdimensionalSystemSolverOptions& options
        );
//! \brief Solve a reduced system, function provided for convenience
//!
//! \param data_ptr the database
//! \param constraints constraints applied to the system
//! \param previous_solution a previous solution
//! \param options options for the solver
AdimensionalSystemSolution SPECMICP_DLL_PUBLIC solve_equilibrium(
        RawDatabasePtr data_ptr,
        const AdimensionalSystemConstraints& constraints,
        const AdimensionalSystemSolution& previous_solution,
        const AdimensionalSystemSolverOptions& options
        );

} // end namespace specmicp

#endif // SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSOLVER_HPP
