/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_EQUILIBRIUMCURVE_HPP
#define SPECMICP_SPECMICP_EQUILIBRIUMCURVE_HPP

#include "adimensional_system_solver_structs.hpp"
#include "adimensional_system_solution.hpp"

#include "specmicp_common/types.hpp"
#include "specmicp_database/database.hpp"

//! \file equilibrium_curve.hpp ABC to compute a reaction path

namespace specmicp {

//! \brief Abstract Base Class to compute an equilibrium curve
//!
//!
class SPECMICP_DLL_PUBLIC EquilibriumCurve
{
public:
    //! \brief Default constructor, must be initialized
    EquilibriumCurve() {}
    //! \brief Constructor
    EquilibriumCurve(RawDatabasePtr database,
                     AdimensionalSystemConstraints constraints
                     ):
        m_data(database),
        m_constraints(constraints)

    {}

    //! \brief Run one step of the equilibrium curve
    void run_step();
    //! \brief Return the raw database
    RawDatabasePtr database() const {return m_data;}
    //! \brief Return the current solution
    const AdimensionalSystemSolution& current_solution() const {
        return m_current_solution;
    }
    //! \brief Return the current solution vector
    Vector& solution_vector() {
        return m_solution_vector;
    }
    //! \brief Return the current MiCPsolver performance
    const micpsolver::MiCPPerformance& solver_performance() const {
        return m_current_perf;
    }
    //! \brief Return a const reference to the solver options
    const AdimensionalSystemSolverOptions& solver_options() const {
        return m_solver_options;
    }
    //! \brief Return a const reference to the BC of AdimensionalSystem
    const AdimensionalSystemConstraints& constraints() const {
        return m_constraints;
    }
    //! \brief update the problem;
    virtual void update_problem() = 0;

    //! \brief Output - do nothing by default
    virtual void output() {}

    //! \brief How to handle an error
    virtual void error_handling(std::string msg) const;

    //! \brief Solve the first problem
    void solve_first_problem();

protected:
    //! \brief Set the database
    void set_database(RawDatabasePtr the_database) {
        m_data =the_database;
    }
    //! \brief Read-write reference to the conditions
    AdimensionalSystemConstraints& constraints() {
        return m_constraints;
    }
    //! \brief Read-write reference to the options of AdimensionalSystemSolver
    AdimensionalSystemSolverOptions& solver_options() {
        return m_solver_options;
    }

    //! \brief Input the first solution
    void initialize_solution(const AdimensionalSystemSolution& init) {
        m_current_solution = init;
    }

private:
    //! \brief Set the problem, according user input
    void set_problem();
    //! \brief Solve the problem
    //!
    //! May fail, call error_handling
    void solve_problem();
    //! \brief Post processing of the data
    void post_processing();

    RawDatabasePtr m_data; //!< The database

    AdimensionalSystemConstraints m_constraints; //!< The constraints for the adim system

    AdimensionalSystemSolverOptions m_solver_options;

    AdimensionalSystemSolution m_current_solution;

    micpsolver::MiCPPerformance m_current_perf;

    Vector m_solution_vector;

};

} // end namespace specmicp

#endif // SPECMICP_SPECMICP_EQUILIBRIUMCURVE_HPP
