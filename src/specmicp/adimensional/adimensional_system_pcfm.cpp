/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "adimensional_system_pcfm.hpp"
#include "adimensional_system.hpp"

#include "specmicp_common/log.hpp"

#include <iostream>

namespace specmicp {

// AdimensionalSystemPCFM

AdimensionalSystemPCFM::AdimensionalSystemPCFM(std::shared_ptr<AdimensionalSystem> program):
    m_data(program->get_database()),
    m_program(program)
{
    check_validity();
}

AdimensionalSystemPCFM::AdimensionalSystemPCFM(
        std::shared_ptr<AdimensionalSystem> program,
        const PCFMOptions& options):
    OptionsHandler<PCFMOptions>(options),
    m_data(program->get_database()),
    m_program(program)
{
    check_validity();
}

bool AdimensionalSystemPCFM::check_validity()
{
    bool is_valid = true;
    if (m_program->water_equation_type() != WaterEquationType::NoEquation)
    {
        ERROR << "The implementation of the positive continuous fraction method does not compute the conservation of water";
        is_valid = false;
    }
    if (m_program->get_database()->nb_mineral() != 0.0)
    {
        WARNING << "The implementation of the positive continuous fraction method does not compute the solid phase assemblage";
        is_valid = false;
    }
    return is_valid;
}

PCFMReturnCode AdimensionalSystemPCFM::solve(Vector &x)
{
    index_t cnt = 0;
    m_errors.setZero(m_program->total_variables());

    while (cnt < get_options().max_iterations)
    {
        one_iteration(x);

        scalar_t error = m_errors.lpNorm<Eigen::Infinity>();
        DEBUG << "PCFM error : " << error << " ?< " << get_options().tolerance;
        if (not std::isfinite(error))
            return PCFMReturnCode::Error;
        if (error < get_options().tolerance)
            return PCFMReturnCode::Success;
        ++cnt;
    }
    if (cnt == get_options().max_iterations)
        return PCFMReturnCode::MaxIterationsReached;
    return PCFMReturnCode::NotConvergedYet;
}

void AdimensionalSystemPCFM::one_iteration(Vector &x)
{
    m_program->set_secondary_concentration(x);
    m_program->set_sorbed_concentrations(x);
    for (index_t component: m_data->range_aqueous_component())
    {
        if (m_program->ideq_paq(component) != no_equation)
        {
            solve_component(component, x);
        }
    }
    if (m_program->ideq_surf() != no_equation)
    {
        solve_surface(x);
    }
}

void AdimensionalSystemPCFM::solve_component(index_t component, Vector& x)
{
    specmicp_assert(component > 0 and component < m_data->nb_component() and "Must be an aqueous component");
    specmicp_assert(m_program->ideq_paq(component) != no_equation and "No corresponding equation for this component");
    specmicp_assert(m_program->aqueous_component_equation_type(component) == AqueousComponentEquationType::MassConservation);
    const scalar_t total_concentration = m_program->total_concentration_bc(component);

    scalar_t conc_w = m_program->density_water()*m_program->volume_fraction_water(x);

    scalar_t sum_reac = conc_w*m_program->component_molality(x, component);
    scalar_t sum_prod = 0.0;


    // compute the lhs and rhs of the mass balance so that every contribution is positive
    if (total_concentration >= 0)
    {
        sum_prod += total_concentration;
    }
    else
    {
        sum_reac -= total_concentration;
    }

    for (index_t aqueous: m_data->range_aqueous())
    {
        if (m_data->nu_aqueous(aqueous, component) > 0)
            sum_reac += conc_w*m_data->nu_aqueous(aqueous, component)*m_program->secondary_molality(aqueous);
        else
            sum_prod -= conc_w*m_data->nu_aqueous(aqueous, component)*m_program->secondary_molality(aqueous);
    }
    for (index_t sorbed: m_data->range_sorbed())
    {
        if (m_data->nu_sorbed(sorbed, component) > 0)
            sum_reac += m_data->nu_sorbed(sorbed, component)*m_program->sorbed_species_concentration(sorbed);
        else
            sum_prod -= m_data->nu_sorbed(sorbed, component)*m_program->sorbed_species_concentration(sorbed);
    }
    specmicp_assert(std::isfinite(sum_reac));
    specmicp_assert(sum_reac > 0.0 && "Sum of reactants concentration should be positive");
    specmicp_assert(sum_prod > 0.0 && "Sum of products should be positive");
    // compute the step size
    scalar_t theta;
    const scalar_t factor = (sum_prod/sum_reac);
    if (sum_reac > sum_prod)
        theta = 0.9 - 0.8*factor;
    else
        theta = 0.9 - 0.8/factor;
    //
    theta *= get_options().theta_factor;
    scalar_t new_conc = (1-theta + theta*factor)*m_program->component_molality(x, component);
    x(m_program->ideq_paq(component)) = std::log10(new_conc);

    m_errors(m_program->ideq_paq(component)) = std::abs(sum_reac - sum_prod)/(sum_reac + sum_prod);
}

void AdimensionalSystemPCFM::solve_surface(Vector& x)
{
    specmicp_assert(m_program->ideq_surf() != no_equation);
    specmicp_assert(m_program->surface_total_concentration() > 0);
    scalar_t sum_prod = m_program->surface_total_concentration();
    scalar_t sum_reac = m_program->free_sorption_site_concentration(x);

    for (index_t sorbed: m_data->range_sorbed())
    {
        sum_reac += m_data->nb_sorption_sites(sorbed)*m_program->sorbed_species_concentration(sorbed);
    }

    specmicp_assert(std::isfinite(sum_reac));
    specmicp_assert(sum_reac > 0.0 && "Sum of reactants concentration should be positive");
    specmicp_assert(sum_prod > 0.0 && "Sum of products should be positive");
    scalar_t theta;
    const scalar_t factor = (sum_prod/sum_reac);
    if (sum_reac > sum_prod)
        theta = 0.9 - 0.8*factor;
    else
        theta = 0.9 - 0.8/factor;
    //
    theta *= get_options().theta_factor;
    scalar_t new_conc = (1-theta + theta*factor)*m_program->free_sorption_site_concentration(x);
    x(m_program->ideq_surf()) = std::log10(new_conc);

    m_errors(m_program->ideq_surf()) = std::abs(sum_reac - sum_prod)/(sum_reac + sum_prod);
}

} // end namespace specmicp
