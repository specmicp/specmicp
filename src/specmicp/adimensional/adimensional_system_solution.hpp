/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSOLUTION_HPP
#define SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSOLUTION_HPP

#include "specmicp_common/types.hpp"

//! \file adimensional_system_solution.hpp Structure to contain the solution returned by AdimensionalSystemSolution

namespace specmicp {

//! \brief Solution of an adimensional system
struct SPECMICP_DLL_PUBLIC AdimensionalSystemSolution
{

    bool is_valid {false};          //!< True if this is a valid solution
    scalar_t inert_volume_fraction; //!< Inert volume fraction
    scalar_t ionic_strength;        //!< Ionic strength
    Vector main_variables;          //!< The main variables
    Vector secondary_molalities;    //!< Molalities of secondary aqueous
    Vector log_gamma;               //!< Log_10 of activities coefficients
    Vector gas_fugacities;          //!< Gas fugacities
    Vector sorbed_molalities;       //!< Sorbed molalities

    //! \brief Build an empty solution
    AdimensionalSystemSolution():
        is_valid(false)
    {}

    //! \brief Build a valid solution
    AdimensionalSystemSolution(
            const Vector& variables,
            const Vector& aqueous_molalities,
            const Vector& loggama,
            scalar_t the_ionic_strength,
            const Vector& fugacities,
            const Vector& sorbed_species_molalities,
            scalar_t the_inert_volume_fraction=0.0
            ):
        inert_volume_fraction(the_inert_volume_fraction),
        ionic_strength(the_ionic_strength),
        main_variables(variables),
        secondary_molalities(aqueous_molalities),
        log_gamma(loggama),
        gas_fugacities(fugacities),
        sorbed_molalities(sorbed_species_molalities)
    {
        is_valid = true;
    }

};

} // end namespace specmicp

#endif // SPECMICP_SPECMICP_ADIMENSIONALSYSTEMSOLUTION_HPP
