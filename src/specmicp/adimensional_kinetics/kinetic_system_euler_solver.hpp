/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_ADIMKINETICS_KINETICSYSTEMEULERSOLVER_HPP
#define SPECMICP_SPECMICP_ADIMKINETICS_KINETICSYSTEMEULERSOLVER_HPP

#include "kinetic_system.hpp"
#include "kinetic_system_solver_structs.hpp"

#include "specmicp_common/options_handler.hpp"

namespace specmicp {
namespace kinetics {

class AdimKineticModel;


//! \brief Forward Euler solver for kinetic systems
class SPECMICP_DLL_PUBLIC AdimKineticSystemEulerSolver: public OptionsHandler<AdimKineticSystemSolverOptions>
{
public:
    //! \brief Constructor with default SpecMiCP options
    AdimKineticSystemEulerSolver(
            std::shared_ptr<AdimKineticModel> model,
            const Vector& total_concentrations,
            const Vector& mineral_concentration,
            AdimensionalSystemConstraints& constraints,
            RawDatabasePtr database
            ):
        m_current_dt(-1),
        m_system(model, total_concentrations, mineral_concentration, constraints, database)
    {}

    //! \brief Constructor with custom SpecMiCP options
    AdimKineticSystemEulerSolver(
            std::shared_ptr<AdimKineticModel> model,
            const Vector& total_concentrations,
            const Vector& mineral_concentration,
            AdimensionalSystemConstraints& constraints,
            const AdimensionalSystemSolution& equilibrium_solution,
            RawDatabasePtr database
            ):
        m_current_dt(-1),
        m_system(model, total_concentrations, mineral_concentration, constraints, equilibrium_solution, database)
    {}

    //! \brief Solve a timestep
    void solve(scalar_t dt, scalar_t total);

    //! \brief Return the current timestep
    scalar_t current_dt() {return m_current_dt;}

    //! \brief Return a reference to the variables
    AdimKineticVariables& variables() {return m_system.variables();}

private:
    scalar_t m_current_dt; //!< The current timestep
    AdimKineticSystem m_system; //!< The system to solver
};

} // end namespace kinetics
} // end namespace specmicp

#endif //SPECMICP_SPECMICP_ADIMKINETICS_KINETICSYSTEMEULERSOLVER_HPP
