/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "kinetic_system.hpp"

#include "kinetic_model.hpp"

#include "specmicp/adimensional/adimensional_system_solver.hpp"
#include "specmicp_common/log.hpp"


namespace specmicp {
namespace kinetics {

void AdimKineticSystem::update_total_concentrations(const Vector& y)
{
    //Vector y_temp = y.cwiseMax(Vector::Zero(y.rows()));
    Vector update_minerals = (y - m_variables.concentration_minerals());
    m_variables.total_concentrations() = m_variables.total_concentrations_initial();
    for (auto it=m_model->species_begin(); it!=m_model->species_end(); ++it)
    {
        for (index_t component: m_data->range_component())
        {
            if (m_data->nu_mineral_kinetic(*it, component) == 0.0) continue;
            m_variables.total_concentration(component) -=
                    m_data->nu_mineral_kinetic(*it, component)*update_minerals(it-m_model->species_begin());
        }
    }
}

void AdimKineticSystem::update_to_new_initial_condition(const Vector& y, scalar_t dt)
{
    update_total_concentrations(y);
    m_variables.rate_components() += (
                  m_variables.total_concentrations()
                - m_variables.total_concentrations_initial()
                )/dt;
    m_variables.concentration_minerals() = y;
    m_variables.total_concentrations_initial() = m_variables.total_concentrations();
}

void AdimKineticSystem::compute_rates(scalar_t t, const Vector& y, Vector& dydt)
{
    m_model->compute_rate(t, y, m_variables, dydt);
}


void AdimKineticSystem::compute_equilibrium(
        AdimensionalSystemConstraints& constraints,
        AdimensionalSystemSolverOptions& options)
{
    AdimensionalSystemSolver solver;
    Vector variables;
    specmicp::micpsolver::MiCPPerformance perf;
    constraints.total_concentrations = m_variables.total_concentrations();
    if (m_variables.equilibrium_solution().is_valid)
    {
        solver = AdimensionalSystemSolver(m_data,
                                     constraints,
                                     m_variables.equilibrium_solution(),
                                     options);
        variables = m_variables.equilibrium_solution().main_variables;
        perf = solver.solve(variables);
    }
    else
    {
        solver = AdimensionalSystemSolver(m_data,
                                     constraints,
                                     options);
        perf = solver.solve(variables, true);
    }
    //std::cout << variables << std::endl;
    if ((int) perf.return_code < 0)
    {
        ERROR << "Failed to solve the system ! Error code " << (int) perf.return_code;
    }
    m_variables.update_equilibrium_solution(solver.get_raw_solution(variables));
}

} // end namespace kinetics
} // end namespace specmicp
