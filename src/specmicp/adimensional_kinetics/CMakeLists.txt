set( specmicp_adim_kin_srcs
    kinetic_system.cpp
    kinetic_system_euler_solver.cpp
    kinetic_system_solver.cpp
)

set( specmicp_adim_kin_headers
    kinetic_model.hpp
    kinetic_system.hpp
    kinetic_system_solver.hpp
    kinetic_system_euler_solver.hpp
    kinetic_system_solver_structs.hpp
    kinetic_variables.hpp
)

add_to_specmicp_srcs_list(specmicp_adim_kin_srcs)
add_to_specmicp_headers_list(specmicp_adim_kin_headers)

INSTALL(FILES ${specmicp_adim_kin_headers}
    DESTINATION ${INCLUDE_INSTALL_DIR}/specmicp/adimensional_kinetics
)
