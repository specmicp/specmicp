/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_ADIMKINETICS_KINETICMODEL_HPP
#define SPECMICP_SPECMICP_ADIMKINETICS_KINETICMODEL_HPP

#include "specmicp_common/types.hpp"
#include "kinetic_variables.hpp"

namespace specmicp {
namespace kinetics {

//! \brief Base class for a kinetic model
class AdimKineticModel
{
public:

    //! \brief Default constructor
    AdimKineticModel()
    {}

    //! \brief Constructor
    //!
    //! \param list_species species covered by this model
    AdimKineticModel(const std::vector<index_t>& list_species):
        m_list_species(list_species)
    {}

    virtual ~AdimKineticModel() {}

    //! \brief Return the number of kinetic minerals included in the problem
    index_t get_neq() {return m_list_species.size();}

    //! \brief Compute the kinetic rates and store them in dydt
    virtual void compute_rate(
            scalar_t t,
            const Vector& y,
            AdimKineticVariables& variables,
            Vector& dydt
            ) = 0;

    //! \brief Add a species to the kinetic model
    void add_equation(index_t species) {m_list_species.push_back(species);}

    //! \brief Index of species corresponding to equation 'id_equation'
    index_t index_species(index_t id_equation) {return m_list_species[id_equation];}

    //! \brief Iterator over the species vector
    std::vector<index_t>::iterator species_begin() {return m_list_species.begin();}
    //! \brief Iterator over the species vector
    std::vector<index_t>::iterator species_end() {return m_list_species.end();}

private:
    std::vector<index_t> m_list_species;
};

} // end namespace kinetics
} // end namespace specmicp


#endif //SPECMICP_SPECMICP_ADIMKINETICS_KINETICMODEL_HPP
