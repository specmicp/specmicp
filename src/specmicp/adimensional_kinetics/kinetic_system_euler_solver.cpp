/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "kinetic_system_euler_solver.hpp"

#include "kinetic_model.hpp"
#include "specmicp_common/odeint/runge_kutta_step.hpp"

#include <functional>

namespace specmicp {
namespace kinetics {


void AdimKineticSystemEulerSolver::solve(scalar_t dt, scalar_t total)
{
    double t = 0.0;
    Vector y = m_system.variables().concentration_minerals();
    Vector dydx(y.rows());
    dydx.setZero();

    while (t < total)
    {
        m_system.rhs(t, y, dydx, get_options().speciation_options);
        y += dt*dydx;

        m_system.update_to_new_initial_condition(y, dt);
        t += dt;
        if (t+dt > total) dt =(total - t);
    }
    m_system.update_total_concentrations(y);
    m_system.compute_equilibrium(m_system.constraints(), get_options().speciation_options);

}

} // end namespace kinetics
} // end namespace specmicp
