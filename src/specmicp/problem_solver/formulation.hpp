/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_PROBLEMSOLVER_FORMULATION_HPP
#define SPECMICP_SPECMICP_PROBLEMSOLVER_FORMULATION_HPP

#include "specmicp_common/types.hpp"

#include <map>
#include <vector>
#include <string>

namespace specmicp {


//! \brief Formula
struct Formulation
{
    //! \brief Mass of the solvent
    scalar_t mass_solution;
    //! \brief Contains the amount of each solid phase
    std::map<std::string, scalar_t> concentration_aqueous;
    //! \brief Contains the amount of each solid phase
    //!  included in the problem
    std::map<std::string, scalar_t> amount_minerals;

    //! \brief Extra component to leave in the database
    //!
    //! All components not present in this list and with
    //! a total concentration of zero will be removed.
    std::vector<std::string> extra_components_to_keep;
    //! \brief List of solid phase at equilibrium to keep in the computation
    //!
    //! Leave empty to keep all solid phases present in the database
    std::vector<std::string> minerals_to_keep;

    //! \brief Set the mass of the solution
    void set_mass_solution(scalar_t mass) {
        mass_solution = mass;
    }
    //! \brief Add an aqueous species to the system
    //!
    //! \param label label of a component or a secondary species or a compounds
    //! \param concentration in moles per mass unit of water
    void add_aqueous_species(const std::string& label, scalar_t concentration) {
        concentration_aqueous.insert(std::pair<std::string, scalar_t>(label, concentration));
    }
    //! \brief Add an aqueous species to the system
    //!
    //! \param label label of a component or a secondary species
    //! \param concentration in moles per mass unit of water
    void add_aqueous_species(std::string&& label, scalar_t concentration) {
        concentration_aqueous.emplace(std::move(label), concentration);
    }
    //! \brief Add a mineral to the system
    //!
    //! \param label label of a mineral at equilibrium or governed by kinetics
    //! \param concentration in moles per volume unit
    void add_mineral(const std::string& label, scalar_t concentration) {
        amount_minerals.insert(std::pair<std::string, scalar_t>(label, concentration));
    }
    //! \brief Add a mineral to the system
    //!
    //! \param label label of a mineral at equilibrium or governed by kinetics
    //! \param concentration in moles per volume unit
    void add_mineral(std::string&& label, scalar_t concentration) {
        amount_minerals.emplace(std::move(label), concentration);
    }
    //! \brief Keep a component in the database even if it doesn't exist in the initial composition
    void keep_component(const std::string& label) {
        extra_components_to_keep.push_back(label);
    }
    //! \brief Set the list of minerals at equilibrium
    void set_minerals_list(const std::vector<std::string>& list_minerals_equilibrium) {
        minerals_to_keep = std::vector<std::string>(list_minerals_equilibrium);
    }
    //! \brief Set the list of minerals at equilibrium
    void set_minerals_list(std::vector<std::string>&& list_minerals_equilibrium) {
        minerals_to_keep = std::vector<std::string>(std::move(list_minerals_equilibrium));
    }

};

} // end namespace specmicp

#endif // SPECMICP_SPECMICP_PROBLEMSOLVER_FORMULATION_HPP
