/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_PROBLEMSOLVER_DISSOLVER_HPP
#define SPECMICP_SPECMICP_PROBLEMSOLVER_DISSOLVER_HPP

#include "specmicp_common/types.hpp"
#include "specmicp_common/physics/units.hpp"
#include "specmicp_database/database.hpp"

namespace specmicp {

struct Formulation;

//! \brief Return the total concentration of a given formulation
class SPECMICP_DLL_PUBLIC Dissolver: public units::UnitBaseClass
{
public:
    //! \brief Constructor
    Dissolver(RawDatabasePtr the_database):
      m_database(the_database),
      m_total_concentration(Vector::Zero(the_database->nb_component())),
      m_components_to_keep(the_database->nb_component())
    {}

    //! \brief Dissolve the problem into the components
    //!
    //! Also simplify the database
    Vector dissolve(const Formulation& the_problem);
    //! \brief Dissolve the sytem into components
    //!
    //! If modify db is true then the database is simplified
    Vector dissolve(const Formulation& the_problem, bool modify_db);
    //! \brief Return the total concentration vector
    Vector get_total_concentration() {return m_total_concentration;}

private:

    void SPECMICP_DLL_LOCAL set_solvent(scalar_t amount);
    void SPECMICP_DLL_LOCAL dissolve_aqueous(std::string label, scalar_t concentration, scalar_t mass_solution);
    void SPECMICP_DLL_LOCAL dissolve_mineral(std::string label, scalar_t amount);
    void SPECMICP_DLL_LOCAL keep_extra_components(const std::vector<std::string>& list_component_to_keep);
    void SPECMICP_DLL_LOCAL reduce_problem();

    RawDatabasePtr m_database;
    Vector m_total_concentration;
    std::vector<bool> m_components_to_keep;

};

} // end namespace specmicp

#endif //SPECMICP_SPECMICP_PROBLEMSOLVER_DISSOLVER_HPP
