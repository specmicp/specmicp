/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "dissolver.hpp"

#include "formulation.hpp"
#include "specmicp_database/database.hpp"


namespace specmicp {

Vector Dissolver::dissolve(const Formulation &the_problem)
{
    return dissolve(the_problem, true);
}

Vector Dissolver::dissolve(const Formulation& the_problem, bool modify_db)
{
    m_components_to_keep[0] = true;
    m_components_to_keep[1] = true;
    for (index_t component: m_database->range_aqueous_component())
    {
        m_components_to_keep[component] = false;
    }
    m_total_concentration.setZero();

    set_solvent(the_problem.mass_solution);
    for (auto it=the_problem.concentration_aqueous.begin(); it!=the_problem.concentration_aqueous.end(); ++it)
    {
        dissolve_aqueous(it->first, it->second, the_problem.mass_solution);
    }
    for (auto it=the_problem.amount_minerals.begin(); it!=the_problem.amount_minerals.end(); ++it)
    {
        dissolve_mineral(it->first, it->second);
    }
    if (modify_db)
    {
        keep_extra_components(the_problem.extra_components_to_keep);
        reduce_problem();
    }
    // Remove minerals if needed
    if (the_problem.minerals_to_keep.size() > 0)
    {
        database::Database(m_database).minerals_keep_only(the_problem.minerals_to_keep);
    }

    return m_total_concentration;
}


void Dissolver::set_solvent(scalar_t amount)
{
    index_t idw = m_database->water_index();
    specmicp_assert(idw != no_species and idw == 0);
    m_total_concentration(0) += amount/m_database->molar_mass_basis(idw, get_units());
    m_components_to_keep[0] = true;
}

void Dissolver::dissolve_aqueous(std::string label, scalar_t concentration, scalar_t mass_solution)
{
    index_t idaq = m_database->get_id_component(label);
    // component
    if (idaq != no_species)
    {
        m_total_concentration(idaq) += mass_solution*concentration;
        m_components_to_keep[idaq] = true;
    }
    // aqueous
    else
    {
        index_t idsaq = m_database->get_id_aqueous(label);
        if (idsaq != no_species)
        {
            for (index_t component: m_database->range_component())
            {
                if (m_database->nu_aqueous(idsaq, component) == 0.0) continue;
                m_total_concentration(component) +=
                        m_database->nu_aqueous(idsaq, component)*mass_solution*concentration;
                m_components_to_keep[component] = true;
            }
        }
        // compounds
        else
        {
            index_t idcomp = m_database->get_id_compound(label);
            if (idcomp == no_species) {
                throw std::invalid_argument("Unknown species : " + label + ".");
            }
            for (index_t component: m_database->range_component())
            {
                if (m_database->nu_compound(idcomp, component) == 0.0) continue;
                m_total_concentration(component) +=
                        m_database->nu_compound(idcomp, component)*mass_solution*concentration;
                m_components_to_keep[component] = true;
            }
        }
    }
}

void Dissolver::dissolve_mineral(std::string label, scalar_t amount)
{
    index_t id_mineral = m_database->get_id_mineral(label);
    if (id_mineral != no_species)
    {
        for (index_t component: m_database->range_component())
        {
            if (m_database->nu_mineral(id_mineral, component) == 0.0) continue;
            m_total_concentration(component) +=
                    m_database->nu_mineral(id_mineral, component)*amount;
            m_components_to_keep[component] = true;
        }
    }
    else
    {
        index_t id_minkin = m_database->get_id_mineral_kinetic(label);
        if (id_minkin == no_species)
        {
            throw std::invalid_argument("Unknown mineral : " + label + ".");
        }
        for (index_t component: m_database->range_component())
        {
            if (m_database->nu_mineral_kinetic(id_minkin, component) == 0.0) continue;
            m_total_concentration(component) +=
                    m_database->nu_mineral_kinetic(id_minkin, component)*amount;
            m_components_to_keep[component] = true;
        }
    }
}

void Dissolver::keep_extra_components(const std::vector<std::string>& list_component_to_keep)
{
    for (auto it:list_component_to_keep)
    {
        index_t idc = m_database->get_id_component(it);
        if (idc == no_species)
        {
            throw std::invalid_argument("Species '" + it + "' is not a component");
        }
        m_components_to_keep[idc] = true;
    }
}

void Dissolver::reduce_problem()
{
    std::vector<index_t> to_remove;
    to_remove.reserve(m_database->nb_component());
    index_t new_id = 0;
    Vector reduced_tot_conc(m_database->nb_component());
    const index_t h_component = m_database->get_id_component_from_element("H");
    m_components_to_keep[h_component] = true; // do not remove H[+]/HO[-]
    // Copy information
    for (index_t component: m_database->range_component())
    {
        if (m_components_to_keep[component])
        {
            reduced_tot_conc(new_id) = m_total_concentration(component);
            ++new_id;
        }
        else
        {
            to_remove.push_back(component);
        }
    }
    // Remove components from database
    database::Database(m_database).remove_components(to_remove);
    // Resize total concentrations
    reduced_tot_conc.conservativeResize(m_database->nb_component());
    m_total_concentration = reduced_tot_conc;
}

} // end namespace specmicp
