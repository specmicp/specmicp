/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_PBSOLVER_REACTANTBOX_HPP
#define SPECMICP_SPECMICP_PBSOLVER_REACTANTBOX_HPP

//! \file problem_solver/reactant_box.hpp
//! \brief Handle initial composition, with units

#include "specmicp_common/types.hpp"
#include "specmicp_common/pimpl_ptr.hpp"

#include "specmicp_common/physics/units.hpp"
#include "specmicp_database/database_holder.hpp"

namespace specmicp {

struct AdimensionalSystemConstraints;

using water_partial_pressure_f = std::function<scalar_t (scalar_t)>;

//! \brief A set of reactants
//!
//! Take care of the units
class SPECMICP_DLL_PUBLIC ReactantBox :
    public units::UnitBaseClass,
    public database::DatabaseHolder

{
public:
    //! \brief Constructor
    ReactantBox(
        std::shared_ptr<database::DataContainer> raw_data,
        const units::UnitsSet& units_set
    );
    //! \brief Destructor
    ~ReactantBox();
    //! \brief Move constructor
    ReactantBox(ReactantBox&& other);
    //! \brief Move constructor
    ReactantBox& operator=(ReactantBox&& other);

    // Solution
    // =========
    //! \brief Set the solution amount
    void set_solution(scalar_t value, std::string str_unit);
    //! \brief Set the solution amount
    void set_solution(
            scalar_t                   value,
            const units::AmountUnit&   unit
            );

    // Aqueous
    // =======
    //! \brief Add an aqueous species
    void add_aqueous_species(
            std::string name,
            scalar_t    value,
            std::string str_unit
            );
    //! \brief Add an aqueous species
    void add_aqueous_species(
            std::string                name,
            scalar_t                   value,
            const units::AmountUnit&   unit
            );
    //! \brief Set the amount of an aqueous species
    void set_aqueous_species(
            std::string name,
            scalar_t    value,
            std::string str_unit
            );
    //! \brief Set the amount an aqueous species
    void set_aqueous_species(
            std::string                name,
            scalar_t                   value,
            const units::AmountUnit&   unit
            );

    // Solid
    // =====
    //! \brief Add a solid phase
    void add_solid_phase(
            std::string name,
            scalar_t    value,
            std::string str_unit
            );
    //! \brief Add a solid phase
    void add_solid_phase(
            std::string                name,
            scalar_t                   value,
            const units::AmountUnit&   unit
            );
    //! \brief Set the amount of a solid phase
    void set_solid_phase(
            std::string name,
            scalar_t    value,
            std::string str_unit
            );
    //! \brief Set the amount of a solid phase
    void set_solid_phase(
            std::string                name,
            scalar_t                   value,
            const units::AmountUnit&   unit
            );

    // Total concentration
    // ===================
    //! \brief add a component without corresponding species
    //!
    //! It is useful if the database must be modified
    void add_component(const std::string& name);
    //! \brief Return the total concentration vector
    Vector get_total_concentration(bool modify_db=false)  const;

    // Constraints
    // ===========

    //! \brief Set the charge keeper
    void set_charge_keeper(std::string charge_keeper);
    //! \brief Set the inert volume fraction (0 by default)
    void set_inert_volume_fraction(scalar_t inert_volume_fraction);
    //! \brief The system is saturated
    void set_saturated_system();
    //! \brief The saturation of the system is fixed
    void set_fixed_saturation(scalar_t saturation);
    //! \brief Disable the water conservation equation
    //!
    //! \warning probably not a good idead
    void disable_conservation_water();
    //! \brief Set the water partial pressure model
    void set_water_partial_pressure_model(water_partial_pressure_f h2o_pressure_model);

    //! \brief Add a fixed fugacity gas
    void add_fixed_fugacity_gas(
            std::string gas,
            std::string component,
            scalar_t    fugacity
            );
    //! \brief Add a fixed activity component
    void add_fixed_activity_component(
            std::string component,
            scalar_t    activity
            );
    //! \brief Add a fixed molality component
    //!
    //! Molality is in mol/kg !
    void add_fixed_molality_component(
            std::string component,
            scalar_t    molality
            );


    //! \brief Set an upper bound for a mineral
    //!
    //! It's an upper bound on the volume fraction
    void set_mineral_upper_bound(
            std::string mineral,
            scalar_t max_volume_fraction
            );

    //! \brief Return a set of constraints to be passed to the specmicp solver
    AdimensionalSystemConstraints get_constraints(bool modify_db=true) const;

private:
    struct SPECMICP_DLL_LOCAL ReactantBoxImpl;
    utils::pimpl_ptr<ReactantBoxImpl> m_impl;

};

} // end namespace specmicp

#endif // SPECMICP_SPECMICP_PBSOLVER_REACTANTBOX_HPP
