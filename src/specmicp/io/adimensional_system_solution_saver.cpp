/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "specmicp_database/data_container.hpp"
#include "adimensional_system_solution_saver.hpp"
#include "specmicp/adimensional/adimensional_system_solution.hpp"
#include "specmicp/adimensional/adimensional_system_numbering.hpp"

#include "specmicp_common/log.hpp"
#include "specmicp_common/dateandtime.hpp"
#include "specmicp_common/io/yaml.hpp"

#include <yaml-cpp/emitter.h>
#include <yaml-cpp/yaml.h>
#include <fstream>
#include <chrono>
#include <ctime>
#include <stdexcept>

// formatting
#include "config_solution_output_format.h"

namespace specmicp {
namespace io {


struct AdimensionalSystemSolutionSaver::AdimensionalSystemSolutionSaverImpl:
        private AdimemsionalSystemNumbering
{
public:
    AdimensionalSystemSolutionSaverImpl(
            RawDatabasePtr valid_database):
        AdimemsionalSystemNumbering(valid_database),
        m_data(valid_database)
    {}


    std::string format_solution(
            const AdimensionalSystemSolution& solution
            );

    void save_solution(
            const std::string& filename,
            const AdimensionalSystemSolution& solution,
            const std::string& db_path
            );

    void save_solutions(
            const std::string& filename,
            const std::vector<AdimensionalSystemSolution>& solutions,
            const std::string& db_path
            );



private:
    std::shared_ptr<database::DataContainer> m_data;

    //! \brief Build the Json::Value tree corresponding to the solution (+metadata)
    void set_yaml_emitter(
            const AdimensionalSystemSolution& solution,
            YAML::Emitter& yaml_tree,
            const std::string& db_path
            );
    //! \brief Build the Json::Value tree corresponding to the solution (w/o metadata)
    void format_solution_impl(
            const AdimensionalSystemSolution& solution,
            YAML::Emitter& out
            );

    //! \brief Format the main variables
    void format_main_variables(
            const AdimensionalSystemSolution& solution,
            YAML::Emitter& out
            );
    //! \brief Format the main variables
    void format_aqueous_molalities(
            const AdimensionalSystemSolution& solution,
            YAML::Emitter& out
            );
    //! \brief Format the log of activity coefficients
    void format_loggamma(
            const AdimensionalSystemSolution& solution,
            YAML::Emitter& out
            );
    //! \brief Format the gas fugacities
    void format_gas_fugacities(
            const AdimensionalSystemSolution& solution,
            YAML::Emitter& out
            );
    //! \brief Format the sorbed molalities
    void format_sorbed_molalities(
            const AdimensionalSystemSolution& solution,
            YAML::Emitter& out
            );
    //! \brief Format the metadata section
    void format_metadata(
            YAML::Emitter& yaml_tree,
            const std::string& db_path
            );
};



AdimensionalSystemSolutionSaver::AdimensionalSystemSolutionSaver(
        std::shared_ptr<database::DataContainer> the_database):
    m_impl(utils::make_pimpl<AdimensionalSystemSolutionSaverImpl>(the_database))
{}

AdimensionalSystemSolutionSaver::~AdimensionalSystemSolutionSaver() = default;

std::string AdimensionalSystemSolutionSaver::format_solution(
        const AdimensionalSystemSolution& solution
        )
{
    return m_impl->format_solution(solution);
}

void AdimensionalSystemSolutionSaver::save_solution(
        const std::string& filename,
        const AdimensionalSystemSolution& solution,
        std::string db_path
        )
{
    m_impl->save_solution(filename, solution, db_path);
}


void AdimensionalSystemSolutionSaver::save_solutions(
        const std::string& filename,
        const std::vector<AdimensionalSystemSolution>& solutions,
        std::string db_path
        )
{
    m_impl->save_solutions(filename, solutions, db_path);
}


void AdimensionalSystemSolutionSaver::AdimensionalSystemSolutionSaverImpl::save_solution(
        const std::string& filename,
        const AdimensionalSystemSolution& solution,
        const std::string& db_path
        )
{
    YAML::Emitter solution_yaml;
    io::configure_yaml_emitter(solution_yaml);
    set_yaml_emitter(solution, solution_yaml, db_path);
    io::save_yaml(filename, solution_yaml);
}

//! \brief Save a set of solutions as a JSON file
void
AdimensionalSystemSolutionSaver::AdimensionalSystemSolutionSaverImpl::save_solutions(
        const std::string& filename,
        const std::vector<AdimensionalSystemSolution>& solutions,
        const std::string& db_path
        )
{
    YAML::Emitter yaml_tree;
    format_metadata(yaml_tree, db_path);

    yaml_tree << YAML::Key << SECTION_VALUES << YAML::Value;
    yaml_tree << YAML::BeginSeq;
    for (std::size_t ind=0; ind<solutions.size(); ++ind)
        format_solution_impl(solutions[ind], yaml_tree);
    yaml_tree << YAML::EndSeq;

    yaml_tree << YAML::EndMap;
    io::save_yaml(filename, yaml_tree);
}


std::string
AdimensionalSystemSolutionSaver::AdimensionalSystemSolutionSaverImpl::format_solution(
        const AdimensionalSystemSolution& solution
        )
{
    YAML::Emitter solution_yaml;
    io::configure_yaml_emitter(solution_yaml);
    set_yaml_emitter(solution, solution_yaml, "");
    if (solution_yaml.good())
    {
        throw std::runtime_error("Error in the emitter");
    }
    return std::string(solution_yaml.c_str());
}

void AdimensionalSystemSolutionSaver::AdimensionalSystemSolutionSaverImpl::format_metadata(
        YAML::Emitter& yaml_tree,
        const std::string& db_path
        )
{
    yaml_tree << YAML::Comment("Solution file generated by SpecMiCP");
    yaml_tree << YAML::BeginMap;
    yaml_tree << YAML::Key << VALUE_META_DATE << YAML::Value <<  dateandtime::now();
    yaml_tree << YAML::Key << VALUE_META_DATABASE << YAML::Value << m_data->metadata.name;
    yaml_tree << YAML::Key << VALUE_META_DATABASE_VERSION << YAML::Value << m_data->metadata.version;

    if (not db_path.empty())
    {
        yaml_tree << YAML::Key << VALUE_META_DATABASE_PATH
                  << YAML::Value << db_path;
    }
}

void
AdimensionalSystemSolutionSaver::AdimensionalSystemSolutionSaverImpl::set_yaml_emitter(
        const AdimensionalSystemSolution& solution,
        YAML::Emitter& yaml_tree,
        const std::string& db_path
        )
{
    format_metadata(yaml_tree, db_path);
    yaml_tree << YAML::Key << SECTION_VALUES << YAML::Value;
    yaml_tree << YAML::BeginSeq;
    format_solution_impl(solution, yaml_tree);
    yaml_tree << YAML::EndSeq;
    yaml_tree << YAML::EndMap;
}



void AdimensionalSystemSolutionSaver::AdimensionalSystemSolutionSaverImpl::format_solution_impl(
        const AdimensionalSystemSolution& solution,
        YAML::Emitter& out
        )
{
    out << YAML::BeginMap;
    format_main_variables(solution, out);
    format_aqueous_molalities(solution, out);
    format_loggamma(solution, out);
    out << YAML::Key << VALUE_IONIC_STRENGTH
        << YAML::Value << solution.ionic_strength
        << YAML::Comment("mol/kg");
    format_gas_fugacities(solution, out);
    format_sorbed_molalities(solution, out);
    out << YAML::Key << VALUE_INERT
        << YAML::Value << solution.inert_volume_fraction;
    out << YAML::EndMap;
}

void AdimensionalSystemSolutionSaver::AdimensionalSystemSolutionSaverImpl::format_main_variables(
        const AdimensionalSystemSolution& solution,
        YAML::Emitter& out)
{
    out << YAML::Key << SECTION_MAIN << YAML::Value;
    out << YAML::BeginMap;
    out << YAML::Key << VALUE_COMPONENT << YAML::Value;
    out << YAML::BeginMap;
    // water
    out << YAML::Key << m_data->get_label_component(0)
        << YAML::Value << solution.main_variables(dof_component(0))
        << YAML::Comment("Volume fraction solution");
    // electron
    out << YAML::Key << m_data->get_label_component(1)
        << YAML::Value << solution.main_variables(dof_component(1))
        << YAML::Comment("pE");
    // aqueous components
    for (auto component: m_data->range_aqueous_component())
    {
        out << YAML::Key << m_data->get_label_component(component)
            << YAML::Value << solution.main_variables(dof_component(component))
            << YAML::Comment("log10 molality (mol/kgw)");
    }
    out << YAML::EndMap;
    out << YAML::Key << VALUE_FREE_SURFACE
        << YAML::Value << solution.main_variables(dof_surface())
        << YAML::Comment("mol/kgw");
    out << YAML::Key << VALUE_MINERAL
        << YAML::Value << YAML::Comment("Volume fraction solid phases");
    out << YAML::BeginMap;
    for (auto mineral: m_data->range_mineral())
    {
        out << YAML::Key << m_data->get_label_mineral(mineral)
            << YAML::Value <<  solution.main_variables(dof_mineral(mineral));
    }
    out << YAML::EndMap;
    out << YAML::EndMap;
}

void AdimensionalSystemSolutionSaver::AdimensionalSystemSolutionSaverImpl::format_aqueous_molalities(
        const AdimensionalSystemSolution& solution,
        YAML::Emitter& out
        )
{
    out << YAML::Key << SECTION_AQUEOUS
        << YAML::Value
        << YAML::Comment("molality (mol/kg)");
    out << YAML::BeginMap;
    for (auto aqueous: m_data->range_aqueous())
    {
        out << YAML::Key << m_data->get_label_aqueous(aqueous)
            << YAML::Value << solution.secondary_molalities(aqueous);
    }
    out << YAML::EndMap;
}

void AdimensionalSystemSolutionSaver::AdimensionalSystemSolutionSaverImpl::format_loggamma(
        const AdimensionalSystemSolution& solution,
        YAML::Emitter& out
        )
{
    out << YAML::Key << SECTION_LOGGAMMA
        << YAML::Value
        << YAML::Comment("log10 activity coeficient");
    out << YAML::BeginMap;
    out << YAML::Key << VALUE_COMPONENT << YAML::Value;
    out << YAML::BeginMap;
    for (auto component: m_data->range_aqueous_component())
    {
        out << YAML::Key << m_data->get_label_component(component)
            << YAML::Value << solution.log_gamma(dof_component_gamma(component));
    }
    out << YAML::EndMap;
    out << YAML::Key << VALUE_AQUEOUS << YAML::Value;
    out << YAML::BeginMap;
    for (auto aqueous: m_data->range_aqueous())
    {
        out << YAML::Key << m_data->get_label_aqueous(aqueous)
            << YAML::Value << solution.log_gamma(dof_aqueous_gamma(aqueous));
    }
    out << YAML::EndMap;
    out << YAML::EndMap;
}

void AdimensionalSystemSolutionSaver::AdimensionalSystemSolutionSaverImpl::format_gas_fugacities(
        const AdimensionalSystemSolution& solution,
        YAML::Emitter& out
        )
{
    out << YAML::Key << SECTION_GAS << YAML::Value;
    out << YAML::BeginMap;
    for (auto gas: m_data->range_gas())
    {
        out << YAML::Key << m_data->get_label_gas(gas)
            << YAML::Value << solution.gas_fugacities(gas);
    }
    out << YAML::EndMap;
}


void AdimensionalSystemSolutionSaver::AdimensionalSystemSolutionSaverImpl::format_sorbed_molalities(
        const AdimensionalSystemSolution& solution,
        YAML::Emitter& out
        )
{
    out << YAML::Key << SECTION_SORBED << YAML::Value;
    out << YAML::BeginMap;
    for (auto sorbed: m_data->range_sorbed())
    {
        out << YAML::Key << m_data->get_label_sorbed(sorbed)
            << YAML::Value << solution.sorbed_molalities(sorbed);
    }
    out << YAML::EndMap;
}


} // end namespace io
} //end namespace specmicp
