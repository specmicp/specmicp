/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_IO_HDF5_ADIMENSIONAL_HPP
#define SPECMICP_IO_HDF5_ADIMENSIONAL_HPP

#include "specmicp_common/macros.hpp"
#include <string>

//! \file specmicp/io/hdf5_adimensional.hpp
//! \brief Read and write AdimSolution to HDF5 format


namespace specmicp {

struct AdimensionalSystemSolution;

namespace io {

namespace hdf5 {
    class GroupPath;
} //end namespace hdf5

//! \brief Read a solution from an HDF5 file
void SPECMICP_DLL_PUBLIC save_adimensional_system_solution(
        hdf5::GroupPath& location,
        const std::string& name,
        const AdimensionalSystemSolution& solution
        );
//! \brief Read a solution from an HDF5 file
AdimensionalSystemSolution SPECMICP_DLL_PUBLIC
read_adimensional_system_solution(
        const hdf5::GroupPath& location,
        const std::string& name
        );

} //end namespace io
} //end namespace specmicp

#endif // SPECMICP_IO_HDF5_ADIMENSIONAL_HPP
