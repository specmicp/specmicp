/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPECMICP_ADIMSOLUTION_READER_HPP
#define SPECMICP_SPECMICP_ADIMSOLUTION_READER_HPP

#include "specmicp_common/pimpl_ptr.hpp"
#include <string>
#include <vector>
#include "specmicp_database/database_fwd.hpp"
#include "specmicp_common/types.hpp"
#include "specmicp/adimensional/adimensional_system_solution.hpp"

namespace specmicp {
namespace io {

//! \brief Read saved solutions from a YAML file
//!
//! \sa AdimensionalSystemSolutionSaver
class SPECMICP_DLL_PUBLIC AdimensionalSystemSolutionReader
{
public:
    //! \brief Constructor, database is read from solution file
    AdimensionalSystemSolutionReader(std::string filepath);
    ~AdimensionalSystemSolutionReader();

    //! \brief Constructor, database is provided
    AdimensionalSystemSolutionReader(
            std::string filepath,
            RawDatabasePtr valid_database);

    //! \brief Read solutions from YAML file
    std::vector<AdimensionalSystemSolution> parse_solutions();

    //! \brief return the database
    RawDatabasePtr get_database();

private:
    //! \brief Implementation of the reader
    struct SPECMICP_DLL_LOCAL AdimensionalSystemSolutionReaderImpl;
    specmicp::utils::pimpl_ptr<AdimensionalSystemSolutionReaderImpl> m_impl; //!< The implementation
};


//! \brief Read solutions from a yaml file
//!
//! If the database is not valid read it from ad yaml databse file,
//! as given in the solution file
std::vector<AdimensionalSystemSolution> SPECMICP_DLL_PUBLIC parse_solutions_yaml(
        std::string filepath,
        RawDatabasePtr& valid_or_not_database
        );

//! \brief Read a solution from a yaml file
//!
//! If the database is not valid read it from ad yaml databse file,
//! as given in the solution file
AdimensionalSystemSolution SPECMICP_DLL_PUBLIC parse_solution_yaml(
        std::string filepath,
        RawDatabasePtr& valid_or_not_database
        );


} // end namespace io
} // end namespace specmicp

#endif // SPECMICP_SPECMICP_ADIMSOLUTION_READER_HPP
