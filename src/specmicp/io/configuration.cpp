/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "configuration.hpp"


#include "specmicp/adimensional/adimensional_system_solver_structs.hpp"
#include "specmicp/adimensional/config_default_options_solver.h"

#include "specmicp_common/io/safe_config.hpp"
#include "specmicp_common/physics/units.hpp"
#include "specmicp_common/physics/io/configuration.hpp"
#include "specmicp_common/physics/laws.hpp"

#include "specmicp/problem_solver/formulation.hpp"
#include "specmicp/problem_solver/reactant_box.hpp"
#include "specmicp/problem_solver/smart_solver.hpp"

#include "specmicp_database/database.hpp"

#include "specmicp_common/io/config_yaml_sections.h"

#include <unordered_map>

namespace specmicp {
namespace io {

// Additional declarations
// =======================

//! \brief Configure a fixed fugacity component
//!
//! \deprecated
void configure_specmicp_constraints_fixed_activity(
        AdimensionalSystemConstraints& constraints,
        const YAML::Node& conf_constraints_fixed_activity,
        const RawDatabasePtr& raw_db
        );

//! \brief Configure a fixed fugacity component
//!
//! \deprecated
void configure_specmicp_constraints_fixed_fugacity(
        AdimensionalSystemConstraints& constraints,
        const YAML::Node& conf_constraints_fixed_fugacity,
        const RawDatabasePtr& raw_db
        );

//! \brief Configure a fixed molality constraint
//!
//! \deprecated
void configure_specmicp_constraints_fixed_molality(
        AdimensionalSystemConstraints& constraints,
        const YAML::Node& conf_constraints_fixed_molality,
        const RawDatabasePtr& raw_db
        );

//! \brief Configure a fixed fugacity component
void configure_specmicp_constraints_fixed_activity(
        AdimensionalSystemConstraints& constraints,
        const database::DataContainer * const raw_db,
        YAMLConfigHandle&& conf_constraints_fixed_activity
        );

//! \brief Configure a fixed fugacity component
void configure_specmicp_constraints_fixed_fugacity(
        AdimensionalSystemConstraints& constraints,
        const database::DataContainer * const raw_db,
        YAMLConfigHandle&& conf_constraints_fixed_fugacity
        );

//! \brief Configure a fixed molality constraint
void configure_specmicp_constraints_fixed_molality(
        AdimensionalSystemConstraints& constraints,
        const database::DataContainer * const raw_db,
        YAMLConfigHandle&& conf_constraints_fixed_molality
        );

// Implementation
// ==============

void configure_specmicp_options(
        AdimensionalSystemSolverOptions& options,
        const units::UnitsSet& the_units,
        YAMLConfigHandle&& conf
        )
{
    micpsolver::MiCPSolverOptions& solver = options.solver_options;
    conf.set_if_attribute_exists<scalar_t>(
                solver.fvectol, SPC_CF_S_SPECMICP_A_RES_TOL, 0.0);
    conf.set_if_attribute_exists<scalar_t>(
                solver.steptol, SPC_CF_S_SPECMICP_A_STEP_TOL, 0.0);
    conf.set_if_attribute_exists<index_t>(
                solver.max_iter, SPC_CF_S_SPECMICP_A_MAX_ITER, 0);
    conf.set_if_attribute_exists<scalar_t>(
                solver.maxstep, SPC_CF_S_SPECMICP_A_MAX_STEP_LENGTH, 0.0);
    conf.set_if_attribute_exists<index_t>(
                solver.maxiter_maxstep, SPC_CF_S_SPECMICP_A_MAX_STEP_MAX_ITER, 0);
    conf.set_if_attribute_exists<bool>(
                solver.use_scaling, SPC_CF_S_SPECMICP_A_SCALING);
    conf.set_if_attribute_exists<bool>(
                solver.non_monotone_linesearch, SPC_CF_S_SPECMICP_A_NONMONOTONE);
    conf.set_if_attribute_exists<scalar_t>(
                solver.factor_descent_condition, SPC_CF_S_SPECMICP_A_DESCENT_DIRECTION);
    conf.set_if_attribute_exists<scalar_t>(
                solver.condition_limit, SPC_CF_S_SPECMICP_A_COND_CHECK);
    conf.set_if_attribute_exists<scalar_t>(
                solver.threshold_cycling_linesearch, SPC_CF_S_SPECMICP_A_TRSHOLD_CYCLING_LSEARCH);

    AdimensionalSystemOptions& system = options.system_options;
    conf.set_if_attribute_exists<scalar_t>(
                system.non_ideality_tolerance, SPC_CF_S_SPECMICP_A_NONIDEAL_TOL, 0.0);
    conf.set_if_attribute_exists<index_t>(
                system.non_ideality_max_iter, SPC_CF_S_SPECMICP_A_NONIDEAL_MAX_ITER, 0);
    conf.set_if_attribute_exists<scalar_t>(
                system.cutoff_total_concentration, SPC_CF_S_SPECMICP_A_CUTOFF_TOT_CONC, 0.0);
    conf.set_if_attribute_exists<scalar_t>(
                system.restart_concentration, SPC_CF_S_SPECMICP_A_RESTART_CONCENTRATION);
    conf.set_if_attribute_exists<scalar_t>(
                system.restart_water_volume_fraction, SPC_CF_S_SPECMICP_A_RESTART_WATER_VOL_FRAC, 0.0);
    conf.set_if_attribute_exists<scalar_t>(
                system.under_relaxation_factor, SPC_CF_S_SPECMICP_A_UNDER_RELAXATION, 0.0, 1.0);

    options.units_set = the_units;
}

// ReactantBox
// ============

namespace internal {

// helper functions
static void configure_formulation(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& configuration
        );

static void configure_formulation_aqueous(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& cf_aqueous
        );

static void configure_formulation_solid(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& cf_aqueous
        );

static void configure_constraints(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& configuration
        );

static void configure_constraints_fixed_activity(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& cf_f_act
        );

static void configure_constraints_fixed_fugacity(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& cf_f_act
        );

static void configure_constraints_fixed_molality(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& cf_f_mol
        );

} // end namespace internal


void SPECMICP_DLL_PUBLIC configure_specmicp_reactant_box(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& configuration
        )
{
    internal::configure_formulation(
                reactant_box,
                configuration.get_section(SPC_CF_S_FORMULATION)
                );
    internal::configure_constraints(
                reactant_box,
                configuration.get_section(SPC_CF_S_CONSTRAINTS)
                );
}

ReactantBox SPECMICP_DLL_PUBLIC configure_specmicp_reactant_box(
        RawDatabasePtr raw_db,
        const units::UnitsSet& the_units,
        YAMLConfigHandle&& configuration
        )
{
    ReactantBox reactant_box(raw_db, the_units);

    configure_specmicp_reactant_box(reactant_box, std::move(configuration));

    return reactant_box;
}

namespace internal {

// helper functions
static void configure_formulation(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& cf_formulation)
{
    // solution
    auto cf_solution   = cf_formulation.get_section(SPC_CF_S_FORMULATION_A_SOLUTION);
    const auto amount  =
            cf_solution.get_required_attribute<double>(SPC_CF_S_FORMULATION_A_AMOUNT);
    const auto unit    =
            cf_solution.get_required_attribute<std::string>(SPC_CF_S_FORMULATION_A_UNIT);

    reactant_box.set_solution(amount, unit);

    if (cf_formulation.has_section(SPC_CF_S_FORMULATION_A_AQUEOUS))
    {
        configure_formulation_aqueous(
                    reactant_box,
                    cf_formulation.get_section(SPC_CF_S_FORMULATION_A_AQUEOUS)
                    );
    }
    if (cf_formulation.has_section(SPC_CF_S_FORMULATION_A_MINERALS))
    {
        configure_formulation_solid(
                    reactant_box,
                    cf_formulation.get_section(SPC_CF_S_FORMULATION_A_MINERALS)
                    );
    }
}

static void configure_formulation_aqueous(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& cf_aqueous
        )
{
    if (not cf_aqueous.is_sequence()) {
        cf_aqueous.report_error(
                    YAMLConfigError::ListExpected,
                    "Aqueous species must be provide as a list of triplet"
                    " {label amount, unit}"
                    );
    }
    for (auto ind: RangeIterator<uindex_t>(cf_aqueous.size()))
    {
        auto cf = cf_aqueous.get_section(ind);
        const auto label =
                cf.get_required_attribute<std::string>(SPC_CF_S_FORMULATION_A_LABEL);
        const auto value =
                cf.get_required_attribute<double>(SPC_CF_S_FORMULATION_A_AMOUNT);
        const auto unit =
                cf.get_required_attribute<std::string>(SPC_CF_S_FORMULATION_A_UNIT);

        reactant_box.add_aqueous_species(label, value, unit);
    }
}

static void configure_formulation_solid(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& cf_solid
        )
{
    if (not cf_solid.is_sequence()) {
        cf_solid.report_error(
                    YAMLConfigError::ListExpected,
                    "Solid species must be provide as a list of triplet"
                    " {label amount, unit}"
                    );
    }
    for (auto ind: RangeIterator<uindex_t>(cf_solid.size()))
    {
        auto cf = cf_solid.get_section(ind);
        const auto label = cf.get_required_attribute<
                           std::string>(SPC_CF_S_FORMULATION_A_LABEL);
        const auto value = cf.get_required_attribute<
                           double>(SPC_CF_S_FORMULATION_A_AMOUNT);
        const auto unit  = cf.get_required_attribute<
                           std::string>(SPC_CF_S_FORMULATION_A_UNIT);

        reactant_box.add_solid_phase(label, value, unit);
    }
}


static void configure_constraints(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& cf)
{
    // find the constraint to apply
    bool has_saturated = false;
    bool has_fixed_saturation = false;
    bool has_water_conservation = false;
    bool does_conservation_water = true;
    scalar_t water_param = -1.0;
    if (cf.has_attribute(SPC_CF_S_CONSTRAINTS_A_SATURATED_SYSTEM))
    {
        has_saturated = cf.get_attribute<bool>(SPC_CF_S_CONSTRAINTS_A_SATURATED_SYSTEM);
    }
    if (cf.has_attribute(SPC_CF_S_CONSTRAINTS_A_FIXEDSATURATION))
    {
        water_param = cf.get_attribute<scalar_t>(SPC_CF_S_CONSTRAINTS_A_FIXEDSATURATION, 0.0, 1.0);
        if (water_param == 1.0)
        {
            if (cf.has_attribute(SPC_CF_S_CONSTRAINTS_A_SATURATED_SYSTEM)) {
                cf.report_error(YAMLConfigError::InvalidArgument,
                                "SaturatedSystem and a Fixed saturation of 1.0 are both set."
                                "Provide only one of the options");
            }
            has_saturated = true;
        }
        else
            has_fixed_saturation = true;
    }
    if (cf.has_attribute(SPC_CF_S_CONSTRAINTS_A_CONSERVATION_WATER))
    {
        has_water_conservation = true;
        does_conservation_water = cf.get_attribute<bool>(SPC_CF_S_CONSTRAINTS_A_CONSERVATION_WATER);
    }
    else
    {
        has_water_conservation = false;
        does_conservation_water = true;
    }

    if ( (int) has_saturated + (int) has_fixed_saturation + (int) has_water_conservation > 1)
    {
        cf.report_error(YAMLConfigError::InvalidArgument,
                        "Incompatible options, choose only one of : "
                        "Saturated system, fixed saturation or conservation water");
    }

    if (has_saturated) {
        reactant_box.set_saturated_system();
    }
    else if (has_fixed_saturation) {
        reactant_box.set_fixed_saturation(water_param);
    }
    else if (not does_conservation_water) {
        reactant_box.disable_conservation_water();
    }

    // Charge keeper
    // -------------
    if (cf.has_attribute(SPC_CF_S_CONSTRAINTS_A_CHARGEKEEPER))
    {
        auto label = cf.get_attribute<
                     std::string>(SPC_CF_S_CONSTRAINTS_A_CHARGEKEEPER);
        reactant_box.set_charge_keeper(label);
    }

    // Fixed activity
    // ---------------
    if (cf.has_section(SPC_CF_S_CONSTRAINTS_A_FIXEDACTIVITY))
    {
        configure_constraints_fixed_activity(
                    reactant_box,
                    cf.get_section(SPC_CF_S_CONSTRAINTS_A_FIXEDACTIVITY)
                    );
    }

    // Fixed fugacity
    // --------------
    if (cf.has_section(SPC_CF_S_CONSTRAINTS_A_FIXEDFUGACITY))
    {
        configure_constraints_fixed_fugacity(
                    reactant_box,
                    cf.get_section(SPC_CF_S_CONSTRAINTS_A_FIXEDFUGACITY)
                    );
    }

    // Fixed molality
    // ---------------
    if (cf.has_section(SPC_CF_S_CONSTRAINTS_A_FIXEDMOLALITY))
    {
        configure_constraints_fixed_molality(
                    reactant_box,
                    cf.get_section(SPC_CF_S_CONSTRAINTS_A_FIXEDMOLALITY)
                    );
    }

    if (cf.has_attribute(SPC_CF_S_CONSTRAINTS_A_INERT_VOLUME_FRACTION))
    {
        reactant_box.set_inert_volume_fraction(
                    cf.get_attribute<double>(SPC_CF_S_CONSTRAINTS_A_INERT_VOLUME_FRACTION)
                    );
    }
}


static void configure_constraints_fixed_activity(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& cf_f_act
        )
{

    if (not cf_f_act.is_sequence()) {
        cf_f_act.report_error(
                    YAMLConfigError::ListExpected,
                    "Fixed activity component must be provide as a list of"
                    " triplet {label amount, unit}"
                    );
    }
    for (auto ind: RangeIterator<uindex_t>(cf_f_act.size()))
    {
        auto cf = cf_f_act.get_section(ind);
        const auto label = cf.get_required_attribute<
                           std::string>(SPC_CF_S_CONSTRAINTS_A_LABEL);
        double value {0.0}; // value can be provided as value or log_value
        if (cf.has_attribute(SPC_CF_S_CONSTRAINTS_A_AMOUNT))
        {
            value = cf.get_attribute<double>(SPC_CF_S_CONSTRAINTS_A_AMOUNT);
        }
        else if (cf.has_attribute(SPC_CF_S_CONSTRAINTS_A_AMOUNTLOG))
        {
            const auto log_val = cf.get_attribute<
                                 double>(SPC_CF_S_CONSTRAINTS_A_AMOUNTLOG);
            value = std::pow(10.0, log_val);
        }
        else
        {
            cf.report_error(
                        YAMLConfigError::MissingRequiredAttribute,
                        "Either amount or log amount must be provided."
                        );
        }
        reactant_box.add_fixed_activity_component(label, value);
    }
}


static void configure_constraints_fixed_fugacity(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& cf_f_fug
        )
{

    if (not cf_f_fug.is_sequence()) {
        cf_f_fug.report_error(
                    YAMLConfigError::ListExpected,
                    "Fixed fugacity gas must be provide as a list of"
                    " triplet {label amount, unit}"
                    );
    }
    for (auto ind: RangeIterator<uindex_t>(cf_f_fug.size()))
    {
        auto cf = cf_f_fug.get_section(ind);
        // both component and gas must be provided
        const auto label_c = cf.get_required_attribute<
                           std::string>(SPC_CF_S_CONSTRAINTS_A_LABEL_COMPONENT);
        const auto label_g = cf.get_required_attribute<
                           std::string>(SPC_CF_S_CONSTRAINTS_A_LABEL_GAS);
        double value {0.0}; // value can be provided as value or log_value
        if (cf.has_attribute(SPC_CF_S_CONSTRAINTS_A_AMOUNT))
        {
            value = cf.get_attribute<double>(SPC_CF_S_CONSTRAINTS_A_AMOUNT);
        }
        else if (cf.has_attribute(SPC_CF_S_CONSTRAINTS_A_AMOUNTLOG))
        {
            const auto log_val = cf.get_attribute<
                                 double>(SPC_CF_S_CONSTRAINTS_A_AMOUNTLOG);
            value = std::pow(10.0, log_val);
        }
        else
        {
            cf.report_error(
                        YAMLConfigError::MissingRequiredAttribute,
                        "Either amount or log amount must be provided."
                        );
        }
        reactant_box.add_fixed_fugacity_gas(label_g, label_c, value);
    }
}


static void configure_constraints_fixed_molality(
        ReactantBox& reactant_box,
        YAMLConfigHandle&& cf_f_mol
        )
{

    if (not cf_f_mol.is_sequence()) {
        cf_f_mol.report_error(
                    YAMLConfigError::ListExpected,
                    "Fixed molality component must be provide as a list of"
                    " triplet {label amount, unit}"
                    );
    }
    for (auto ind: RangeIterator<uindex_t>(cf_f_mol.size()))
    {
        auto cf = cf_f_mol.get_section(ind);
        const auto label = cf.get_required_attribute<
                           std::string>(SPC_CF_S_CONSTRAINTS_A_LABEL);
        double value {0.0}; // value can be provided as value or log_value
        if (cf.has_attribute(SPC_CF_S_CONSTRAINTS_A_AMOUNT))
        {
            value = cf.get_attribute<double>(SPC_CF_S_CONSTRAINTS_A_AMOUNT);
        }
        else if (cf.has_attribute(SPC_CF_S_CONSTRAINTS_A_AMOUNTLOG))
        {
            const auto log_val = cf.get_attribute<
                                 double>(SPC_CF_S_CONSTRAINTS_A_AMOUNTLOG);
            value = std::pow(10.0, log_val);
        }
        else
        {
            cf.report_error(
                        YAMLConfigError::MissingRequiredAttribute,
                        "Either amount or log amount must be provided."
                        );
        }
        reactant_box.add_fixed_molality_component(label, value);
    }
}



} // end namespace internal


// Constraints
// ===========


// new interface
// -------------

void configure_specmicp_constraints(
        AdimensionalSystemConstraints& constraints,
        const database::DataContainer * const raw_db,
        YAMLConfigHandle&& conf_constraints
        )
{
    // equation for water
    // ==================
    // enable conservation of water
    if (conf_constraints.has_attribute(SPC_CF_S_CONSTRAINTS_A_CONSERVATION_WATER))
    {

        auto tmp_water = conf_constraints.get_attribute<bool>(
                    SPC_CF_S_CONSTRAINTS_A_CONSERVATION_WATER);
        if (tmp_water)
        {
            // only one constraints
            if (conf_constraints.get_optional_attribute<bool>(SPC_CF_S_CONSTRAINTS_A_SATURATED_SYSTEM, false))
            {
                conf_constraints.report_error(YAMLConfigError::InvalidArgument,
                                          "Attributes " SPC_CF_S_CONSTRAINTS_A_CONSERVATION_WATER
                                          "and " SPC_CF_S_CONSTRAINTS_A_SATURATED_SYSTEM
                                          " cannot be set to true at the same time");
            }
            constraints.enable_conservation_water();
        }
        else
            constraints.disable_conservation_water();
    }
    // or enable saturated system
    if (conf_constraints.has_attribute(SPC_CF_S_CONSTRAINTS_A_SATURATED_SYSTEM))
    {
        if (conf_constraints.get_attribute<bool>(SPC_CF_S_CONSTRAINTS_A_SATURATED_SYSTEM))
        {
            constraints.set_saturated_system();
        }
    }

    conf_constraints.set_if_attribute_exists(constraints.inert_volume_fraction,
                                             SPC_CF_S_CONSTRAINTS_A_INERT_VOLUME_FRACTION
                                             );

    // Aqueous components
    // ==================
    // charge keeper
    // -------------
    if (conf_constraints.has_attribute(SPC_CF_S_CONSTRAINTS_A_CHARGEKEEPER))
    {
        const auto label = conf_constraints.get_attribute<std::string>(
                    SPC_CF_S_CONSTRAINTS_A_CHARGEKEEPER);
        const index_t id = raw_db->get_id_component(label);
        if (id == no_species)
        {
            conf_constraints.report_error(
                        YAMLConfigError::InvalidArgument,
                        "Species " + label + " does not exist in the database."
                        " It can't be the charge keeper."
                        );
        }
        constraints.set_charge_keeper(id);

    }
    // Fixed activity
    // ---------------
    if (conf_constraints.has_section(SPC_CF_S_CONSTRAINTS_A_FIXEDACTIVITY))
    {
        configure_specmicp_constraints_fixed_activity(
                    constraints,
                    raw_db,
                    conf_constraints.get_section(SPC_CF_S_CONSTRAINTS_A_FIXEDACTIVITY)
                    );
    }
    // Fixed fugacity
    // --------------
    if (conf_constraints.has_section(SPC_CF_S_CONSTRAINTS_A_FIXEDFUGACITY))
    {
        configure_specmicp_constraints_fixed_fugacity(
                    constraints,
                    raw_db,
                    conf_constraints.get_section(SPC_CF_S_CONSTRAINTS_A_FIXEDFUGACITY)
                    );
    }
    // Fixed activity
    // ---------------
    if (conf_constraints.has_section(SPC_CF_S_CONSTRAINTS_A_FIXEDMOLALITY))
    {
        configure_specmicp_constraints_fixed_molality(
                    constraints,
                    raw_db,
                    conf_constraints.get_section(SPC_CF_S_CONSTRAINTS_A_FIXEDMOLALITY)
                    );
    }
    // Surface model
    // =============
    if (conf_constraints.has_attribute(SPC_CF_S_CONSTRAINTS_A_SURFACE_MODEL))
    {
        const bool is_enabled = conf_constraints.get_attribute<bool>(
                SPC_CF_S_CONSTRAINTS_A_SURFACE_MODEL
                );
        if (is_enabled)
        {
            const auto value = conf_constraints.get_required_attribute<scalar_t>(
                SPC_CF_S_CONSTRAINTS_A_SURFACE_CONCENTRATION);
            constraints.enable_surface_model(value);
        }
        else
        {
            constraints.disable_surface_model();
        }
    }
}

// helpers functions
//! \brief Return the id of a component
index_t get_id_component(
        const database::DataContainer* const raw_db,
        YAMLConfigHandle& conf,
        const std::string& attr_key=SPC_CF_S_CONSTRAINTS_A_LABEL
        )
{
    const auto label = conf.get_required_attribute<std::string>(attr_key);
    const index_t id = raw_db->get_id_component(label);
    if (id == no_species)
    {
        conf.report_error(
                    YAMLConfigError::InvalidArgument,
                    "'" + label + "' is not a valid component."
                    );
    }
    return id;
}

//! \brief Return the id of a gas
index_t get_id_gas(
        const database::DataContainer* const raw_db,
        YAMLConfigHandle& conf,
        const std::string& attr_key=SPC_CF_S_CONSTRAINTS_A_LABEL_GAS
        )
{
    const auto label = conf.get_required_attribute<std::string>(attr_key);
    const index_t id = raw_db->get_id_gas(label);
    if (id == no_species)
    {
        conf.report_error(
                    YAMLConfigError::InvalidArgument,
                    "'" + label + "' is not a valid gas."
                    );
    }
    return id;
}

//! \brief Return a value in log10 for fixed fugacity
scalar_t get_value(YAMLConfigHandle& conf)
{
    scalar_t value = 0;
    if (conf.has_attribute(SPC_CF_S_CONSTRAINTS_A_AMOUNT)) {
        value = std::log10(conf.get_attribute<scalar_t>(SPC_CF_S_CONSTRAINTS_A_AMOUNT));
    }
    else if (conf.has_attribute(SPC_CF_S_CONSTRAINTS_A_AMOUNTLOG)) {
        value = conf.get_attribute<scalar_t>(SPC_CF_S_CONSTRAINTS_A_AMOUNTLOG);
    }
    else {
        conf.report_error(
                    YAMLConfigError::InvalidArgument,
                    "Expected one of '" SPC_CF_S_CONSTRAINTS_A_AMOUNT "' or '"
                    SPC_CF_S_CONSTRAINTS_A_AMOUNTLOG "'."
                    );
    }
    return value;
}

void configure_specmicp_constraints_fixed_activity(
        AdimensionalSystemConstraints& constraints,
        const database::DataContainer * const raw_db,
        YAMLConfigHandle&& conf_constraints_fixed_activity
        )
{
    for (uindex_t ind=0; ind<conf_constraints_fixed_activity.size(); ++ind)
    {
        YAMLConfigHandle conf = conf_constraints_fixed_activity.get_section(ind);

        const index_t id = get_id_component(raw_db, conf);
        const scalar_t value = get_value(conf);
        constraints.add_fixed_activity_component(id, value);
    }
}

void configure_specmicp_constraints_fixed_fugacity(
        AdimensionalSystemConstraints& constraints,
        const database::DataContainer* const raw_db,
        YAMLConfigHandle&& conf_constraints_fixed_fugacity
        )
{
    for (uindex_t ind=0; ind<conf_constraints_fixed_fugacity.size(); ++ind)
    {

        YAMLConfigHandle conf = conf_constraints_fixed_fugacity.get_section(ind);

        const index_t id_c = get_id_component(raw_db, conf, SPC_CF_S_CONSTRAINTS_A_LABEL_COMPONENT);
        const index_t id_g = get_id_gas(raw_db, conf, SPC_CF_S_CONSTRAINTS_A_LABEL_GAS);
        const scalar_t value = get_value(conf);
        constraints.add_fixed_fugacity_gas(id_g, id_c, value);
    }

}

void configure_specmicp_constraints_fixed_molality(
        AdimensionalSystemConstraints& constraints,
        const database::DataContainer * const raw_db,
        YAMLConfigHandle&& conf_constraints_fixed_molality
        )
{
    for (uindex_t ind=0; ind<conf_constraints_fixed_molality.size(); ++ind)
    {
        YAMLConfigHandle conf = conf_constraints_fixed_molality.get_section(ind);

        const index_t id = get_id_component(raw_db, conf);
        const scalar_t value = get_value(conf);
        constraints.add_fixed_molality_component(id, value);
    }
}

void configure_smart_solver_initialization(
        SmartAdimSolver& solver,
        YAMLConfigHandle&& conf_init
        )
{
    if (conf_init.has_attribute(SPC_CF_S_INITIALIZATION_A_SOLUTION))
    {
        solver.set_init_volfrac_water(
              conf_init.get_attribute<scalar_t>(SPC_CF_S_INITIALIZATION_A_SOLUTION));
    }
    if (conf_init.has_section(SPC_CF_S_INITIALIZATION_A_AQUEOUS))
    {
        auto scf = conf_init.get_section(SPC_CF_S_INITIALIZATION_A_AQUEOUS);
        if (not scf.is_map())
        {
            scf.report_error(YAMLConfigError::MapExpected,
                             "A map {component, value} is expected.");
        }
        if (scf.has_attribute("all"))
        {
            solver.set_init_molality(scf.get_attribute<scalar_t>("all"));
        }
        std::unordered_map<std::string, scalar_t> tmap;
        tmap.reserve(scf.size());
        for (auto it=scf.map_begin(); it!=scf.map_end(); ++it)
        {
            auto name = (*it).first;
            if (name != "all")
            {
                tmap.insert({name, scf.get_attribute<scalar_t>(name)});
            }
        }
        if (not tmap.empty())
        {
            solver.set_init_molality(tmap);
        }
    }
    if (conf_init.has_section(SPC_CF_S_INITIALIZATION_A_MINERALS))
    {
        auto scf = conf_init.get_section(SPC_CF_S_INITIALIZATION_A_MINERALS);
        if (not scf.is_map())
        {
            scf.report_error(YAMLConfigError::MapExpected,
                             "A map {component, value} is expected.");
        }
        std::unordered_map<std::string, scalar_t> tmap;
        tmap.reserve(scf.size());
        for (auto it=scf.map_begin(); it!=scf.map_end(); ++it)
        {
            auto name = (*it).first;
            tmap.insert({name, scf.get_attribute<scalar_t>(name)});
        }
        if (not tmap.empty())
        {
            solver.set_init_volfrac_mineral(tmap);
        }
    }
}

} //end namespace io
} //end namespace specmicp
