/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "print.hpp"
#include "specmicp/adimensional/adimensional_system_structs.hpp"
#include "specmicp/adimensional/adimensional_system_solver_structs.hpp"

#include "specmicp_common/physics/io/units.hpp"
#include <iostream>

namespace specmicp {
namespace io {

void print_specmicp_constraints(
        const RawDatabasePtr& the_database,
        const AdimensionalSystemConstraints& constraints
        )
{
    std::cout << "Constraints \n ---------- \n";
    std::cout << "Total concentrations :\n";
    for (auto comp: the_database->range_component())
        std::cout << the_database->get_label_component(comp) << " - " << constraints.total_concentrations(comp) << std::endl;
    if (constraints.charge_keeper != no_species)
    {
        std::cout << "Charge keeper : " << the_database->get_label_component(constraints.charge_keeper) << std::endl;
    }
    if (constraints.fixed_molality_cs.size())
    {
        std::cout << "Fixed  molality :\n";
        for (auto comp: constraints.fixed_molality_cs)
        {
            std::cout << "  - " << the_database->get_label_component(comp.id_component)
                      << ": " << comp.log_value << "\n";
        }
    }
    if (constraints.fixed_activity_cs.size())
    {
        std::cout << "Fixed  activity :\n";
        for (auto comp: constraints.fixed_activity_cs)
        {
            std::cout << "  - " << the_database->get_label_component(comp.id_component)
                      << " - " << comp.log_value << "\n";
        }
    }
    if (constraints.fixed_fugacity_cs.size())
    {
        std::cout << "Fixed  fugacity :\n";
        for (auto comp: constraints.fixed_fugacity_cs)
        {
            std::cout << the_database->get_label_component(comp.id_component) << " - " << comp.log_value << "\n";
        }
    }
    std::cout << "Inert volume fraction : " << constraints.inert_volume_fraction << "\n";

    std::cout << "Water equation : ";
    switch (constraints.water_equation) {
    case WaterEquationType::MassConservation:
        std::cout << "Unsaturated system, mass conservation \n";
        break;
    case WaterEquationType::FixedSaturation:
        std::cout << "Unsaturated system, fixed saturation ("
                  << constraints.water_parameter << ")\n";
        break;
    case WaterEquationType::SaturatedSystem:
        std::cout << "saturated system\n";
        break;
    case WaterEquationType::NoEquation:
        std::cout << "WARNING : no water equations\n";
    }
    std::cout << "-------------" << std::endl;


}


//! \brief Output options
void print_specmicp_options(
    std::ostream& out,
    const AdimensionalSystemSolverOptions& options
    )
{
    const micpsolver::MiCPSolverOptions& sopts = options.solver_options;
    out << std::boolalpha;
    out << "  + MiCPSolver options "
        << "\n     - residual tolerance          " << sopts.fvectol
        << "\n     - step tolerance              " << sopts.steptol
        << "\n     - max. # iterations           " << sopts.max_iter
        << "\n     - max. step length            " << sopts.maxstep
        << "\n     - # iters at max. step        " << sopts.maxiter_maxstep
        << "\n     - threshold stationary        " << sopts.threshold_stationary_point
        << "\n    <-> Advanced options"
        << "\n     - threshold cycling           " << sopts.threshold_cycling_linesearch
        << "\n     - use crashing                " << sopts.use_crashing
        << "\n     - use scaling                 " << sopts.use_scaling
        << "\n     - nonmonotone linesearch      " << sopts.non_monotone_linesearch
        << "\n     - max. factorization step     " << sopts.max_factorization_step
        << "\n     - power descent direction     " << sopts.power_descent_condition
        << "\n     - factor descent direction    " << sopts.factor_descent_condition
        << "\n     - gradient step factor        " << sopts.factor_gradient_search_direction
        << std::endl;
    const AdimensionalSystemOptions& systs = options.system_options;
    out << "  + System options"
        << "\n     - non-ideality model          " << systs.non_ideality
        << "\n     - non-ideality max. iter      " << systs.non_ideality_max_iter
        << "\n     - non-ideality tolerance      " << systs.non_ideality_tolerance
        << "\n     - non-ideality start          " << systs.start_non_ideality_computation
        << "\n     - under-relaxation factor     " << systs.under_relaxation_factor
        << "\n     - restart log(concentration)  " << systs.restart_concentration
        << "\n     - new log(concentration)      " << systs.new_component_concentration
        << "\n     - cutoff total concentration  " << systs.cutoff_total_concentration
        << std::endl;
    out << "  + Units "
        << "\n     - length                      " << to_string(options.units_set.length)
        << "\n     - mass                        " << to_string(options.units_set.mass)
        << "\n     - quantity                    " << to_string(options.units_set.quantity)
        << std::endl;
    out << "  + Misc."
        << "\n     - allow restart               " << options.allow_restart
        << "\n     - use pcfm                    " << options.use_pcfm
        << "\n     - force_pcfm                  " << options.force_pcfm
        << std::endl;
}

} //end namespace io
} //end namespace specmicp
