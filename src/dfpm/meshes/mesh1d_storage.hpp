/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_MESH_MESH1DSTORAGE
#define SPECMICP_MESH_MESH1DSTORAGE

#include "specmicp_common/types.hpp"

/*!
\file mesh1d_storage.hpp
\brief Storage for the 1d mesh

*/

namespace specmicp {

namespace io {
namespace internal {
class MeshHDF5Saver;
} // end namespace internal
} // end namespace io

namespace mesh {

//! \brief Type of a 1D mesh
enum class Mesh1DType
{
    uniform,      //!< Uniform mesh
    axisymmetric, //!< Axisymmetric 1D mesh
    ramp,         //!< Ramp mesh
    user_defined  //!< Custom mesh
};

//! \brief Geometry information for a uniform axisymmetric mesh
//!
//! If (nb_nodes==-1) it is computed from the radius and dx
//! If (dx==-1) it is computed from the radius and the number of nodes
struct SPECMICP_DLL_PUBLIC UniformAxisymmetricMesh1DGeometry
{
    index_t nb_nodes; //!< Number of nodes in the mesh
    scalar_t radius;  //!< Outside radius of the cylinder
    scalar_t dx {-1};      //!< Discretization step
    scalar_t height;  //!< Height of the cylinder
};


//! \brief Information needed to build a uniform linear 1D mesh
//!
//! \sa uniform_mesh_1d(const Uniform1DMeshGeometry&)
struct SPECMICP_DLL_PUBLIC Uniform1DMeshGeometry
{
    scalar_t dx;
    index_t nb_nodes;
    scalar_t section;
};

//! \brief Geometry information for a ramp1D
//!
//! \sa ramp_mesh1d
struct SPECMICP_DLL_PUBLIC Ramp1DMeshGeometry
{
    scalar_t dx_max;
    scalar_t dx_min;
    scalar_t length_ramp;
    scalar_t length_plateau;

    scalar_t section;
};


//! \brief Storage class of a 1D mesh
class SPECMICP_DLL_PUBLIC Mesh1DStorage
{
    // where information are stored in the data matrix
    constexpr static unsigned int id_node_coord   {0}; //!< index of the coordinates
    constexpr static unsigned int id_face_section {1}; //!< index of the face area
    constexpr static unsigned int id_cell_vol_0   {2}; //!< index of the cell vol on the left
    constexpr static unsigned int id_cell_vol_1   {3}; //!< index of the cell vol on the right

public:
    //! \brief Create the structure for a custom 1D mesh with 'nb_nodes'
    Mesh1DStorage(uindex_t nb_nodes):
        m_type(Mesh1DType::user_defined),
        m_data(nb_nodes, 4)
    {}
    //! \brief Create the structure for a 1D mesh of type 'type' with 'nb_nodes'
    Mesh1DStorage(uindex_t nb_nodes, Mesh1DType type):
        m_type(type),
        m_data(nb_nodes, 4)
    {}

    //! \brief Set the coordinates of the nodes
    void set_node_coords(const Eigen::Ref<const Vector>& v_coords)
    {
        specmicp_assert(v_coords.rows() == nb_nodes());
        m_data.col(id_node_coord) = v_coords;
    }
    //! \brief Set the section of the faces
    void set_face_sections(const Eigen::Ref<const Vector>& v_sections)
    {
        specmicp_assert(v_sections.rows() == nb_nodes());
        m_data.col(id_face_section) = v_sections;
    }
    //! \brief Set the section of faces to a constant
    void set_face_sections(scalar_t value)
    {
        m_data.col(id_face_section).setConstant(value);
    }
    //! \brief Set the half-cell volumes on the left on the node
    void set_cell_volumes_left(const Eigen::Ref<const Vector>& v_cell_vol)
    {
        specmicp_assert(v_cell_vol.rows() == nb_nodes());
        m_data.col(id_cell_vol_0) = v_cell_vol;
    }
    //! \brief Set the half-cell volumes on the right on the node
    void set_cell_volumes_right(const Eigen::Ref<const Vector>& v_cell_vol)
    {
        specmicp_assert(v_cell_vol.rows() == nb_nodes());
        m_data.col(id_cell_vol_1) = v_cell_vol;
    }

    //! \brief Set the cell volume
    //!
    //! \param v_cell_vol_0 volume of the cell in the element on the left
    //! \param v_cell_vol_1 volume of the cell in the element on the right
    void set_cell_volumes(
        const Eigen::Ref<Vector>& v_cell_vol_0,
        const Eigen::Ref<Vector>& v_cell_vol_1
        )
    {
        specmicp_assert(v_cell_vol_0.rows() == nb_nodes());
        specmicp_assert(v_cell_vol_1.rows() == nb_nodes());
        m_data.col(id_cell_vol_0) = v_cell_vol_0;
        m_data.col(id_cell_vol_1) = v_cell_vol_1;
    }

    //! \brief Return the type of the mesh
    Mesh1DType type() const
    {
        return m_type;
    }
    //! \brief Return the number of nodes
    index_t nb_nodes() const
    {
        return m_data.rows();
    }
    //! \brief Return the number of elements
    index_t nb_elements() const
    {
        return (nb_nodes() - 1);
    }
    //! \brief Return the coordinates of the node
    scalar_t node_coord(uindex_t node) const
    {
        return m_data(node, id_node_coord);
    }
    //! \brief Return the coordinates of the node
    scalar_t& node_coord(uindex_t node)
    {
        return m_data(node, id_node_coord);
    }
    //! \brief Return the coordinates of the node
    scalar_t face_section(uindex_t element) const
    {
        return m_data(element, id_face_section);
    }
    //! \brief Return the coordinates of the node
    scalar_t& face_section(uindex_t element)
    {
        return m_data(element, id_face_section);
    }
    //! \brief Return the volume of the cell in the left element
    scalar_t cell_volume_left(uindex_t node) const
    {
        return m_data(node,id_cell_vol_0);
    }
    //! \brief Return the volume of the cell in the left element
    scalar_t& cell_volume_left(uindex_t node)
    {
        return m_data(node,id_cell_vol_0);
    }
    //! \brief Return the volume of the cell in the right element
    scalar_t cell_volume_right(uindex_t node) const
    {
        return m_data(node,id_cell_vol_1);
    }
    //! \brief Return the volume of the cell in the right element
    scalar_t& cell_volume_right(uindex_t node)
    {
        return m_data(node,id_cell_vol_1);
    }

private:
    friend class specmicp::io::internal::MeshHDF5Saver; // allow access to matrix

    Mesh1DType m_type;
    Matrix m_data;
};

//! \brief Create and fill storage for 1d uniform mesh
Mesh1DStorage SPECMICP_DLL_PUBLIC uniform_1d_storage(
        const Uniform1DMeshGeometry& geom
        );
//! \brief Create and fill storage for 1d ramp mesh
Mesh1DStorage SPECMICP_DLL_PUBLIC rampmesh_1d_storage(
        const Ramp1DMeshGeometry& geom
        );
//! \brief Create and fill storage for 1d uniform axisymmetric mesh
Mesh1DStorage SPECMICP_DLL_PUBLIC uniform_axisymmetric_mesh_storage(
        const UniformAxisymmetricMesh1DGeometry& geom
        );
//! \brief Create and fill storage for 1d axisymmetric mesh
Mesh1DStorage SPECMICP_DLL_PUBLIC axisymmetric_mesh_storage(
        const Eigen::Ref<Vector>&  coords,
        scalar_t height
        );

} // end namespace mesh
} // end namespace specmicp


#endif // SPECMICP_MESH_MESH1DSTORAGE
