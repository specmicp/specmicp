/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_MESH_MESH1D_HPP
#define SPECMICP_REACTMICP_MESH_MESH1D_HPP

//! \file mesh1d.hpp
//! \brief Abstract base class for the 1D meshes

#include "dfpm/types.hpp"
#include <memory>
#include "mesh1d_storage.hpp"

namespace specmicp {
//! \namespace specmicp::mesh
//! \brief Meshes for dfpm
//!
//! The meshes that can be use in dfpm and ReactMiCP.
namespace mesh {

//! \brief Abstract base class for a 1D mesh
class Mesh1D
{
public:
    const index_t nen = 2; //!< Number of elemental nodes

    //! \brief Create a 1D mesh
    Mesh1D(index_t nb_nodes): m_storage(nb_nodes) {}
    //! \brief Acquire a mesh structure
    Mesh1D(Mesh1DStorage&& storage): m_storage(storage) {}

    //! \brief Return the number of elements
    index_t nb_elements() {return m_storage.nb_elements();}
    //! \brief Return the number of nodes
    index_t nb_nodes() {return m_storage.nb_nodes();}

    //! \brief Return the node from local nod enumber and element
    index_t get_node(index_t element, index_t index)
    {
        specmicp_assert(index >= 0 && index < nen);
        return element+index;
    }
    //! \brief Return the position (meaning may vary with the mesh) of the node
    scalar_t get_position(index_t node)
    {
        specmicp_assert(node >= 0 && node < nb_nodes());
        return m_storage.node_coord(node);
    }

    //! \brief Return the length of an element
    scalar_t get_dx(index_t element)
    {
        specmicp_assert(element >= 0 && element < nb_elements());
        return  std::abs( m_storage.node_coord(element+1)
                - m_storage.node_coord(element)
                );
    }
    //! \brief Return the area of the face at the middle of an element
    scalar_t get_face_area(index_t element)
    {
        specmicp_assert(element >= 0 && element < nb_elements());
        return m_storage.face_section(element);
    }
    //! \brief Return the volume of an element
    scalar_t get_volume_element(index_t element)
    {
        return ( m_storage.cell_volume_right(element)
               + m_storage.cell_volume_left(element+1)
               );
    }
    //! \brief Return the volume of a cell (element of the dual mesh)
    scalar_t get_volume_cell(index_t node)
    {
        specmicp_assert(node >= 0 && node < nb_nodes());
        return ( m_storage.cell_volume_left(node)
               + m_storage.cell_volume_right(node)
               );
    }
    //! \brief Return the volume of a cell inside an element
    scalar_t get_volume_cell_element(index_t element, index_t enode)
    {
        specmicp_assert(element >= 0 && element < nb_elements());
        if (enode == 0)
        {
            return m_storage.cell_volume_right(element);
        }
        else
        {
            specmicp_assert(enode == 1);
            return m_storage.cell_volume_left(element+1);
        }
    }

    //! \brief Range over the elements
    range_t range_elements() {return range_t(nb_elements());}
    //! \brief Range over the nodes
    range_t range_nodes() {return range_t(nb_nodes());}
    //! \brief Range over the elemental nodes
    range_t range_nen() {return range_t(nen);}

    //! \brief Return the low-level information
    //!
    //! \warning Only use should be for output
    const Mesh1DStorage& get_storage() const {return m_storage;}

private:
    Mesh1DStorage m_storage; //!< The store informations about the mesh
};

//! \brief type of a pointer to a mesh
//!
//! This is a smart pointer allowing the mesh to be shared freely
using Mesh1DPtr = std::shared_ptr<Mesh1D>;



//! \brief Return a uniform 1D mesh
inline Mesh1DPtr SPECMICP_DLL_PUBLIC uniform_mesh1d(
        const Uniform1DMeshGeometry& geometry
        )
{
    return std::make_shared<Mesh1D>(uniform_1d_storage(geometry));
}

//! \brief Generate a 'ramp' mesh
//!
//!  this version cares about space discretization, not the total length
inline Mesh1DPtr SPECMICP_DLL_PUBLIC ramp_mesh1d(
        const Ramp1DMeshGeometry& geometry
        )
{
    return std::make_shared<Mesh1D>(rampmesh_1d_storage(geometry));
}

//! \brief Return a shared_ptr to an axisymmetric mesh
//!
//! \param radius is a vector of the position of the nodes
//! \param height is the height of the sample (or depth)
inline Mesh1DPtr SPECMICP_DLL_PUBLIC axisymmetric_mesh1d(
        Vector radius, scalar_t height
        )
{
    return std::make_shared<Mesh1D>(axisymmetric_mesh_storage(radius, height));
}

//! \brief Factory method to build a pointer to a uniform axisymmetric 1D mesh
//!
//! Note: this method buil an AxisymmetricMesh1D instance while
//! the specmicp::mesh::axisymmetric_uniform_mesh1d method build a
//! specmicp::mesh::AxisymmetricUniformMesh1D instance.
inline Mesh1DPtr SPECMICP_DLL_PUBLIC uniform_axisymmetric_mesh1d(
        const UniformAxisymmetricMesh1DGeometry& geometry
        )
{
    return std::make_shared<Mesh1D>(uniform_axisymmetric_mesh_storage(geometry));
}

} // end namespace mesh
} // end namespace specmicp

#endif // SPECMICP_REACTMICP_MESH_MESH1D_HPP
