/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "meshes.hpp"

#include "dfpm/meshes/mesh1d.hpp"
#include "specmicp_common/physics/io/units.hpp"

#include "specmicp_common/io/csv_formatter.hpp"

#include <iostream>
#include <fstream>

namespace specmicp {
namespace io {

void print_mesh(std::ostream* output, mesh::Mesh1DPtr the_mesh, units::LengthUnit lenght_unit)
{
    (*output) << "# dfpm mesh \n";
    (*output) << "# units : " << length_unit_to_string(lenght_unit) << "\n";
    (*output) << "Node\tDistance\tCell_volume" << std::endl;
    for (index_t node: the_mesh->range_nodes())
    {
        (*output) << node << "\t" << the_mesh->get_position(node) << "\t" << the_mesh->get_volume_cell(node) << "\n";
    }
}

//! \brief Print 'the_mesh' in the file 'filepath'
void print_mesh(std::string filepath, mesh::Mesh1DPtr the_mesh, units::LengthUnit lenght_unit)
{
    std::ofstream outfile(filepath);
    if (not outfile)
    {
        throw std::runtime_error("Cannot open file : '"+filepath+"'.");
    }
    print_mesh(&outfile, the_mesh, lenght_unit);
    outfile.close();
}

void
print_header_for_each_node(CSVFile& output, mesh::Mesh1DPtr the_mesh, const std::string& x_label)
{
    output << x_label;
    for (index_t node: the_mesh->range_nodes())
    {
        output.separator();
        output << the_mesh->get_position(node);
    }
    output.eol();
}


void
print_for_each_node(CSVFile& output, mesh::Mesh1DPtr the_mesh, scalar_t x, to_print_each_node_f& func)
{
    output << x;
    for (index_t node: the_mesh->range_nodes())
    {
        output.separator();
        output << func(node);
    }
    output.eol();
}

} //end namespace io
} //end namespace specmicp
