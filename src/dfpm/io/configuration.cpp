/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "configuration.hpp"

#include "specmicp_common/io/yaml.hpp"
#include "specmicp_common/io/safe_config.hpp"

#include "dfpm/meshes/mesh1d.hpp"

#include "dfpm/solver/parabolic_structs.hpp"

#include "specmicp_common/io/config_yaml_sections.h"

#include "specmicp_common/log.hpp"

#include <stdexcept>


namespace specmicp {
namespace io {

// new interface
// -------------

//! \brief Configure the mesh
mesh::Mesh1DPtr SPECMICP_DLL_PUBLIC
configure_mesh(YAMLConfigHandle&& mesh_section)
{
    auto type = mesh_section.get_required_attribute<std::string>(SPC_CF_S_MESH_A_TYPE);

    mesh::Mesh1DPtr the_mesh {nullptr};
    if (type == SPC_CF_S_MESH_SS_UNIFMESH) {
        SPC_CONF_LOG << "Uniform mesh";
        the_mesh = configure_uniform_mesh1D(
                    mesh_section.get_section(SPC_CF_S_MESH_SS_UNIFMESH));
    }
    else if (type == SPC_CF_S_MESH_SS_RAMPMESH) {
        SPC_CONF_LOG << "Ramp mesh";
        the_mesh = configure_ramp_mesh1D(
                    mesh_section.get_section(SPC_CF_S_MESH_SS_RAMPMESH));
    }
    else if (type == SPC_CF_S_MESH_SS_UNIFAXISYMESH) {
        SPC_CONF_LOG << "Uniform axisymmetric mesh";
        the_mesh = configure_uniform_axisymmetric_mesh1D(
                    mesh_section.get_section(SPC_CF_S_MESH_SS_UNIFAXISYMESH));
    }
    else
        mesh_section.report_error(YAMLConfigError::InvalidArgument,
                                  "'"+type+"' is not a valid type of mesh.");
    uindex_t nb_nodes = the_mesh->nb_nodes();
    SPC_CONF_LOG << "Coordinates : "
                 << "\n  - 0 : " << the_mesh->get_position(0)
                 << "\n  - 1 : " << the_mesh->get_position(1)
                 << "\n  - 2 : " << the_mesh->get_position(2)
                 << "\n  - ---- "
                 << "\n  - " << nb_nodes-3 << " : " << the_mesh->get_position(nb_nodes - 3)
                 << "\n  - " << nb_nodes-2 << " : " << the_mesh->get_position(nb_nodes - 2)
                 << "\n  - " << nb_nodes-1 << " : " << the_mesh->get_position(nb_nodes - 1)
                 ;

    return the_mesh;
}

//! \brief Configure an uniform mesh
mesh::Mesh1DPtr
configure_uniform_mesh1D(YAMLConfigHandle&& uniform_section)
{
    mesh::Uniform1DMeshGeometry geometry;
    geometry.dx = uniform_section.get_required_attribute<scalar_t>(
                SPC_CF_S_MESH_SS_UNIFMESH_A_DX, 0.0);
    geometry.nb_nodes = uniform_section.get_required_attribute<index_t>(
                SPC_CF_S_MESH_SS_UNIFMESH_A_NBNODES, 0);
    geometry.section = uniform_section.get_required_attribute<scalar_t>(
                SPC_CF_S_MESH_SS_UNIFMESH_A_SECTION, 0.0);
    return mesh::uniform_mesh1d(geometry);
}


//! \brief Configure a 'ramp' mesh
mesh::Mesh1DPtr
configure_ramp_mesh1D(YAMLConfigHandle&& ramp_section)
{
    mesh::Ramp1DMeshGeometry geometry;
    geometry.dx_min = ramp_section.get_required_attribute<scalar_t>(
                SPC_CF_S_MESH_SS_RAMPMESH_A_MIN_DX, 0.0);
    geometry.dx_max = ramp_section.get_required_attribute<scalar_t>(
                SPC_CF_S_MESH_SS_RAMPMESH_A_MAX_DX, 0.0);
    geometry.length_ramp = ramp_section.get_required_attribute<scalar_t>(
                SPC_CF_S_MESH_SS_RAMPMESH_A_RAMP_LENGTH, 0.0);
    geometry.length_plateau = ramp_section.get_required_attribute<scalar_t>(
                SPC_CF_S_MESH_SS_RAMPMESH_A_PLATEAU_LENGTH, 0.0);
    geometry.section = ramp_section.get_required_attribute<scalar_t>(
                SPC_CF_S_MESH_SS_RAMPMESH_A_SECTION, 0.0);
    return mesh::ramp_mesh1d(geometry);
}


//! \brief Configure an uniform axisymmetric mesh
mesh::Mesh1DPtr
configure_uniform_axisymmetric_mesh1D(YAMLConfigHandle&& axisymmetric_section)
{
    mesh::UniformAxisymmetricMesh1DGeometry geometry;
    geometry.dx = axisymmetric_section.get_optional_attribute<scalar_t>(
                SPC_CF_S_MESH_SS_UNIFAXISYMESH_A_DX, -1.0);
    geometry.nb_nodes = axisymmetric_section.get_optional_attribute<index_t>(
                SPC_CF_S_MESH_SS_UNIFAXISYMESH_A_NBNODES, -1.0);
    geometry.height = axisymmetric_section.get_required_attribute<scalar_t>(
                SPC_CF_S_MESH_SS_UNIFAXISYMESH_A_HEIGHT, 0.0);
    geometry.radius = axisymmetric_section.get_required_attribute<scalar_t>(
                SPC_CF_S_MESH_SS_UNIFAXISYMESH_A_RADIUS, 0.0);
    return mesh::uniform_axisymmetric_mesh1d(geometry);
}


// transport options
// =================

void configure_transport_options(
        dfpmsolver::ParabolicDriverOptions& options,
        io::YAMLConfigHandle&& conf
        )
{
    conf.set_if_attribute_exists<scalar_t>(
                options.absolute_tolerance, SPC_CF_S_DFPM_A_ABS_TOL, 0.0);
    conf.set_if_attribute_exists<scalar_t>(
                options.residuals_tolerance, SPC_CF_S_DFPM_A_RES_TOL, 0.0, 1.0);
    conf.set_if_attribute_exists<scalar_t>(
                options.step_tolerance, SPC_CF_S_DFPM_A_STEP_TOL, 0.0, 1.0);
    conf.set_if_attribute_exists<index_t>(
                options.maximum_iterations, SPC_CF_S_DFPM_A_MAX_ITER, 0);
    conf.set_if_attribute_exists<scalar_t>(
                options.maximum_step_length, SPC_CF_S_DFPM_A_MAX_STEP_LENGTH, 0.0);
    conf.set_if_attribute_exists<index_t>(
                options.max_iterations_at_max_length, SPC_CF_S_DFPM_A_MAX_STEP_MAX_ITER, 0);
    conf.set_if_attribute_exists<scalar_t>(
                options.threshold_stationary_point, SPC_CF_S_DFPM_A_TRSHOLD_STATIONARY, 0.0, 1.0);
    conf.set_if_attribute_exists<index_t>(
                options.quasi_newton, SPC_CF_S_DFPM_A_QUASI_NEWTON);

    if (conf.has_attribute(SPC_CF_S_DFPM_A_SPARSE_SOLVER))
    {
        std::string sparse_solver = conf.get_attribute<std::string>(
                    SPC_CF_S_DFPM_A_SPARSE_SOLVER);
        if (sparse_solver == "LU")
            options.sparse_solver = sparse_solvers::SparseSolver::SparseLU;
        else if (sparse_solver == "QR")
            options.sparse_solver = sparse_solvers::SparseSolver::SparseQR;
#ifdef EIGEN3_UNSUPPORTED_FOUND
        else if (sparse_solver == "GMRES")
            options.sparse_solver = sparse_solvers::SparseSolver::GMRES;
#endif
        else if (sparse_solver == "BiCGSTAB")
            options.sparse_solver = sparse_solvers::SparseSolver::BiCGSTAB;
        else
            conf.report_error(
                        YAMLConfigError::InvalidArgument,
                        "Invalid argument for the sparse solver : '"
                        + sparse_solver + "'.\n"
                        "Available solvers :"
                        "'LU' / 'QR' / 'GMRES' / 'BiCGSTAB'."
                        );
    }
    if (conf.has_attribute(SPC_CF_S_DFPM_A_SPARSE_PIVOTS_TRSHOLD))
    {
        options.sparse_solver_pivots_threshold = conf.get_attribute<scalar_t>(
                                         SPC_CF_S_DFPM_A_SPARSE_PIVOTS_TRSHOLD);
    }
    if (conf.has_attribute(SPC_CF_S_DFPM_A_LINESEARCH))
    {
        std::string linesearch = conf.get_attribute<std::string>(SPC_CF_S_DFPM_A_LINESEARCH);
        if (linesearch == "backtracking")
            options.linesearch = dfpmsolver::ParabolicLinesearch::Backtracking;
        else if (linesearch == "strang")
            options.linesearch = dfpmsolver::ParabolicLinesearch::Strang;
        else
            conf.report_error(
                        YAMLConfigError::InvalidArgument,
                        "Invalid argument for the linesearch : '"
                        + linesearch + "'.\n"
                        "Available options : 'backtracking' / 'strang'."
                        );
    }
}

} //end namespace io
} //end namespace specmicp
