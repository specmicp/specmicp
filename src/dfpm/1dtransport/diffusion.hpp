/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DFPM_1DTRANSPORT_DIFFUSION_HPP
#define SPECMICP_DFPM_1DTRANSPORT_DIFFUSION_HPP

//! \file diffusion.hpp
//! \brief A simple diffusion program

#include <memory>

#include "specmicp_common/types.hpp"

#include "dfpm/types.hpp"
#include "dfpm/solver/parabolic_program.hpp"
#include "dfpm/meshes/mesh1dfwd.hpp"

namespace specmicp {

//! \namespace specmicp::dfpm
//! \brief The finite element module
namespace dfpm {

struct SaturatedDiffusion1DParameters;

//! \class SaturatedDiffusion1D
//! \brief A saturated diffusion equation
//!
//! This is an example of a dfpm program
//!
//! \sa SaturatedDiffusion1DParameters
class SPECMICP_DLL_PUBLIC SaturatedDiffusion1D: public dfpmsolver::ParabolicProgram<SaturatedDiffusion1D>
{
public:
    //! \brief Build a 1D saturated diffusion system of equations
    //!
    //! \param the_mesh The mesh
    //! \param parameters Transport parameters
    //! \param list_bcs
    SaturatedDiffusion1D(
            mesh::Mesh1DPtr the_mesh,
            std::shared_ptr<SaturatedDiffusion1DParameters>  parameters,
            std::vector<index_t> list_bcs
            );

    //! \brief Return the number of equations
    index_t get_neq() const {return m_neq;}

    //! \brief Return the number of degrees of freedom per node
    index_t get_ndf() const {return 1;}

    //! \brief Return the total number of degrees of freedom
    index_t get_tot_ndf() const {return m_tot_ndf;}

    //! \brief Return the id of the equation corresponding to the degree of freedom 'id_dof'
    //!
    //! Return 'no_equation' if no equation exist
    index_t id_equation(index_t id_dof) const {return m_id_equations(id_dof);}

    //! \brief Compute the residuals for an element
    void element_residuals(index_t element,
                           const Vector& displacement,
                           const Vector& velocity,
                           Vector& element_residual
                           );

    //! \brief Compute the residuals
    void compute_residuals(const Vector& displacement,
                           const Vector& velocity,
                           Vector& residual
                           );

    //! \brief Compute the jacobian
    void compute_jacobian(Vector& displacement,
                          Vector& velocity,
                          Eigen::SparseMatrix<scalar_t>& jacobian,
                          scalar_t alphadt
                          );
    //! \brief Compute the element contribution to the jacobian
    void element_jacobian(
            index_t element,
            Vector& displacement,
            Vector& velocity,
            list_triplet_t& jacobian,
            scalar_t alphadt);

    //! \brief Update the solutions
    void update_solution(const Vector& update,
                         scalar_t lambda,
                         scalar_t alpha_dt,
                         Vector& predictor,
                         Vector& displacement,
                         Vector& velocity);

    //! \brief Apply boundary conditions to the velocity vector
    //!
    //! by default do nothing.
    void apply_bc(scalar_t dt,
                  const Vector& displacement,
                  Vector& velocity)
    {}

    //! \brief Return the value of the external flow for dof 'id_dof'
    scalar_t external_flow(index_t id_dof) const {return m_external_flow(id_dof);}
    //! \brief Return a reference to the value of the external flow for dof 'id_dof'
    scalar_t& external_flow(index_t id_dof) {return m_external_flow(id_dof);}
    //! \brief Return a reference to the vector of external flow
    Vector& external_flow() {return m_external_flow;}

private:
    void SPECMICP_DLL_LOCAL number_equations(std::vector<index_t> list_bcs);

    index_t m_tot_ndf;
    index_t m_neq;
    Eigen::VectorXi m_id_equations;

    mesh::Mesh1DPtr m_mesh;
    std::shared_ptr<SaturatedDiffusion1DParameters> m_param;

    Vector m_internal_flow;
    Vector m_external_flow;
};


} // end namespace dfpm

} // end namespace specmicp

#endif // SPECMICP_DFPM_1DTRANSPORT_DIFFUSION_HPP
