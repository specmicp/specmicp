/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DFPMSOLVER_PARABOLICPROGRAM_HPP
#define SPECMICP_DFPMSOLVER_PARABOLICPROGRAM_HPP

#include "specmicp_common/types.hpp"
#include "specmicp_common/eigen/incl_eigen_sparse_core.hpp"
#include <Eigen/Sparse>
#include "dfpm_program.hpp"

namespace specmicp {
namespace dfpmsolver {

//! \brief Concept class for a program
template <class Derived>
class ParabolicProgram: DFPMProgram<ParabolicProgram<Derived>>
{
public:
    //! \brief Return the derived class
    Derived* derived(
            ) {return static_cast<Derived*>(this);}

    //! \brief Compute the residuals
    void compute_residuals(const Vector& displacement,
                           const Vector& velocity,
                           Vector& residual
                           )
    {
        derived()->compute_residuals(displacement, velocity, residual);
    }
    //! \brief Compute the initial residuals
    //!
    //! By default, it calls the same residuals as compute_residuals
    void compute_residuals_0(const Vector& displacement,
                           const Vector& velocity,
                           Vector& residual
                           )
    {
        derived()->compute_residuals(displacement, velocity, residual);
    }
    //! \brief Compute the jacobian
    void compute_jacobian(Vector& displacement,
                          Vector& velocity,
                          Eigen::SparseMatrix<scalar_t>& jacobian,
                          scalar_t alphadt
                          )
    {
        derived()->compute_jacobian(displacement, velocity, jacobian, alphadt);
    }
    //! \brief Update the solutions
    void update_solution(const Vector& update,
                         scalar_t lambda,
                         scalar_t alpha_dt,
                         Vector& predictor,
                         Vector& displacement,
                         Vector& velocity)
    {
        derived()->update_solution(update, lambda, alpha_dt, predictor, displacement, velocity);
    }
    //! \brief Apply boundary conditions to the velocity vector
    //!
    //! by default do nothing.
    void apply_bc(scalar_t dt,
                  const Vector& displacement,
                  Vector& velocity)
    {}

};

} // end namespace dfpmsolver
} // end namespace specmicp

#endif // SPECMICP_DFPMSOLVER_PARABOLICPROGRAM_HPP
