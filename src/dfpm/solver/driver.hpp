/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DFPMSOLVER_DRIVER_HPP
#define SPECMICP_DFPMSOLVER_DRIVER_HPP

#include "specmicp_common/types.hpp"

namespace specmicp {
//! \namespace specmicp::dfpmsolver
//! \brief The DFPM solvers
//!
//! Contains the solvers to solve the DFPM programs
//!
//! \sa specmicp::dfpm
namespace dfpmsolver {

//! \brief Base class for a driver
//!
//! Options should be a subclass of DriverOptions
//! Performance should be a subclass of DriverPerformance
//!
template <class Program, class Options, class Performance>
class Driver
{
public:
    //! \brief Constructor with default options
    Driver(Program& the_program):
        m_program(the_program),
        m_options(),
        m_scaling_is_initialized(false)
    {
    }

    //! \brief Constructor with custom options
    Driver(Program& the_program,
            Options some_options):
        m_program(the_program),
        m_options(some_options),
        m_scaling_is_initialized(false)
    {
    }

    // basic program management
    // ------------------------

    //! Return the program
    Program& program() {return m_program;}

    //! Return the number of equations
    index_t get_neq() const {return m_program.get_neq();}

    // common process
    // --------------
    //! \brief rescale the update if needed
    //!
    //! Return the step length
    scalar_t is_step_too_long(Vector& update);


    // options
    // -------

    //! \brief Return a read/write reference to the options
    Options& get_options() {return m_options;}
    //! \brief Return a read-only reference to the options
    const Options& get_options() const {return m_options;}

    // performance
    // -----------

    //! \brief Return a const reference to the performance
    const Performance& get_perfs() const {return m_performance;}

    // Scaling
    // -------

    //! \brief (Re-)Initialize the scaling
    //!
    //! By default, scaling factor is 1
    void initialize_scaling() {
        if (not m_scaling_is_initialized)
        {
            m_scaling.resize(get_neq());
            m_scaling.setOnes();
        }
    }
    //! \brief Set a constant scaling
    void set_scaling(scalar_t scaling) {
        m_scaling.resize(get_neq());
        m_scaling.setConstant(scaling);
        m_scaling_is_initialized = true;
    }
    //! \brief Set scaling for every equations
    void set_scaling(const Vector& scale) {
        specmicp_assert(scale.rows() == get_neq());
        m_scaling = scale;
        m_scaling_is_initialized = true;
    }

    //! \brief Return a const reference to the vector of scaling factors
    const Vector& scaling() const {return m_scaling;}
    //! \brief Return the scaling factor for an equation
    scalar_t scaling(index_t id_eq) const {return m_scaling(id_eq);}


protected:
    //! \brief Read/write access to the performance
    Performance& get_perfs() {return m_performance;}

private:
    Program& m_program;        //!< The program
    Performance m_performance; //!< The performance
    Options m_options;         //!< The options

    Vector m_scaling;            //!< Scaling factor
    bool m_scaling_is_initialized;
};

} // end namespace dfpmsolver
} // end namespace specmicp

// implementation
// ==============

#include "driver.inl"

#endif // SPECMICP_DFPMSOLVER_DRIVER_HPP
