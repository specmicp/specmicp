/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "configuration.hpp"

#include "specmicp_common/io/safe_config.hpp"
#include "specmicp_common/io/config_yaml_sections.h"

namespace specmicp {
namespace io {

units::UnitsSet configure_units(YAMLConfigHandle&& units_conf)
{
    units::UnitsSet the_units;
    the_units.length   = configure_length_unit(units_conf);
    the_units.mass     = configure_mass_unit(units_conf);
    the_units.quantity = configure_quantity_unit(units_conf);
    return the_units;
}

units::LengthUnit configure_length_unit(YAMLConfigHandle& units_conf)
{
    // acceptable values
    static const char* centimeter[] {"centimeter", "cm"};
    static const char* decimeter[]  {"decimeter" , "dm"};
    static const char* meter[]      {"meter"     ,  "m", "default"};

    units::LengthUnit length = units::LengthUnit::meter;
    //
    //  Note : goto is used in this function to bypass tests once a solution
    //   has been found.
    //  An error is raised if no acceptable solution can be found.
    //
    if (not units_conf.has_attribute(SPC_CF_S_UNITS_A_LENGTH))
    {
        // if no configuration exist we just exit
        goto exit;
    }
    else
    {
        // conf
        //    => test all possible values
        auto conf_value = units_conf.get_attribute<std::string>(SPC_CF_S_UNITS_A_LENGTH);
        for (auto test: centimeter)
        {
            if (conf_value == test)
            {
                length = units::LengthUnit::centimeter;
                goto exit;
            }
        }
        for (auto test: decimeter)
        {
            if (conf_value == test)
            {
                length = units::LengthUnit::decimeter;
                goto exit;
            }
        }
        for (auto test: meter)
        {
            if (conf_value == test)
            {
                length = units::LengthUnit::meter;
                goto exit;
            }
        }
        // throw an error if no solution has been found
        units_conf.report_error(YAMLConfigError::InvalidArgument,
                                "Unknown mass unit : '"+conf_value+"'.\n" +
                                "Valid options : "
                                    "'centimeter'"
                                    ", 'decimeter'"
                                    ", 'meter'."
                                );
    }
    exit:
    return length;
}

units::MassUnit configure_mass_unit(YAMLConfigHandle& units_conf)
{
    // acceptable values
    static const char* kilogram[] {"kilogram", "kg", "default"};
    static const char*     gram[] {    "gram" , "g"};

    units::MassUnit mass = units::MassUnit::kilogram;
    // for explanation about the goto, see the configure_length_unit function

    // no conf
    if (not units_conf.has_attribute(SPC_CF_S_UNITS_A_MASS))
    {
        goto exit;
    }
    // conf
    //    => test all possible values
    else
    {
        auto conf_value = units_conf.get_attribute<std::string>(SPC_CF_S_UNITS_A_MASS);
        for (auto test: kilogram)
        {
            if (conf_value == test)
            {
                mass = units::MassUnit::kilogram;
                goto exit;
            }
        }
        for (auto test: gram)
        {
            if (conf_value == test)
            {
                mass = units::MassUnit::gram;
                goto exit;
            }
        }
        // throw an error
        units_conf.report_error(YAMLConfigError::InvalidArgument,
                                "Unknown length unit : '" + conf_value + "'.\n" +
                                "Valid options :"
                                    "'kilogram'"
                                    "', gram'."
                                );
    }
    exit:
    return mass;
}

units::QuantityUnit configure_quantity_unit(YAMLConfigHandle& units_conf)
{
    // acceptable values
    static const char*      moles[] {     "moles", "mol", "default"};
    static const char* millimoles[] {"millimoles", "mmol"};

    units::QuantityUnit qty = units::QuantityUnit::moles;
    // for explanation about the goto, see the configure_length_unit function

    // no conf
    if (not units_conf.has_attribute(SPC_CF_S_UNITS_A_QUANTITY))
    {
        goto exit;
    }
    // conf
    //    => test all possible values
    else
    {
        auto conf_value = units_conf.get_attribute<std::string>(SPC_CF_S_UNITS_A_QUANTITY);
        for (auto test: moles)
        {
            if (conf_value == test)
            {
                qty = units::QuantityUnit::moles;
                goto exit;
            }
        }
        for (auto test: millimoles)
        {
            if (conf_value == test)
            {
                qty = units::QuantityUnit::millimoles;
                goto exit;
            }
        }
        // throw an error
        units_conf.report_error(YAMLConfigError::InvalidArgument,
                                "Unknown quantity unit : '" + conf_value + "'.\n" +
                                "Valid options :"
                                    "'moles'"
                                    "', millimoles'."
                                );
    }
    exit:
    return qty;
}
} //end namespace io
} //end namespace specmicp
