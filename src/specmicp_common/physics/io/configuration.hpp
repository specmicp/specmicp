/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_PHYSICS_CONFIGURATION_HPP
#define SPECMICP_PHYSICS_CONFIGURATION_HPP

//! \file physics/io/configuration.hpp
//! \brief YAML configuration parser for units

#include "specmicp_common/types.hpp"
#include "specmicp_common/physics/units.hpp"

namespace specmicp {
namespace io {

class YAMLConfigHandle;

/*! \brief Parse a YAML units section

YAML example :
\code
units:
    length: centimeter # or cm
    mass: kilogram # or g
\endcode
 */
units::UnitsSet   SPECMICP_DLL_PUBLIC configure_units(
        YAMLConfigHandle&& units_conf
        );
//! \brief Parse the length unit
units::LengthUnit SPECMICP_DLL_PUBLIC configure_length_unit(
        YAMLConfigHandle& units_conf
        );
//! \brief Parse the mass unit
units::MassUnit   SPECMICP_DLL_PUBLIC configure_mass_unit(
        YAMLConfigHandle& units_conf
        );
//! \brief Parse the mass unit
units::QuantityUnit   SPECMICP_DLL_PUBLIC configure_quantity_unit(
        YAMLConfigHandle& units_conf
        );

} //end namespace io
} //end namespace specmicp

#endif // SPECMICP_PHYSICS_CONFIGURATION_HPP
