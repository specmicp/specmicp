/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "units.hpp"

#include "specmicp_common/physics/units.hpp"

namespace specmicp {
namespace io {


std::string to_string(units::LengthUnit length_u)
{
    std::string ret;
    switch(length_u) {
    case units::LengthUnit::meter:
        ret = "meter";
        break;
    case units::LengthUnit::decimeter:
        ret = "decimeter";
        break;
    case units::LengthUnit::centimeter:
        ret= "centimeter";
        break;
    }
    return ret;
}

std::string to_string(units::MassUnit mass_u)
{
    std::string ret;
    switch(mass_u) {
    case units::MassUnit::kilogram:
        ret = "kilogram";
        break;
    case units::MassUnit::gram:
        ret = "gram";
        break;
    }
    return ret;
}

std::string to_string(units::QuantityUnit qty_u)
{
    std::string ret;
    switch(qty_u) {
    case units::QuantityUnit::moles:
        ret = "moles";
        break;
    case units::QuantityUnit::millimoles:
        ret = "millimoles";
        break;
    }
    return ret;
}

//! \brief Transform a length unit into a string
std::string length_unit_to_string(units::LengthUnit length_u)
{
    std::string str;
    str.reserve(2);
    switch (length_u) {
    case units::LengthUnit::meter:
        str = "m";
        break;
    case units::LengthUnit::decimeter:
        str = "dm";
        break;
    case units::LengthUnit::centimeter:
        str = "cm";
        break;
    }
    return str;
}

//! \brief Return a string describing the surface unit
std::string surface_unit_to_string(units::LengthUnit length_u)
{
    return length_unit_to_string(length_u) + "^2";
}


//! \brief Return a string describing the volume unit
std::string volume_unit_to_string(units::LengthUnit length_u)
{
    return length_unit_to_string(length_u) + "^3";
}


} //end namespace io
} //end namespace specmicp
