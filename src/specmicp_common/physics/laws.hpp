/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_LAWS_LAWS_HPP
#define SPECMICP_LAWS_LAWS_HPP

//! \file laws.hpp
//! \brief Simple physical laws

#include "../types.hpp"
#include "constants.hpp"

namespace specmicp {

namespace units {
enum class LengthUnit;
enum class MassUnit;

struct UnitsSet;
} //end namespace units

//! \namespace specmicp::laws
//! \brief Physical laws
namespace laws {

//! \brief Return the pressure of a perfect gas
//!
//! \param n the number of moles (mol)
//! \param V the volume (m^3)
//! \param T the temperature (K)
inline constexpr scalar_t pressure_perfect_gas(scalar_t n, scalar_t V, scalar_t T=298.15)
{
    return n*constants::gas_constant*T/V;
}

//! \brief Return the number of moles for a perfect gas
//!
//! \param P the pressure (Pa)
//! \param V the volume (m^3)
//! \param T the temperature (K)
inline constexpr scalar_t mole_perfect_gas(scalar_t P, scalar_t V, scalar_t T=298.15)
{
    return P*V/(constants::gas_constant*T);
}

//! \brief The Debye-Huckel law
//!
//! at 25°C
//!
//! \f[ \log \gamma_k = - \frac{A z_k^2 \sqrt{I}}{1 + a B \sqrt{I}} \f]
//!
//! \param sqrtI square root of the ionic strength
//! \param zi Charge of the ion
//! \param ao ion size parameter
//!
scalar_t
SPECMICP_DLL_PUBLIC SPECMICP_PURE_F
debye_huckel(scalar_t sqrtI, scalar_t zi, scalar_t ao);


//! \brief The Bdot law
//!
//! at 25°C
//!
//! \f[ \log \gamma_k = - \frac{A z_k^2 \sqrt{I}}{1 + a B \sqrt{I}} + \dot{B} I  \f]
//!
//!
//! \param I square ionic strength
//! \param zi Charge of the ion
//! \param ao ion size parameter
//! \param bdot 'bdot' parameter
//!
scalar_t
SPECMICP_DLL_PUBLIC SPECMICP_PURE_F
extended_debye_huckel(scalar_t I, scalar_t zi, scalar_t ao, scalar_t bdot);

//! \brief The Bdot law
//!
//! at 25°C
//!
//! This version accepts the value of \f$\sqrt{I}\f$.
//!
//! \f[ \log \gamma_k = - \frac{A z_k^2 \sqrt{I}}{1 + a B \sqrt{I}}  + \dot{B} I \f]
//!
//! \param I square ionic strength
//! \param sqrtI square root of the ionic strength
//! \param zi Charge of the ion
//! \param ao ion size parameter
//! \param bdot 'bdot' parameter
//!
scalar_t
SPECMICP_DLL_PUBLIC SPECMICP_PURE_F
extended_debye_huckel(scalar_t I, scalar_t sqrtI, scalar_t zi, scalar_t ao, scalar_t bdot);

//! \brief Return the density of water in the correct units
//!
//! \param temperature the temperature
//! \param length_unit the lenght unit
//! \param mass_unit the mass unit
scalar_t
SPECMICP_DLL_PUBLIC SPECMICP_PURE_F
density_water(scalar_t temperature, units::LengthUnit length_unit, units::MassUnit mass_unit);
//! \brief Return the density of water in the correct units at 25C
//!
//! \param units_set the units set
scalar_t
SPECMICP_DLL_PUBLIC SPECMICP_PURE_F
density_water(units::UnitsSet units_set);

} // end namespace laws
} // end namespace specmicp

#endif // SPECMICP_LAWS_LAWS_HPP
