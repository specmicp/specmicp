/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_CONSTANTS_CONSTANTS_HPP
#define SPECMICP_CONSTANTS_CONSTANTS_HPP

//! \file constants.hpp
//! \brief  Contains physical constants

namespace specmicp {
//! \namespace specmicp::constants
//! \brief Constants used by SpecMiCP
namespace constants {

constexpr double gas_constant = 8.3144621; //!< Perfect gas constant (J / K mol)

constexpr double water_density_25 = 997.05; //!< Density of water (kg / m^3)
constexpr double water_density_25_kgl = 0.99705; //!< Density of water (kg / L)

constexpr double water_viscosity =  8.937e-4; //!< Viscosity water (Pa.s)

constexpr double water_molar_mass = 18.01528e-3; //!< Molar mass water (kg / mol)

constexpr double Adebye = 0.5092; //!< constant 'A' for Debye-Huckel activity coefficient
constexpr double Bdebye = 0.3283; //!< constant 'B' for Debye-Huckel activity coefficient

constexpr double faraday_constant = 9.6485339924e4 ; //!< the Faraday constant (C / mol)

} // end namespace constants
} // end namespace specmicp

#endif // SPECMICP_CONSTANTS_CONSTANTS_HPP
