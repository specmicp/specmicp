/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_INCLEIGENCORE_HPP
#define SPECMICP_INCLEIGENCORE_HPP

//! \file eigen/incl_eigen_core.hpp
//! \brief Include Eigen/Core with correct options

//! \def EIGEN_DEFAULT_DENSE_INDEX_TYPE
//! \brief The default type for an index in a dense matrix
//!
//! This may be redefined to be consistent.
//! For more information see the Eigen documentation.

#ifndef EIGEN_DEFAULT_DENSE_INDEX_TYPE
#define EIGEN_DEFAULT_DENSE_INDEX_TYPE specmicp::index_t
#endif

#include "specmicp_common/config.h"
// If asked by the user, eigen can use lapacke functions
// for dense matrix multiplications
#ifdef SPECMICP_USE_BLAS
    #define EIGEN_USE_BLAS
    #define EIGEN_USE_LAPACKE_STRICT
#endif

// avoid error due to binder1st and binder2nd in eigen...
#if (defined __GNUC__) && (__GNUC__ >= 5)
    #pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#endif
#if (defined __GNUC__) && (__GNUC__ >= 7)
    #pragma GCC diagnostic ignored "-Wint-in-bool-context"
#endif
#include <Eigen/Core>
#if (defined __GNUC__) && (__GNUC__ >= 5)
    #pragma GCC diagnostic warning "-Wdeprecated-declarations"
#endif
#if (defined __GNUC__) && (__GNUC__ >= 7)
    #pragma GCC diagnostic warning "-Wint-in-bool-context"
#endif

#endif // SPECMICP_INCLEIGENCORE_HPP
