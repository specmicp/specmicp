/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_IO_YAML_HPP
#define SPECMICP_IO_YAML_HPP

#include "specmicp_common/types.hpp"
#include <string>
#include <memory>

#include <yaml-cpp/yaml.h>

//! \file io/yaml.hpp
//! \brief YAML parsing and emitting
//!
//! \deprecated the wrappers defined in 'safe_config.hpp' should be prefered instead

namespace specmicp {
namespace io {

//! \brief Configure the yaml emitter
void configure_yaml_emitter(YAML::Emitter& emitter);

//! \brief Parse a yaml formatted stream
//!
//! \param input stream to parse
YAML::Node parse_yaml(std::istream& input);
//! \brief Parse a yaml formatted file
//!
//! \param filepath path to the yaml file
YAML::Node parse_yaml_file(const std::string& filepath);
//! \brief Parse a yaml formatted string
//!
//! \param config_string string in yaml format
YAML::Node parse_yaml_string(const std::string& config_string);

//! \brief Save a YAML emitter into a file
//!
//! \param filepath Path to the file where the YAML tree will be saved
//! \param yaml_tree YAML emitter
void save_yaml(const std::string& filepath, YAML::Emitter& yaml_tree);

//! \brief Check that node[child] exist, if it doesn't throws an exception
//!
//! \param node Yaml node
//! \param child the named attribute to obtain
//! \param section the name of the current node
void check_mandatory_yaml_node(const YAML::Node& node, const std::string& child, const std::string& section);

//! \brief Return node[attribute]
//!
//! This version do not check if the attribute exists
//!
//! \tparam T type of the attribute
//! \param node Yaml node
//! \param attribute the named attribute to obtain
//! \param section the name of the current node
template <typename T>
T get_yaml_attribute(const YAML::Node& node, const std::string& attribute, const std::string& section)
{
    T value;
    try
    {
        value = node[attribute].as<T>();
    }
    catch (YAML::BadConversion)
    {
        throw std::invalid_argument("Error while reading attribute '"+attribute+"' in section '"+section+"'.");
    }
    return value;
}

// Specialization for scalar
template <>
scalar_t get_yaml_attribute(const YAML::Node& node, const std::string& attribute, const std::string& section);
// Specialization for integer
template <>
index_t get_yaml_attribute(const YAML::Node& node, const std::string& attribute, const std::string& section);
// Specialization for string
template <>
std::string get_yaml_attribute(const YAML::Node& node, const std::string& attribute, const std::string& section);
// Specialization for booleans
template <>
bool get_yaml_attribute(const YAML::Node& node, const std::string& attribute, const std::string& section);


//! \brief Return node[attribute] if it exist else return default_value
//!
//! \tparam T type of the attribute
//! \param node Yaml node
//! \param attribute the named attribute to obtain
//! \param section the name of the current node
//! \param default_value value used if it doesn't exist in the database
template <typename T>
T get_yaml_optional(const YAML::Node& node, const std::string& attribute, const std::string& section, T default_value)
{
    if (node[attribute])
        return get_yaml_attribute<T>(node, attribute, section);
    else
        return default_value;
}

//! \brief Return node[attribute], if it doesn't exist throws an exception
//!
//! \tparam T type of the attribute
//! \param node Yaml node
//! \param attribute the named attribute to obtain
//! \param section the name of the current node
template <typename T>
T get_yaml_mandatory(const YAML::Node& node, const std::string& attribute, const std::string& section)
{
    if (not node[attribute])
        throw std::invalid_argument("Attribute '"+attribute+"' is required in section '"+section+"'.");
    return get_yaml_attribute<T>(node, attribute, section);
}

//! \brief Read-Only rich yaml node
//!
//! Manage section name automaticaly
class SPECMICP_DLL_PUBLIC RORichYAMLNode
{
public:
    //! \brief Parse YAML from a input stream
    static RORichYAMLNode parse_yaml(
            std::istream& input,
            std::string name="__main__"
    ) {
        return RORichYAMLNode(YAML::Load(input), name);
    }

    //! \brief Parse YAML from a file
    static RORichYAMLNode parse_yaml(
            const std::string& filepath
    ) {
        return RORichYAMLNode(YAML::LoadFile(filepath), filepath);
    }

    //! \brief Parse YAML from a string
    static RORichYAMLNode parse_yaml_string(
            const std::string& config_string,
            std::string name="__main__"

    ) {
        return RORichYAMLNode(YAML::Load(config_string), name);
    }


    //! \brief Return the raw YAML::Node
    //!
    //! Use carefully !
    const YAML::Node& get_raw_node() const {return m_node;}

    //! \brief return true if the child exist
    bool check(const std::string& child_name) const {
        return m_node[child_name];
    }

    //! \brief Check that the mandatory child exist
    void check_mandatory(const std::string& child_name) const {
        check_mandatory_yaml_node(m_node, child_name, m_name);
    }
    //! \brief Return a mandatory value
    template <typename T>
    T get(const std::string& child_name) const
    {
        return get_yaml_attribute<T>(m_node, child_name, m_name);
    }

    //! \brief Return a mandatory value
    template <typename T>
    T get_mandatory(const std::string& child_name) const
    {
        return get_yaml_mandatory<T>(m_node, child_name, m_name);
    }

    //! \brief Return an optional value
    template <typename T>
    T get_optional(const std::string& child_name, const T& default_value) const
    {
        return get_yaml_optional<T>(m_node, child_name, m_name, default_value);
    }

    //! \brief Return a child
    RORichYAMLNode operator[] (const std::string& child_name);

    //! \brief Return a child
    const RORichYAMLNode operator[] (const std::string& child_name) const;

private:

    RORichYAMLNode(const YAML::Node& node, const std::string& name):
        m_node(node), m_name(name)
    {}

    YAML::Node m_node;
    std::string m_name;
};

} //end namespace io
} //end namespace specmicp

#endif // SPECMICP_IO_YAML_HPP
