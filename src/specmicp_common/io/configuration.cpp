/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "configuration.hpp"
#include "safe_config.hpp"

#include "specmicp_common/log.hpp"

#include "specmicp_common/plugins/plugin_manager.hpp"
#include "specmicp_common/io/config_yaml_sections.h"

#include "specmicp_common/compat.hpp"
#include "specmicp_common/filesystem.hpp"
#include "specmicp_common/dateandtime.hpp"

#include <stdexcept>
#include <iostream>
#include <fstream>

namespace specmicp {
namespace io {


void configure_plugin_manager(
        YAMLConfigHandle&& config
        )
{
    plugins::PluginManager& manager = plugins::get_plugin_manager();

    if (config.has_node(SPC_CF_S_PLUGINS)) {
        manager.add_plugin_directories(
                    config.list_to_vector<std::string>(SPC_CF_S_PLUGINS_A_SEARCHDIRS));
    }
    if (config.has_node(SPC_CF_S_PLUGINS_A_TOLOAD))
    {
        std::vector<std::string> to_load =
                config.list_to_vector<std::string>(SPC_CF_S_PLUGINS_A_TOLOAD);
        for (auto plugin: to_load) {
            auto done = manager.load_plugin(plugin);
            if (done)
                SPC_CONF_LOG << "Loaded plugin : "  << plugin;
            else
                SPC_CONF_LOG_ERROR << "Error while loading plugin : " << plugin;
        }
    }
}


static logger::LogLevel configure_log_level(
        YAMLConfigHandle& conf_log
        )
{
    logger::LogLevel level = logger::LogLevel::Warning;
    if (not conf_log.has_attribute(SPC_CF_S_LOGS_A_LEVEL)) {
        return level;
    }
    auto expr = conf_log.get_attribute<std::string>(SPC_CF_S_LOGS_A_LEVEL);
    if (expr == SPC_CF_S_LOGS_V_CRITICAL) {
        level = logger::LogLevel::Critical;
    } else if (expr == SPC_CF_S_LOGS_V_ERROR) {
        level = logger::LogLevel::Error;
    } else if (expr == SPC_CF_S_LOGS_V_WARNING) {
        level = logger::LogLevel::Warning;
    } else if (expr == SPC_CF_S_LOGS_V_DEBUG) {
        level = logger::LogLevel::Debug;
    } else {
        conf_log.report_error(
                    YAMLConfigError::InvalidArgument,
                    "'" + expr + "' is not a valid value for '"
                    SPC_CF_S_LOGS_A_LEVEL
                    "'. Accepted values are : '"
                    SPC_CF_S_LOGS_V_CRITICAL "', '"
                    SPC_CF_S_LOGS_V_ERROR    "', '"
                    SPC_CF_S_LOGS_V_WARNING  "', and '"
                    SPC_CF_S_LOGS_V_DEBUG    "'."
                    );
    }
    return level;
}

std::unique_ptr<std::ostream> configure_log(
        YAMLConfigHandle&& conf_log,
        AllIOFiles* all_io_file
        )
{

    logger::LogLevel level = configure_log_level(conf_log);
    std::unique_ptr<std::ostream> output = nullptr;
    auto out = conf_log.get_required_attribute<std::string>(SPC_CF_S_LOGS_A_OUTPUT);
    if (out == SPC_CF_S_LOGS_V_COUT) {
        init_logger(&std::cout, level);
    } else if (out == SPC_CF_S_LOGS_V_CERR) {
        init_logger(&std::cerr, level);
    } else if (out == SPC_CF_S_LOGS_V_FILE) {
        std::string working_dir {""};

        if (all_io_file != nullptr) {
            working_dir = all_io_file->get_working_dir();
        }

        auto filepath = conf_log.get_required_attribute<std::string>(SPC_CF_S_LOGS_A_FILEPATH);
        if (not working_dir.empty() and
            not utils::is_path_absolute(filepath)) {
            filepath = utils::complete_path(working_dir, filepath);
        }
        if (all_io_file != nullptr) {
            all_io_file->add_log_file(
                        io::output_file("Log", filepath,
                        "level : logger::to_string(level)"
                                 ));
        }
        output = make_unique<std::ofstream>(filepath,
                                            std::ios_base::trunc | std::ios_base::out
                                            );
        init_logger(output.get(), level);
    } else {
        conf_log.report_error(
                    YAMLConfigError::InvalidArgument,
                    "'" + out + "' is not a valid value for '"
                    SPC_CF_S_LOGS_A_OUTPUT
                    "'. Accepted values are  : '"
                    SPC_CF_S_LOGS_V_COUT "', '"
                    SPC_CF_S_LOGS_V_CERR "', and '"
                    SPC_CF_S_LOGS_V_FILE "'."
                    );
    }
    return output;
}


std::unique_ptr<std::ostream> configure_conf_log(
        YAMLConfigHandle&& conf_log,
        AllIOFiles* all_io_file
        )
{
    std::unique_ptr<std::ostream> output = nullptr;
    auto out = conf_log.get_required_attribute<std::string>(SPC_CF_S_CONF_LOGS_A_OUTPUT);
    if (out == SPC_CF_S_LOGS_V_COUT) {
        init_conf_logger(&std::cout);
    } else if (out == SPC_CF_S_LOGS_V_CERR) {
        init_conf_logger(&std::cerr);
    } else if (out == SPC_CF_S_LOGS_V_FILE) {
        std::string working_dir {""};
        if (all_io_file != nullptr) {
            working_dir = all_io_file->get_working_dir();
        }
        auto filepath = conf_log.get_required_attribute<std::string>(SPC_CF_S_LOGS_A_FILEPATH);
        if (not working_dir.empty() and
            not utils::is_path_absolute(filepath)) {
            filepath = utils::complete_path(working_dir, filepath);
        }
        if (all_io_file != nullptr) {
            all_io_file->add_log_file(
                        io::output_file("Config log", filepath,
                        "level : logger::to_string(level)"
                         ));
        }
        output = make_unique<std::ofstream>(filepath,
                                            std::ios_base::trunc | std::ios_base::out
                                            );
        init_conf_logger(output.get());
    } else {
        conf_log.report_error(
                    YAMLConfigError::InvalidArgument,
                    "'" + out + "' is not a valid value for '"
                    SPC_CF_S_CONF_LOGS_A_OUTPUT
                    "'. Accepted values are  : '"
                    SPC_CF_S_LOGS_V_COUT "', '"
                    SPC_CF_S_LOGS_V_CERR "', and '"
                    SPC_CF_S_LOGS_V_FILE "'."
                    );
    }
    SPC_CONF_LOG << SPC_CONF_LOG_HLINE
                 << "\nonfiguration logger initialized\n    "
                 << dateandtime::now_localized()
                 << "\n" SPC_CONF_LOG_HLINE;
    return output;
}

} // end namespace io
} // end namespace specmicp
