/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_CONFIGURATION_HPP
#define SPECMICP_UTILS_CONFIGURATION_HPP

//! \file specmicp_common/io/configuration.hpp
//! \brief yaml configuration for the specmicp_common module

#include "specmicp_common/macros.hpp"
#include "specmicp_common/io/all_io_files.hpp"
#include <memory>
#include <iosfwd>

namespace specmicp {
namespace io {

class YAMLConfigHandle;

//! \brief Configure the plugin manager
void SPECMICP_DLL_PUBLIC configure_plugin_manager(YAMLConfigHandle&& config);

//! \brief Configure the logger
//!
//! \param log configuration section
//! \param all_io_files pointer to IO files logger
//!
//! \return a unique pointer to the logger file, may return a nullptr if the
//!  logger output to standard output (cout) or standard error output (cerr)
std::unique_ptr<std::ostream> SPECMICP_DLL_PUBLIC configure_log(
        YAMLConfigHandle&& log,
        AllIOFiles* all_io_files=nullptr
        //std::string working_dir=""
        );

//! \brief Configure the configuration logger
//!
//! \param log conmfiguration section
//! \param all_io_files pointer to IO files logger
//!
//! \return a unique pointer to the logger file, may return a nullptr if the
//!  logger output to standard output (cout) or standard error output (cerr)
std::unique_ptr<std::ostream> SPECMICP_DLL_PUBLIC configure_conf_log(
        YAMLConfigHandle&& log,
        AllIOFiles* all_io_files=nullptr
        //std::string working_dir=""
        );


} // end namespace io
} // end namespace specmicp

#endif // SPECMICP_UTILS_CONFIGURATION_HPP
