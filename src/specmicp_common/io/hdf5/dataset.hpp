/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_IO_HDF5_DATASET_HPP
#define SPECMICP_IO_HDF5_DATASET_HPP

//! \file hdf5/dataset.hpp
//! \brief H5D wrapper

#include "path.hpp"
#include "attribute.hpp" // for template

//! \file hdf5/dataset.hpp
//! \brief an HDF5 dataset

namespace specmicp {
namespace io {
namespace hdf5 {

class Dataspace;
class Attribute;

//! \brief A HDF5 dataset
class Dataset:
        public Path
{
public:
    //! \brief Move constructor
    Dataset(Dataset&& other) = default;
    //! \brief Destructor, close the dataset
    ~Dataset();

    //! \brief Get ownership of a dataset
    static Dataset acquire(hid_t id, const std::string& path);

    //! \brief Return the dataspace of the dataset
    Dataspace get_dataspace() const;

protected:
    //! \brief Create a dataset wrapper
    Dataset(hid_t id, const std::string& path);

private:
    Dataset(const Dataset& other) = delete;
    Dataset& operator=(const Dataset& other) = delete;

    friend class GroupPath;
};



} // end namespace hdf5
} // end namespace io
} // end namespace specmicp

#endif // SPECMICP_IO_HDF5_DATASET_HPP
