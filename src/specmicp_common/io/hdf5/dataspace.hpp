/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_IO_HDF5_DATASPACE_HPP
#define SPECMICP_IO_HDF5_DATASPACE_HPP

//! \file hdf5/dataspace.hpp
//! \brief H5S wrapper

#include "id_class.hpp"
#include <array>

namespace specmicp {
namespace io {
namespace hdf5 {

//! \brief Wrapper for an HDF5 dataspace
class Dataspace:
        public IdClass
{
public:
    //! \brief Move constructor
    Dataspace(Dataspace&& other) = default;

    //! \brief Acquire ownership of a ressource
    static Dataspace acquire(hid_t id);

    //! \brief Create a new dataspace
    static Dataspace create_simple(int rank, hsize_t dims[]);

    ~Dataspace();

    //! \brief Return the rank of the dataspace
    //!
    //! The rank is the number of dimensions
    int get_rank() const;

    //! \brief Get the dimensions of the dataspace
    //!
    //! \param dims an array to story the siz eof each dimension
    //! \return the number of dimensions
    int get_dimensions(hsize_t dims[]) const;

    //! \brief Get the dimensions of the dataspace
    //!
    //! \tparam N the (or the max.) number of dimensions
    //! \param dims an array to story the siz eof each dimension
    //! \return the number of dimensions
    template <std::size_t N>
    int get_dimensions(std::array<hsize_t, N>& dims) const;

protected:
    //! \brief Create a dataspace wrapper
    Dataspace(hid_t id);

private:
    Dataspace(const Dataspace& other) = delete;
    Dataspace& operator=(const Dataspace& other) = delete;
};


template <std::size_t N>
int Dataspace::get_dimensions(std::array<hsize_t, N>& dims) const
{
    return get_dimensions(dims.data());
}


} // end namespace hdf5
} // end namespace io
} // end namespace specmicp

#endif // SPECMICP_IO_HDF5_DATASPACE_HPP
