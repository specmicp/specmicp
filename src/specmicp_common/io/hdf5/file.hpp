/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_IO_HDF5_FILE_HPP
#define SPECMICP_IO_HDF5_FILE_HPP

#include "path.hpp"

//! \file hdf5/file.hpp
//! \brief H5F wrapper

namespace specmicp {
namespace io {
//! \namespace specmicp::io::hdf5
//! \brief Wrapper over the HDF5 C API
//!
//! HDF5 C++ API is a bitch.
//! This give a wrapper over the better defined, more availaible C API
//! It is designed as a transparent wrapper. It provides convenient classes
//! and functions, but the user can still access and use the low-level
//! functions if needed.
namespace hdf5 {

//! \brief Mode to open/create a file
enum class OpenMode
{
    CreateTruncate,    //!< Create a file, truncate if it exists
    CreateFailIfExist, //!< Create a file, fail if it exists
    OpenReadOnly,      //!< Open a file, read only mode
    OpenReadWrite      //!< Open a file, read write mode
};

//! \brief A HDF5 file
//!
//! There is two methods to create a file, acquiring an already opened file
class File:
        public GroupPath // that's where the interesting stuff is
{
public:
    //! \brief Get ownership of an HDF5 file already opened
    static File acquire(const std::string& filename);
    //! \brief Open a HDF5 path
    static File open(const std::string& filename, OpenMode mode);

    //! \brief move constructor
    File(File&& other) = default;

    //! \brief Destructor, close the file
    ~File();

private:
    //! Create a wrapper for an HDF5 wrapper
    File(hid_t id, const std::string& filename);
    // no copy, no assignement
    File(const File& other) = delete;
    File& operator=(const File& other) = delete;
};


} // end namespace hdf5
} // end namespace io
} // end namespace specmicp

#endif // SPECMICP_IO_HDF5_FILE_HPP
