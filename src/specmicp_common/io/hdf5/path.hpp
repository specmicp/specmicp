/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_IO_HDF5_PATH_HPP
#define SPECMICP_IO_HDF5_PATH_HPP

#include "id_class.hpp"

#include "H5Lpublic.h"
#include "specmicp_common/types.hpp"

#include "attribute.hpp" // for template

#include <string>
#include <vector>

#include <array>

namespace specmicp {
namespace io {
namespace hdf5 {

class Group;
class Dataset;
class Attribute;
class Dataspace;


//! \brief A basic location, have an id and path
class Path:
        public IdClass
{
public:
    //! \brief Move constructor
    Path(Path&& other) = default;


    //! \brief Return the path.
    std::string get_path() const {return m_path;}

    //! \brief Return a subpath
    std::string add_to_path(const std::string& to_add) const;

    //! \brief create an attribute of scalar type
    Attribute create_scalar_attribute(
            const std::string& name,
            const Dataspace& d_space
            );
    //! \brief Create an attribute
    //!
    //! The attribute will be a Nx1 array of doubles
    template <std::size_t N>
    Attribute create_scalar_attribute(
        const std::string& name,
        const std::array<double, N>& values
    );
    //! \brief Create an attribute
    //!
    //! The attribute will be a Nx1 array of doubles
    Attribute create_scalar_attribute(
        const std::string& name,
        std::size_t size,
        const double values[]
    );
    //! \brief Open an attribute
    Attribute open_attribute(const std::string& name) const;
    //! \brief Read an attribute
    template <std::size_t N>
    std::array<double, N> read_scalar_attribute(
            const std::string& name
    ) const;
    //! \brief Read an attribute
    void read_scalar_attribute(
        const std::string& name,
        std::size_t size,
        double values[]
    ) const;
    //! \brief Read an attribute with an unknown size
    std::vector<double> read_scalar_attribute(const std::string& name);

protected:
    //! \brief Constructor for a HDF5 stuff with a path
    Path(hid_t id, const std::string& path):
        IdClass(id),
        m_path(path)
    {}

    //! \brief Set the location path3
    void set_path(const std::string& path) {m_path = path;}

private:
    std::string m_path; //!< The path

    Path(const Path& other) = delete;
    Path& operator=(const Path& other) = delete;
};

//! \brief Signature of a filter when iterating over elements
using iterate_function_t = herr_t (*)(hid_t, const char*, const H5L_info_t*, void*);

//! \brief A group
//!
//! similar to H5::CommonFG
class GroupPath:
        public Path
{
public:
    //! \brief Move constructor
    GroupPath(GroupPath&& other) = default;
    //! \brief Open a group
    Group open_group(const std::string& name) const;
    //! \brief Create a group
    Group create_group(const std::string& name) const;
    //! \brief Open a dataset
    Dataset open_dataset(const std::string& name) const;
    //! \brief Create a dataset from an eigen vector
    Dataset create_vector_dataset(
            const std::string& name,
            const Eigen::Ref<const Vector>& data
            );
    //! \brief Create a dataset from a vector of strings
    Dataset create_string_dataset(
            const std::string& name,
            const std::vector<std::string>& data
            );
    //! \brief Read a dataset and transfer its data to an eigen vector
    Vector read_vector_dataset(const std::string& path) const;

    //! \brief Read a dataset containing strings
    std::vector<std::string> read_string_dataset(const std::string& name);

    //! \brief Check if 'name' exist in this group
    bool has_link(const std::string& name) const;

    //! \brief Iterate over the links of the group
    herr_t iterate_over_elements(
            iterate_function_t func,
            void* extra_data=nullptr
            );

    //! \brief Return the number fo links in a group
    hsize_t get_number_links();

protected:
    //! \brief Protected constructor
    //!
    //! Created by file, or other group
    GroupPath(hid_t id, const std::string& path):
        Path(id, path)
    {}

private:
    // no copy no assignement
    GroupPath(const GroupPath& other) = delete;
    GroupPath& operator=(const GroupPath& other) = delete;

};

// implementation
// ==============

template <std::size_t N>
Attribute Path::create_scalar_attribute(
    const std::string& name,
    const std::array<double, N>& values
)
{
    return create_scalar_attribute(name, N, values.data());
}

template <std::size_t N>
std::array<double, N> Path::read_scalar_attribute(
        const std::string& name
        ) const
{
    std::array<double, N> attributes;
    read_scalar_attribute(name,N, attributes.data());
    return attributes;
}


} // end namespace hdf5
} // end namespace io
} // end namespace specmicp

#endif // SPECMICP_IO_HDF5_PATH_HPP
