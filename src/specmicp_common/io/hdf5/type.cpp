/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "type.hpp"

#include <stdexcept>
#include "H5Tpublic.h"

namespace specmicp {
namespace io {
namespace hdf5 {

Type::Type(hid_t id):
    IdClass(id)
{}

Type::~Type()
{
    H5Tclose(get_id());
}


Type Type::acquire(hid_t id)
{
    if (id < 0) {
        throw std::runtime_error("Invalid id for a type.");
    }
    return Type(id);
}

Type Type::copy(hid_t id)
{
    return acquire(H5Tcopy(id));
}

Type Type::copy(const Type &other)
{
    return acquire(H5Tcopy(other.get_id()));
}

Type Type::get_c_str_type()
{
    // see doc : https://www.hdfgroup.org/HDF5/doc/RM/RM_H5T.html#CreateVLString
    hid_t  vls_type_c_id = H5Tcopy(H5T_C_S1);
    if (vls_type_c_id < 0) {
        throw std::runtime_error("Error while copying string character.");
    }
    herr_t status        = H5Tset_size(vls_type_c_id, H5T_VARIABLE);
    if (status < 0) {
        throw std::runtime_error("Error while setting the size of a string character.");
    }
    // check were performed below
    return Type(vls_type_c_id);

}

} // end namespace hdf5
} // end namespace io
} // end namespace specmicp
