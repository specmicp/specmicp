/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_IO_HDF5TIMESTEPS_HPP
#define SPECMICP_UTILS_IO_HDF5TIMESTEPS_HPP

//! \file io/hdf5_timesteps.hpp
//! \brief Tools to manage the list of timesteps in a HDF5 file

#include "specmicp_common/macros.hpp"


#include "H5public.h" // herr_t
#include "H5Ipublic.h" // hid_t
#include "H5Lpublic.h" // H5L_info_t

#include <string>
#include <vector>
#include <algorithm>


namespace specmicp {
namespace io {

namespace hdf5 {
class GroupPath;
} // end namespace hdf5


/*!
 \brief Contain the list of timesteps in a HDF5 file

 Call HDF5Timesteps::initialize to initialize the container with the timesteps

*/
class SPECMICP_DLL_PUBLIC HDF5Timesteps
{
public:
    //! Type of the timestep as numbers
    using ScalarT = double;
    //! Type of the timestep as strings
    using StrT = std::string;
    //! Type of the pair <timestep-number,timestep-string>
    using ValT = std::pair<ScalarT, StrT>;
    //! Type of the iterator over the vector
    using iterator = std::vector<ValT>::iterator;
    //! Type of the const iterator over the container
    using const_iterator = std::vector<ValT>::const_iterator;

    //! \brief Default constructor
    //!
    //! When using tihs constructor, the user must call 'initialize' to
    //! initialize the timestep vector
    //!
    //! \sa initialize
    HDF5Timesteps() {}

    //! \brief Overloaded constructor, initialize the timestep vector
    HDF5Timesteps(hdf5::GroupPath& the_file)
    {
        initialize(the_file);
    }

    //! \brief Initialize the timesteps
    void initialize(hdf5::GroupPath& the_file);

    //! \brief Return the timestep-number from the timestep-string
    ScalarT get_number(const StrT&  str) const {
        return string_to_number(str);
    }

    //! \brief Return the timestep-string from the timestep-number
    std::string get_string(const ScalarT& value) const;

    //! \brief Return the last element
    ValT front() const;
    //! \brief Return the last element
    ValT back() const;

    //! \brief Return a const iterator to the beginning
    const_iterator begin() const {return m_values.cbegin();}
    //! \brief Return a const iterator to the beginning
    const_iterator cbegin() const {return m_values.cbegin();}

    //! \brief Return a const iterator to the end
    const_iterator end() const {return m_values.cend();}
    //! \brief Return a const iterator to the end
    const_iterator cend() const {return m_values.cend();}

    //! \brief Return the number of timesteps
    std::size_t size() const {return m_values.size();}

    //! \brief Return the n-th timesteps (as a number)
    ScalarT operator[] (std::size_t pos) const {
        return m_values[pos].first;
    }



private:
    //! \brief Transform a string to a number
    static ScalarT string_to_number(const StrT& str) {
        return std::stod(str);
    }
    static ScalarT string_to_number(const char* str) {
        return string_to_number(std::string(str));
    }

    //! \brief Find the lower_bound of value using a binary search
    const_iterator lower_bound(const ScalarT& value) const;

    //! \brief Sort the vector
    void sort();

    //! \brief Add a value
    void push_back(const StrT& value) {
        m_values.emplace_back(string_to_number(value), value);
    }
    //! \brief Add a value
    void push_back(const ScalarT& number, const StrT& str) {
        m_values.emplace_back(number, str);
    }
    //! \brief Compare two timesteps
    static bool compare_keys(const ValT& a, const ValT& b) {
        return (a.first < b.first);
    }
    //! \brief Add a value if this is a timestep
    //!
    //! This is the function call by H5LIterate
    //!
    //! https://www.hdfgroup.org/HDF5/doc/RM/RM_H5L.html#Link-Iterate
    static herr_t add_element_if_timestep(hid_t _,
                                          const char* name,
                                          const H5L_info_t* info,
                                          void* void_this
                                          );
    //! \brief Allocate memory in advance
    void reserve(std::size_t size_to_reserve) {
        m_values.reserve(size_to_reserve);
    }

    std::vector<ValT> m_values; //! \brief Contains the values
};


} //end namespace io
} //end namespace specmicp

#endif // SPECMICP_UTILS_IO_HDF5TIMESTEPS_HPP
