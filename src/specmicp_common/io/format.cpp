/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "format.hpp"
#include <iostream>

namespace specmicp {
namespace io {

struct FormatDataContainer
{
    int yaml_indent {2};
    int yaml_scalar_precision {6};

    int equation_precision {8};

    std::string csv_comment {"#"};
    std::string csv_separator {"\t"};
};

FormatInformation& FormatInformation::get_format()
{
    static FormatInformation format_info;
    return format_info;
}

FormatInformation::FormatInformation()
{
    m_data = new FormatDataContainer;
}

FormatInformation::~FormatInformation()
{
    delete m_data;
}

int FormatInformation::get_yaml_scalar_precision() const
{
    return m_data->yaml_scalar_precision;
}

void FormatInformation::set_yaml_scalar_precision(int n)
{
    specmicp_assert(n > 0);
    m_data->yaml_scalar_precision = n;
}

int FormatInformation::get_yaml_indent() const
{
    return m_data->yaml_indent;
}

void FormatInformation::set_yaml_indent(int n)
{
    specmicp_assert(n > 0);
    m_data->yaml_indent = n;
}

int FormatInformation::get_stoichiometric_coefficient_precision() const
{
    return m_data->equation_precision;
}

void FormatInformation::set_stoichiometric_coefficient_precision(int n)
{
    specmicp_assert(n > 0);
    m_data->equation_precision = n;
}

void FormatInformation::format_stoichiometric_coefficient(std::ostream& out, scalar_t coeff) const
{
    format_scalar(&out, coeff, get_stoichiometric_coefficient_precision());
}


std::ostream* FormatInformation::format_scalar(std::ostream* out, scalar_t coeff, int precision) const
{
    out->precision(precision);
    (*out) << coeff;
    return out;

}

std::string FormatInformation::get_csv_separator()
{
    return m_data->csv_separator;
}

void FormatInformation::set_csv_separator(const std::string& sep)
{
    m_data->csv_separator = sep;
}

std::string FormatInformation::get_csv_comment()
{
    return m_data->csv_comment;
}

void FormatInformation::set_csv_comment(const std::string& comment_trigger)
{
    m_data->csv_comment = comment_trigger;
}

} //end namespace io
} //end namespace specmicp
