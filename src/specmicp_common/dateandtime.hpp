/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_DATEANDTIME_HPP
#define SPECMICP_UTILS_DATEANDTIME_HPP

//! \file dateandtime.hpp
//! \brief Date and time tools

#include "types.hpp"
#include <string>
#include <ctime>

namespace specmicp {
//! \namespace specmicp::dateandtime
//! \brief Date and time utilities
namespace dateandtime {


//! \brief Return a textual representation of time_point
std::string SPECMICP_DLL_PUBLIC to_text(std::time_t& time_point);

//! \brief Return a localized textual representation of time point
std::string SPECMICP_DLL_PUBLIC to_text_localized(std::time_t& time_point);

//! \brief Return a time point from a textual representation
std::time_t SPECMICP_DLL_PUBLIC from_text(const std::string& str_time);

//! \brief Return a time point from a localized textual representation
std::time_t SPECMICP_DLL_PUBLIC from_text_localized(const std::string& str_time);

//! \brief Return a textual representation of the current date and time
std::string SPECMICP_DLL_PUBLIC now();

//! \brief Return a localized textual representation of the current date and time
std::string SPECMICP_DLL_PUBLIC now_localized();

} //end namespace dateandtime
} //end namespace specmicp


#endif // SPECMICP_UTILS_DATEANDTIME_HPP
