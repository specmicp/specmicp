/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_SPARSESOLVERS_SPARSEBICGSTAB_HPP
#define SPECMICP_SPARSESOLVERS_SPARSEBICGSTAB_HPP

/*!
\file sparse_solvers/sparse_bicgstab.hpp
\brief Wrapper around the Eigen BiCGStab solver
 */

#include "sparse_solver_structs.hpp"
#include "sparse_solver_base.hpp"

#include <Eigen/IterativeLinearSolvers>

namespace specmicp {
namespace sparse_solvers {


//! \brief Wrapper around the Eigen BiCGStab solver
template <class MatrixT, class DerivedR, class DerivedS>
class SparseSolverBiCGSTAB: public SparseSolverBase<MatrixT, DerivedR, DerivedS>
{
    using SolverT = Eigen::BiCGSTAB<MatrixT, Eigen::IncompleteLUT<typename MatrixT::Scalar>>;

public:
    // has an effect
    void analyse_pattern(const MatrixT& jacobian) override
    {
        m_solver.analyzePattern(jacobian);
    }

    SparseSolverReturnCode decompose(const MatrixT& jacobian) override
    {
        m_solver.compute(jacobian);
        if (m_solver.info() != Eigen::Success)
        {
            return SparseSolverReturnCode::FailedDecomposition;
        }
        return SparseSolverReturnCode::Success;
    }

    SparseSolverReturnCode solve(
            const DerivedR& residuals,
            DerivedS& solution
            ) override
    {
        solution = m_solver.solve(-residuals);
        if (m_solver.info() != Eigen::Success)
        {
            return SparseSolverReturnCode::FailedSystemSolving;
        }
        return SparseSolverReturnCode::Success;
    }

    SparseSolverReturnCode solve_scaling(
            const DerivedR& residuals,
            const DerivedR& scaling,
            DerivedS& solution
            ) override
    {
        solution = m_solver.solve(scaling.asDiagonal()*(-residuals));
        if (m_solver.info() != Eigen::Success)
        {
            return SparseSolverReturnCode::FailedSystemSolving;
        }
        return SparseSolverReturnCode::Success;
    }

private:
     SolverT m_solver;
};

} // end namespace sparse_solvers
} // end namespace specmicp

#endif //SPECMICP_SPARSESOLVERS_SPARSEBICGSTAB_HPP
