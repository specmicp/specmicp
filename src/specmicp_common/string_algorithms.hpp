/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_STRINGALGORITHMS_HPP
#define SPECMICP_UTILS_STRINGALGORITHMS_HPP

//! \file string_algorithms.hpp
//! \brief Algorithms for string
//!
//! Commom algorithms to handle/parse/modify strings.

#include "types.hpp"
#include <string>
#include <vector>
#include <unordered_map>

namespace specmicp {
namespace utils {

//! \brief Split a string
//!
//! Split a string into several string
//!
//! \param to_split the string to split
//! \param separator the separator to use
std::vector<std::string> SPECMICP_DLL_PUBLIC  split(
        const std::string& to_split,
        char separator
        );

//! \brief Strip a string
//!
//! Remove spaces at the beginning and the end
std::string SPECMICP_DLL_PUBLIC strip(const std::string& to_trim);


//! \brief Get a range of index from a string
//!
//! The different format accepted for range_str are :
//!  - "x"
//!  - "x-y"
//!  - "u,v"
//!
//! where x,y are number and u,v are valid range_str
template <typename T=index_t>
std::vector<T> range_indices(const std::string& range_str);
template <>
std::vector<index_t> range_indices(const std::string& range_str);
template <>
std::vector<uindex_t> range_indices(const std::string& range_str);

//! \brief Parse a simple algebraic expression of type T
template <typename T>
T parse_expression(
        const std::string& expr,
        const std::unordered_map<std::string, T>& variables
        );
// Note : this is a template for API consistence with the overload
// which doesn't take a map of variables


//! \brief Parse a simple algebraic expression of floating numbers
//!
//! Parenthesis are not parsed yet and will result in errors
template <>
scalar_t parse_expression(
        const std::string& expr,
        const std::unordered_map<std::string, scalar_t>& variables
        );

//! \brief Parse a simple algebraic expression of floating numbers
//!
//! Parenthesis are not parsed yet and will result in errors
template <>
index_t parse_expression(
        const std::string& expr,
        const std::unordered_map<std::string, index_t>& variables
        );

//! \brief Parse a simple algebraic equation
template <typename T>
T parse_expression(
        const std::string& expr
        )
{
    return parse_expression(expr, std::unordered_map<std::string, T>());
}

//! \brief Transform a string to a boolean
bool string_to_bool(const std::string& to_bool_str);

} //end namespace utils
} //end namespace specmicp

#endif // SPECMICP_UTILS_STRINGALGORITHMS_HPP
