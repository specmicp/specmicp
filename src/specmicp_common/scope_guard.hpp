/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_SCOPEGUARD_HPP
#define SPECMICP_UTILS_SCOPEGUARD_HPP

//! \file scope_guard.hpp
//! \brief Scope guard tools

#include <utility>

namespace specmicp {
namespace utils {

//! \brief Base class for a scope guard
class ScopeGuardBase
{
public:
    //! \param on_failure True if the scope guard is active on failure
    ScopeGuardBase(bool on_failure=true):
        m_active(on_failure)
    {}

    //! \brief Return true if the scope guard is active
    bool is_active() {return m_active;}

    //! \brief Release the scope guard
    void release() {m_active = false;}

    //! \brief Engage the scope guard
    void engage() {m_active = true;}
private:
    bool m_active;
};

//! \brief Scope guard with a function
//!
//! If the scope guard is still engaged,
//! it will call the function
template <typename F>
class ScopeGuardFunc: public ScopeGuardBase
{
public:
    //! \param func A function that will be called if the guard is active
    //! \param on_failure True if the scope guard is active on failure
    ScopeGuardFunc(F&& func, bool on_failure=true):
        ScopeGuardBase(on_failure),
        m_func(func)
    {}

    ~ScopeGuardFunc() {
        if (is_active())
            m_func();
    }

private:
    F m_func;
};

//! \brief Create a scope guard taking a function
//!
//! \param func A function that will be called if the guard is active
//! \param on_failure True if the scope guard is active on failure
template <typename F>
ScopeGuardFunc<F> make_scope_guard(F&& func, bool on_failure=true)
{
    return ScopeGuardFunc<F>(std::forward<F>(func), on_failure);
}

} //end namespace utils
} //end namespace specmicp

#endif // SPECMICP_UTILS_SCOPEGUARD_HPP
