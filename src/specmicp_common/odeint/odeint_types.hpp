/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_ODEINT_ODEINTTYPES_HPP
#define SPECMICP_ODEINT_ODEINTTYPES_HPP

//! \file odeint_types.hpp
//! \brief Common types for odeint

#include "specmicp_common/types.hpp"
#include <functional>
#include <specmicp_common/eigen/incl_eigen_core.hpp>

namespace specmicp {

//! \namespace specmicp::odeint
//! \brief ODE integrators and related structs
namespace odeint {

namespace internal {

//! \brief implementation for the vector type (which may not be a vector for dim=1)
template <int dim>
struct vector_trait
{
    using type = Eigen::Matrix<scalar_t, Eigen::Dynamic, 1>; //!< type of a vector
};

#ifndef SPC_DOXYGEN_SHOULD_SKIP_THIS

// Specialisation for dimension 1 (scalar)
template <>
struct vector_trait<1>
{
    using type = scalar_t;
};

// Specialization for dimension 2 (fixed-size vector)
template <>
struct vector_trait<2>
{
    using type = Eigen::Matrix<scalar_t, 2, 1>;
};

// Specialization for dimension 3 (fixed-size vector)
template <>
struct vector_trait<3>
{
    using type = Eigen::Matrix<scalar_t, 3, 1>;
};
#endif // SPC_DOXYGEN_SHOULD_SKIP_THIS

} // end namespace internal

//! The vector type
template <int dim>
using vector_t = typename internal::vector_trait<dim>::type;

//! Return the maximum coefficient of the vector
template <int dim>
inline scalar_t max_coeff(const vector_t<dim>& vec) {return vec.maxCoeff();}


//! \brief Type of the function f in dy/dx=f(x,y)
template <int dim>
using rhs_f = std::function<void (scalar_t, const vector_t<dim>&, vector_t<dim>&)>;

//! \brief Return true of all members of 'vec' are non-negative
template <int dim>
inline bool is_nonnegative(vector_t<dim>& vec)
{
    return (vec.array() >= Eigen::VectorXd::Zero(vec.rows()).array()).all();
}

//! \brief Project to the positive quadrant
template <int dim>
inline void set_non_negative(vector_t<dim>& vec)
{
    vec = vec.cwiseMax(Eigen::VectorXd::Zero(vec.rows()));
}

#ifndef SPC_DOXYGEN_SHOULD_SKIP_THIS
// specialization for 1 D
template <>
inline scalar_t max_coeff<1>(const vector_t<1>& vec) {return vec;}

template <>
inline bool is_nonnegative<1>(vector_t<1>& vec)
{
    return vec >= 0.0;
}

template <>
inline void set_non_negative<1>(vector_t<1>& vec)
{
    vec = std::max(0.0, vec);
}
#endif // SPC_DOXYGEN_SHOULD_SKIP_THIS

} // end namespace odeint
} // end namespace specmicp

#endif // SPECMICP_ODEINT_ODEINTTYPES_HPP
