set(specmicp_common_odeint_headers
    odeint_types.hpp
    butcher_tableau.hpp
    runge_kutta_step.hpp
    runge_kutta_step.inl
    runge_kutta_step_structs.hpp
)

add_to_main_headers_list(specmicp_common_odeint_headers)

INSTALL(FILES ${specmicp_common_odeint_headers}
    DESTINATION ${INCLUDE_INSTALL_DIR}/specmicp_common/odeint
 )
