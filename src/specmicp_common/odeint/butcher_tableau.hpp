/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_ODEINT_BUTCHERTABLEAU_HPP
#define SPECMICP_ODEINT_BUTCHERTABLEAU_HPP

//! \file butcher_tableau.hpp
//! \brief Butcher tableau for embedded runge kutta method

#include <array>
#include "specmicp_common/types.hpp"

namespace specmicp {
namespace odeint {

//! \brief Butcher tableau describing the diffrent Runge-Kutta method
template <int order, int s>
class ButcherTableau {
public:
    static const int RK_order = order; //!< Runge kutta order
    static const int RK_evaluations = s+1; //!< Number of evaluations

    //! \brief Construct a Butcher tableau
    //!
    //! See Runge Kutta theory for explanation of these terms
    ButcherTableau(const std::array<scalar_t, s>& aa,
                   const std::array<std::array<scalar_t, s>, s>& ba,
                   const std::array<scalar_t, s+1>& ca,
                   const std::array<scalar_t, s+1>& csa
                   ):
        m_a(aa), m_b(ba), m_c(ca), m_cs(csa)
    {}
    //! \brief Construct a Butcher tableau
    //!
    //! See Runge Kutta theory for explanation of these terms
    ButcherTableau(std::array<scalar_t, s>&& aa,
                   std::array<std::array<scalar_t, s>, s>&& ba,
                   std::array<scalar_t, s+1>&& ca,
                   std::array<scalar_t, s+1>&& csa
                   ):
        m_a(aa), m_b(ba), m_c(ca), m_cs(csa)
    {}
    //! \brief Return the 'a_i' parameter
    double a(int i) const {return m_a[i-2];}
    //! \brief Return the 'b_ij' parameter
    double b(int i, int j) const {return m_b[i-2][j-1];}
    //! \brief Return the 'c_i' parameter
    double c(int i) const {return m_c[i-1];}
    //! \brief Return the 'cs_i' parameter
    double cs(int i) const {return m_cs[i-1];}
    //! \brief Return the 'a_i' parameter


private:
    std::array<scalar_t, s> m_a; //!< Parameters for the RK method
    std::array<std::array<scalar_t, s>, s> m_b; //!< Parameter for the RK method
    std::array<scalar_t, s+1> m_c; //!< Parameter for the RK method
    std::array<scalar_t, s+1> m_cs; //!< Parameter for the RK method
};

//! \brief The Butcher Tableau for the Cash-Karp method
const ButcherTableau<5, 5> butcher_cash_karp45({1.0/5.0, 3.0/10.0, 3.0/5.0, 1.0, 7.0/8.0},
                                   {{{1.0/5.0, 0, 0, 0, 0},
                                     {3.0/40.0, 9.0/40.0, 0, 0, 0},
                                     {3.0/10.0, -9.0/10.0, 6.0/5.0, 0, 0},
                                     {-11.0/54.0, 5.0/2.0, -70.0/27.0, 35.0/27.0, 0},
                                     {1631.0/55296.0, 175.0/512.0, 575.0/13824.0, 44275.0/110592.0, 253.0/4096.0}
                                   }},
                                   {37.0/378.0, 0.0, 250.0/621.0, 125.0/594.0, 0.0, 512.0/1771.0},
                                   {2825.0/27648.0, 0.0, 18575.0/48384.0, 13525.0/55296.0, 277.0/14336.0, 1.0/4.0}
                                   );
//! \brief Type of the Cash-Karp Butcher tableau
using ButcherTableauCashKarp_t = ButcherTableau<5, 5>;

//! \brief The Butcher tableau for the Dormand-Prince method
const ButcherTableau<5, 6> butcher_dormand_prince45(
                                {1.0/5.0, 3.0/10.0, 4.0/5.0, 8.0/9.0, 1.0, 1.0},
                                   { {{1.0/5.0, 0, 0, 0, 0, 0},
                                     {3.0/40.0, 9.0/40.0, 0, 0, 0, 0},
                                     {44.0/45.0, -56.0/15.0, 32.0/9.0, 0, 0, 0},
                                     {19372.0/6561.0, -25360.0/2187.0, 64448.0/6561.0, -212.0/729.0, 0, 0},
                                     {9017.0/3168.0, -355.0/33.0, 46732.0/5247.0, 49.0/176.0, -5103.0/18656.0, 0},
                                     {35.0/384.0, 0.0, 500.0/1113.0, 125.0/192.0, -2187.0/6784.0, 11.0/84.0}
                                   }},
                                    {35.0/384.0, 0.0, 500.0/1113.0, 125.0/192.0, -2187.0/6784.0, 11.0/84.0, 0.0},
                                   {5179.0/57600.0, 0.0, 7571.0/16695.0, 393.0/640.0, -92097.0/339200.0, 187.0/2100.0, 1.0/40.0}
                                   );
//! \brief Type of the Dormand-Prince Butcher tableau
using ButcherTableauDormandPrince_t = ButcherTableau<5, 6>;

} // end namespace odeint
} // end namespace specmicp


#endif // SPECMICP_ODEINT_BUTCHERTABLEAU_HPP
