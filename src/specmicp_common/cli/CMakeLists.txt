# CLI module

set( specmicp_common_cli_srcs
    parser.cpp
)

set( specmicp_common_cli_headers
    parser.hpp
)

add_to_main_srcs_list(specmicp_common_cli_srcs)
add_to_main_headers_list(specmicp_common_cli_headers)

INSTALL(FILES ${specmicp_common_cli_headers}
    DESTINATION ${INCLUDE_INSTALL_DIR}/specmicp_common/cli
 )
