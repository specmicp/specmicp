/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_CLI_PARSER_HPP
#define SPECMICP_CLI_PARSER_HPP

//! \file cli/parser.hpp
//! \brief The option parser

#include "specmicp_common/macros.hpp"

#include <string>
#include <vector>
#include <memory>

namespace specmicp {
//! \namespace specmicp::cli
//! \brief Namespace for the option parsing module
namespace cli {

//! \enum ValueType
//! \brief Type of the options
enum class ValueType
{
    boolean,  //!< Boolean
    string,   //!< String of characters
    integer,  //!< Integer
    floating  //!< Floating point number
};

//! \brief Type writer to obtain the correct underlying type
//!
//! \tparam value_type type of the option
template <ValueType value_type>
struct ValueTypeConversion{};

//! \brief Conversion type for a boolean
template <>
struct ValueTypeConversion<ValueType::boolean>
{
    using type = bool;
};
//! \brief Conversion type for an integer
template <>
struct ValueTypeConversion<ValueType::integer>
{
    using type = int;
};
//! \brief Conversion type for a floating-point number
template <>
struct ValueTypeConversion<ValueType::floating>
{
    using type = double;
};
//! \brief Conversion type for a string
template <>
struct ValueTypeConversion<ValueType::string>
{
    using type = std::string;
};

/*!
\brief The command line parser

This class is the interface to the command line parser.
It works in 3 steps :
   - Add options
   - Parse command line arguments
   - Obtain value for the options
 */
class SPECMICP_DLL_PUBLIC CommandLineParser
{
public:
    CommandLineParser();
    ~CommandLineParser();

    //! \brief Add a required option of 'value_type'
    //!
    //! The option would need to be provided by the user
    void add_option(
            char short_flag,
            const std::string& long_flag,
            ValueType value_type,
            const std::string& help_message
            );

    //! \brief Add an optional integer option
    //!
    //! 'default_value' will be used as the default value
    void add_option(
            char short_flag,
            const std::string& long_flag,
            int default_value,
            const std::string& help_message
            );

    //! \brief Add an optional floating number option
    //!
    //! 'default_value' will be used as the default value
    void add_option(
            char short_flag,
            const std::string& long_flag,
            double default_value,
            const std::string& help_message
            );

    //! \brief Add an optional boolean option
    //!
    //! 'default_value' will be used as the default value
    void add_option(
            char short_flag,
            const std::string& long_flag,
            bool default_value,
            const std::string& help_message
            );

    //! \brief Add an optional string option
    //!
    //! 'default_value' will be used as the default value
    void add_option(
            char short_flag,
            const std::string& long_flag,
            const std::string& default_value,
            const std::string& help_message
            );

    //! \brief Add a positional argument
    void add_pos_argument(
            const std::string& name,
            ValueType value_type,
            const std::string& help_message
            );

    //! \brief Register the name of the program
    void register_program_name(std::string&& name);
    //! \brief Set the help message of the program
    void set_help_message(std::string&& help_msg);
    //! \brief Parse the options
    int parse(const std::vector<std::string>& opts);
    //! \brief Parse the options provided in standard format
    int parse(int argc, char* argv[]);

    //! \brief Return the value of an option
    //!
    //! \tparam val_type type of the option
    //! \param long_flag long flag used for the option
    template <ValueType val_type>
    auto get_option(const std::string& long_flag)
        -> typename ValueTypeConversion<val_type>::type;

    //! \brief Return the value of a positional argument
    //!
    //! \tparam val_type type of the argument
    //! \param name name of the argument
    template <ValueType val_type>
    auto get_pos_argument(const std::string& name)
        -> typename ValueTypeConversion<val_type>::type;

private:
    struct SPECMICP_DLL_LOCAL CommandLineParserImpl;
    //! \brief Implementation details
    std::unique_ptr<CommandLineParserImpl> m_impl;
};

// The following macros define the only template specialization
// allowed for the get_option and get_pos_argument values

#ifndef SPC_DOXYGEN_SHOULD_SKIP_THIS

#define spc_def_cli_get_option(x) \
    template <> \
    auto CommandLineParser::get_option<x>( \
        const std::string& long_flag \
        ) -> typename ValueTypeConversion<x>::type;

spc_def_cli_get_option(ValueType::boolean)
spc_def_cli_get_option(ValueType::floating)
spc_def_cli_get_option(ValueType::integer)
spc_def_cli_get_option(ValueType::string)

#undef spc_def_get_option

#define spc_def_cli_get_pos_argument(x) \
    template <> \
    auto CommandLineParser::get_pos_argument<x>( \
        const std::string& long_flag \
        ) -> typename ValueTypeConversion<x>::type;


spc_def_cli_get_pos_argument(ValueType::boolean)
spc_def_cli_get_pos_argument(ValueType::floating)
spc_def_cli_get_pos_argument(ValueType::integer)
spc_def_cli_get_pos_argument(ValueType::string)

#undef spc_def_get_pos_argument

#endif // SPC_DOXYGEN_SHOULD_SKIP_THIS

} //end namespace cli
} //end namespace specmicp

#endif // SPECMICP_CLI_PARSER_HPP
