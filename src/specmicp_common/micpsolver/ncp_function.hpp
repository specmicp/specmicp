/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_MICPSOLVE_NCPFUNCTION_HPP
#define SPECMICP_MICPSOLVE_NCPFUNCTION_HPP

#include <cmath>
#include "specmicp_common/macros.hpp"

//! \file ncp_function.hpp
//! \brief Implements the ncp function

namespace specmicp {
namespace micpsolver {

//! \brief Return the square root of the sum of the square
template <typename ScalarT>
ScalarT SPECMICP_CONST_F sqrt_square_sum(ScalarT a, ScalarT b)
{
    ScalarT s = std::abs(a) + std::abs(b);
    if (s != 0.0)
    {
        s = s*std::sqrt(std::pow(a/s, 2) + std::pow(b/s, 2));
    }
    return s;
}

//! \brief The Fisher-Burmeister NCP-function
//!
//! \param a first variable of the NCP-function
//! \param b second variable of the NCP-function
//!
//!  References:
//!     - \cite Munson2001
//!     - \cite Facchinei2003
template <typename ScalarT>
ScalarT SPECMICP_CONST_F fisher_burmeister(ScalarT a, ScalarT b)
{
    ScalarT s = sqrt_square_sum(a, b);
    if ( a + b <= 0)
    {
        return s - (a + b);
    }
    else
    {
        return -2*a*b/(s + (a+b));
    }
}

//! \brief The penalized Fisher-Burmeister NCP-function
//!
//! \param t in (0, 1), penalization factor
//! \param a first variable of the NCP-function
//! \param b second varaible of the NCP-function
//!
//! References:
//!     - \cite Chen1997a
//!     - \cite Chen2000
//!     - \cite Munson2001
template <typename ScalarT>
ScalarT SPECMICP_CONST_F penalized_fisher_burmeister(ScalarT a, ScalarT b, ScalarT t)
{
    specmicp_assert(t >= 0);
    specmicp_assert(t <= 1);
    return t*fisher_burmeister(a,b)-(1.0-t)*std::max(0.0, a)*std::max(0.0, b);
}
//! \brief The box-constrained VI reformulation function
//!
//! \param r variable
//! \param s function value
//! \param u lower bound
//! \param v upper bound
//!
//! References:
//!     - \cite Facchinei2003
template <typename ScalarT>
ScalarT SPECMICP_CONST_F box_constrained_vi_b_function(
        ScalarT r,
        ScalarT s,
        ScalarT u,
        ScalarT v)
{
    if (r >= u and r <= v)
    {
        if (s >= 0) {
            return - fisher_burmeister(r-u, s);
        } else {
            return fisher_burmeister(v - r, -s);
        }
    }
    else if (r < u) {
        ScalarT res = r - u;
        if (s >= 0) {
            return res;
        } else {
            return res + fisher_burmeister(v - r, -s) - fisher_burmeister(r-u, s);
        }
    }
    else { //if (r > v) {
        ScalarT res = v - r;
        if (s <= 0) {
            return -res;
        } else  {
            return -res + fisher_burmeister(v - r, -s) - fisher_burmeister(r-u, s);
        }
    }
}



} // end namespace micpsolver
} // end namespace specmicp

#endif // SPECMICP_MICPSOLVE_NCPFUNCTION_HPP
