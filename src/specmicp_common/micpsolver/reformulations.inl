/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

//! \file reformulations.inl
//! \brief Reformulations used in the MiCPsolver

#ifndef SPECMIC_MICPSOLVER_MICPSOLVER_HPP
#include "micpsolver.hpp"
#endif

#include "ncp_function.hpp"

namespace specmicp {
namespace micpsolver {

// ========================================== //
//                                            //
// Penalized Fisher-Burmeister reformulation  //
//                                            //
// ========================================== //

template <class program_t, ReformulationF reform_f>
template <int dummy>
struct MiCPSolver<program_t, reform_f>::Reformulation<ReformulationF::CCK, dummy>
{
    static void reformulate_residuals(
            MiCPSolver<program_t, reform_f>* const parent,
            const Vector &x,
            const Vector &r,
            Vector &r_phi
            )
    {
        const auto neq = parent->get_neq();
        const auto neq_free = parent->get_neq_free();
        const auto t_cck = parent->get_options().penalization_factor;
        // reformulation with copy from r to r_phi
        r_phi.resizeLike(r);
        r_phi.block(0, 0, neq_free, 1) = r.block(0, 0, neq_free, 1);
        for (index_t i =  neq_free; i<neq; ++i)
        {
            r_phi(i) = penalized_fisher_burmeister(x(i), r(i), t_cck);
        }
    }

    static void reformulate_residuals_inplace(
            MiCPSolver<program_t, reform_f>* const parent,
            const Vector& x,
            Vector& r
            )
    {
        const auto neq = parent->get_neq();
        const auto neq_free = parent->get_neq_free();
        const auto t_cck = parent->get_options().penalization_factor;
        for (index_t i =  neq_free; i<neq; ++i)
        {
            r(i) = penalized_fisher_burmeister(x(i), r(i), t_cck);
        }
    }

    static void reformulate_jacobian(
            MiCPSolver<program_t, reform_f>* const parent,
            const Vector& x) {

        const auto neq = parent->get_neq();
        const auto neq_free = parent->get_neq_free();
        const auto t_cck = parent->get_options().penalization_factor;

        auto& r = parent->get_residuals();
        auto& jacobian = parent->get_jacobian();

        // set the z vector : contains 1 for degenerate points
        Eigen::VectorXd z(Eigen::VectorXd::Zero(neq));
        for (index_t i=neq_free; i<neq; ++i)
        {
            if (x(i) == 0 and r(i) == 0)
                z(i) = 1.0;
        }
        // modify the jacobian
        for (index_t i=neq_free; i<neq; ++i)
        {
            if (z(i) != 0)
            {
                const scalar_t gpdotz = jacobian.row(i).dot(z);
                const scalar_t s = sqrt_square_sum(z(i), gpdotz);
                const scalar_t c = t_cck*(z(i)/s - 1);
                const scalar_t d = t_cck*(gpdotz/s -1);
                jacobian.row(i) *= d;
                jacobian(i, i) += c;
            }
            else
            {
                const scalar_t s = sqrt_square_sum(x(i), r(i));
                scalar_t c = t_cck*(x(i)/s - 1);
                scalar_t d = t_cck*(r(i)/s - 1);
                if ((t_cck <1) and (r(i) > 0)  and (x(i) >0))
                {
                    c -= (1-t_cck)*r(i);
                    d -= (1-t_cck)*x(i);
                }
                jacobian.row(i) *= d;
                jacobian(i, i) += c;
            }
        }
    }
};




// ========================================== //
//                                            //
//     Box Constrained VI reformulation       //
//                                            //
// ========================================== //


template <class program_t, ReformulationF reform_f>
template <int dummy>
struct MiCPSolver<program_t, reform_f>::Reformulation<ReformulationF::BoxVI, dummy>
{
    static void reformulate_residuals(
            MiCPSolver<program_t, reform_f>* const parent,
            const Vector &x,
            const Vector &r,
            Vector &r_phi
            )
    {
        const auto neq = parent->get_neq();
        const auto neq_free = parent->get_neq_free();
        const auto t_cck = parent->get_options().penalization_factor;

        scalar_t upper_bound;
        // reformulation with copy from r to r_phi
        r_phi.resizeLike(r);
        r_phi.block(0, 0, neq_free, 1) = r.block(0, 0, neq_free, 1);
        for (index_t i =  neq_free; i<neq; ++i)
        {
            if (not parent->get_program()->is_box_vi(i, upper_bound))
            {
                r_phi(i) = penalized_fisher_burmeister(
                               x(i), r(i), t_cck);
            }
            else
            {
                if (upper_bound == 0.0)
                    r_phi(i) = 0;
                else
                    r_phi(i) = box_constrained_vi_b_function(
                                   x(i), r(i), 0.0, upper_bound);
            }
        }
    }


    static void reformulate_residuals_inplace(
            MiCPSolver<program_t, reform_f>* const parent,
            const Vector& x,
            Vector& r
            )
    {
        const auto neq = parent->get_neq();
        const auto neq_free = parent->get_neq_free();
        const auto t_cck = parent->get_options().penalization_factor;

        scalar_t upper_bound;
        for (index_t i =  neq_free; i<neq; ++i)
        {
            if (not parent->get_program()->is_box_vi(i, upper_bound))
            {
                r(i) = penalized_fisher_burmeister(x(i), r(i), t_cck);
            }
            else
            {
                if (upper_bound == 0.0)
                    r(i) = 0.0;
                else
                    r(i) = box_constrained_vi_b_function(
                               x(i), r(i), 0.0, upper_bound);
            }
        }
    }


    static void reformulate_jacobian(
            MiCPSolver<program_t, reform_f>* const parent,
            const Vector& x
            )
    {

        const auto neq = parent->get_neq();
        const auto neq_free = parent->get_neq_free();
        const auto t_cck = parent->get_options().penalization_factor;

        scalar_t upper_bound;
        Vector& r = parent->get_residuals();
        Matrix& jacobian = parent->get_jacobian();

        Eigen::VectorXd z(Eigen::VectorXd::Zero(neq));
        for (index_t i=neq_free; i<neq; ++i)
        {
            bool is_degenerate = (x(i) == 0 and r(i) == 0.0);
            if ( not is_degenerate and
                 parent->get_program()->is_box_vi(i, upper_bound) )
            {
                is_degenerate = (x(i) == upper_bound and r(i) == 0.0);
            }
            if (is_degenerate)
                z(i) = 1.0;
        }

        for (index_t i =  neq_free; i<neq; ++i)
        {
            if (parent->get_program()->is_box_vi(i, upper_bound))
            {
                derivative_box_constrained_vi_b_function(
                            i, x(i), r(i), upper_bound, z, jacobian);
            }
            else
            {
                deritative_cck(i, x(i), r(i), t_cck, z, jacobian);
            }
        }
    }

    // Box VI jacobian reformulation
    static void derivative_box_constrained_vi_b_function(
            index_t i,
            scalar_t x,
            scalar_t r,
            scalar_t v,
            const Vector& z,
            Matrix& jacobian
            )
    {
        bool is_degenerate = false;

        const scalar_t u = 0.0;
        scalar_t Da = 0.0;
        scalar_t Db = 0.0;

        if (v == 0.0)
        {
            jacobian.row(i).setZero();
            jacobian(i,i) = 1;
        }


        if (x <= u and r >= 0)
        {
            if (x == u and r ==0) {is_degenerate = true;}
            Da = 1;
            Db = 0;
        }
        else if (x >= v and r <= 0)
        {
            if (x == v and r ==0) {is_degenerate = true;}
            Da = 1;
            Db = 0;
        }
        else if (x > u and x <= v and r >= 0)
        {
            if (x == v and r == 0) {is_degenerate = true;}
            const scalar_t hsqm = sqrt_square_sum((x-u), r);
            Da = 1 - (x - u)/hsqm;
            Db = 1 - r / hsqm;
        }
        else if ( x >= u and r < v and r <= 0)
        {
            if (x == u and r == 0) {is_degenerate = true;}
            const scalar_t hsqp = sqrt_square_sum((x-v), r);
            Da = 1 + (x - v)/hsqp;
            Db = 1 + r / hsqp;
        }
        else if (   (x > v and r > 0)
                 or (x < u and r < 0))
        {
            const scalar_t hsqm = sqrt_square_sum((x-u), r);
            const scalar_t hsqp = sqrt_square_sum((x-v), r);
            Da = 1 - (x - u)/hsqm + (x - v)/hsqp;
            Db = - r/hsqm + r/hsqp;
        }

        if (is_degenerate) {
            if (x==u)
            {
                const scalar_t gpdotz = jacobian.row(i).dot(z);
                const scalar_t s = sqrt_square_sum(z(i), gpdotz);
                Da = (z(i)/s - 1);
                Db = (gpdotz/s -1);

            } else
            {
                const scalar_t gpdotz = jacobian.row(i).dot(z);
                const scalar_t s = sqrt_square_sum(z(i), gpdotz);
                Da = (z(i)/s - 1);
                Db = (gpdotz/s -1);
            }

        }

        // update jacobian
        jacobian.row(i) *= Db;
        jacobian(i, i) += Da;


        return;
    }

    // CCK jacobian reformulation
    static void deritative_cck(
            index_t i,
            scalar_t x,
            scalar_t r,
            scalar_t t,
            const Vector& z,
            Matrix& jacobian
            )
    {

        scalar_t Da;
        scalar_t Db;

        if (z(i) != 0)
        {
            const scalar_t gpdotz = jacobian.row(i).dot(z);
            const scalar_t s = sqrt_square_sum(z(i), gpdotz);
            Da = t*(z(i)/s - 1);
            Db = t*(gpdotz/s -1);
        }
        else
        {
            const scalar_t s = sqrt_square_sum(x, r);
            Da = t*(x/s - 1);
            Db = t*(r/s - 1);
            if ((t <1) and (r > 0)  and (x >0))
            {
                Da -= (1-t)*r;
                Db -= (1-t)*x;
            }
        }

        jacobian.row(i) *= Db;
        jacobian(i, i) += Da;
    }

};








} // end namespace micpsolver
} // end namespace specmicp
