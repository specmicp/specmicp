/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "micpsolver_base.hpp" // syntaxic coloration


#include "ncp_function.hpp"

namespace specmicp {
namespace micpsolver {


// ref : Munson et al. (2001)
template <class program_t>
void MiCPSolverBaseProgram<program_t>::scaling_jacobian(
        const Matrix& jacobian,
        const Vector& residuals,
        Vector& rscaler,
        Vector& cscaler)
{
    const double norm_scaling = 1e-10; // value from Munson et al. (2001)
                                       // avoid division by 0
    for (int i=0; i<jacobian.cols(); ++i)
    {
        const double sumhsq = jacobian.row(i).array().square().sum();
        double s = std::sqrt(residuals(i)*residuals(i) + sumhsq);
        rscaler(i) = 1.0/std::max(s, norm_scaling);
    }
    for (int i=0; i<jacobian.cols(); ++i)
    {
        const double sumhsq = ( rscaler.asDiagonal()*jacobian
              ).col(i).array().square().sum();
        double s = std::sqrt(sumhsq);
        cscaler(i) = 1.0/std::max(s, norm_scaling);
    }
}

template <class program_t>
MiCPSolverReturnCode MiCPSolverBaseProgram<program_t>::check_convergence(
        int nb_iterations,
        const Vector& update,
        const Vector& solution,
        const Vector& residuals,
        bool may_have_converged
        )
{
    MiCPSolverReturnCode termcode = MiCPSolverReturnCode::NotConvergedYet;
    const scalar_t norm_residuals = residuals.lpNorm<Eigen::Infinity>();
    if (norm_residuals < get_options().fvectol)
    {
        if (may_have_converged == true)
            termcode = MiCPSolverReturnCode::ResidualMinimized;
    }
    else if (nb_iterations >0 and norm_update<Eigen::Infinity>(update, solution) < get_options().steptol)
    {
        if (norm_residuals > get_options().threshold_stationary_point)
        {
            ERROR << "Stationary point detected !\n"
                     "Residuals could not be minimized \n"
                     "Current residual : " << norm_residuals << "\n"
                     "Norm update      : " << norm_update<Eigen::Infinity>(update, solution);
            termcode = MiCPSolverReturnCode::StationaryPoint;
        }
        if (may_have_converged == true)
        {
            WARNING << "MiCP solver : Error is minimized !\n"
                       "It may indicate a stationnary point \n"
                       "Current residual : " << norm_residuals << "\n"
                       "Norm update      : " << norm_update<Eigen::Infinity>(update, solution);
            termcode = MiCPSolverReturnCode::ErrorMinimized;
        }
    }
    else if (nb_iterations >  get_options().max_iter)
    {
        ERROR << "Maximum number of iteration reached (" <<
                 get_options().max_iter << ")\n"
                 "Current residual : " << norm_residuals << "\n"
                 "Current solution vector \n ---- \n" <<
                 solution <<
                 "\n ---- \n";

        termcode = MiCPSolverReturnCode::MaxIterations;
    }
    else if (get_perfs().max_taken)
    {
        ++get_perfs().nb_consecutive_max_taken;
        ++get_perfs().nb_max_taken;
        if (get_perfs().nb_consecutive_max_taken == get_options().maxiter_maxstep) {
            ERROR << "Divergence detected\n"
                     "Maximum step length taken two many times.\n"
                     "Current residual : " << norm_residuals;
            termcode = MiCPSolverReturnCode::MaxStepTakenTooManyTimes;
        }
    }
    else
    {
        get_perfs().nb_consecutive_max_taken = 0;
    }
    return termcode;
}

template <class program_t>
void MiCPSolverBaseProgram<program_t>::reformulate_jacobian_cck(
        const Vector& x,
        const Vector& r,
        Matrix& jacobian
        )
{
    // set the z vector : contains 1 for degenerate points
    Eigen::VectorXd z(Eigen::VectorXd::Zero(get_neq()));
    for (index_t i=get_neq_free(); i<get_neq(); ++i)
    {
        if (x(i) == 0 and r(i) == 0)
            z(i) = 1.0;
    }
    // modify the jacobian
    const scalar_t lambda = get_options().penalization_factor;
    for (index_t i=get_neq_free(); i<get_neq(); ++i)
    {
        if (z(i) != 0)
        {
            const scalar_t gpdotz = jacobian.row(i).dot(z);
            const scalar_t s = sqrt_square_sum(z(i), gpdotz);
            const scalar_t c = lambda*(z(i)/s - 1);
            const scalar_t d = lambda*(gpdotz/s -1);
            jacobian.row(i) *= d;
            jacobian(i, i) += c;
        }
        else
        {
            const scalar_t s = sqrt_square_sum(x(i), r(i));
            scalar_t c = lambda*(x(i)/s - 1);
            scalar_t d = lambda*(r(i)/s - 1);
            if ((lambda <1) and (r(i) > 0)  and (x(i) >0))
            {
                c -= (1-lambda)*r(i);
                d -= (1-lambda)*x(i);
            }
            jacobian.row(i) *= d;
            jacobian(i, i) += c;
        }
    }
}

// Projection of the variables onto the feasible set
template <class program_t>
void MiCPSolverBaseProgram<program_t>::projection(Vector& x)
{
    for (index_t i=0; i<get_program()->nb_complementarity_variables(); ++i)
    {
        if (x(i+get_program()->nb_free_variables()) < get_options().projection_min_variable)
        {
            x(i+get_program()->nb_free_variables()) = 0;
        }
    }
}



template <class program_t>
scalar_t MiCPSolverBaseProgram<program_t>::is_step_too_long(Vector& update)
{
    scalar_t steplength = update.norm();
    if (steplength > get_options().maxstep)
    {
        get_perfs().max_taken = true;
        update  = get_options().maxstep / steplength * update;
        steplength = get_options().maxstep;
    }
    return steplength;
}


} // end namespace micpsolver
} // end namespace specmicp
