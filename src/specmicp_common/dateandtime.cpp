/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "dateandtime.hpp"
#include "log.hpp"
#include <string>

#define TIME_FORMAT "%Y-%m-%d %H:%M:%S %z"
#define L10N_TIME_FORMAT "%x %X %z"

namespace specmicp {
namespace dateandtime {


std::string to_text(std::time_t& time_point)
{
    std::tm timeinfo {};
    char buffer[100];

    timeinfo = *std::localtime(&time_point);

    std::strftime(buffer, sizeof(buffer), TIME_FORMAT, &timeinfo);
    //std::cout << "to str : is dst ? " << timeinfo.tm_isdst << std::endl;
    return std::string(buffer);
}

std::string to_text_localized(std::time_t& time_point)
{
    std::tm timeinfo {};
    char buffer[100];

    timeinfo = *std::localtime(&time_point);

    std::strftime(buffer, sizeof(buffer), L10N_TIME_FORMAT, &timeinfo);

    return std::string(buffer);
}


std::time_t from_text(const std::string& str_time)
{
    std::tm timeinfo {};

    if( not strptime(str_time.c_str(), TIME_FORMAT, &timeinfo)) {
        ERROR_THROW("Error while parsing date and time : " + str_time + ".");
    };
    timeinfo.tm_isdst = -1; // let mktime check for daylight saving time
                            // because it is not done by strptime
    return std::mktime(&timeinfo);
}

std::time_t from_text_localized(const std::string& str_time)
{
    std::tm timeinfo {};

    if( not strptime(str_time.c_str(), L10N_TIME_FORMAT, &timeinfo)) {
        ERROR_THROW("Error while parsing date and time : " + str_time + ".");
    }
    timeinfo.tm_isdst = -1; // let mktime check for daylight saving time
                            // because it is not done by strptime

    return std::mktime(&timeinfo);
}

std::string now()
{
    std::time_t rawtime = std::time(nullptr);
    return to_text(rawtime);
}

std::string now_localized()
{
    std::time_t rawtime = std::time(nullptr);
    return to_text_localized(rawtime);
}

} //end namespace dateandtime
} //end namespace specmicp
