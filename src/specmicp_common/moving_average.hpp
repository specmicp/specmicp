/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_UTILS_MOVINGAVERAGE_HPP
#define SPECMICP_UTILS_MOVINGAVERAGE_HPP

/*!
\file moving_average.hpp
\brief Exponential moving average

The moving average allows to take a weighted average
of the last x points in the data series.

It is used in particular in the adaptive timestepping algorithm.

 */

#include "types.hpp"

namespace specmicp {
//! \namespace specmicp::utils
//! \brief misc and others used in different places
namespace utils {

/*! \brief Exponential moving average

The exponential moving average \f$Y_n\f$ is given by
\f$Y_n = \alpha y + (1 - \alpha) Y_{n-1}\f$
where y is a value to add to the average, and \f$Y_{n-1}\f$ the previous value.

The average allows to take into account several previous values while storing
only one value. The impact of the previous iteration on the average value is
 controlled by the parameter \f$\alpha\f$, which is between 0 and 1.

\code{.cpp}
  ExponentialMovingAverage moving_average(0.1, 1.0);
  for (int i=0; i<10; ++i) {
     std::cout << "Current value : " << moving_average.add_point(i);
  }
  // reset to 1.0
  moving_average.reset(1.0);
\endcode

*/
class SPECMICP_DLL_PUBLIC ExponentialMovingAverage
{
public:
    //! \param alpha coefficient between 0 and 1
    //! \param init initial value of the average
    ExponentialMovingAverage(scalar_t alpha, scalar_t init):
        m_alpha(alpha),
        m_current_value(init)
    {
    }
    //! \brief Add a point in the series, return current average value
    scalar_t add_point(scalar_t value);

    //! \brief Return the current average value
    scalar_t current_value() {
        return m_current_value;
    }
    //! \brief Reset the average to 'value'
    void reset(scalar_t value) {
        m_current_value = value;
    }

    //! \brief Set the average parameter value
    void set_alpha(scalar_t alpha) {m_alpha = alpha;}


private:
    scalar_t m_alpha;         //!< Parameters to control average
    scalar_t m_current_value; //!< Current value of the average
};


} // end namespace utils
} // end namespace specmicp

#endif // SPECMICP_UTILS_MOVINGAVERAGE_HPP
