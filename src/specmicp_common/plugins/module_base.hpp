/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_PLUGINS_MODULEBASE_HPP
#define SPECMICP_PLUGINS_MODULEBASE_HPP

//! \file module_base.hpp
//! \brief Base class for a module

#include "plugin_types.hpp"

#include <memory>
#include <unordered_map>

namespace specmicp {
namespace plugins {


//! \brief A module is a set of objects of the same type
//!
//! The module class can be subclassed if needed
class SPECMICP_DLL_PUBLIC ModuleBase
{
public:
    ModuleBase();
    ~ModuleBase();

    //! \brief Create a module
    static std::unique_ptr<ModuleBase> create_module();

    //! \brief Return an object of type T
    template<typename T>
    std::unique_ptr<T> get_object(const std::string& name) {
        return std::unique_ptr<T>(dynamic_cast<T>(get_object(name)));
    }

    //! \brief Register an object type
    virtual bool register_object(const std::string& name, object_factory_f func);

    //! \brief Return a generic object
    virtual void* get_object(const std::string& name);

private:
    struct ModuleBaseImpl;
    std::unique_ptr<ModuleBaseImpl> m_impl; //!< The implementation
};

} //end namespace plugins
} //end namespace specmicp

#endif // SPECMICP_PLUGINS_MODULEBASE_HPP
