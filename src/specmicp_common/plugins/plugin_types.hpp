/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_PLUGINS_PLUGINTYPES_HPP
#define SPECMICP_PLUGINS_PLUGINTYPES_HPP

//! \file plugins/plugin_types.hpp
//! \brief Common types and struct used in the plugins system

#include "specmicp_common/macros.hpp"

#include <string>
#include <functional>
#include <memory>

namespace specmicp {

//! \namespace specmicp::plugins
//! \brief Plugin framework
namespace plugins {

//! \brief type used in the version
using api_version_t = unsigned int;

//! \brief Version of the plugin API
struct SPECMICP_DLL_PUBLIC PluginAPIVersion
{
    api_version_t v_major {0}; //!< Major version number
    api_version_t v_minor {0}; //!< Minor version number
    api_version_t v_patch {0}; //!< Patch version number

    //! \brief Default constructor
    PluginAPIVersion() {}

    //! \brief Constructor
    //!
    //! \param maj major version number
    //! \param min minor version number
    //! \param pat version number
    PluginAPIVersion(
            api_version_t maj,
            api_version_t min,
            api_version_t pat):
        v_major(maj),
        v_minor(min),
        v_patch(pat)
    {}
};

//! \brief Type of an object factory function
using object_factory_f = std::function<void* ()>;

class ModuleBase;

//! \brief Services offered by the PluginManager to a plugin
struct SPECMICP_DLL_PUBLIC PluginManagerServices
{
    //! \brief Type of a label
    using label_t = const std::string&;
    //! \brief Type of a function to register a module
    using register_module_f = std::function<
            bool (label_t, std::unique_ptr<ModuleBase>&&)>;
    //! \brief Type of a function to register an object
    using register_object_f = std::function<
        bool (label_t, label_t, object_factory_f)>;

    //! \brief Version of the plugin API
    PluginAPIVersion api_version;
    //! \brief Function to register a module
    register_module_f register_module;
    //! \brief Function to register an object
    register_object_f register_object;
};


} //end namespace plugins
} //end namespace specmicp

#endif // SPECMICP_PLUGINS_PLUGINTYPES_HPP
