/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "dynamic_library.hpp"

#include <dlfcn.h>

#include "specmicp_common/compat.hpp"
#include "specmicp_common/log.hpp"
#include "specmicp_common/filesystem.hpp"

namespace specmicp {
namespace plugins {

DynamicLibraryPtr DynamicLibrary::load(
        const std::string& path,
        std::string& error
        )
{
    void* handle = dlopen(path.c_str(), RTLD_LAZY);

    if (handle == NULL or dlerror() != NULL)
    {
        error = "Error when loading dynamic library : " + path + ".\n";
        const char* str_error = dlerror();
        if (str_error != NULL)
        {
            error += str_error;
        }
        ERROR << error;
        return nullptr;
    }

    return std::unique_ptr<DynamicLibrary>(new DynamicLibrary(handle));
}

DynamicLibraryPtr DynamicLibrary::load(
        const std::string& file,
        const std::vector<std::string>& list_dir,
        std::string& error
        )
{
    std::string path = utils::find_path(file, list_dir);
    if (path != "") {
        std::string test_error;
        auto test = load(path, test_error);
        if (test != nullptr) {
            error = test_error;
            return test;
        }
    }
    return load(file, error);
}

DynamicLibrary::DynamicLibrary(void *handle):
    m_handle(handle)
{

}

DynamicLibrary::~DynamicLibrary()
{
    if (m_handle)
    {
        int ret = dlclose(m_handle);
        if (ret > 0)
        {
            ERROR << dlerror();
        }
    }
}


void* DynamicLibrary::get_symbol(
        const std::string& name,
        std::string& error)
{
    if (not m_handle)
    {
        error = "The library is not open !";
        return nullptr;
    }
    void* symbol = dlsym(m_handle, name.c_str());
    const char* str_error = dlerror();
    if (str_error != NULL)
    {
        error = "Failed to load symbol '" + name + "'\n";
        error += str_error;

        return nullptr;
    }
    return symbol;

}

} //end namespace plugins
} //end namespace specmicp
