/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_PLUGINS_PLUGINMANAGER_HPP
#define SPECMICP_PLUGINS_PLUGINMANAGER_HPP

//! \file plugins/plugin_manager.hpp
//! \brief The plugin manager
//!
//! Use the method specmicp::plugins::get_plugin_manager()
//! to obtain the instance of the plugin manager.

#include "module_base.hpp"

#include <vector>
#include <memory>

namespace specmicp {
namespace plugins {

//! \brief The plugin manager
//!
//! It registers the plugins and return their objects
//!
//! \sa get_plugin_manager
class SPECMICP_DLL_PUBLIC PluginManager
{
public:
    PluginManager();
    ~PluginManager();

    //! \brief Return a module
    ModuleBase* get_module(const std::string& name);

    //! \brief Return an object
    void* get_object(const std::string& module,
                     const std::string& name);

    //! \brief Return a type object
    template <typename T>
    std::unique_ptr<T> get_object(const std::string& module,
                                  const std::string& name)
    {
        return std::unique_ptr<T>(static_cast<T*>(get_object(module, name)));
    }

    //! \brief Load a plugin
    //!
    //! \param filepath path to the shared library
    //! \return True if the loading was successful
    bool load_plugin(const std::string& filepath);

    //! \brief Register an object
    bool register_object(
            const std::string& module,
            const std::string& name,
            object_factory_f func
            );

    //! \brief Register a module
    bool register_module(
            const std::string& module_name,
            std::unique_ptr<ModuleBase>&& module
            );

    //! \brief Add a plugin directory
    void add_plugin_directory(
            const std::string& directory_name
            );

    //! \brief Add plugin directories
    void add_plugin_directories(
            const std::vector<std::string>& directory_names
            );

    //! \brief Return the list of directories
    std::vector<std::string> get_plugin_directories();

    //! \brief Check the API version
    bool check_version(const PluginAPIVersion& api_version);


private:
    struct SPECMICP_DLL_LOCAL PluginManagerImpl;
    std::unique_ptr<PluginManagerImpl> m_impl;
};

//! \brief Return the plugin manager
PluginManager& get_plugin_manager();

} //end namespace plugins
} //end namespace specmicp

#endif // SPECMICP_PLUGINS_PLUGINMANAGER_HPP
