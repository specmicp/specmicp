/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_SWITCHBASIS_HPP
#define SPECMICP_DATABASE_SWITCHBASIS_HPP

//! \file switch_basis.hpp
//! \brief Switch the basis

#include "module.hpp"

#include <map>

namespace specmicp {
namespace database {

//! \brief Use this class to switch the basis of the database
class BasisSwitcher: public DatabaseModule
{
public:
    //! \brief Constructor
    BasisSwitcher(RawDatabasePtr thedata):
        DatabaseModule(thedata)
    {}

    //! \brief Switch the basis
    //!
    //! \param new_basis list of id of the new basis
    //!
    //! The new basis is a list of id, id = id_component for no swapping
    //! or id = id_aqueous + nb_component for swapping a secondary species
    //!
    //! \sa swap_components
    void switch_basis(std::vector<index_t>& new_basis);

    //! \brief Swap components
    //!
    //! \param swap_to_make is a map where the keys are the components to swap and the values the id of the aqueous to put in the database
    void swap_components(const std::map<std::string, std::string>& swap_to_make);

private:
    //! \brief Return the true aqueous index
    index_t get_true_aqueous_index(index_t ind);

    //! \brief Swap aqueous parameters during a basis transformation
    void swap_aq_param(std::vector<index_t>& new_basis);
    //! \brief Swap labels - called during a basis transformation
    void swap_labels(std::vector<index_t>& new_basis);
};

} // end namespace database
} // end namespace specmicp

#endif // SPECMICP_DATABASE_SWITCHBASIS_HPP
