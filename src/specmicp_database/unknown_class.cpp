/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "unknown_class.hpp"
#include "data_container.hpp"
#include "errors.hpp"

namespace specmicp {
namespace database {

AqueousSpeciesClass
SpeciesTypeFinder::find_aqueous_species_class(const std::string& label) const
{
    if (data->get_id_component(label) != no_species)
    {
        return AqueousSpeciesClass::Component;
    }
    else if (data->get_id_aqueous(label) != no_species)
    {
        return AqueousSpeciesClass::Aqueous;
    }
    else if (data->get_id_compound(label) != no_species)
    {
        return AqueousSpeciesClass::Compound;
    }
    else
    {
        return AqueousSpeciesClass::Invalid;
    }
}

GenericAqueousSpecies
SpeciesTypeFinder::find_aqueous_species(const std::string& label) const
{
    index_t ida = data->get_id_component(label);
    if (ida != no_species)
    {
        return {AqueousSpeciesClass::Component, ida};
    }
    ida = data->get_id_aqueous(label);
    if (ida != no_species)
    {
        return {AqueousSpeciesClass::Aqueous, ida};
    }
    ida = data->get_id_compound(label);
    if (ida != no_species)
    {
        return {AqueousSpeciesClass::Compound, ida};
    }
    return {AqueousSpeciesClass::Invalid, no_species};
}


scalar_t SpeciesTypeFinder::molar_mass_aqueous(const std::string& label) const
{
    GenericAqueousSpecies ida = find_aqueous_species(label);
    scalar_t molar_mass {-1};
    switch (ida.type) {
    case AqueousSpeciesClass::Component:
        molar_mass = data->molar_mass_basis(ida.id);
        break;
    case AqueousSpeciesClass::Aqueous:
        molar_mass = data->molar_mass_aqueous(ida.id);
        break;
    case AqueousSpeciesClass::Compound:
        molar_mass = data->molar_mass_compound(ida.id);
        break;
    default:
        throw InvalidGenericAqueousSpecies(label);
    }
    return molar_mass;
}

SolidPhaseClass
SpeciesTypeFinder::find_solid_phase_class(const std::string& label) const
{
    if (data->get_id_mineral(label) != no_species)
    {
        return SolidPhaseClass::EquilibriumMineral;
    }
    else if (data->get_id_mineral_kinetic(label) != no_species)
    {
        return SolidPhaseClass::MineralKinetics;
    }
    else
        return SolidPhaseClass::Invalid;
}


GenericSolidPhase
SpeciesTypeFinder::find_solid_phase(const std::string& label) const
{
    index_t ids = data->get_id_mineral(label);
    if (ids != no_species)
    {
        return {SolidPhaseClass::EquilibriumMineral, ids};
    }
    ids = data->get_id_mineral_kinetic(label);
    if (ids != no_species)
    {
        return {SolidPhaseClass::MineralKinetics, ids};
    }
    else
        return {SolidPhaseClass::Invalid, no_species};
}

scalar_t SpeciesTypeFinder::molar_mass_solid_phase(const std::string& label) const
{
    GenericSolidPhase ids = find_solid_phase(label);
    scalar_t molar_mass = -1;
    switch (ids.type) {
    case SolidPhaseClass::EquilibriumMineral:
        molar_mass = data->molar_mass_mineral(ids.id);
        break;
    case SolidPhaseClass::MineralKinetics:
        molar_mass = data->molar_mass_mineral_kinetic(ids.id);
        break;
    default:
        throw InvalidSolidPhase(label);
        break;
    }
    return molar_mass;
}

scalar_t SpeciesTypeFinder::molar_volume_solid_phase(const std::string& label) const
{
    GenericSolidPhase ids = find_solid_phase(label);
    scalar_t molar_volume = -1;
    switch (ids.type) {
    case SolidPhaseClass::EquilibriumMineral:
        molar_volume = data->molar_volume_mineral(ids.id);
        break;
    case SolidPhaseClass::MineralKinetics:
        molar_volume = data->molar_volume_mineral_kinetic(ids.id);
        break;
    default:
        throw InvalidSolidPhase(label);
        break;
    }
    return molar_volume;
}


} //end namespace database
} //end namespace specmicp
