set( specmicp_database_io_srcs
    configuration.cpp
)
set( specmicp_database_io_headers
    configuration.hpp
)
set( specmicp_database_io_hdf5_srcs
    hdf5_database.cpp
)
set( specmicp_database_io_hdf5_headers
    hdf5_database.hpp
)


if (HDF5_FOUND)
    list(APPEND specmicp_database_io_srcs
         ${specmicp_database_io_hdf5_srcs}
    )
    list(APPEND specmicp_database_io_headers
        ${specmicp_database_io_hdf5_headers}
    )
    set_source_files_properties(
        ${specmicp_database_io_hdf5_srcs}
        PROPERTIES COMPILE_DEFINITIONS HDF5_DEFINITIONS
    )
endif()

add_to_database_srcs_list(specmicp_database_io_srcs)
add_to_database_headers_list(specmicp_database_io_headers)

INSTALL(FILES ${specmicp_database_io_headers}
    DESTINATION ${INCLUDE_INSTALL_DIR}/specmicp_database/io
)
