/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_IO_CONFIGURATION_HPP
#define SPECMICP_IO_CONFIGURATION_HPP

//! \file database/io/configuration.hpp
//! \brief Configure a database

#include "specmicp_common/types.hpp"
#include "specmicp_database/database_fwd.hpp"

#include <vector>
#include <string>

namespace YAML {
class Node;
} //end namespace YAML

namespace specmicp {
namespace io {

class YAMLConfigHandle;

//! \brief Read SPECMICP_DATABASE_DIR
void SPECMICP_DLL_PUBLIC add_db_dirs_from_env(std::vector<std::string>& db_dirs);

//! \brief Configure the database
//!
//! \deprecated
RawDatabasePtr SPECMICP_DLL_PUBLIC configure_database(const YAML::Node& conf);

//! \brief Configure a database from a YAML configuration
RawDatabasePtr SPECMICP_DLL_PUBLIC configure_database(
        YAMLConfigHandle&& conf
        );

//! \brief Configure a database from a YAML configuration
//!
//! \param conf database section of the configuration
//! \param database_dirs directories where to search the database
RawDatabasePtr SPECMICP_DLL_PUBLIC configure_database(
        YAMLConfigHandle&& conf,
        const std::vector<std::string>& database_dirs
        );


} //end namespace io
} //end namespace specmicp

#endif // SPECMICP_IO_CONFIGURATION_HPP
