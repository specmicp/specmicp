/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "hdf5_database.hpp"
#include "specmicp_database/data_container.hpp"


#include "specmicp_common/io/hdf5/path.hpp"
#include "specmicp_common/io/hdf5/dataspace.hpp"
#include "specmicp_common/io/hdf5/group.hpp"
#include "specmicp_common/io/hdf5/dataset.hpp"

#include "H5Tpublic.h"

namespace specmicp {
namespace io {

namespace internal {

//! \brief Implementation of the database saver
class SPECMICP_DLL_LOCAL DatabaseHDF5Saver
{
public:

    DatabaseHDF5Saver(database::RawDatabasePtr raw_data):
        m_data(raw_data)
    {}

    //!
    //! \brief Save the labels of a database
    //! \param file HDF5 file
    //! \param name Name of the group to be created
    //! \param section Name of the group where the new group willbe created
    //!
    void save_labels(hdf5::GroupPath& location,
                     const std::string& name
                     );

private:

    database::RawDatabasePtr m_data;
};

} //end namespace internal

void save_database_labels(
        hdf5::GroupPath& location,
        const std::string& name,
        database::RawDatabasePtr raw_data
        )
{
    internal::DatabaseHDF5Saver saver(raw_data);
    saver.save_labels(location, name);
}


// Implementation
// ==============

namespace internal {

void DatabaseHDF5Saver::save_labels(
        hdf5::GroupPath& location,
        const std::string& name
        )
{
    auto db_group = location.create_group(name);

    db_group.create_string_dataset("components", m_data->components.labels());
    db_group.create_string_dataset("aqueous"   , m_data->aqueous.labels());
    db_group.create_string_dataset("gas"       , m_data->gas.labels());
    db_group.create_string_dataset("minerals"  , m_data->minerals.labels());
}

} //end namespace internal


} //end namespace io
} //end namespace specmicp
