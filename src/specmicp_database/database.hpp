/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_DATABASE_HPP
#define SPECMICP_DATABASE_DATABASE_HPP

//! \file specmicp_database/database.hpp
//! \brief Database management

#include "module.hpp"

#include <map>

namespace specmicp {
//! \namespace specmicp::database
//! \brief Database management
namespace database {

//! \brief Prepare the database for the simulation.
//!
//! This is the class that should be used to handle the database.
//! It does not contain the data, only the algorithms.
//!
//! \ingroup specmicp_api
class SPECMICP_DLL_PUBLIC Database: public DatabaseModule
{
public:
    //! \brief Default constructor
    //!
    //! the method parse_database must be called to parse the json database
    Database() {}

    //! \brief Initialise the database py parsing 'filepath'
    Database(std::string filepath, bool check_compo=true) {
        parse_database(filepath, check_compo);
    }

    //! \brief initialise the database with the raw database
    Database(std::shared_ptr<DataContainer> raw_data):
        DatabaseModule(raw_data)
    {}


    //! \brief Parse the database 'filepath'
    void parse_database(std::string filepath, bool check_compo=true);

    //! \brief Parse the database from a stream
    void parse_database(std::istream& input, bool check_compo=true);

    //! \brief Return the database
    //!
    //! Return a smart pointer of a DataCotnainer instance
    //! Note : this is a read write access, be careful
    std::shared_ptr<DataContainer> get_database() {return data;}

    //! \brief Change the basis
    //!
    //! @param new_basis list of id of the new basis
    //!
    //! The new basis is a list of id, id = id_component for no swapping
    //! or id = id_aqueous + nb_component for swapping a secondary species
    void switch_basis(std::vector<index_t>& new_basis);

    //! \brief Swap some component in the basis
    //! \param swap_to_make a map where the keys are the current
    //!         component and the values are the new component
    void swap_components(const std::map<std::string, std::string>& swap_to_make);


    //! \brief Remove components not present in the system
    //!
    //! "H2O" and "E[-]" cannot be removed from the database.
    //!
    //! \param labels_components_to_remove list of labels of the component to remove from the basis
    void remove_components(const std::vector<std::string>& labels_components_to_remove);

    //! \brief Remove components not present in the system
    //!
    //! "H2O" and "E[-]" cannot be removed from the database.
    //!
    //! \param id_components_to_remove list of id of the component to remove from the basis
    void remove_components(const std::vector<index_t>& id_components_to_remove);

    //! \brief Keep only components in the id_components_to_keep list
    //!
    //! "H2O" and "E[-]" will always be kept in the database.
    //!
    //! \param id_components_to_keep list of id of the component to keep in the basis
    void keep_only_components(const std::vector<index_t>& id_components_to_keep);

    //! \brief Keep only components in the labels_components_to_keep list
    //!
    //! "H2O" and "E[-]" will always be kept in the database.
    //!
    //! \param labels_components_to_keep list of labels of the component to keep in the basis
    void keep_only_components(const std::vector<std::string>& labels_components_to_keep);

    //! \brief Keep only some minerals at equilibrium
    //!
    //! The effect is to flag all the other minerals as "kinetic"
    void minerals_keep_only(const std::vector<std::string>& minerals_to_keep);
    //! \brief Keep only some minerals at equilibrium
    //!
    //! The effect is to flag all the other minerals as "kinetic"
    void minerals_keep_only(const std::vector<index_t>& minerals_to_keep);

    //! \brief Remove all gas species
    void remove_gas_phases();
    //! \brief Add gas phases into the database
    //!
    //! The input is a JSON list of gas (formated like the database)
    void add_gas_phases(const std::string& gas_input, bool check_compo=true);

    //! \brief Remove all solid phases
    void remove_solid_phases();
    //! \brief Add some solid phases into the database
    //!
    //! The input is a JSON list of minerals (formated like the database)
    void add_solid_phases(const std::string& solid_phases_input, bool check_compo=true);

    //! \brief Remove all sorbed species
    void remove_sorbed_species();
    //! \brief Add sorbed species into the database
    //!
    //! The input is a JSON list of sorbed species (formated like the database)
    void add_sorbed_species(const std::string& sorbed_species_input, bool check_compo=true);


    //! \brief Remove the compounds
    void remove_compounds();
    //! \brief Add some compounds into the database
    //!
    //! The input is a JSON list of compounds (formated like the database)
    void add_compounds(const std::string& solid_phases_input, bool check_compo=true);


    //! \brief Remove all the half-cells reactions
    void remove_half_cell_reactions();
    //! \brief Remove the half-cells reactions for components in 'list_components'
    void remove_half_cell_reactions(const std::vector<std::string>& list_components);
    //! \brief Remove the half-cells reactions for components in 'list_id_components'
    void remove_half_cell_reactions(const std::vector<index_t>& list_id_components);

    //! \brief Freeze the database
    void freeze() {data->freeze_db();}

    //! \brief Save the database on disk
    void save(const std::string& filename);
};

} // end namespace database
} // end namespace specmicp

#endif // SPECMICP_DATABASE_DATABASE_HPP
