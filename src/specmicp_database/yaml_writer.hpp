/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_YAMLWRITER
#define SPECMICP_DATABASE_YAMLWRITER

//! \file database/yaml_writer.hpp
//! \brief Write a database on disk

#include "module.hpp"

namespace YAML {
class Emitter;
} //end namespace YAML

namespace specmicp {
namespace database {

//! \brief Write a database in the JSON format
//!
//! This class can be used to save the database used in a computation.
//! It will save the current state of the database
class SPECMICP_DLL_LOCAL DatabaseWriterYaml: public DatabaseModule
{
public:
    //! \brief Constructor
    DatabaseWriterYaml(RawDatabasePtr& the_database):
        DatabaseModule(the_database)
    {}

    //! \brief Write the database
    //!
    //! \param filepath path to the file where the database will be saved
    void write(const std::string& filepath);


    //! \brief Format the database as a YAML::Emitter
    void set_yaml_tree(YAML::Emitter& root);

private:
    // The following methods set a section of the database
    void set_metadata(YAML::Emitter& root);
    void set_basis(YAML::Emitter& root);
    void set_component(index_t component, YAML::Emitter& root);
    void set_elements(YAML::Emitter& root);
    void set_aqueous(YAML::Emitter& root);
    void set_aqueous_species(index_t aqueous, YAML::Emitter& root);
    void set_minerals(YAML::Emitter& root);
    void set_mineral(index_t mineral, YAML::Emitter& root);
    void set_mineral_kinetic(index_t mineral, YAML::Emitter& root);
    void set_gas(YAML::Emitter& root);
    void set_gas_phase(index_t gas, YAML::Emitter& root);
    void set_sorbed(YAML::Emitter& root);
    void set_sorbed_species(index_t sorbed, YAML::Emitter& root);
    void set_compound(YAML::Emitter& root);
    void set_compound_species(index_t compound, YAML::Emitter& root);

};

} //end namespace database
} //end namespace specmicp


#endif // SPECMICP_DATABASE_YAMLWRITER
