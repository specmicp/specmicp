/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATAABASE_MODULE_HPP
#define SPECMICP_DATAABASE_MODULE_HPP

//! \file module.hpp
//! \brief base class for a database module


#include "data_container.hpp"
#include "errors.hpp"

namespace specmicp {
namespace database {


//! \brief Base class for a database module
class SPECMICP_DLL_PUBLIC DatabaseModule
{
public:

    //! \brief Create a blank database
    DatabaseModule() {
        reset_database();
    }
    //! \brief Use the raw database thedata
    //!
    //! If it exist we assume that it is non-null
    DatabaseModule(RawDatabasePtr& thedata);

    //! \brief set the database
    void set_database(RawDatabasePtr& thedata) {data = thedata;}

    //! \brief set the database
    void set_database(RawDatabasePtr&& thedata) {data = thedata;}

    //! \brief (Re)set the database - create a blank database
    void reset_database() {data = std::make_shared<DataContainer>();}

    //! \brief Return true if the database is valid
    bool is_valid() {return data->is_valid();}

    //! \brief Return the label for the component 'id'
    std::string component_id_to_label(index_t id)
    {
        return data->components.get_label(id);
    }
    //! \brief Return the id of the component "label"
    //!
    //! Return no_species if "label" is not a component
    index_t component_label_to_id(const std::string& label)
    {
        return data->components.get_id(label);
    }
    //! \brief Safe version of 'component_label_to_id
    //! throw an error if the compounds cannot be found
    index_t safe_component_label_to_id(const std::string& label);

    //! \brief Return the label for the aqueous species 'id'
    std::string aqueous_id_to_label(index_t id)
    {
        return data->aqueous.get_label(id);
    }
    //! \brief Return the id of the secondary aqueous species "label"
    //!
    //! Return no_species if "label" is not an aqueous species.
    //! A component (of the basis) is not considered as a secondary aqueous species.
    index_t aqueous_label_to_id(const std::string& label)
    {
        return data->aqueous.get_id(label);
    }
    //! \brief Safe version of 'aqueous_label_to_id
    //! throw an error if the compounds cannot be found
    index_t safe_aqueous_label_to_id(const std::string& label);


    //! \brief Return the label for the solid phase 'id'
    std::string mineral_id_to_label(index_t id)
    {
        return data->minerals.get_label(id);
    }
    //! \brief Return the id of the mineral "label"
    //!
    //! Return no_species if label is not a mineral (at equilibrium)
    index_t mineral_label_to_id(const std::string& label)
    {
        return data->minerals.get_id(label);
    }
    //! \brief Safe version of mineral_label_to_id
    //! throw an error if the compounds cannot be found
    index_t safe_mineral_label_to_id(const std::string& label);


    //! \brief Return the label for the kinetic solid phase 'id'
    std::string mineral_kinetic_id_to_label(index_t id)
    {
        return data->minerals_kinetic.get_label(id);
    }
    //! \brief Return the id of the mineral "label"
    //!
    //! Return no_species if label is not a mineral (at equilibrium)
    index_t mineral_kinetic_label_to_id(const std::string& label)
    {
        return data->minerals_kinetic.get_id(label);
    }
    //! \brief Safe version of mineral_kinetic_label_to_id
    //! throw an error if the compounds cannot be found
    index_t safe_mineral_kinetic_label_to_id(const std::string& label);

    //! \brief Return the label for the gas 'id'
    std::string gas_id_to_label(index_t id)
    {
        return data->gas.get_label(id);
    }
    //! \brief Return the id of the gas "label"
    //!
    //! Return no_species if "label" is not a gas
    index_t gas_label_to_id(const std::string& label)
    {
        return data->gas.get_id(label);
    }
    //! \brief Safe version of 'gas_label_to_id
    //! throw an error if the compounds cannot be found
    index_t safe_gas_label_to_id(const std::string& label);

    //! \brief Return the label for the sorbed species 'id'
    std::string sorbed_id_to_label(index_t id)
    {
        return data->sorbed.get_label(id);
    }
    //! \brief Return the id of the gas "label"
    //!
    //! Return no_species if "label" is not a gas
    index_t sorbed_label_to_id(const std::string& label)
    {
        return data->sorbed.get_id(label);
    }
    //! \brief Safe version of 'sorbed_label_to_id
    //! throw an error if the compounds cannot be found
    index_t safe_sorbed_label_to_id(const std::string& label);

protected:
    RawDatabasePtr data; //!< The database
};

} // end namespace database
} // end namespace specmicp

#endif // SPECMICP_DATABASE_MODULE_HPP
