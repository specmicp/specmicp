/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "yaml_writer.hpp"

#include "section_name.hpp"

#include "specmicp_common/io/yaml.hpp"
#include "specmicp_common/dateandtime.hpp"
#include "specmicp_common/io/format.hpp"

#include <fstream>
#include <yaml-cpp/emitter.h>


namespace specmicp {
namespace database {

//! \brief Format a composition list
std::string format_equation(
        index_t species,
        const ReactiveSpeciesList& slist,
        const ComponentList& basis
        );

void DatabaseWriterYaml::write(const std::string& filename)
{
    YAML::Emitter root;
    io::configure_yaml_emitter(root);
    //auto& format = io::FormatInformation::get_format();
    set_yaml_tree(root);
    io::save_yaml(filename, root);
}

void DatabaseWriterYaml::set_yaml_tree(YAML::Emitter& root)
{
    root << YAML::Comment("This database was generated automatically by SpecMiCP");
    root << YAML::BeginMap;
    set_metadata(root);
    set_elements(root);
    set_basis(root);
    set_aqueous(root);
    set_minerals(root);
    set_gas(root);
    set_sorbed(root);
    set_compound(root);
    root << YAML::EndMap;
}

void DatabaseWriterYaml::set_metadata(YAML::Emitter& root)
{
    root << YAML::Key << INDB_SECTION_METADATA << YAML::Key;
    root << YAML::BeginMap;
    root << YAML::Key << INDB_ATTRIBUTE_NAME << YAML::Value << data->metadata.name;
    root << YAML::Key << INDB_ATTRIBUTE_VERSION << YAML::Value << dateandtime::now();
    root << YAML::Key << INDB_ATTRIBUTE_PATH << YAML::Value << "N/A";
    root << YAML::EndMap;
}

void DatabaseWriterYaml::set_basis(YAML::Emitter& root)
{
    root << YAML::Key << INDB_SECTION_BASIS << YAML::Key;
    root << YAML::BeginSeq;
    for (auto id: data->range_component())
    {
        set_component(id, root);
    }
    root << YAML::EndSeq;
}


void DatabaseWriterYaml::set_component(index_t component, YAML::Emitter& root)
{
    root << YAML::BeginMap;
    root << YAML::Key << INDB_ATTRIBUTE_LABEL << YAML::Value << data->get_label_component(component);
    root << YAML::Key << INDB_ATTRIBUTE_MOLARMASS
         << YAML::Value << data->molar_mass_basis(component, database_units);
    if (component >= 2)
    {
        root << YAML::Key << INDB_ATTRIBUTE_ACTIVITY << YAML::Value;
        root << YAML::BeginMap;
        root << YAML::Key << INDB_ATTRIBUTE_ACTIVITY_A << YAML::Value << data->a_debye_component(component);
        root << YAML::Key << INDB_ATTRIBUTE_ACTIVITY_B << YAML::Value << data->b_debye_component(component);
        root << YAML::EndMap;
    }
    root << YAML::EndMap;
}

void DatabaseWriterYaml::set_elements(YAML::Emitter& root)
{
    root << YAML::Key << INDB_SECTION_ELEMENTS << YAML::Value;
    root << YAML::BeginSeq;
    for (auto id: data->range_component())
    {
        root << YAML::BeginMap;
        root << YAML::Key << INDB_ATTRIBUTE_ELEMENT << YAML::Value
             << YAML::DoubleQuoted  << data->elements.get_label_element(id);
        root << YAML::Key << INDB_ATTRIBUTE_COMPONENT << YAML::Value << data->get_label_component(id);
        root << YAML::EndMap;
    }
    root << YAML::EndSeq;
}

void DatabaseWriterYaml::set_aqueous(YAML::Emitter& root)
{
    root << YAML::Key << INDB_SECTION_AQUEOUS << YAML::Value;
    root << YAML::BeginSeq;
    for (auto id: data->range_aqueous())
    {
        set_aqueous_species(id, root);
    }
    root << YAML::EndSeq;
}

void DatabaseWriterYaml::set_aqueous_species(index_t aqueous, YAML::Emitter& root)
{
    root << YAML::BeginMap;
    root << YAML::Key << INDB_ATTRIBUTE_LABEL
         << YAML::Value << YAML::DoubleQuoted << data->get_label_aqueous(aqueous);
    root << YAML::Key << INDB_ATTRIBUTE_COMPOSITION
         << YAML::Value << YAML::DoubleQuoted << format_equation(aqueous, data->aqueous, data->components);
    root << YAML::Key << INDB_ATTRIBUTE_LOGK
         << YAML::Value << data->logk_aqueous(aqueous);

    root << YAML::Key << INDB_ATTRIBUTE_ACTIVITY << YAML::Value;
    root << YAML::BeginMap;
    root << YAML::Key << INDB_ATTRIBUTE_ACTIVITY_A
         << YAML::Value << data->a_debye_aqueous(aqueous);
    root << YAML::Key << INDB_ATTRIBUTE_ACTIVITY_B
         << YAML::Value << data->b_debye_aqueous(aqueous);
    root << YAML::EndMap;

    root << YAML::EndMap;
}

void DatabaseWriterYaml::set_minerals(YAML::Emitter& root)
{
    root << YAML::Key << INDB_SECTION_MINERALS << YAML::Value;
    root << YAML::BeginSeq;
    for (auto id: data->range_mineral())
    {
        set_mineral(id, root);
    }
    for (auto id: data->range_mineral_kinetic())
    {
        set_mineral_kinetic(id, root);
    }
    root << YAML::EndSeq;
}


void DatabaseWriterYaml::set_mineral(index_t mineral, YAML::Emitter& root)
{
    root << YAML::BeginMap;
    root << YAML::Key << INDB_ATTRIBUTE_LABEL
         << YAML::Value << YAML::DoubleQuoted << data->get_label_mineral(mineral);
    root << YAML::Key << INDB_ATTRIBUTE_COMPOSITION
         << YAML::Value << YAML::DoubleQuoted << format_equation(mineral, data->minerals, data->components) ;
    root << YAML::Key << INDB_ATTRIBUTE_LOGK << YAML::Value << data->logk_mineral(mineral);

    if (data->unsafe_molar_volume_mineral(mineral) > 0)
    {
        root << YAML::Key << INDB_ATTRIBUTE_MOLARVOLUME
             << YAML::Value << data->molar_volume_mineral(mineral, database_units);
    }
    root << YAML::EndMap;
}

void DatabaseWriterYaml::set_mineral_kinetic(index_t mineral, YAML::Emitter& root)
{
    root << YAML::BeginMap;
    root << YAML::Key << INDB_ATTRIBUTE_LABEL
         << YAML::Value << YAML::DoubleQuoted  << data->get_label_mineral_kinetic(mineral);
    root << YAML::Key << INDB_ATTRIBUTE_COMPOSITION
         << YAML::Value << YAML::DoubleQuoted << format_equation(mineral, data->minerals_kinetic, data->components) ;
    root << YAML::Key << INDB_ATTRIBUTE_LOGK << YAML::Value << data->logk_mineral_kinetic(mineral);

    if (data->unsafe_molar_volume_mineral_kinetic(mineral) > 0)
    {
        root << YAML::Key << INDB_ATTRIBUTE_MOLARVOLUME
             << YAML::Value << data->molar_volume_mineral_kinetic(mineral, database_units);
    }
    root << YAML::Key << INDB_ATTRIBUTE_FLAG_KINETIC << YAML::Value << true;
    root << YAML::EndMap;
}


void DatabaseWriterYaml::set_gas(YAML::Emitter& root)
{
    root << YAML::Key << INDB_SECTION_GAS << YAML::Value;
    root << YAML::BeginSeq;
    for (auto id: data->range_gas())
    {
        set_gas_phase(id, root);
    }
    root << YAML::EndSeq;
}

void DatabaseWriterYaml::set_gas_phase(index_t gas, YAML::Emitter& root)
{
    root << YAML::BeginMap;
    root << YAML::Key << INDB_ATTRIBUTE_LABEL
         << YAML::Value << YAML::DoubleQuoted << data->get_label_gas(gas);
    root << YAML::Key << INDB_ATTRIBUTE_COMPOSITION
         << YAML::Value << YAML::DoubleQuoted  << format_equation(gas, data->gas, data->components);
    root << YAML::Key << INDB_ATTRIBUTE_LOGK << YAML::Value << data->logk_gas(gas);
    root << YAML::EndMap;
}


void DatabaseWriterYaml::set_sorbed(YAML::Emitter& root)
{
    root << YAML::Key << INDB_SECTION_SORBED << YAML::Value;
    root << YAML::BeginSeq;
    for (auto id: data->range_sorbed())
    {
        set_sorbed_species(id, root);
    }
    root << YAML::EndSeq;
}

void DatabaseWriterYaml::set_sorbed_species(index_t sorbed, YAML::Emitter& root)
{
    root << YAML::BeginMap;
    root << YAML::Key << INDB_ATTRIBUTE_LABEL
         << YAML::Value << YAML::DoubleQuoted << data->get_label_sorbed(sorbed);
    root << YAML::Key << INDB_ATTRIBUTE_COMPOSITION
         << YAML::DoubleQuoted << YAML::Value << format_equation(sorbed, data->sorbed, data->components);
    root << YAML::Key << INDB_ATTRIBUTE_NBSITEOCCUPIED << YAML::Value << data->nb_sorption_sites(sorbed);
    root << YAML::Key << INDB_ATTRIBUTE_LOGK << YAML::Value << data->logk_sorbed(sorbed);
    root << YAML::EndMap;
}

void DatabaseWriterYaml::set_compound(YAML::Emitter& root)
{
    root << YAML::Key << INDB_SECTION_COMPOUNDS << YAML::Value;
    root << YAML::BeginSeq;
    for (auto id: data->range_compounds())
    {
        set_compound_species(id, root);
    }
    root << YAML::EndSeq;
}

void DatabaseWriterYaml::set_compound_species(index_t compound, YAML::Emitter& root)
{
    root << YAML::BeginMap;
    root << YAML::Key << INDB_ATTRIBUTE_LABEL
           << YAML::Value << YAML::DoubleQuoted  << data->get_label_compound(compound);
    root << YAML::Key << INDB_ATTRIBUTE_COMPOSITION
         << YAML::DoubleQuoted << YAML::Value << format_equation(compound, data->compounds, data->components);
    root << YAML::EndMap;
}


std::string format_equation(index_t species, const ReactiveSpeciesList& slist, const ComponentList& basis)
{
    std::ostringstream equation;
    const io::FormatInformation& formatter = io::FormatInformation::get_format();

    for (index_t component: basis.range())
    {
        if (slist.nu_ji(species, component) != 0)
        {
            if (slist.nu_ji(species, component) == 1.0)
            {
                equation <<  basis.get_label(component) << ", ";
            }
            else if (slist.nu_ji(species, component) == -1.0)
            {
                equation << "- " << basis.get_label(component) << ", ";
            }
            else
            {
                formatter.format_stoichiometric_coefficient(equation, slist.nu_ji(species, component));
                equation << " " << basis.get_label(component) << ", ";
            }
        }
    }
    std::string str_eq = equation.str();
    str_eq.erase(str_eq.size()-2, 2); // remove tailing ', '
    return str_eq;
}

} //end namespace database
} //end namespace specmicp
