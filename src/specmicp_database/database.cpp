/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "database.hpp"

#include "selector.hpp"
#include "switch_basis.hpp"
#include "mineral_selector.hpp"
#include "yaml_reader.hpp"
#include "oxydo_selector.hpp"
#include "appender.hpp"
#include "yaml_writer.hpp"

namespace specmicp {
namespace database {

// Implementation note
// --------------------
// This class does not contain any major algorithm
// Instead it calls the database modules methods
// This is done in the source file to reduce compilation dependancy
// and avoid exporting too many symbols
//
// This class should only contains short functions.

void Database::parse_database(std::string filepath, bool check_compo)
{
    DataReaderYaml reader(filepath, check_compo);
    set_database(reader.get_database());
}

void Database::parse_database(std::istream& input, bool check_compo)
{
    DataReaderYaml reader(input, check_compo);
    set_database(reader.get_database());
}

void Database::remove_components(const std::vector<std::string>& labels_components_to_remove)
{
    std::vector<index_t> id_components_to_remove;
    id_components_to_remove.reserve(labels_components_to_remove.size());
    for (auto it: labels_components_to_remove)
    {
        id_components_to_remove.push_back(safe_component_label_to_id(it));
    }
    remove_components(id_components_to_remove);
}

void Database::remove_components(const std::vector<index_t>& id_components_to_remove)
{
    DatabaseSelector selector(data);
    selector.remove_component(id_components_to_remove);
}

void Database::keep_only_components(const std::vector<std::string>& labels_components_to_keep)
{
    std::vector<index_t> id_components_to_keep;
    id_components_to_keep.reserve(labels_components_to_keep.size());
    for (auto it: labels_components_to_keep)
    {
        id_components_to_keep.push_back(safe_component_label_to_id(it));
    }
    keep_only_components(id_components_to_keep);
}

void Database::keep_only_components(const std::vector<index_t>& id_components_to_keep)
{
    DatabaseSelector selector(data);
    selector.keep_only_component(id_components_to_keep);
}

void Database::minerals_keep_only(const std::vector<std::string>& minerals_to_keep)
{
    MineralSelector(data).keep_only(minerals_to_keep);
}
void Database::minerals_keep_only(const std::vector<index_t>& minerals_to_keep)
{
    MineralSelector(data).keep_only(minerals_to_keep);
}

void Database::swap_components(const std::map<std::string, std::string>& swap_to_make)
{
    BasisSwitcher(data).swap_components(swap_to_make);
}

void Database::remove_gas_phases()
{
    DatabaseSelector(data).remove_all_gas();
}

void Database::add_gas_phases(const std::string &gas_input, bool check_compo)
{
    DataAppender(data).add_gas(gas_input, check_compo);
}

void Database::remove_solid_phases()
{
    MineralSelector(data).remove_all_minerals();
}

void Database::add_solid_phases(const std::string &solid_phases_input, bool check_compo)
{
    DataAppender(data).add_minerals(solid_phases_input, check_compo);
}

void Database::remove_sorbed_species()
{
    DatabaseSelector(data).remove_all_sorbed();
}

void Database::add_sorbed_species(const std::string& sorbed_input, bool check_compo)
{
    DataAppender(data).add_sorbed(sorbed_input, check_compo);
}

void Database::remove_compounds()
{
    DatabaseSelector(data).remove_all_compounds();
}

void Database::add_compounds(const std::string& compounds_input, bool check_compo)
{
    DataAppender(data).add_compounds(compounds_input, check_compo);
}

void Database::remove_half_cell_reactions()
{
    OxydoSelector(data).remove_half_cells();
}

void Database::remove_half_cell_reactions(const std::vector<std::string>& list_components)
{
    OxydoSelector(data).remove_half_cells(list_components);
}

void Database::remove_half_cell_reactions(const std::vector<index_t>& list_id_components)
{
    OxydoSelector(data).remove_half_cells(list_id_components);
}

void Database::save(const std::string& filename)
{
    DatabaseWriterYaml(data).write(filename);
}

} // end namespace database
} // end namespace specmicp
