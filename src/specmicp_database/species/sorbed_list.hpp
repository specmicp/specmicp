/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_SORBEDLIST_HPP
#define SPECMICP_DATABASE_SORBEDLIST_HPP

#include "specmicp_common/types.hpp"
#ifndef SPECMICP_DATABASE_SPECIES_HPP
#include "species.hpp"
#endif
#include "aqueous_list.hpp"

//! \file aqueous_list.hpp
//! \brief The list of sorbed species
//!
//! This list stores the informations about sorbed species.

namespace specmicp {
namespace database {

//! \struct SorbedValues
//! \brief Struct to initialize a sorbed species in the sorbed species list
//!
//! \ingroup database_species
struct SorbedValues {
    std::string label;
    scalar_t logk;
    index_t sorption_site_occupied;
};

//! \class SorbedList
//! \brief The sorbed species
//!
//! This is the list of sorbed species in the database
//!
//! \ingroup database_species
class SorbedList: public ReactiveSpeciesList
{
public:
    //! Initialize an empty list
    SorbedList() {}
    //! Initialize a list of 'size' sorbed species with 'nb_components' components
    SorbedList(index_t size, index_t nb_components):
        ReactiveSpeciesList(size, nb_components),
        m_sorption_site(size)
    {}

    //! \name Size
    //! \brief Manage the size of the list
    // --------
    //! @{
    //! \brief Resize the list
    void resize(index_t size) override
    {
        ReactiveSpeciesList::resize(size);
        m_sorption_site.resize(size);
    }
    //! \brief Resize the list, adjust the number of components
    void resize(index_t size, index_t nb_components) override
    {
        ReactiveSpeciesList::resize(size, nb_components);
        m_sorption_site.resize(size);
    }
    //! @}

    //! \name Getter
    //! \brief Return values
    // ----------------------
    //! @{
    //! \brief Return the ionic size of component 'k'
    const scalar_t& nb_sorption_site_occupied(index_t k) const {
        return m_sorption_site(k);
    }
    //! @}

    //! \name Setter
    //! \brief Set values
    // -------------
    //! @{
    //! \brief Set the values
    //! \warning Do no set stoichiometric coefficients
    void set_values(index_t k, const SorbedValues& values) {
        set_label(k, values.label);
        set_logk(k, values.logk);
        m_sorption_site.set_value(k, values.sorption_site_occupied);
    }
    //! \brief Set the values
    //! \warning Do no set stoichiometric coefficients
    void set_values(index_t k, SorbedValues&& values) {
        set_label(k, std::move(values.label));
        set_logk(k, values.logk);
        m_sorption_site.set_value(k, values.sorption_site_occupied);
    }
    //! @}


    //! \name Move
    //! \brief Move species inside the list or to other lists
    // ------------
    //! @{
    //! \brief Move component 'old_ind' to 'new_ind'
    void move_erase(index_t old_ind, index_t new_ind) override
    {
        m_sorption_site.move_erase(old_ind, new_ind);
        ReactiveSpeciesList::move_erase(old_ind, new_ind);
    }
    //! \brief Move component 'old_ind' to 'new_ind'
    void move_erase(index_t old_ind, index_t new_ind, const std::vector<index_t>& is_reactants_to_remove) override
    {
        m_sorption_site.move_erase(old_ind, new_ind);
        ReactiveSpeciesList::move_erase(old_ind, new_ind, is_reactants_to_remove);
    }
    //! \brief move the species at index 'old' to 'other_ind' in 'other'
    void move_erase_to(index_t old, SorbedList& other, index_t other_ind);
    //! @}

    //! \brief Add the aqueous species 'other_species' to 'k', to obtain a canonical system
    void canonicalize(
            index_t ind,
            const AqueousList& aqueous,
            index_t aqueous_ind,
            scalar_t coeff
            )
    {
        add_alien_species_to(ind, aqueous, aqueous_ind, coeff);
    }

    //! \brief Append the species in this list to the other list
    void append_to(SorbedList& other);

private:
    VectorSpeciesWrapper m_sorption_site;
};

} // end namespace database
} // end namespace specmicp

#endif //SPECMICP_DATABASE_SORBEDLIST_HPP
