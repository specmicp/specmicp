/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_AQUEOUSLIST_HPP
#define SPECMICP_DATABASE_AQUEOUSLIST_HPP

#include "specmicp_common/types.hpp"
#ifndef SPECMICP_DATABASE_SPECIES_HPP
#include "species.hpp"
#endif
#ifndef SPECMICP_DATABASE_IONICPARAMETERS_HPP
#include "ionic_parameters.hpp"
#endif

//! \file aqueous_list.hpp
//! \brief The list of aqueous species

namespace specmicp {

class AdimensionalSystem;

namespace database {

//! \struct AqueousValues
//! \brief Struct to initialize an aqueous species in the aqueous species list
//!
//! \ingroup database_species
struct AqueousValues {
    std::string label;
    scalar_t logk;
    IonicModelValues ionic_values;
};

//! \class AqueousList
//! \brief The aqueous species
//!
//! This is the list of aqueous species in the system
//!
//! \ingroup database_species
class AqueousList: public ReactiveSpeciesList
{
public:
    //! Initialize an empty list
    AqueousList() {}
    //! Initialize a list of 'siz' aqueous species with 'nb_components' components
    AqueousList(index_t size, index_t nb_components):
        ReactiveSpeciesList(size, nb_components),
        m_ionic_param(size)
    {}

    //! \name Size
    //! \brief Manage the size of the list
    // --------
    //! @{
    //! \brief Resize the list
    void resize(index_t size) override
    {
        ReactiveSpeciesList::resize(size);
        m_ionic_param.resize(size);
    }
    //! \brief Resize the list, adjust the number of components
    void resize(index_t size, index_t nb_components) override
    {
        ReactiveSpeciesList::resize(size, nb_components);
        m_ionic_param.resize(size);
    }
    //! @}

    //! \name Getter
    //! \brief Return values
    // ----------------------
    //! @{
    //! \brief Return the ionic size of component 'k'
    const scalar_t& a_debye(index_t k) const {
        return m_ionic_param.a_debye(k);
    }

    //! \brief Return the 'b-dot' parameter of component 'k'
    const scalar_t& b_debye(index_t k) const {
        return m_ionic_param.b_debye(k);
    }
    //! \brief Return the charge of component 'k'
    const scalar_t& charge(index_t k) const {
        return m_ionic_param.charge(k);
    }
    //! \brief Return the ionic model values of component 'k'
    IonicModelValues ionic_values(index_t k) const {
        return m_ionic_param.get_values(k);
    }
    //! @}

    //! \name Setter
    //! \brief Set values
    // -------------
    //! @{
    //! \brief Set the ionic model parameters
    void set_ionic_values(index_t k, const IonicModelValues& values) {
        m_ionic_param.set_values(k, values);
    }
    //! \brief Set the values
    //! \warning Do no set stoichiometric coefficients
    void set_values(index_t k, const AqueousValues& values) {
        set_label(k, values.label);
        set_logk(k, values.logk);
        m_ionic_param.set_values(k, values.ionic_values);
    }
    //! \brief Set the values
    //! \warning Do no set stoichiometric coefficients
    void set_values(index_t k, AqueousValues&& values) {
        set_label(k, std::move(values.label));
        set_logk(k, values.logk);
        m_ionic_param.set_values(k, std::move(values.ionic_values));
    }
    //! @}


    //! \name Move
    //! \brief Move species inside the list or to other lists
    // ------------
    //! @{
    //! \brief Move component 'old_ind' to 'new_ind'
    void move_erase(index_t old_ind, index_t new_ind) override
    {
        m_ionic_param.move_erase(old_ind, new_ind);
        ReactiveSpeciesList::move_erase(old_ind, new_ind);
    }
    //! \brief Move component 'old_ind' to 'new_ind'
    void move_erase(index_t old_ind, index_t new_ind, const std::vector<index_t>& is_reactants_to_remove) override
    {
        m_ionic_param.move_erase(old_ind, new_ind);
        ReactiveSpeciesList::move_erase(old_ind, new_ind, is_reactants_to_remove);
    }
    //! @}

    //! \brief Add the aqueous species 'other_species' to 'k', to obtain a canonical system
    void canonicalize(index_t k, index_t other_species, scalar_t coeff) {
        add_species_to(k, other_species, coeff);
    }


    friend class specmicp::AdimensionalSystem;

private:
    IonicModelParameters m_ionic_param;
};

} // end namespace database
} // end namespace specmicp

#endif //SPECMICP_DATABASE_AQUEOUSLIST_HPP
