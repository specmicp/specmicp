/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "mineral_list.hpp"
#include "specmicp_database/errors.hpp"

namespace specmicp {
namespace database {

void MineralList::move_erase_to(
        index_t ind,
        MineralList& other,
        index_t other_ind
        )
{
    ReactiveSpeciesList::move_erase_to(ind, other, other_ind);
    m_molar_volume.move_erase_to(ind, other.m_molar_volume, other_ind);
}


void MineralList::append_to(MineralList &other)
{
    if (nb_components() != other.nb_components()) {
        throw db_invalid_data("The two list of solid are not compatible.");
    }

    const auto o_size = other.size();
    const auto t_size = size();
    const auto new_size = o_size+t_size;
    other.resize(new_size);
    for (auto i=0; i<t_size; ++i)
    {
        move_erase_to(i, other, o_size+i);
    }
    resize(0);
    other.set_valid();
    set_valid();
}

}   // end namespace database
}   // end namespace specmicp
