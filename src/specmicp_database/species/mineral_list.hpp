/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_SPECIES_MINERALLIST_HPP
#define SPECMICP_DATABASE_SPECIES_MINERALLIST_HPP

#include "specmicp_common/types.hpp"
#ifndef SPECMICP_DATABASE_SPECIES_HPP
#include "species.hpp"
#endif
#ifndef SPECMICP_DATABASE_AQUEOUSLIST_HPP
#include "aqueous_list.hpp"
#endif

//! \file mineral_list.hpp
//! \brief A list of solid phases

namespace specmicp {
namespace database {

//! \struct MineralValue
//! Initializer struct for a solid phase
//!
//! \ingroup database_species
struct MineralValue {
    std::string label;     //!< The label
    scalar_t logk;         //!< log_10 of the equilibrium constant
    scalar_t molar_volume; //!< The molar volume
};

//! \class MineralList
//! \brief A list of solid phases
//!
//! In addition to the basic features of a 'ReactiveSpeciesList'
//! this class handles the molar volume of the solid phases.
//!
//! \ingroup database_species
class MineralList: public ReactiveSpeciesList
{
public:
    //! \brief Default constructor
    MineralList() {}
    //! \brief Constructor
    //!
    //! \param size number of minerals
    //! \param nb_component number of component in the database
    MineralList(index_t size, index_t nb_component):
        ReactiveSpeciesList(size, nb_component),
        m_molar_volume(size)
    {}
    // Getter
    // ------
    //! \brief Return the molar volume
    const scalar_t& molar_volume(index_t k) const {
        return m_molar_volume(k);
    }

    // Setter
    // ------
    //! \brief Initialise the solid phase at index_t k
    //! \warning Do no set stoichiometric coefficients
    void set_values(index_t k, const MineralValue& values) {
        set_label(k, values.label);
        set_logk(k, values.logk);
        m_molar_volume.set_value(k, values.molar_volume);
    }
    //! \brief Initialise the solid phase at index_t k
    //! \warning Do no set stoichiometric coefficients
    void set_values(index_t k, MineralValue&& values) {
        set_label(k, std::move(values.label));
        set_logk(k, std::move(values.logk));
        m_molar_volume.set_value(k, std::move(values.molar_volume));
    }

    // Move
    // ----

    //! \brief Move the solid phase at index 'old_ind' to 'new_ind'
    void move_erase(index_t old_ind, index_t new_ind) override
    {
        ReactiveSpeciesList::move_erase(old_ind, new_ind);
        m_molar_volume.move_erase(old_ind, new_ind);
    }
    //! \brief Move the solid phase at index 'old_ind' to 'new_ind' and removes components
    void move_erase(
            index_t old_ind,
            index_t new_ind,
            const std::vector<index_t>& is_reactants_to_remove
            ) override
    {
        ReactiveSpeciesList::move_erase(old_ind, new_ind, is_reactants_to_remove);
        m_molar_volume.move_erase(old_ind, new_ind);
    }
    //! \brief Move solid phase 'ind' to 'other_ind' in the 'other' list
    void move_erase_to(
            index_t ind,
            MineralList& other,
            index_t other_ind
            );

    //! \brief Append the phases in this list to the other list
    void append_to(MineralList& other);

    // Resize
    // ------

    //! \brief Resize the list
    void resize(index_t size) override
    {
        ReactiveSpeciesList::resize(size);
        m_molar_volume.resize(size);
    }
    //! \brief Resize the list and the stoichiometric coefficients matrix
    void resize(index_t size, index_t nb_component) override
    {
        ReactiveSpeciesList::resize(size, nb_component);
        m_molar_volume.resize(size);
    }

    // Misc
    // ----


    //! \brief Add the aqueous species 'other_species' to 'k', to obtain a canonical system
    void canonicalize(
            index_t ind,
            const AqueousList& aqueous,
            index_t aqueous_ind,
            scalar_t coeff
            )
    {
        add_alien_species_to(ind, aqueous, aqueous_ind, coeff);
    }


private:
    VectorSpeciesWrapper m_molar_volume;
};

} // end namespace database
} // end namespace specmicp

#endif // SPECMICP_DATABASE_SPECIES_MINERALLIST_HPP
