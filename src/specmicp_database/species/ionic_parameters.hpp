/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_IONICPARAMETERS_HPP
#define SPECMICP_DATABASE_IONICPARAMETERS_HPP

#ifndef SPECMICP_DATABASE_BASEWRAPPER_HPP
#include "base_wrapper.hpp"
#endif

//! \file ionic_parameters.hpp
//! \brief The ionic model parameters
//!
//! These parameters are needed for all (primary and secondary)
//! aqueous species.

namespace specmicp {

class AdimensionalSystem;

namespace database {

//! \struct IonicModelValues
//! The set of main values for the ionic model
//! This is used for the initialisation of an aqueous species.
//!
//! \ingroup database_species
struct IonicModelValues
{
    scalar_t charge {0.0}; //!< Charge of the species
    scalar_t a_debye {0.0}; //!< Ionic radius
    scalar_t b_debye {0.0}; //!< Correction parameter

    //! Default Constructor
    IonicModelValues() {}
    //! Constructor with initialization
    IonicModelValues(scalar_t acharge, scalar_t adebye, scalar_t bdebye):
        charge(acharge),
        a_debye(adebye),
        b_debye(bdebye)
    {}

};

//! \class IonicModelParameters
//! \brief Class that contains the ionic model parameters
//!
//! This class is used for all (primary and secondary) aqueous species.
//!
//! \ingroup database_species
class IonicModelParameters: public MatrixSpeciesWrapper
{
public:
    static constexpr index_t charge_ind = 0; //!< Column index of the charge
    static constexpr index_t adebye_ind = 1; //!< Column index of the ionic size parameter
    static constexpr index_t bdebye_ind = 2; //!< Column index of the b-dot parameter

    //! \brief Default constructor, size=0
    IonicModelParameters() {}
    //! \brief Constructor
    IonicModelParameters(index_t size):
        MatrixSpeciesWrapper(size, 3)
    {}

    void resize_columns(index_t colums) = delete; //!< The number of columns is fixed
    void resize_matrix(index_t rows, index_t cols) = delete; //!< The number of columns is fixed

    //! \brief Return the charge
    const scalar_t& charge(index_t ind) const {return get_value(ind, charge_ind);}
    //! \brief Return the ionic size parameter
    const scalar_t& a_debye(index_t ind) const {return get_value(ind, adebye_ind);}
    //! \brief Return the b-dot parameter
    const scalar_t& b_debye(index_t ind) const {return get_value(ind, bdebye_ind);}

    //! \brief Set the values
    void set_values(index_t ind, const IonicModelValues& values) {
        set_value(ind, charge_ind, values.charge);
        set_value(ind, adebye_ind, values.a_debye);
        set_value(ind, bdebye_ind, values.b_debye);
    }
    //! \brief Return the ionic value
    IonicModelValues get_values(index_t ind) const {
        return IonicModelValues(charge(ind), a_debye(ind), b_debye(ind));
    }


    friend class specmicp::AdimensionalSystem;
};

} // end namespace database
} // end namespace specmicp

#endif //SPECMICP_DATABASE_IONICPARAMETERS_HPP
