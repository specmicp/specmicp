/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_CONFIG_DATABASE_HPP
#define SPECMICP_DATABASE_CONFIG_DATABASE_HPP

// This file is used at compilation time to configure basic features of the database
// It should not be included in header files

// they are advanced usage, and not offered as an option through CMAKE

// This is precision used to test if two charges are the same when reading the database
#ifndef EPS_TEST_CHARGE
#define EPS_TEST_CHARGE 1e-5
#endif // EPS_TEST_CHARGE

// This is the precision used to test if two compositions are the same when reading the database
#ifndef EPS_TEST_COMPOSITION
#define EPS_TEST_COMPOSITION EPS_TEST_CHARGE
#endif // EPS_TEST_COMPOSITION

// This is the label of water
#ifndef LABEL_WATER
#define LABEL_WATER "H2O"
#endif // LABEL_WATER

// This is the label of the electron
#ifndef LABEL_ELECTRON
#define LABEL_ELECTRON "E[-]"
#endif // LABEL_ELECTRON

#endif // SPECMICP_DATABASE_CONFIG_DATABASE_HPP
