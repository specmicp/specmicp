/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_READERCOMMON_HPP
#define SPECMICP_DATABASE_READERCOMMON_HPP

//! \file reader_common.hpp
//! \brief Common functions for database readers

#include "specmicp_common/types.hpp"

#include <map>
#include <string>

namespace specmicp {
namespace database {

//! \brief Parse an equation
void parse_equation(const std::string& equation, std::map<std::string, scalar_t> &compo);

//! \brief Get the charge of a species by parsing the label
//!
//! Examples : label -> charge
//!     - neutral -> 0
//!     - neutral[] -> 0
//!     - charge[+] -> +1
//!     - charge[-1] -> -1
//!     - charge[+2] -> +2
//!     - charge[2-] -> -2
scalar_t charge_from_label(const std::string& label);

//! \brief A map giving the stoichiometric coefficient of each element in a compound
using element_map = std::map<std::string, scalar_t>;

//! \brief Find the composition (in element) from a label
void element_composition_from_label(std::string label, element_map& compo);

//! \brief Add an element map to another
//!
//! \brief to_update the map that is being updated
//! \brief to_add the map used to update to_update
//! \brief coeff the stoichiometric coefficient
void add_to_element_map(element_map& to_update, const element_map& to_add, const scalar_t coeff);

} //end namespace database
} //end namespace specmicp

#endif // SPECMICP_DATABASE_READERCOMMON_HPP
