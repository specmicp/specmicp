/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "appender.hpp"
#include "yaml_reader.hpp"

#include "specmicp_common/io/yaml.hpp"

#include <yaml-cpp/yaml.h>
#include <iostream>

namespace specmicp {
namespace database {

void DataAppender::add_gas(const std::string& yaml_input, bool check_compo)
{
    std::istringstream input(yaml_input);
    add_gas(input, check_compo);
}

void DataAppender::add_gas(std::istream& yaml_input, bool check_compo)
{
    DataReaderYaml reader(data, check_compo);

    YAML::Node root = io::parse_yaml(yaml_input);

    GasList gas;
    reader.parse_gas(root, gas);

    gas.append_to(data->gas);
}


void DataAppender::add_minerals(const std::string& yaml_input, bool check_compo)
{
    std::istringstream input(yaml_input);
    add_minerals(input, check_compo);
}

void DataAppender::add_minerals(std::istream& yaml_input, bool check_compo)
{
    DataReaderYaml reader(data, check_compo);
    YAML::Node root = io::parse_yaml(yaml_input);

    MineralList minerals;
    MineralList minerals_kinetic;
    reader.parse_minerals(root, minerals, minerals_kinetic);

    minerals.append_to(data->minerals);
    minerals_kinetic.append_to(data->minerals_kinetic);
}


void DataAppender::add_sorbed(const std::string& yaml_input, bool check_compo)
{
    std::istringstream input(yaml_input);
    add_sorbed(input, check_compo);
}

void DataAppender::add_sorbed(std::istream& yaml_input, bool check_compo)
{
    DataReaderYaml reader(data, check_compo);
    YAML::Node root = io::parse_yaml(yaml_input);

    SorbedList sorbed;
    reader.parse_sorbed(root, sorbed);

    sorbed.append_to(data->sorbed);
}

void DataAppender::add_compounds(const std::string& yaml_input, bool check_compo)
{
    std::istringstream input(yaml_input);
    add_compounds(input, check_compo);
}

void DataAppender::add_compounds(std::istream& yaml_input, bool check_compo)
{
    DataReaderYaml reader(data, check_compo);
    YAML::Node root = io::parse_yaml(yaml_input);

    CompoundList compounds;
    reader.parse_compounds(root, compounds);

    compounds.append_to(data->compounds);
}

} // end namespace database
} // end namespace specmicp
