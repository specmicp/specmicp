/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_SELECTOR_HPP
#define SPECMICP_DATABASE_SELECTOR_HPP

//! \file selector.hpp
//! \brief Select components in the database

#include <vector>
#include "module.hpp"

namespace specmicp {
namespace database {

//! \brief Select components in the database
//!
//! This class reduce the database by removing some components
//! and all the species associated with these components.
class DatabaseSelector: public DatabaseModule
{
public:
    //! \brief Constructor
    DatabaseSelector(RawDatabasePtr thedata):
        DatabaseModule(thedata),
        m_is_component_to_remove(thedata->nb_component(), 0)
    {}

    //! \brief Remove compoment in the list "id_component_to_remove"
    //!
    //! \param id_component_to_remove list of id of the components to remove
    void remove_component(const std::vector<index_t>& id_component_to_remove);
    //! \brief keep only components in the "id_to_keep" list
    //!
    //! \param id_to_keep list of id of the components to keep in the database
    void keep_only_component(const std::vector<index_t>& id_to_keep);

    //! \brief Remove all gas phase
    void remove_all_gas();
    //! \brief Remove all sorbed species
    void remove_all_sorbed();
    //! \brief Remove all compounds
    void remove_all_compounds();

    //! \brief Remove some specific aqueous species
    //!
    //! This method should not be used in normal usage.
    //! Removing the aqueous species will break the consistency of the database
    //!
    //! \param id_aqueous list of id of the aqueous to remove
    void remove_aqueous(const std::vector<index_t>& id_aqueous);
private:

    //! \brief Analyse wich component should be removed
    void analyse_component(const std::vector<index_t>& id_component_to_remove);

    void select_secondary(
            ReactiveSpeciesList* toselect,
            const std::vector<index_t>& id_component_to_remove
    );

    //! \brief Select the correct aqueous species
    void select_aqueous(const std::vector<index_t>& id_component_to_remove);

    //! \brief Select the correct minerals
    void select_minerals(const std::vector<index_t>& id_component_to_remove);
    //! \brief Select the correct minerals governed by kinetics
    void select_minerals_kinetic(const std::vector<index_t>& id_component_to_remove);

    //! \brief Select the gas phases
    void select_gas(const std::vector<index_t>& id_component_to_remove);

    //! \brief Select the sorbed species
    void select_sorbed(const std::vector<index_t>& id_component_to_remove);

    //! \brief Select the compounds
    void select_compounds(const std::vector<index_t>& id_component_to_remove);

    //! \brief Select components
    //!
    //! This should be the last operation
    void select_components();

    //! \brief Return the number of component to keep in the db
    uindex_t nb_component_to_keep() const {return m_nb_component_to_keep;}

    std::vector<index_t> m_is_component_to_remove; // -1 if component will be removed, else new index
    uindex_t m_nb_component_to_keep; // total number of component after the purging is done
};

} // end namespace database
} // end namespace specmicp

#endif // SPECMICP_DATABASE_SELECTOR_HPP
