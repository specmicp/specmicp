/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_DATABASE_ERRORS_HPP
#define SPECMICP_DATABASE_ERRORS_HPP

#include <vector>
#include <stdexcept>

#include "specmicp_common/types.hpp"

//! \file errors.hpp database errors definitions

namespace specmicp {
namespace database {

// Error used when parsing and handling the database
// -------------------------------------------------

//! \brief This error is thrown when a label does not exist
class db_invalid_label: public std::invalid_argument
{
public:
    //! \brief Constructor
    db_invalid_label(const std::string &msg):
        std::invalid_argument("Unknown label : " + msg)
    {}
};

//! \brief This error is thrown when a bad syntax is detected in the database
class db_invalid_syntax : public std::invalid_argument
{
public:
    //! \brief Constructor
    db_invalid_syntax (const std::string &msg):
        std::invalid_argument("Invalid syntax : " + msg)
    {}
};

//! \brief This error is thrown when a bad syntax is detected in the database
class db_invalid_data : public std::invalid_argument
{
public:
    //! \brief Constructor
    db_invalid_data (const std::string &msg):
        std::invalid_argument("Invalid syntax : " + msg)
    {}
};

//! \brief This error is thrown when the database is not as expected
class invalid_database : public std::invalid_argument
{

public:
    //! \brief Constructor
    invalid_database (const std::string &msg):
        std::invalid_argument("Invalid Database : " + msg)
    {}
};

//! \brief This error is thrown when a non initialised value is wanted
class db_noninitialized_value : public std::runtime_error
{
public:
    //! \brief Constructor
    db_noninitialized_value (const std::string &value, const std::string &species):
        std::runtime_error("Value was not initialized : " + value +",  for species : "+species+".")
    {}
};

//! \brief This error is raised when the species already exist
class db_species_already_exist: public std::invalid_argument
{
public:
    //! \brief Constructor
    db_species_already_exist(std::string species, std::string destination):
        std::invalid_argument("The species '" + species + "' already exist in " + destination + ".")
    {}
};


//! \brief Error be raised when a given species is invalid
class SPECMICP_DLL_PUBLIC InvalidSpecies: public std::invalid_argument
{
public:
    //! \brief Constructor
    InvalidSpecies(const std::string& label, const std::string& msg):
        std::invalid_argument("Invalid species '"+label+"' : " +msg)
    {}
};

//! \brief Error be raised when a species is not a valid component
class SPECMICP_DLL_PUBLIC InvalidComponent: public InvalidSpecies
{
public:
    //! \brief Constructor
    InvalidComponent(const std::string& label):
        InvalidSpecies(label, "is not a component.")
    {}
};

//! \brief Error be raised when a species is not a valid aqueous species
class SPECMICP_DLL_PUBLIC InvalidAqueous: public InvalidSpecies
{
public:
    //! \brief Constructor
    InvalidAqueous(const std::string& label):
        InvalidSpecies(label, "is not a secondary aqueous species.")
    {}
};

//! \brief Error be raised when a species is not a valid aqueous species
class SPECMICP_DLL_PUBLIC InvalidGenericAqueousSpecies: public InvalidSpecies
{
public:
    //! \brief Constructor
    InvalidGenericAqueousSpecies(const std::string& label):
        InvalidSpecies(label, "is not a generic aqueous species.")
    {}
};

//! \brief Error be raised when a species is not a valid compound
class SPECMICP_DLL_PUBLIC InvalidCompound: public InvalidSpecies
{
public:
    //! \brief Constructor
    InvalidCompound(const std::string& label):
        InvalidSpecies(label, "is not a compound.")
    {}
};

//! \brief Error be raised when a species is not a valid solid phase
class SPECMICP_DLL_PUBLIC InvalidSolidPhase: public InvalidSpecies
{
public:
    //! \brief Constructor
    InvalidSolidPhase(const std::string& label):
        InvalidSpecies(label, "is not a valid solid phase.")
    {}
};

//! \brief Error be raised when a species is not a valid mineral (at equilibrium)
class SPECMICP_DLL_PUBLIC InvalidMineral: public InvalidSpecies
{
public:
    //! \brief Constructor
    InvalidMineral(const std::string& label):
        InvalidSpecies(label, "is not a valid a mineral at equilibrium.")
    {}
};

//! \brief Error be raised when a species is not a valid mineral governed by kinetics
class SPECMICP_DLL_PUBLIC InvalidMineralKinetics: public InvalidSpecies
{
public:
    //! \brief Constructor
    InvalidMineralKinetics(const std::string& label):
        InvalidSpecies(label, "is not a valid a mineral governed by kinetics.")
    {}
};

//! \brief Error raised when a species is not a valid gas
class SPECMICP_DLL_PUBLIC InvalidGas: public InvalidSpecies
{
public:
    //! \brief Constructor
    InvalidGas(const std::string& label):
        InvalidSpecies(label, "is not a valid gas")
    {}
};

//! \brief Error raised when a species is not a valid sorbed species
class SPECMICP_DLL_PUBLIC InvalidSorbed: public InvalidSpecies
{
public:
    //! \brief Constructor
    InvalidSorbed(const std::string& label):
        InvalidSpecies(label, "is not a sorbed species")
    {}
};


} // end namespace database
} // end namespace specmicp

#endif // SPECMICP_DATABASE_ERRORS_HPP
