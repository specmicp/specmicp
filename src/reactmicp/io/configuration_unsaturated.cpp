/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "specmicp_common/io/safe_config.hpp"

#include "configuration_unsaturated.hpp"
#include "specmicp_database/data_container.hpp"
#include "specmicp/adimensional/adimensional_system_solver_structs.hpp"
#include "specmicp/io/configuration.hpp"
#include "reactmicp/systems/unsaturated/boundary_conditions.hpp"
#include "reactmicp/systems/unsaturated/equilibrium_stagger.hpp"
#include "specmicp_common/string_algorithms.hpp"

#include "specmicp_common/log.hpp"

#include "specmicp/io/print.hpp"

#define S_BCS_A_GASNODE "gas_nodes"
#define S_BCS_A_FIXEDNODE "fixed_nodes"
#define S_BCS_A_NOPRECIPITATION "no_precipitation"

#define S_BCS_SS_CONSTRAINTS "constraints"
#define S_BCS_SS_CONSTRAINTS_A_DEFAULT "default"

#define S_BCS_SS_CONSTRAINTS_A_OTHERS "others"
#define S_BCS_SS_CONSTRAINTS_A_NAME "name"
#define S_BCS_SS_CONSTRAINTS_A_FORK_FROM "fork_from"
#define S_BCS_SS_CONSTRAINTS_A_NODE "nodes"


#define S_OPTIONS_SS_DEFAULT "default"
#define S_OPTIONS_SS_OTHERS "others"
#define S_OPTIONS_SS_OTHERS_A_NAME "name"
#define S_OPTIONS_SS_OTHERS_A_FORK_FROM "fork_from"
#define S_OPTIONS_SS_OTHERS_A_NODE "nodes"

namespace specmicp {

using reactmicp::systems::unsaturated::BoundaryConditions;
using reactmicp::systems::unsaturated::EquilibriumOptionsVector;

namespace io {


static void configure_bcs_constraints(
        BoundaryConditions* bcs,
        const database::DataContainer * const raw_data,
        YAMLConfigHandle&& conf);

//! \brief Configure the boundary condition
std::shared_ptr<BoundaryConditions>
configure_unsaturated_boundary_conditions(
        uindex_t nb_nodes,
        const database::DataContainer * const raw_data,
        YAMLConfigHandle&& configuration
        )
{
    auto bcs = BoundaryConditions::make(nb_nodes, raw_data->nb_component());
    if (configuration.has_node(S_BCS_A_FIXEDNODE)) {
        std::vector<uindex_t> fixed_nodes =
                configuration.list_to_vector<uindex_t>(S_BCS_A_FIXEDNODE);
        std::ostringstream msg;
        msg << "Fixed nodes : ";
        for (auto node : fixed_nodes) {
            bcs->add_fixed_node(node);
            msg << node << "; ";
        }
        SPC_CONF_LOG << msg.str();
    }
    if (configuration.has_node(S_BCS_A_GASNODE)) {
        const std::vector<uindex_t> gas_nodes =
                configuration.list_to_vector<uindex_t>(S_BCS_A_GASNODE);
        std::ostringstream msg;
        msg << "Gas nodes : ";
        for (auto node: gas_nodes) {
            bcs->add_gas_node(node);
            msg << node << "; ";
        }
        SPC_CONF_LOG << msg.str();
    }
    if (configuration.has_node(S_BCS_A_NOPRECIPITATION)) {
        std::ostringstream msg;
        msg << "No precipitation minerals : ";
        std::vector<std::string> list_minerals =
                configuration.list_to_vector<std::string>(S_BCS_A_NOPRECIPITATION);
        for (const auto& m_label: list_minerals) {
            index_t m_id = raw_data->get_id_mineral(m_label);
            if (m_id == no_species) {
                configuration.report_error(
                            YAMLConfigError::InvalidArgument,
                            "'"+m_label+"' is not a valid solid phase"
                            );
            }
            bcs->add_mineral_no_precipitation(m_id);
            msg << m_label << "; ";
        }
        SPC_CONF_LOG << msg.str();
    }
    if (configuration.has_section(S_BCS_SS_CONSTRAINTS)) {
        configure_bcs_constraints(bcs.get(),
                                  raw_data,
                                  configuration.get_section(S_BCS_SS_CONSTRAINTS)
                                  );
    }
    return bcs;
}


void configure_bcs_constraints(
        BoundaryConditions* bcs,
        const database::DataContainer * const raw_data,
        YAMLConfigHandle&& conf
        )
{
    // default constraints
    if (conf.has_section(S_BCS_SS_CONSTRAINTS_A_DEFAULT)) {
        configure_specmicp_constraints(
                    bcs->get_constraint("default"),
                    raw_data,
                    conf.get_section(S_BCS_SS_CONSTRAINTS_A_DEFAULT)
                    );
    }
    // other constraints
    if (conf.has_section(S_BCS_SS_CONSTRAINTS_A_OTHERS))
    {
        auto others = conf.get_section(S_BCS_SS_CONSTRAINTS_A_OTHERS);
        for (uindex_t ind=0; ind<others.size(); ++ind)
        {
            auto subconf = others.get_section(ind);
            const auto name = subconf.get_required_attribute<std::string>(
                        S_BCS_SS_CONSTRAINTS_A_NAME
                        );
            if (subconf.has_attribute(S_BCS_SS_CONSTRAINTS_A_FORK_FROM))
            {
                auto old_name = subconf.get_attribute<std::string>(
                            S_BCS_SS_CONSTRAINTS_A_FORK_FROM
                            );
                if (not bcs->has_constraint(old_name)) {
                    subconf.report_error(
                                YAMLConfigError::InvalidArgument,
                                "'" + old_name + "' is not an "
                                "existing constraint"
                                );
                }
                bcs->fork_constraint(old_name, name);
            }
            else {
                bcs->add_constraint(name);
            }
            if (subconf.has_node(S_BCS_SS_CONSTRAINTS_A_NODE)) {
                auto nodes = subconf.list_to_vector<uindex_t>(
                            S_BCS_SS_CONSTRAINTS_A_NODE);
                for (const auto& node: nodes) {
                    bcs->set_constraint(node, name);
                }
            }
            configure_specmicp_constraints(
                        bcs->get_constraint(name),
                        raw_data,
                        std::move(subconf));
        }
    }
}


std::shared_ptr<EquilibriumOptionsVector>
configure_unsaturated_equilibrium_options(
        uindex_t nb_nodes,
        const units::UnitsSet& the_units,
        YAMLConfigHandle&& configuration
        )
{
    auto opts = EquilibriumOptionsVector::make(nb_nodes);
    if (configuration.has_section(S_OPTIONS_SS_DEFAULT))
    {
        auto& def_opts = opts->get("default");
        configure_specmicp_options(
                    def_opts,
                    the_units,
                    configuration.get_section(S_OPTIONS_SS_DEFAULT));
        SPC_CONF_LOG << "Default SpecMiCP options : ";
        std::ostringstream msg;
        io::print_specmicp_options(msg, def_opts);
        SPC_CONF_LOG << msg.str() << "\n" << SPC_CONF_LOG_HLINE;


    }
    if (configuration.has_section(S_OPTIONS_SS_OTHERS))
    {
        auto subconf = configuration.get_section(S_OPTIONS_SS_OTHERS);
        const auto size = subconf.size();
        for (uindex_t ind=0; ind<size; ++ind) {
            io::YAMLConfigHandle conf = subconf.get_section(ind);
            const auto name = conf.get_required_attribute<std::string>(
                        S_OPTIONS_SS_OTHERS_A_NAME);

            if (conf.has_attribute(S_OPTIONS_SS_OTHERS_A_FORK_FROM)) {
                const auto fork_from = conf.get_attribute<std::string>(
                            S_OPTIONS_SS_OTHERS_A_FORK_FROM);
                if (not opts->has_value(fork_from)) {
                    conf.report_error(
                                YAMLConfigError::InvalidArgument,
                                "'" + fork_from + "' is not an "
                                "existing set of options. \n"
                                );
                }
                opts->fork(fork_from, name);
            } else {
                opts->push_back_cache(name, AdimensionalSystemSolverOptions());
            }
            if (conf.has_node(S_OPTIONS_SS_OTHERS_A_NODE)) {
                const auto nodes = conf.list_to_vector<uindex_t>(
                            S_OPTIONS_SS_OTHERS_A_NODE);
                for (const auto& node: nodes) {
                    opts->set(node, name);
                }
            }
            auto& new_opts = opts->get(name);
            configure_specmicp_options(
                        new_opts, the_units, std::move(conf));

            SPC_CONF_LOG << "Extra SpecMiCP options : " << name;
            std::ostringstream msg;
            io::print_specmicp_options(msg, new_opts);
            SPC_CONF_LOG << msg.str() << "\n" << SPC_CONF_LOG_HLINE;
        }

    }
    return opts;
}

} //end namespace io
} //end namespace specmicp
