/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_IO_SATURATEDREACT_HPP
#define SPECMICP_IO_SATURATEDREACT_HPP

//! \file saturated_react.hpp
//! \brief Print information about the saturated reactive transport module

#include "specmicp_common/types.hpp"
#include "specmicp_common/io/csv_formatter.hpp"

#include "specmicp_common/physics/units.hpp"

#include <iosfwd>
#include <memory>

#include <functional>

namespace specmicp {

// forward declarations
class AdimensionalSystemSolutionExtractor;

namespace database
{
    struct DataContainer;
} //end namespace database
//! \brief Shared pointer to the databse
using RawDatabasePtr = std::shared_ptr<database::DataContainer>;

namespace mesh {
class Mesh1D;
using Mesh1DPtr = std::shared_ptr<Mesh1D>;
} //end namespace mesh

namespace reactmicp {
namespace solver {

class VariablesBase;
//! \brief Shared pointer to the variables
using VariablesBasePtr = std::shared_ptr<VariablesBase>;

} //end namespace solver

namespace systems {
namespace satdiff {

class SaturatedVariables;
//! \brief Shared pointer to the saturated variables
using SaturatedVariablesPtr =  std::shared_ptr<SaturatedVariables>;

} //end namespace satdiff
} //end namespace systems
} //end namespace reactmicp

namespace io {

using namespace specmicp::reactmicp::systems::satdiff;

//! \brief Print the total aqueous concentrations in the CSV format in the file 'filepath'
void SPECMICP_DLL_PUBLIC print_components_total_aqueous_concentration(
        RawDatabasePtr the_database,
        SaturatedVariablesPtr variables,
        mesh::Mesh1DPtr the_mesh,
        const units::UnitsSet& units_set,
        const std::string& filepath
        );

//! \brief Print the total solid concentrations in the CSV format in the file 'filepath'
void SPECMICP_DLL_PUBLIC print_components_total_solid_concentration(
        RawDatabasePtr the_database,
        SaturatedVariablesPtr variables,
        mesh::Mesh1DPtr the_mesh,
        const units::UnitsSet& units_set,
        const std::string& filepath
        );

//! \brief Print the solid phases profiles in 'filepath'
void SPECMICP_DLL_PUBLIC print_minerals_profile(
        RawDatabasePtr the_database,
        SaturatedVariablesPtr variables,
        mesh::Mesh1DPtr the_mesh,
        const units::UnitsSet& units_set,
        const std::string& filepath
        );


//! \brief Output Nodal variables requested by the user
//!
//! This class can be used during a ReacMiCP simulation to
//! output nodal variables in CSV files
class SPECMICP_DLL_PUBLIC OutputNodalVariables
{
public:
    //! \brief Constructor
    OutputNodalVariables(RawDatabasePtr the_database,
                         mesh::Mesh1DPtr the_mesh,
                         const units::UnitsSet& the_units);

    ~OutputNodalVariables();

    OutputNodalVariables(OutputNodalVariables&) = delete;
    OutputNodalVariables* operator= (OutputNodalVariables&) = delete;

    //! \brief The total aqueous concentration of 'component' will be saved in 'filepath'
    void register_total_aqueous_concentration(index_t component, const std::string& filepath);
    //! \brief The total solid concentration of 'component' will be saved in 'filepath'
    void register_total_solid_concentration(index_t component, const std::string& filepath);
    //! \brief The total concentration of 'component' will be saved in 'filepath'
    void register_total_concentration(index_t component, const std::string& filepath);
    //! \brief The porosity will be saved in 'filepath'
    void register_porosity(const std::string& filepath);
    //! \brief The diffusion_coefficient will be save in 'filepath'
    void register_diffusion_coefficient(const std::string& filepath);

    //! \brief The molality of 'component' will be saved in 'filepath'
    void register_molality_component(index_t component, const std::string& filepath);
    //! \brief The pH will be saved in 'filepath'
    void register_pH(const std::string& filepath);
    //! \brief The volume fraction of 'mineral' will be saved in 'filepath'
    void register_volume_fraction_mineral(index_t mineral, const std::string& filepath);
    //! \brief The saturation index of mineral kinetic will be saved in 'filepath'
    void register_saturation_index_mineral_kinetic(
            index_t mineral_kinetic,
            const std::string& filepath
            );
    //! \brief A function that return a scalar from a node in a variables
    using custom_f = std::function<scalar_t (index_t, const SaturatedVariablesPtr&)> ;
    //! \brief Register a custom function
    void register_custom_function(
            custom_f getter_function,
            const std::string& filepath,
            const std::string& msg
            );

    //! \brief Output the variables in their respective files
    void output(scalar_t time, reactmicp::solver::VariablesBasePtr var);

    //! \brief Return the output function for the ReactMiCP solver
    std::function<void (scalar_t, reactmicp::solver::VariablesBasePtr)> get_output_for_reactmicp();

    //! \brief Return the database
    RawDatabasePtr get_database();

private:
    //! \brief Implementation details for OutputNodalVariables
    //!
    //! \internal
    struct OutputNodalVariablesImpl;
    std::unique_ptr<OutputNodalVariablesImpl> m_impl;
};

} // end namespace io
} // end namespace specmicp

#endif // SPECMICP_IO_SATURATEDREACT_HPP
