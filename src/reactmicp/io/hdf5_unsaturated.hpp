/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_IO_REACTMICP_HDF5_UNSATURATED_HPP
#define SPECMICP_IO_REACTMICP_HDF5_UNSATURATED_HPP

//! \file reactmicp/io/hdf5_unsaturated.hpp
//! \brief HDF5 formatter for the unsaturated system

#include "specmicp_common/types.hpp"
#include "specmicp_common/pimpl_ptr.hpp"

#include "dfpm/meshes/mesh1dfwd.hpp"

#include <memory>

namespace specmicp {

struct AdimensionalSystemSolution;

namespace units {
     struct UnitsSet;
} //end namespace units

namespace reactmicp {
namespace systems {
namespace unsaturated {

class UnsaturatedVariables;

} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp

namespace io {

class HDF5File;
class HDF5Timesteps;

//! \brief Save the unsaturated set of variables in HDF5 format
//!
class SPECMICP_DLL_PUBLIC UnsaturatedHDF5Saver
{
    using UnsaturatedVariables = reactmicp::systems::unsaturated::UnsaturatedVariables;
public:
    //! \brief Constructor
    UnsaturatedHDF5Saver(
            const std::string& filename,
            std::shared_ptr<UnsaturatedVariables> vars,
            const units::UnitsSet& the_units
            );
    ~UnsaturatedHDF5Saver();


    //! \brief Save the variables
    //!
    //! All the values will be saved in a new group 'section/name'
    void save_timestep(scalar_t timestep);

private:
    struct SPECMICP_DLL_LOCAL UnsaturatedHDF5SaverImpl;
    //! \brief implementation details
    //! \internal
    std::unique_ptr<UnsaturatedHDF5SaverImpl> m_impl;

    // delete copy and assignement operator, no need for them
    UnsaturatedHDF5Saver(const UnsaturatedHDF5Saver& other) = delete;
    UnsaturatedHDF5Saver& operator= (const UnsaturatedHDF5Saver& other) = delete;
};

//! \brief Read saved data from an HDF5 file
class SPECMICP_DLL_PUBLIC UnsaturatedHDF5Reader
{
public:

    //UnsaturatedHDF5Reader(const HDF5File& file);
    //! \brief Constructor
    UnsaturatedHDF5Reader(const std::string& filepath);
    ~UnsaturatedHDF5Reader();
    //! \brief Return the timestep list
    const HDF5Timesteps& get_timesteps() const;
    //! \brief Return the units used in the computation
    units::UnitsSet get_units();
    //! \brief Return a SpecMiCP solution
    AdimensionalSystemSolution get_adim_solution(
            std::string timestep,
            index_t node
            );
    //! \brief Return a SpecMiCP solution
    AdimensionalSystemSolution get_adim_solution(
            scalar_t timestep,
            index_t node
            );

    //! \brief Return a main variable against x
    Vector main_variable_vs_x(
            scalar_t timestep,
            const std::string& component,
            const std::string& variable
            );
    //! \brief Return a main variable against x
    Vector main_variable_vs_x(
            const std::string& timestep,
            const std::string& component,
            const std::string& variable
            );
    //! \brief Return a main variable against t
    Vector main_variable_vs_t(
            index_t node,
            const std::string& component,
            const std::string& variable
            );
    //! \brief Return a transport variable against x
    Vector transport_variable_vs_x(
            const std::string& timestep,
            const std::string& variable
            );
    //! \brief Return a transport variable against x
    Vector transport_variable_vs_x(
            scalar_t timestep,
            const std::string& variable
            );
    //! \brief Return a transport variable against t
    Vector transport_variable_vs_t(
            index_t node,
            const std::string& variable
            );

    //! \brief Initialize variables from a saved timestep
    //!
    //! The Mesh and the database must already be ok
    void initialize_variables(
            scalar_t timestep,
            reactmicp::systems::unsaturated::UnsaturatedVariables* vars
            );

    //! \brief Return the mesh saved in the file
    mesh::Mesh1DPtr get_mesh();

private:
    struct SPECMICP_DLL_LOCAL UnsaturatedHDF5ReaderImpl;
    utils::pimpl_ptr<UnsaturatedHDF5ReaderImpl> m_impl; //!< The implementation

    // delete copy and assignement operator
    UnsaturatedHDF5Reader(const UnsaturatedHDF5Reader& other) = delete;
    UnsaturatedHDF5Reader& operator= (const UnsaturatedHDF5Reader& other) = delete;
};

} //end namespace io
} //end namespace specmicp

#endif // SPECMICP_IO_REACTMICP_HDF5_UNSATURATED_HPP
