/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

//! \file reactmicp_unsaturated.hpp
//! \brief The unsaturated system of the ReactMiCP solver

#ifndef SPECMICP_REACMICP_UNSATURATED_HPP
#define SPECMICP_REACMICP_UNSATURATED_HPP

// Core files

#include "reactmicp_core.hpp"

// System

#include "reactmicp/systems/unsaturated/variables_interface.hpp"
#include "reactmicp/systems/unsaturated/variables.hpp"
#include "reactmicp/systems/unsaturated/init_variables.hpp"

#include "reactmicp/systems/unsaturated/boundary_conditions.hpp"

#include "reactmicp/systems/unsaturated/equilibrium_stagger.hpp"
#include "reactmicp/systems/unsaturated/transport_stagger.hpp"

#include "reactmicp/io/configuration_unsaturated.hpp"
#include "reactmicp/systems/unsaturated/simulation_data.hpp"

#include "reactmicp/systems/unsaturated/upscaling_stagger_factory.hpp"

// Output

#endif // SPECMICP_REACMICP_UNSATURATED_HPP
