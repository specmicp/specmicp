/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "init_variables.hpp"

#include "variables.hpp"
#include "dfpm/meshes/mesh1d.hpp"

#include "specmicp/adimensional/adimensional_system_solution_extractor.hpp"

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace satdiff {

//! \brief Initialise an instance of SaturatedVariables
SaturatedVariablesPtr init_variables(
        mesh::Mesh1DPtr the_mesh,
        RawDatabasePtr the_database,
        units::UnitsSet the_units,
        const std::vector<index_t>& list_fixed_nodes,
        const std::vector<AdimensionalSystemSolution>& list_initial_states,
        const std::vector<int>& index_initial_state
        )
{
    SaturatedVariablesFactory factory(the_mesh, the_database, the_units,
                                     list_fixed_nodes,
                                     list_initial_states, index_initial_state);
    return factory.get_variable();
}

// SaturatedVariablesFactory::

SaturatedVariablesFactory::SaturatedVariablesFactory(
        mesh::Mesh1DPtr the_mesh,
        RawDatabasePtr the_database,
        units::UnitsSet the_units,
        const std::vector<index_t>& list_fixed_nodes,
        const std::vector<AdimensionalSystemSolution>& list_initial_states,
        const std::vector<int>& index_initial_state
        ):
    m_variable(std::make_shared<SaturatedVariables>(the_mesh, the_database)),
    m_database(the_database),
    nb_component(the_database->nb_component()),
    nb_nodes(the_mesh->nb_nodes())
{
    init_size();
    set_fixed_nodes(list_fixed_nodes);
    init_chemistry(the_units, index_initial_state, list_initial_states);
}

void SaturatedVariablesFactory::init_size()
{
    index_t main_ndf = nb_nodes*2*nb_component;

    m_variable->displacement() = Vector::Zero(main_ndf);
    m_variable->chemistry_rate() = Vector::Zero(main_ndf);
    m_variable->transport_rate() = Vector::Zero(main_ndf);
    m_variable->predictor() = Vector::Zero(main_ndf);
    m_variable->velocity() = Vector::Zero(main_ndf);

    m_variable->m_upscaling = Vector::Zero(m_variable->ndf_upscaling()*nb_nodes);
}

void SaturatedVariablesFactory::set_fixed_nodes(const std::vector<index_t>& list_fixed_nodes)
{
    m_variable->m_is_fixed_composition = std::vector<bool>(nb_nodes, false);
    for (index_t node: list_fixed_nodes)
    {
        m_variable->m_is_fixed_composition[node] = true;
    }
}

void SaturatedVariablesFactory::init_chemistry(
        units::UnitsSet  the_units,
        const std::vector<int>& index_initial_state,
        const std::vector<AdimensionalSystemSolution>& list_initial_states)
{
    m_variable->m_equilibrium_solutions.reserve(nb_nodes);


    for (index_t node=0; node<nb_nodes; ++node)
    {
        m_variable->m_equilibrium_solutions.push_back(list_initial_states[index_initial_state[node]]);

        AdimensionalSystemSolutionExtractor extractor(m_variable->m_equilibrium_solutions[node],
                                                      m_database, the_units);

        scalar_t rho_w = extractor.density_water();

        for (index_t component: m_database->range_component())
        {
            m_variable->aqueous_concentration(node, component, m_variable->displacement())
                    = rho_w*extractor.total_aqueous_concentration(component);
            m_variable->solid_concentration(node, component, m_variable->displacement())
                    = extractor.total_immobile_concentration(component);
        }
    }

}

} // end namespace satdiff
} // end namespace systems
} // end namespace reactmicp
} // end namespace specmicp
