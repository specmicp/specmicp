/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_SYSTEMS_SATURATED_TRANSPORTSTAGGER_HPP
#define SPECMICP_REACTMICP_SYSTEMS_SATURATED_TRANSPORTSTAGGER_HPP

#include "variablesfwd.hpp"
#include "reactmicp/solver/staggers_base/transport_stagger_base.hpp"
#include "dfpm/solver/parabolic_structs.hpp"

#include <vector>
#include <map>

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace satdiff {

using VariablesBase = solver::VariablesBase;

//! \brief The transport stagger for the saturated system
class SPECMICP_DLL_PUBLIC SaturatedTransportStagger: public solver::TransportStaggerBase
{
public:
    //! \brief Simple Constructor
    SaturatedTransportStagger(SaturatedVariablesPtr variables,
                              std::vector<index_t> list_fixed_nodes);

    //! \brief Constructor
    //!
    //! \param variables the variables
    //! \param list_fixed_nodes list of fixed nodes
    //! \param list_slave_nodes list of slave nodes (same equation)
    //! \param list_immobile_components list of immobile components
    SaturatedTransportStagger(SaturatedVariablesPtr variables,
                              std::vector<index_t> list_fixed_nodes,
                              std::map<index_t, index_t>  list_slave_nodes,
                              std::vector<index_t> list_immobile_components);

    ~SaturatedTransportStagger();
    SaturatedTransportStagger& operator= (const SaturatedTransportStagger& other) = delete;
    SaturatedTransportStagger(const SaturatedTransportStagger& other) = delete;

    //! \brief Return the options of the solver
    dfpmsolver::ParabolicDriverOptions& options_solver();

    //! \brief Initialize the stagger at the beginning of an iteration
    void initialize_timestep(scalar_t dt, VariablesBase * const var) override;

    //! \brief Solve the equation for the timestep
    solver::StaggerReturnCode restart_timestep(VariablesBase * const var) override;

    //! \brief Compute the residuals norm
    scalar_t get_residual(VariablesBase * const var) override;
    //! \brief Compute the residuals norm
    scalar_t get_residual_0(VariablesBase * const var) override;

    //! \brief Obtain the update
    scalar_t get_update(VariablesBase * const var) override;

private:
    //! \brief Implementation structure
    struct SaturatedTransportStaggerImpl;
    std::unique_ptr<SaturatedTransportStaggerImpl> m_impl; //!< Implementation

};

} // end namespace satdiff
} // end namespace systems
} // end namespace reactmicp
} // end namespace specmicp

#endif // SPECMICP_REACTMICP_SYSTEMS_SATURATED_TRANSPORTSTAGGER_HPP
