/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_SYSTEMS_SATURATED_TRANSPORTPROGRAM_HPP
#define SPECMICP_REACTMICP_SYSTEMS_SATURATED_TRANSPORTPROGRAM_HPP

//! \name reactmicp/saturated_react/transport_program.hpp
//! \brief The diffusion equations

#include "variablesfwd.hpp"

#include "dfpm/meshes/mesh1dfwd.hpp"
#include "dfpm/types.hpp"
#include "dfpm/solver/parabolic_program.hpp"

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace satdiff {

//! \brief Diffusion equations for the saturated system
//!
//! Solve all equations together.
class SaturatedDiffusion:
        public dfpmsolver::ParabolicProgram<SaturatedDiffusion>
{
public:
    static constexpr bool NO_CHEMISTRY_RATE {false}; //!< do not include chemistry rates
    static constexpr bool CHEMISTRY_RATE {true};     //!< include chemistry rates

    //! \brief Fancy constructor
    SaturatedDiffusion(SaturatedVariables* variables,
                       std::vector<index_t> list_fixed_nodes,
                       std::map<index_t, index_t>  list_slave_nodes,
                       std::vector<index_t> list_immobile_components);

    //! \brief Constructor
    SaturatedDiffusion(SaturatedVariables* variables,
                       std::vector<index_t> list_fixed_nodes);

    //! \brief Return the number of equations
    index_t get_neq() const {return m_neq;}
    //! \brief Return the number of degrees of freedom per node
    index_t get_ndf() const {return m_ndf;}
    //! \brief Return the total number of degrees of freedom
    index_t get_tot_ndf() const  {return m_tot_ndf;}

    //! \brief Method to update the variables
    void set_variables(SaturatedVariables* variables) {m_variables = variables;}

    //! \brief Return the id of the equation corresponding to the degree of freedom 'id_dof'
    //!
    //! Return 'no_equation' if no equation exist
    index_t id_equation(index_t id_dof) const {return m_ideq(id_dof);}

    //! \brief Compute the residuals
    //!
    //! \param displacement variables
    //! \param velocity time derivatives of the variables
    //! \param residual vector containing the residuals
    //! \param use_chemistry_rate if true compute residuals with chemistry rate
    void compute_residuals(const Vector& displacement,
                           const Vector& velocity,
                           Vector& residual,
                           bool use_chemistry_rate
                           );
    //! \brief Compute the residualts
    //!
    //! \param displacement variables
    //! \param velocity time derivatives of the variables
    //! \param residual vector containing the residuals
    void compute_residuals(const Vector& displacement,
                           const Vector& velocity,
                           Vector& residual
                           )
    {
        compute_residuals(displacement, velocity, residual, CHEMISTRY_RATE);
    }


    //! \brief Compute the residuals inside 'element' for 'component'
    void residuals_element_component(
            index_t element,
            index_t component,
            const Vector& displacement,
            const Vector& velocity,
            Eigen::Vector2d& element_residual,
            bool use_chemistry_rate
            );
    //! \brief Compute the residuals inside 'element' for 'component'
    void residuals_element_component(
            index_t element,
            index_t component,
            const Vector& displacement,
            const Vector& velocity,
            Eigen::Vector2d& element_residual
            )
    {
        residuals_element_component(element, component, displacement, velocity, element_residual, CHEMISTRY_RATE);
    }

    //! \brief Compute the jacobian
    void compute_jacobian(Vector& displacement,
                          Vector& velocity,
                          Eigen::SparseMatrix<scalar_t>& jacobian,
                          scalar_t alphadt
                          );
    //! \brief Compute the contribution of 'element' in the jacobian
    void jacobian_element(
            index_t element,
            Vector& displacement,
            Vector& velocity,
            dfpm::list_triplet_t& jacobian,
            scalar_t alphadt);

    //! \brief Update the solutions
    void update_solution(const Vector& update,
                         scalar_t lambda,
                         scalar_t alpha_dt,
                         Vector& predictor,
                         Vector& displacement,
                         Vector& velocity);

    //! \brief Apply boundary conditions to the velocity vector
    //!
    //! by default do nothing.
    void apply_bc(scalar_t dt,
                  const Vector& displacement,
                  Vector& velocity)
    {}


private:
    //! \brief Number the equations
    void number_equations(std::vector<index_t> list_fixed_nodes,
                          std::map<index_t, index_t>  list_slave_nodes,
                          std::vector<index_t> list_immobile_components
                          );


    index_t m_neq;     //!< Number fo equations
    index_t m_ndf;     //!< Number of degree of freedoms per node
    index_t m_tot_ndf; //!< Total number of degrees of freedoms

    Eigen::Matrix<index_t, Eigen::Dynamic, 1> m_ideq; //!< equations indexes

    mesh::Mesh1DPtr m_mesh;          //!< The mesh
    SaturatedVariables* m_variables; //!< Variables, Don't own it, reset every timestep

    bool m_is_in_residual_computation; //!< True if algo currently computing residuals

    std::vector<bool> m_fixed_components; //!< True if component is immobile

};

} // end namespace satdiff
} // end namespace systems
} // end namespace reactmicp
} // end namespace specmicp

#endif // SPECMICP_REACTMICP_SYSTEMS_SATURATED_TRANSPORTPROGRAM_HPP
