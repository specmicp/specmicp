/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_SATDIFF_DIFFUSIVEUPSCALING_STAGGER
#define SPECMICP_REACTMICP_SATDIFF_DIFFUSIVEUPSCALING_STAGGER

//! \file diffusive_upscaling_stagger.hpp
//! \brief A diffusive upscaling stagger for the saturated system

#include "specmicp_database/data_container.hpp"

#include "reactmicp/solver/staggers_base/decl.inl"
#include "reactmicp/solver/staggers_base/variables_base.hpp"
#include "reactmicp/solver/staggers_base/upscaling_stagger_base.hpp"

#include "variablesfwd.hpp"

#include "specmicp_common/physics/units.hpp"
#include "specmicp/adimensional/adimensional_system_solution_extractor.hpp"
#include <functional>

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace satdiff {

#ifndef SPC_DOXYGEN_SHOULD_SKIP_THIS
using StaggerReturnCode = solver::StaggerReturnCode;
using VariablesBase = solver::VariablesBase;
#endif // SPC_DOXYGEN_SHOULD_SKIP_THIS

//! \brief The type of a diffusion law
using diffusion_f = std::function<scalar_t (index_t, SaturatedVariables * const)>;
//! \brief A function that return false if the computation must be stopped
using check_f = std::function<bool (SaturatedVariables * const)>;

//! \brief Never stop the computation, always return true
inline bool dummy_check(SaturatedVariables * const _) {return true;}

//! \brief A upscaling stagger for a diffusive case
class SPECMICP_DLL_PUBLIC DiffusiveUpscalingStagger:
        public solver::UpscalingStaggerBase
{
public:
    //! \brief Constructor
    //!
    //! This is not call automatically so can be adjusted by the user
    DiffusiveUpscalingStagger(
            const diffusion_f& diffusion_law,
            RawDatabasePtr& the_database,
            units::UnitsSet the_units):
        m_diffusion_law(diffusion_law),
        m_data(the_database),
        m_units_set(the_units),
        m_dt(HUGE_VAL)
    {
    }

    //! \brief Initialize the stagger at the beginning of the computation
    //!
    //! The porosity and transport properties should be initialized here
    void initialize(VariablesBase * const var) override;

    //! \brief Initialize the stagger at the beginning of a timestep
    //!
    //! Typically do nothing but erase the "dot" variable (velocity such as the vel_porosity)
    void initialize_timestep(scalar_t dt, VariablesBase * const var) override;

    //! \brief This is the main function called during a timestep
    StaggerReturnCode restart_timestep(VariablesBase * const var) override;

    //! \brief Compute the upscaling for 'node'
    void upscaling_one_node(index_t node, SaturatedVariables * const true_var);
    //! \brief Set a function to check a condition to stop the computation
    void set_checker(const check_f& checker) {m_checker = checker;}

private:
    diffusion_f m_diffusion_law; //!< the function to compute the diffusion coefficient
    check_f m_checker {&dummy_check}; //!< the function to check when the computation must be stopped
    RawDatabasePtr m_data; //!< The database
    units::UnitsSet m_units_set; //!< The set of units
    scalar_t m_dt; //!< The current timestep
};

//! \brief The parameters for a power law
struct SPECMICP_DLL_PUBLIC PowerLawParameters
{
    scalar_t exponent;    //!< The exponent
    scalar_t d_eff_0;     //!< The diffusion coefficient at the reference porosity
    scalar_t porosity_0;  //!< The reference porosity
    scalar_t porosity_res;//!< The residual porosity
};

//! \brief The power law
class PowerLaw
{
public:
    //! \brief Constructor
    PowerLaw(const PowerLawParameters& params):
        m_param(params)
    {}
    //! \brief Return the diffusion coefficient
    scalar_t get_diffusion_coefficient(
            index_t node,
            SaturatedVariables * const true_var
            );
    //! \brief Return the function to pass to the upscaling stagger
    diffusion_f get_law();

private:
    PowerLawParameters m_param; //! \brief The parameters
};

//! \brief The parameters for the capped power law
struct SPECMICP_DLL_PUBLIC CappedPowerLawParameters
{
    scalar_t exponent;   //!< The exponent
    scalar_t d_eff_0;    //!< The diffusion coefficient at the reference porosity
    scalar_t porosity_0; //!< The reference porosity
    scalar_t porosity_res;//!< The residual porosity
    scalar_t cap; //!< The maximum diffusion coefficient
};


//! \brief The capped power law
class SPECMICP_DLL_PUBLIC CappedPowerLaw
{
public:
    //! \brief Constructor
    CappedPowerLaw(const CappedPowerLawParameters& params):
        m_param(params)
    {}
    //! \brief Return the diffusion coefficient
    scalar_t get_diffusion_coefficient(
            index_t node,
            SaturatedVariables * const true_var
            );
    //! \brief Return the function to pass to the upscaling stagger
    diffusion_f get_law();

private:
    CappedPowerLawParameters m_param; //!< The parameters
};

} //end namespace satdiff
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp

#endif // SPECMICP_REACTMICP_SATDIFF_DIFFUSIVEUPSCALING_STAGGER
