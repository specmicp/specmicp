/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "diffusive_upscaling_stagger.hpp"
#include "reactmicp/solver/staggers_base/stagger_structs.hpp"
#include "variables.hpp"

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace satdiff {

void  DiffusiveUpscalingStagger::initialize(VariablesBase * const var)
{
    SaturatedVariables * const true_var = static_cast<SaturatedVariables * const>(var);
    true_var->upscaling_variables().setZero();
    for (index_t node=0; node<true_var->nb_nodes(); ++node)
    {
        upscaling_one_node(node, true_var);
    }
}

void DiffusiveUpscalingStagger::initialize_timestep(
        scalar_t dt,
        VariablesBase * const var
        )
{
    SaturatedVariables * const true_var = static_cast<SaturatedVariables * const>(var);
    m_dt = dt;

    for (index_t node=1; node<true_var->nb_nodes(); ++node)
    {
        upscaling_one_node(node, true_var);
        true_var->vel_porosity(node) = 0.0;
    }
}

//! This is the main function called during a timestep
StaggerReturnCode
DiffusiveUpscalingStagger::restart_timestep(VariablesBase * const var)
{
    SaturatedVariables * const true_var = static_cast<SaturatedVariables * const>(var);
    for (index_t node=1; node<true_var->nb_nodes(); ++node)
    {
        upscaling_one_node(node, true_var);
    }
    if (not m_checker(true_var))
        return StaggerReturnCode::UserTermination;
    return StaggerReturnCode::ResidualMinimized; // This is the value that should be returned if everything is ok
}

void DiffusiveUpscalingStagger::upscaling_one_node(
        index_t node,
        SaturatedVariables * const true_var
        )
{
    // AdimensionalSystemSolutionExtractor is the class to use to
    // extract information from a SpecMiCP solution
    // To obtain correct information the correct units must be used
    AdimensionalSystemSolutionExtractor extractor(
                true_var->equilibrium_solution(node),
                m_data, m_units_set
                );
    // We can obtain the porosity very easily :
    scalar_t porosity = extractor.porosity();
    true_var->vel_porosity(node) += (porosity - true_var->porosity(node))/m_dt;
    true_var->porosity(node) = porosity;
    true_var->diffusion_coefficient(node) = m_diffusion_law(node, true_var);
}

scalar_t PowerLaw::get_diffusion_coefficient(
        index_t node,
        SaturatedVariables * const true_var
        )
{
    scalar_t tmp_1 = true_var->porosity(node) - m_param.porosity_res;
    scalar_t tmp_2 = m_param.porosity_0 - m_param.porosity_res;

    scalar_t res = tmp_1/tmp_2;
    tmp_1 = std::pow(res, m_param.exponent);
    res = m_param.d_eff_0*tmp_1;
    return res;
}

diffusion_f PowerLaw::get_law()
{
    return std::bind(std::mem_fn(&PowerLaw::get_diffusion_coefficient), this,
                     std::placeholders::_1, std::placeholders::_2);
}

scalar_t CappedPowerLaw::get_diffusion_coefficient(
        index_t node,
        SaturatedVariables * const true_var
        )
{
    scalar_t tmp_1 = true_var->porosity(node) - m_param.porosity_res;
    scalar_t tmp_2 = m_param.porosity_0 - m_param.porosity_res;

    scalar_t res = tmp_1/tmp_2;
    tmp_1 = std::pow(res, m_param.exponent);
    res = m_param.d_eff_0*tmp_1;
    return std::min(m_param.cap, res);
}

diffusion_f CappedPowerLaw::get_law()
{
    return std::bind(std::mem_fn(&CappedPowerLaw::get_diffusion_coefficient),
                     this, std::placeholders::_1, std::placeholders::_2);
}

} //end namespace satdiff
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp
