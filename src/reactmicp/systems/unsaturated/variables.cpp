/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "variables.hpp"
#include "dfpm/meshes/mesh1d.hpp"

#include "variables_box.hpp"

#include "specmicp_common/log.hpp"

#include <stdexcept>

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace unsaturated {

//  ===================
//  List main variables
//  ===================

ListMainVariable::ListMainVariable(index_t nb_vars, index_t nb_nodes)
{
    variables.reserve(nb_vars);
    variables.emplace_back(make_unique<MainVariable>(nb_nodes));
    variables.emplace_back(nullptr);
    for (index_t i=2; i<nb_vars; ++i)
    {
        variables.emplace_back(make_unique<MainVariable>(nb_nodes));
    }
}

ListMainVariable::ListMainVariable(
        index_t nb_vars,
        index_t nb_nodes,
        std::vector<bool> to_init
        )
{
    variables.reserve(nb_vars);
    for (const auto& is_present: to_init) {
        if (is_present) {
            variables.emplace_back(make_unique<MainVariable>(nb_nodes));
        } else {
            variables.emplace_back(nullptr);
        }
    }
}

//! \brief Reset the variables
void ListMainVariable::reset()
{
    //if (variables[0] != nullptr)
    //    variables[0]->reset();
    for (vector_type::size_type i=0; i<variables.size(); ++i)
    {
        if (variables[i] != nullptr)
            variables[i]->reset();
    }
}


void ListMainVariable::hard_reset(index_t component, index_t nb_nodes)
{
    specmicp_assert(component >= 0 and (unsigned) component < variables.size());
    if (nb_nodes == 0)
        variables[component].reset(nullptr);
    else
        variables[component].reset(new MainVariable(nb_nodes));
}



//  ==============
//  Variables box
//  ==============

SaturationVariableBox::SaturationVariableBox(UnsaturatedVariables& vars):
    liquid_saturation(vars.m_liquid_var.water()),
    solid_concentration(vars.m_solid_var.water()),
    vapor_pressure(vars.m_gas_var.water()),
    aqueous_concentration(vars.m_water_aq_concentration),
    porosity(vars.m_porosity),
    liquid_permeability(vars.m_liquid_permeability),
    liquid_diffusivity(vars.m_liquid_diffusivity),
    relative_liquid_permeability(vars.m_relative_liquid_permeability),
    relative_liquid_diffusivity(vars.m_relative_liquid_diffusivity),
    capillary_pressure(vars.m_capillary_pressure),
    advection_flux(vars.m_advection_flux),
    constants(vars.m_constants),
    capillary_pressure_f(vars.m_capillary_pressure_f),
    relative_liquid_permeability_f(vars.m_relative_liquid_permeability_f),
    relative_liquid_diffusivity_f(vars.m_relative_liquid_diffusivity_f)
{}

SaturationPressureVariableBox::SaturationPressureVariableBox(
        UnsaturatedVariables& vars):
    binary_diffusion_coefficient(vars.get_binary_gas_diffusivity(0)),
    liquid_saturation(vars.m_liquid_var.water()),
    partial_pressure(vars.get_pressure_main_variables(0)),
    solid_concentration(vars.m_solid_var.water()),
    aqueous_concentration(vars.m_water_aq_concentration),
    porosity(vars.m_porosity),
    liquid_permeability(vars.m_liquid_permeability),
    liquid_diffusivity(vars.m_liquid_diffusivity),
    relative_liquid_permeability(vars.m_relative_liquid_permeability),
    relative_liquid_diffusivity(vars.m_relative_liquid_diffusivity),
    capillary_pressure(vars.m_capillary_pressure),
    resistance_gas_diffusivity(vars.m_resistance_gas_diffusivity),
    relative_gas_diffusivity(vars.m_relative_gas_diffusivity),
    advection_flux(vars.m_advection_flux),
    constants(vars.m_constants),
    capillary_pressure_f(vars.m_capillary_pressure_f),
    relative_liquid_permeability_f(vars.m_relative_liquid_permeability_f),
    relative_liquid_diffusivity_f(vars.m_relative_liquid_diffusivity_f),
    relative_gas_diffusivity_f(vars.m_relative_gas_diffusivity_f),
    partial_pressure_f(vars.m_vapor_pressure_f)
{}

LiquidAqueousComponentVariableBox::LiquidAqueousComponentVariableBox(
        UnsaturatedVariables& vars,
        index_t component
        ):
    aqueous_concentration(vars.m_liquid_var.aqueous_component(component)),
    solid_concentration(vars.m_solid_var.aqueous_component(component)),
    partial_pressure(vars.get_pressure_main_variables(component)),
    saturation(vars.m_liquid_var.water()),
    porosity(vars.m_porosity),
    liquid_diffusivity(vars.m_liquid_diffusivity),
    relative_liquid_diffusivity(vars.m_relative_liquid_diffusivity),
    advection_flux(vars.m_advection_flux),
    constants(vars.m_constants)
{}


LiquidGasAqueousVariableBox::LiquidGasAqueousVariableBox(
        UnsaturatedVariables& vars,
        index_t component
        ):
    LiquidAqueousComponentVariableBox(vars, component),
    binary_diffusion_coefficient(vars.get_binary_gas_diffusivity(component)),
    resistance_gas_diffusivity(vars.m_resistance_gas_diffusivity),
    relative_gas_diffusivity(vars.m_relative_gas_diffusivity)
{}

PressureVariableBox::PressureVariableBox(
        UnsaturatedVariables& vars,
        index_t component
        ):
    binary_diffusion_coefficient(vars.get_binary_gas_diffusivity(component)),
    partial_pressure(vars.get_pressure_main_variables(component)),
    liquid_saturation(vars.m_liquid_var.water()),
    porosity(vars.m_porosity),
    resistance_gas_diffusivity(vars.m_resistance_gas_diffusivity),
    relative_gas_diffusivity(vars.m_relative_gas_diffusivity),
    constants(vars.m_constants)
{}


void ConstantBox::scale(const units::UnitsSet& units_set)
{
    scalar_t scaling_factor_l = units::scaling_factor(units_set.length);
    scalar_t scaling_factor_m = units::scaling_factor(units_set.quantity);
    //viscosity_liquid_water *= 1.0/scaling_factor;
    rt *= scaling_factor_m / std::pow(scaling_factor_l, 3);
    //total_pressure *= 1.0/scaling_factor;

}

//  ======================
//  Unsaturated variables
//  ======================

namespace internal {

//! \brief Find the correspond gas id for a component with gas
static index_t get_id_gas(database::RawDatabasePtr the_database,
                          index_t component)
{
    index_t id = no_species;
    for (auto gas: the_database->range_gas())
    {
        const scalar_t nu = the_database->nu_gas(gas, component);
        if (nu == 0.0)
            continue;

        if (nu != 1.0)
        {
            ERROR << "Database not in the good format"
                  << "; expect : gas <=> component \n"
                  << "component : "
                  << the_database->get_label_component(component) << " \n"
                  << "gas : "
                  << the_database->get_label_gas(gas) << ".";
            throw std::runtime_error("Badly formatted database");
        }
        else
        {
            id = gas;
        }
    }
    if (id == no_species)
    {
        ERROR << "No corresponding gas in the database for component : "
              << the_database->get_label_component(component);
        throw std::runtime_error("Badly formatted database");
    }
    return id;
}

} //end namespace internal

UnsaturatedVariables::UnsaturatedVariables(
        mesh::Mesh1DPtr the_mesh,
        database::RawDatabasePtr the_database,
        std::vector<bool> has_gas
        ):
    database::DatabaseHolder(the_database),
    m_mesh(the_mesh),
    m_has_gas(has_gas),
    m_id_gas(the_database->nb_component(), no_species),
    // main var
    m_liquid_var(the_database->nb_component(),the_mesh->nb_nodes()),
    m_gas_var(the_database->nb_component(),the_mesh->nb_nodes(), has_gas),
    m_solid_var(the_database->nb_component(),the_mesh->nb_nodes()),
    // chem sol
    m_chem_sol(the_mesh->nb_nodes()),
    m_inert_volume_fraction(the_mesh->nb_nodes()),
    // secondary vars
    m_porosity(the_mesh->nb_nodes()),
    m_water_aq_concentration(the_mesh->nb_nodes()),
    m_liquid_permeability(the_mesh->nb_nodes()),
    m_relative_liquid_permeability(the_mesh->nb_nodes()),
    m_liquid_diffusivity(the_mesh->nb_nodes()),
    m_relative_liquid_diffusivity(the_mesh->nb_nodes()),
    m_binary_gas_diffusivity(Vector::Zero(the_database->nb_component())),
    m_resistance_gas_diffusivity(the_mesh->nb_nodes()),
    m_relative_gas_diffusivity(the_mesh->nb_nodes()),
    m_capillary_pressure(the_mesh->nb_nodes()),
    m_advection_flux(the_mesh->nb_nodes()),
    m_aqueous_scaling(Vector::Ones(the_database->nb_component())),
    m_gaseous_scaling(Vector::Ones(the_database->nb_component()))
{
    specmicp_assert(has_gas.size() == (unsigned) the_database->nb_component());
    //
    // trick to avoid more branching in equations :
    m_gas_var.hard_reset(1, the_mesh->nb_nodes());

    for (auto component: the_database->range_aqueous_component())
    {
        if (has_gas[component])
        {
            m_id_gas[component] = internal::get_id_gas(the_database, component);
        }
    }
}

UnsaturatedVariables::UnsaturatedVariables(
        mesh::Mesh1DPtr the_mesh,
        database::RawDatabasePtr the_database,
        std::vector<bool> has_gas,
        const units::UnitsSet& units_set):
    UnsaturatedVariables(the_mesh, the_database, has_gas)
{
    m_constants.scale(units_set);
}


void UnsaturatedVariables::reset_main_variables()
{
    m_liquid_var.reset();
    m_gas_var.reset();
    m_solid_var.reset();

    m_porosity.variable = m_porosity.predictor;
    m_porosity.velocity.setZero();

    m_water_aq_concentration.variable = m_water_aq_concentration.predictor;
    m_water_aq_concentration.velocity.setZero();

    m_advection_flux.set_zero();

    set_relative_variables();

}

SaturationVariableBox
UnsaturatedVariables::get_saturation_variables() {
    return SaturationVariableBox(*this);
}

SaturationPressureVariableBox
UnsaturatedVariables::get_saturation_pressure_variables() {
    return SaturationPressureVariableBox(*this);
}

PressureVariableBox
UnsaturatedVariables::get_vapor_pressure_variables() {
    return PressureVariableBox(*this, 0);
}


LiquidAqueousComponentVariableBox
UnsaturatedVariables::get_liquid_aqueous_component_variables(
        index_t component
        ) {
    return LiquidAqueousComponentVariableBox(*this, component);
}

LiquidGasAqueousVariableBox
UnsaturatedVariables::get_liquid_gas_aqueous_variables(
        index_t component
        ) {
    return LiquidGasAqueousVariableBox(*this, component);
}

PressureVariableBox
UnsaturatedVariables::get_pressure_variables(
        index_t component
        ) {
    return PressureVariableBox(*this, component);
}


MainVariable& UnsaturatedVariables::get_pressure_main_variables(index_t component)
{
    if (component_has_gas(component))
    {
        specmicp_assert(m_gas_var(component) != nullptr);
        return *m_gas_var(component);
    }
    else
        return *m_gas_var(1); // a zero var..., this is a trick to always get a valid info
}

index_t UnsaturatedVariables::nb_gas() {
    return std::count(m_has_gas.begin(), m_has_gas.end(), true);
}

void UnsaturatedVariables::set_relative_variables(
        index_t node
        )
{
    const scalar_t saturation = m_liquid_var.water().variable(node);

    m_relative_liquid_diffusivity(node)  =
            m_relative_liquid_diffusivity_f(node, saturation);
    m_relative_gas_diffusivity(node)     =
            m_relative_gas_diffusivity_f(node, saturation);
    m_relative_liquid_permeability(node) =
            m_relative_liquid_permeability_f(node, saturation);
    m_capillary_pressure(node)           =
            m_capillary_pressure_f(node, saturation);

}

void UnsaturatedVariables::set_relative_variables()
{
    for (index_t node: m_mesh->range_nodes())
    {
        set_relative_variables(node);
    }
}

void UnsaturatedVariables::set_vapor_pressure(index_t node)
{
    if (component_has_gas(0))
    {
        const scalar_t saturation = m_liquid_var.water().variable(node);
        m_gas_var(0)->variable(node) = m_vapor_pressure_f(node, saturation);
    }


}



} //end namespace unsatura
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp
