/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_UNSATURATED_VARIABLESBOX
#define SPECMICP_REACTMICP_UNSATURATED_VARIABLESBOX

//! \file unsaturated/variables_box.hpp
//! \brief Set of variables passed to the equations

#include "specmicp_common/types.hpp"
#include "variables_sub.hpp"

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace unsaturated {

//! \brief Base class for a box of variables
//! \internal
struct SPECMICP_DLL_LOCAL BaseVariableBox
{
};

//! \brief Variables box for the saturation equation
//! \internal
struct SPECMICP_DLL_LOCAL SaturationVariableBox:
        public BaseVariableBox
{
    MainVariable& liquid_saturation;         //!< Liquid saturation
    const MainVariable& solid_concentration; //!< Solid concentrations
    const MainVariable& vapor_pressure;      //!< Water vapor pressure

    const SecondaryTransientVariable& aqueous_concentration; //!< water aq. conc.

    const SecondaryTransientVariable& porosity;   //!< Porosity
    const SecondaryVariable& liquid_permeability; //!< Intrinsic liquid permeability
    const SecondaryVariable& liquid_diffusivity;  //!< Intrinsic liquid diffusivity

    SecondaryVariable& relative_liquid_permeability; //!< Relative liquid permeability
    SecondaryVariable& relative_liquid_diffusivity;  //!< Relative liquid diffusivity
    SecondaryVariable& capillary_pressure;           //!< Capillary pressure
    SecondaryVariable& advection_flux;               //!< Liquid advection flux

    const ConstantBox& constants;                    //!< Physics constants

    user_model_saturation_f capillary_pressure_f;           //!< Pc(S)
    user_model_saturation_f relative_liquid_permeability_f; //!< K_rl(S)
    user_model_saturation_f relative_liquid_diffusivity_f;  //!< D_rl(S)

    //!
    //! \brief SaturationVariableBox
    //! \param vars The main set of variables
    //!
    SaturationVariableBox(UnsaturatedVariables& vars);
    // implementation fo the constructor is in variables.cpp
};

//! \brief Variables box for the saturation equation
//! \internal
struct SPECMICP_DLL_LOCAL SaturationPressureVariableBox:
        public BaseVariableBox
{
    scalar_t binary_diffusion_coefficient; //!< Air-x binary gas diffusion coefficient

    MainVariable& liquid_saturation;         //!< Liquid saturation S
    MainVariable& partial_pressure;          //!< water vapor pressure
    const MainVariable& solid_concentration; //!< Solid concentration

    const SecondaryTransientVariable& aqueous_concentration; //!< Water aq. conc.

    const SecondaryTransientVariable& porosity;   //!< Porosity
    const SecondaryVariable& liquid_permeability; //!< Intrinsic liquid permeability
    const SecondaryVariable& liquid_diffusivity;  //!< Intrinsic liquid diffusivity

    SecondaryVariable& relative_liquid_permeability; //!< Relative liquid permeability
    SecondaryVariable& relative_liquid_diffusivity;  //!< Relative liquid diffusivity
    SecondaryVariable& capillary_pressure;           //!< Capillary pressure

    const SecondaryVariable& resistance_gas_diffusivity; //!< Resistance factor for gas diffusion coefficient
    const SecondaryVariable& relative_gas_diffusivity;   //!< Relative gas diffusion coefficient

    SecondaryVariable& advection_flux;               //!< Liquid advection flux

    const ConstantBox& constants;                    //!< Physics constants

    user_model_saturation_f capillary_pressure_f;           //!< Pc(S)
    user_model_saturation_f relative_liquid_permeability_f; //!< K_rl(S)
    user_model_saturation_f relative_liquid_diffusivity_f;  //!< D_rl(S)
    user_model_saturation_f relative_gas_diffusivity_f;     //!< D_rg(S)
    user_model_saturation_f partial_pressure_f;             //!< P_v(s)

    //!
    //! \brief SaturationVariableBox
    //! \param vars The main set of variables
    //!
    SaturationPressureVariableBox(UnsaturatedVariables& vars);
    // implementation fo the constructor is in variables.cpp
};

//! \brief Set of variables for the liquid transport equation of an aqueous component
//! \internal
struct SPECMICP_DLL_LOCAL LiquidAqueousComponentVariableBox:
        public BaseVariableBox
{
    MainVariable& aqueous_concentration;     //!< Aqueous concentration
    const MainVariable& solid_concentration; //!< Solid concentration
    const MainVariable& partial_pressure;    //!< Partial pressure (if available)

    const MainVariable& saturation; //!< Liquid saturation


    const SecondaryTransientVariable& porosity;           //!< Porosity
    const SecondaryVariable& liquid_diffusivity;          //!< Intrinsic liquid diffusivity
    const SecondaryVariable& relative_liquid_diffusivity; //!< Relative liquid diffusivity
    const SecondaryVariable& advection_flux;              //!< Liquid advection flux

    const ConstantBox& constants;  //!< Physics constants
    //!
    //! \brief LiquidAqueousComponentVariableBox
    //! \param vars The main set of variables
    //! \param component an aqueous component
    //!
    LiquidAqueousComponentVariableBox(UnsaturatedVariables& vars, index_t component);
    // implementation fo the constructor is in variables.cpp
};

//! \brief Set of variables for the liquid and gas transport equation of an
//!  aqueous component
//! \internal
struct SPECMICP_DLL_LOCAL LiquidGasAqueousVariableBox:
        public LiquidAqueousComponentVariableBox
{
    scalar_t binary_diffusion_coefficient; //!< Air-x binary gas diffusion coefficient

    const SecondaryVariable& resistance_gas_diffusivity; //!< Resistance factor for gas diffusion coefficient
    const SecondaryVariable& relative_gas_diffusivity;   //!< Relative gas diffusion coefficient

    //!
    //! \brief LiquidAqueousComponentVariableBox
    //! \param vars The main set of variables
    //! \param component an aqueous component
    //!
    LiquidGasAqueousVariableBox(UnsaturatedVariables& vars, index_t component);
    // implementation fo the constructor is in variables.cpp
};


//! \brief Set of variables for the gas transport equation of an aqueous component
//! \internal
struct SPECMICP_DLL_LOCAL PressureVariableBox:
        public BaseVariableBox
{
    scalar_t binary_diffusion_coefficient; //!< Air-x binary gas diffusion coefficient

    MainVariable& partial_pressure; //!< Partial pressure (if available)

    const MainVariable& liquid_saturation; //!< Liquid saturation

    const SecondaryTransientVariable& porosity; //!< Porosity
    const SecondaryVariable& resistance_gas_diffusivity; //!< Resistance factor for gas diffusion coefficient
    const SecondaryVariable& relative_gas_diffusivity;   //!< Relative gas diffusion coefficient

    const ConstantBox& constants; //!< Physics constants
    //!
    //! \brief PressureVariableBox
    //! \param vars The main set of variables
    //! \param component The component (can be water)
    //!
    PressureVariableBox(UnsaturatedVariables& vars, index_t component);
    // implementation fo the constructor is in variables.cpp
};


} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp

#endif // SPECMICP_REACTMICP_UNSATURATED_VARIABLESBOX
