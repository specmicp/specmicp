/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_UNSATURATED_BOUNDARYCONDITIONS_HPP
#define SPECMICP_REACTMICP_UNSATURATED_BOUNDARYCONDITIONS_HPP

//! \file unsaturated/boundary_conditions.hpp
//! \brief Boundary conditions for the unsaturated systems

#include "specmicp_common/types.hpp"
#include "specmicp_common/pimpl_ptr.hpp"

#include <memory>
#include <vector>
#include <functional>

namespace specmicp {

struct AdimensionalSystemConstraints;

namespace reactmicp {
namespace systems {
namespace unsaturated {

//! \brief Flag to indicates the type of a boundary condition
enum class IdBCs: index_t {
      NoEquation = -1
    , NormalNode =  0
    , FixedDof   =  1
    , FixedFlux  =  2
    , ImplicitFixedFlux = 3
};

//! \brief A function to compute a fixed-flux from value of main variable
using implicit_flux_f = std::function<scalar_t (scalar_t)>;

//! \brief Boundary conditions for the unsaturated system
//!
//! Contains both the transport and chemistry boundary conditions
class SPECMICP_DLL_PUBLIC BoundaryConditions
{
public:
    //! \brief Return a pointer to a new set of boundary conditions
    static std::shared_ptr<BoundaryConditions> make(
            uindex_t nb_nodes,
            uindex_t nb_components
            ) {
        return std::make_shared<BoundaryConditions>(
                    nb_nodes, nb_components);
    }

    //! \brief Build a BC container for the given number of nodes and components
    //!
    //! \sa make
    BoundaryConditions(uindex_t nb_nodes, uindex_t nb_components);
    ~BoundaryConditions();

    //! \brief Return the equation type for a liquid dof
    //!
    //! \sa get_type_gas_dof
    IdBCs get_bcs_liquid_dof(uindex_t node, uindex_t component);
    //! \brief Return the equation type for a gas dof
    //!
    //! \sa get_type_liquid_dof
    IdBCs get_bcs_gas_dof(   uindex_t node, uindex_t component);

    //! \brief Return the fixed flux value for a liquid dof
    //!
    //! \return a value or 0 if the value is not set or it's not a fixed flux dof
    //!
    //! \sa get_flux_gas_dof
    scalar_t get_flux_liquid_dof(uindex_t node, uindex_t component) const;
    //! \brief Return the fixed flux value for a gas dof
    //!
    //! \return a value or 0 if the value is not set or it's not a fixed flux dof
    //!
    //! \sa get_flux_liquid_dof, get_implicit_flux_dof
    scalar_t get_flux_gas_dof(   uindex_t node, uindex_t component) const;
    //! \brief Return the implicit fixed flux value for a gas dof
    //!
    //! \warning The dof must be an implicit fixed flux node !
    //!
    //! \sa get_flux_liquid_dof, get_gas_flux_dof
    scalar_t get_implicit_flux_gas_dof(uindex_t node, uindex_t component, scalar_t variable) const;

    //! \brief Remove boundary conditions for a liquid dof
    //!
    //! \sa reset_flux_gas_dof
    void reset_flux_liquid_dof(uindex_t node, uindex_t component);
    //! \brief Remove boundary conditions for a liquid dof
    //!
    //! \sa reset_flux_liquid_dof
    void reset_flux_gas_dof(   uindex_t node, uindex_t component);

    //! \brief Add a gas node
    //!
    //! Porosity is 1 and liquid saturation is 0 at this node.
    //! This is just the boundary conditions for the liquid transport
    //! equations, the user still need to set up the correct chemistry
    //! constraints.
    //!
    //!
    //! \sa add_gas_nodes
    void add_gas_node( uindex_t node);
    //! \brief Add gas nodes
    //!
    //! \sa add_gas_node
    void add_gas_nodes(const std::vector<uindex_t>& nodes);
    //! \brief Return true if the given node is a gas node
    bool is_gas_node(  uindex_t node) const;

    //! \brief Add a fixed node
    //!
    //! All dof of tgis node will be fixed
    void add_fixed_node(uindex_t node);
    //! \brief Add fixed nodes
    //!
    //! All dof of these nodes will be fixed
    void add_fixed_nodes(const std::vector<uindex_t>& nodes);

    //! \brief Set a liquid dof to be an essential BCs
    //!
    //! \sa add_fixed_gas_dof
    void add_fixed_liquid_dof(uindex_t node, uindex_t component);
    //! \brief Set a gas dof to be an essential BCs
    //!
    //! \sa add_fixed_liquid_dof
    void add_fixed_gas_dof(   uindex_t node, uindex_t component);
    //! \brief Add a fixed flux conditions on a liquid dof
    //!
    //! \sa add_flux_gas_dof set_fixed_flux_liquid_dof
    void add_fixed_flux_liquid_dof(uindex_t node, uindex_t component);
    //! \brief Add a fixed flux conditions on a liquid dof
    //!
    //! \sa add_flux_gas_dof set_fixed_flux_liquid_dof
    void add_fixed_flux_gas_dof(   uindex_t node, uindex_t component);
    //! \brief Add a fixed flux conditions on a liquid dof
    //!
    //! \sa add_flux_gas_dof set_fixed_flux_liquid_dof
    void set_fixed_flux_liquid_dof(
            uindex_t node,
            uindex_t component,
            scalar_t value
            );
    //! \brief Add a fixed flux conditions on a liquid dof
    //!
    //! \sa add_flux_gas_dof set_fixed_flux_liquid_dof
    void set_fixed_flux_gas_dof(
            uindex_t node,
            uindex_t component,
            scalar_t value
            );

    //! \brief Add a fixed flux, solved implicitely, for a gas dof
    //!
    //! The flux is a function of the variable (pressure) at the node,
    //! it will be evaluated when computing the residuals.
    //!
    //! \sa set_fixed_flux_gas_dof
    void set_fixed_flux_implicit_gas(uindex_t node,
                                     uindex_t component,
                                     implicit_flux_f function
                                     );

    //! \brief Return true if the constraints exist
    bool has_constraint(const std::string& name) const;

    //! \brief Return the chemical constraint of the given node
    const AdimensionalSystemConstraints& get_constraint(uindex_t node) const;
    //! \brief Return the chemical constraint of the given node
    //!
    //! \warning Changing the constraint changes it for all node sharing the
    //! same constraint. To modify only this node, use `fork_constraints' first.
    //!
    AdimensionalSystemConstraints& get_constraint(uindex_t node);

    //! \brief Return the constraint identified by 'name'
    const AdimensionalSystemConstraints& get_constraint(
            const std::string& name) const;

    //! \brief Return the constraint identified by 'name'
    AdimensionalSystemConstraints& get_constraint(const std::string& name);

    //! \brief Set a new constraints for the given node
    AdimensionalSystemConstraints& fork_constraint(
            uindex_t node,
            const std::string& name
            );

    //! \brief Fork the given constraint
    AdimensionalSystemConstraints& fork_constraint(
            const std::string& old_name,
            const std::string& new_name
            );

    //! \brief Add a constraint
    AdimensionalSystemConstraints& add_constraint(
            const std::string& name
            );

    //! \brief Set the constraint of the given node
    void set_constraint(uindex_t node, const std::string& name);

    //! \brief Disable the precipitation of a solid phase
    //!
    //! The solid phase can still exist and dissolver, but it can't precipitate
    void add_mineral_no_precipitation(index_t id_mineral);

    //! \brief Return the set of minerals forbidden to precipitate
    const std::vector<index_t>& get_minerals_no_precipitation();

private:
    struct SPECMICP_DLL_LOCAL BoundaryConditionsImpl;
    utils::pimpl_ptr<BoundaryConditionsImpl> m_impl;

    BoundaryConditions(const BoundaryConditions& other) = delete;
    BoundaryConditions& operator=(const BoundaryConditions& other) = delete;
};



} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp

#endif // SPECMICP_REACTMICP_UNSATURATED_BOUNDARYCONDITIONS_HPP
