/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_SYSTEMS_UNSATURATED_FV1DDOFEQUATION_HPP
#define SPECMICP_REACTMICP_SYSTEMS_UNSATURATED_FV1DDOFEQUATION_HPP

//! \file unsaturated/fv_1dof_equation.hpp
//! \brief 1D finite volume equation, with 1 dof per node

#include "specmicp_common/types.hpp"
#include "dfpm/solver/parabolic_program.hpp"
#include "dfpm/meshes/mesh1d.hpp"

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace unsaturated {

class BoundaryConditions;

//! \brief 1D finite volume equation, with 1DOF per node
//!
//! This is to be subclassed by the other equations
template <typename Derived>
class FV1DOFEquation: public dfpmsolver::ParabolicProgram<FV1DOFEquation<Derived>>
{
public:

    //! \brief constructor
    FV1DOFEquation(index_t nb_nodes):
        m_tot_ndf(nb_nodes)
    {}

    //! \brief Return a pointer to the derived class instance
    Derived* derived() {return static_cast<Derived*>(this);}

    //! \brief Return the number of equations
    index_t get_neq() const {return m_neq;}
    //! \brief Return the number of degrees of freedom per node
    index_t get_ndf() const {return 1;}
    //! \brief Return the total number of degrees of freedom
    index_t get_tot_ndf() const  {return m_tot_ndf;}
    //! \brief Return the id of equation dof
    index_t id_equation(index_t node) {
        return derived()->id_equation_impl(node);
    }
    //! \brief Return the mesh
    mesh::Mesh1D* get_mesh() {
        return derived()->get_mesh_impl();
    }
    //! \brief hook called before computing residual for an element
    void pre_nodal_residual_hook(index_t node, const Vector& displacement) {
        return derived()->pre_nodal_residual_hook_impl(node, displacement);
    }
    //! \brief hook called before computing the residuals
    void pre_residual_hook(const Vector& displacement) {
        return derived()->pre_residual_hook_impl(displacement);
    }
    //! \brief hook called after computing the residuals
    void post_residual_hook(
            const Vector& displacement,
            const Vector& residuals) {
        return derived()->post_residual_hook_impl(displacement, residuals);
    }
    //! \brief Compute the residuals for an element
    //!
    //! \param[in] element the element id
    //! \param[in] displacement the vector of variables
    //! \param[in] velocity the vector of time derivatives
    //! \param[out] element_residual 2-sized vector containing residuals
    //! \param[in] use_chemistry_rate if false, chemistry rates are not included in residuals
    //!
    void residuals_element(
            index_t element,
            const Vector& displacement,
            const Vector& velocity,
            Eigen::Vector2d& element_residual,
            bool use_chemistry_rate
            ) {
        derived()->residuals_element_impl(
                    element,
                    displacement, velocity,
                    element_residual,
                    use_chemistry_rate);
    }

    //! \brief Compute the residuals for an element
    void residuals_element(
            index_t element,
            const Vector& displacement,
            const Vector& velocity,
            Eigen::Vector2d& element_residual
            ) {
        residuals_element(
                    element,
                    displacement, velocity,
                    element_residual,
                    true);
    }
    //! \brief Compute the residuals
    //!
    //! \param[in] displacement the vector of variables
    //! \param[in] velocity the vector of time derivatives
    //! \param[out] residuals vector containing the residuals
    //! \param[in] use_chemistry_rate if false, chemistry rates are not included in residuals
    //!
    void compute_residuals(
            const Vector& displacement,
            const Vector& velocity,
            Vector& residuals,
            bool use_chemistry_rate
            );
    //! \brief Compute the residuals
    void compute_residuals(
            const Vector& displacement,
            const Vector& velocity,
            Vector& residuals
            ) {
        compute_residuals(displacement, velocity, residuals, true);
    }
    //! \brief Compute the residuals at beginning of timestep
    void compute_residuals_0(
            const Vector& displacement,
            const Vector& velocity,
            Vector& residuals
            ) {
        compute_residuals(displacement, velocity, residuals, false);
    }
    //! \brief Compute the jacobian
    void compute_jacobian(Vector& displacement,
                          Vector& velocity,
                          Eigen::SparseMatrix<scalar_t>& jacobian,
                          scalar_t alphadt
                          );
    //! \brief Update the solution
    void update_solution(const Vector& update,
                         scalar_t lambda,
                         scalar_t alpha_dt,
                         Vector& predictor,
                         Vector& displacement,
                         Vector& velocity);

    //! \brief Set the scaling factor of the equation
    void set_scaling(scalar_t scaling_factor) {m_scaling = scaling_factor;}

    //! \brief Return the scaling factor
    scalar_t get_scaling() {return m_scaling;}

    //! \brief Register the number of equations
    void register_number_equations(scalar_t neq) {m_neq = neq;}

    //! \brief Number the equations
    void number_equations(const BoundaryConditions * const bcs) {
        derived()->number_equations(bcs);
    }


protected:
    scalar_t m_neq;     //!< number of equation
    scalar_t m_tot_ndf; //!< total number of dofs

    scalar_t m_scaling {1.0}; //!< scaling factor for the equation


};

} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp

#include "fv_1dof_equation.inl"

#endif // SPECMICP_REACTMICP_SYSTEMS_UNSATURATED_FV1DDOFEQUATION_HPP
