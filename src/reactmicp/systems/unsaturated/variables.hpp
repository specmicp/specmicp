/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_UNSATURATED_VARIABLES_HPP
#define SPECMICP_REACTMICP_UNSATURATED_VARIABLES_HPP

//! \file unsaturated/variables.hpp
//! \name Variables for the unsaturated system

#include "types_fwd.hpp"
#include "dfpm/meshes/mesh1d.hpp"
#include "specmicp_database/database_holder.hpp"

#include "variables_sub.hpp"

#include "reactmicp/solver/staggers_base/variables_base.hpp"

#include <functional>

namespace specmicp {
namespace reactmicp {
namespace systems {

//! \namespace unsaturated
//! \brief The unsaturated system
//!
//! The system solve the transport reactive in unsaturated porous medium
namespace unsaturated {


// forward declarations
// declared in variables_box.hpp
struct SaturationVariableBox;
struct SaturationPressureVariableBox;
struct PressureVariableBox;
struct LiquidAqueousComponentVariableBox;
struct LiquidGasAqueousVariableBox;


//! \brief The unsaturated variables
//!
//! This class contains all the variables for the unsaturated system
//!
//! Note : it should be initialized using the
//! specmicp::reactmicp::unsaturated::VariablesInterface class.
class SPECMICP_DLL_PUBLIC UnsaturatedVariables:
        public database::DatabaseHolder,
        public solver::VariablesBase
{
public:
    //! \brief Constructor, use default units
    //!
    //! Use VariablesInterface to create a new variable safely
    //!
    //! \internal
    UnsaturatedVariables(
            mesh::Mesh1DPtr the_mesh,
            database::RawDatabasePtr the_database,
            std::vector<bool> has_gas);
    //! \brief constructor
    //!
    //! Use Variables interface to create a new variable safely
    //!
    //! \internal
    UnsaturatedVariables(
            mesh::Mesh1DPtr the_mesh,
            database::RawDatabasePtr the_database,
            std::vector<bool> has_gas,
            const units::UnitsSet& units_set);

    //! \brief Reset the main variables in case of failure
    void reset_main_variables() override;

    //! \brief Return the number of governing equations
    //!
    //! The governing equations are the transport equations
    index_t nb_governing_equations() const {
        return get_database()->nb_aqueous_components()+1;
    }

    //! \brief Return the number of nodes
    index_t nb_nodes() const {
        return m_mesh->nb_nodes();
    }

    //! \brief Return the mesh
    mesh::Mesh1DPtr get_mesh() {
        return m_mesh;
    }

    //! \brief Return true if a component has a gas equation
    //! \param component water or aqueous component
    bool component_has_gas(index_t component) const {
        specmicp_assert_component_bounds(component, get_database());
        return m_has_gas[component];
    }

    //! \brief Return the number of gas in the system
    index_t nb_gas();

    // Variables Box
    // =============

    //! \name VariableBox
    //! \brief This set of variables are passed to the equations
    //!
    //! They contain all the variables a governing equation should need
    //! @{

    //! \brief Return the variables for the saturation equation
    SaturationVariableBox get_saturation_variables();

    //! \brief Return the variables for the saturation-pressure equation
    SaturationPressureVariableBox get_saturation_pressure_variables();

    //! \brief Return the variables for the vapor pressure equation
    PressureVariableBox get_vapor_pressure_variables();

    //! \brief Return the variables for the liquid transport of an
    //!  aqueous component
    LiquidAqueousComponentVariableBox get_liquid_aqueous_component_variables(
            index_t component);

    //! \brief Return the variables for the liquid and gas transport of an
    //!  aqueous component
    LiquidGasAqueousVariableBox get_liquid_gas_aqueous_variables(
            index_t component);

    //! \brief Return the variables for the pressure diffusion equation
    //!
    //! Valid for  water vapor or the gas corresponding to an aqueous component
    PressureVariableBox get_pressure_variables(index_t component);

    //! @}

    // User model
    // ==========

    //! \name User models
    //! \brief Models given by the user to compute saturated-dependant variables
    //!
    //! A model is a function taking a node and the saturation as the argument
    //! @{


    //! \brief Set the capillary pressure model
    void set_capillary_pressure_model(user_model_saturation_f func) {
        m_capillary_pressure_f = func;
    }

    //! \brief Return the capillary pressure model
    user_model_saturation_f get_capillary_pressure_model() {
        return m_capillary_pressure_f;
    }

    //! \brief Set the vapor pressure model
    void set_vapor_pressure_model(user_model_saturation_f func) {
        m_vapor_pressure_f = func;
    }

    //! \brief Return the vapor pressure model
    user_model_saturation_f get_vapor_pressure_model() {
        return m_vapor_pressure_f;
    }

    //! \brief Set the relative liquid permeability model
    void set_relative_liquid_permeability_model(user_model_saturation_f func) {
        m_relative_liquid_permeability_f = func;
    }

    //! \brief Set the relative liquid diffusivity model
    void set_relative_liquid_diffusivity_model(user_model_saturation_f func) {
        m_relative_liquid_diffusivity_f = func;
    }

    //! \brief Set the relative gas diffusivity model
    void set_relative_gas_diffusivity_model(user_model_saturation_f func) {
        m_relative_gas_diffusivity_f = func;
    }

    //! \brief Set the relative variables at 'node'
    void set_relative_variables(index_t node);
    //! \brief Set the relative variables
    void set_relative_variables();

    //! \brief Set the partial pressure at 'node'
    void set_vapor_pressure(index_t node);

    //! @}



    // Variables Getter
    // ================

    //! \name Variables Getter
    //! \brief Return the variables

    //! @{

    //! \brief Return a reference to the porosity variables
    SecondaryTransientVariable& get_porosity() {
        return m_porosity;}
    //! \brief Return a reference to the water aq. concentration variables
    SecondaryTransientVariable& get_water_aqueous_concentration() {
        return m_water_aq_concentration;}
    //! \brief Return a reference to the liquid permeability
    SecondaryVariable& get_liquid_permeability() {
        return m_liquid_permeability;
    }
    //! \brief Return a reference to the capillary pressure
    SecondaryVariable& get_capillary_pressure() {
        return m_capillary_pressure;
    }

    //! \brief Return a reference to the relative liquid permeability
    SecondaryVariable& get_relative_liquid_permeability() {
        return m_relative_liquid_permeability;
    }
    //! \brief Return a reference to the liquid diffusivity
    SecondaryVariable& get_liquid_diffusivity() {
        return m_liquid_diffusivity;
    }
    //! \brief Return a reference to the relative liquid diffusivity
    SecondaryVariable& get_relative_liquid_diffusivity() {
        return m_relative_liquid_diffusivity;
    }

    scalar_t & get_binary_gas_diffusivity(index_t component) {
        return m_binary_gas_diffusivity(component);
    }

    //! \brief Return a reference to the gas diffusivity resistance factor
    SecondaryVariable& get_resistance_gas_diffusivity() {
        return m_resistance_gas_diffusivity;
    }
    //! \brief Return a reference to the relative gas diffusivity
    SecondaryVariable& get_relative_gas_diffusivity() {
        return m_relative_gas_diffusivity;
    }
    SecondaryVariable& get_advection_flux() {
        return m_advection_flux;
    }

    //! \brief Return the liquid saturation
    MainVariable& get_liquid_saturation() {
        return m_liquid_var.water();
    }

    //! \brief Return the variables corresponding to the pressure
    //! \param component water or aqueous component
    MainVariable& get_pressure_main_variables(index_t component);

    //! \brief Return the aqueous concentration of an aqueous component
    MainVariable& get_aqueous_concentration(index_t aq_component) {
        specmicp_assert(aq_component >= 2 and aq_component < get_database()->nb_component());
        return m_liquid_var.aqueous_component(aq_component);
    }

    //! \brief Return the solid variables
    MainVariable& get_solid_concentration(index_t component) {
        specmicp_assert_component_bounds(component, get_database());
        return m_solid_var.component(component);
    }

    //! \brief Return the R*T product
    //!
    //! R : ideal gas constant
    //! T : temperature
    scalar_t get_rt() {return m_constants.rt;}

    //! \brief return the total pressure
    scalar_t get_total_pressure() {return m_constants.total_pressure;}

    //! \brief return the id of a gas
    scalar_t get_id_gas(index_t component) {
        specmicp_assert_component_bounds(component, get_database());
        return m_id_gas[component];
    }

    AdimensionalSystemSolution& get_adim_solution(index_t node) {
        return m_chem_sol.solution(node);
    }

    void set_adim_solution(index_t node,
                           const AdimensionalSystemSolution& new_solution
                           ) {
        m_chem_sol.update_solution(node, new_solution);
    }

    //! \brief Return the inert volume fraction
    scalar_t get_inert_volume_fraction(index_t node) const {
        return m_inert_volume_fraction(node);
    }
    //! \brief Return the inert volume fractions
    SecondaryVariable& get_inert_volume_fraction() {
        return m_inert_volume_fraction;
    }
    //! \brief Set the inert volume fraction
    void set_inert_volume_fraction(index_t node, scalar_t value) {
        m_inert_volume_fraction(node) = value;
    }


    //! @}

    //! \name Scaling
    //! \brief Scaling of equations
    //!
    //! @{


    //! \brief Return the scaling factor for aqueous equation of 'component'
    scalar_t get_aqueous_scaling(index_t component) {
        return m_aqueous_scaling(component);
    }
    //! \brief set the scaling factor for aqueous equation of 'component'
    void set_aqueous_scaling(index_t component, scalar_t value) {
        m_aqueous_scaling(component) = value;
    }
    //! \brief Return the scaling factor for aqueous equation of 'component'
    scalar_t get_gaseous_scaling(index_t component) {
        return m_gaseous_scaling(component);
    }
    //! \brief set the scaling factor for aqueous equation of 'component'
    void set_gaseous_scaling(index_t component, scalar_t value) {
        m_gaseous_scaling(component) = value;
    }


    //! @}

private:
    mesh::Mesh1DPtr m_mesh;

    std::vector<bool> m_has_gas;
    std::vector<index_t> m_id_gas;

    // main variables
    // --------------
    ListMainVariable m_liquid_var;
    ListMainVariable m_gas_var;
    ListMainVariable m_solid_var;

    // chemistry
    // ---------
    ChemistrySolutions m_chem_sol;
    SecondaryVariable  m_inert_volume_fraction;

    // secondary variables
    // -------------------
    SecondaryTransientVariable m_porosity;
    SecondaryTransientVariable m_water_aq_concentration;

    SecondaryVariable m_liquid_permeability;
    SecondaryVariable m_relative_liquid_permeability;
    SecondaryVariable m_liquid_diffusivity;
    SecondaryVariable m_relative_liquid_diffusivity;

    Vector m_binary_gas_diffusivity;
    SecondaryVariable m_resistance_gas_diffusivity;
    SecondaryVariable m_relative_gas_diffusivity;

    SecondaryVariable m_capillary_pressure;

    SecondaryVariable m_advection_flux;

    // scaled constants
    // ----------------
    ConstantBox m_constants;

    // user models
    // -----------
    user_model_saturation_f m_capillary_pressure_f {nullptr};
    user_model_saturation_f m_vapor_pressure_f {nullptr};

    user_model_saturation_f m_relative_liquid_permeability_f {nullptr};
    user_model_saturation_f m_relative_liquid_diffusivity_f {nullptr};
    user_model_saturation_f m_relative_gas_diffusivity_f {nullptr};

    // vector of scaling
    Vector m_aqueous_scaling;
    Vector m_gaseous_scaling;

    // These struct are declared as friends,
    // To make their initialisation easier
    // (initialisation of referenece, and const reference)
    friend struct SaturationVariableBox;
    friend struct SaturationPressureVariableBox;
    friend struct PressureVariableBox;
    friend struct LiquidAqueousComponentVariableBox;
    friend struct LiquidGasAqueousVariableBox;
};

//! \brief Shared pointer the unsaturated variables
using UnsaturatedVariablesPtr = std::shared_ptr<UnsaturatedVariables>;

//! \brief Cast from base variables
inline UnsaturatedVariablesPtr cast_from_base(
        std::shared_ptr<solver::VariablesBase> ptr) {
    return std::static_pointer_cast<UnsaturatedVariables>(ptr);
}

} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp

#endif // SPECMICP_REACTMICP_UNSATURATED_VARIABLES_HPP
