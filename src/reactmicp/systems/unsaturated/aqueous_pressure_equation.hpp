/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_UNSATURATED_AQUEOUSPRESSUREEQUATION_HPP
#define SPECMICP_REACTMICP_UNSATURATED_AQUEOUSPRESSUREEQUATION_HPP

//! \file unsaturated/aqueous_pressure_equation.hpp
//! \brief The equation for the liquid and gas transport of aqueous component

#include "specmicp_common/types.hpp"
#include "fv_1dof_equation.hpp"

#include "dfpm/meshes/mesh1dfwd.hpp"

#include "variables.hpp"
#include <memory>

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace unsaturated {

class BoundaryConditions;

//! \brief Equation for liquid transport of aqueous component
class SPECMICP_DLL_LOCAL AqueousGasTransportEquation:
        public FV1DOFEquation<AqueousGasTransportEquation>
{
    using base = FV1DOFEquation<AqueousGasTransportEquation>;
    using base::get_scaling;
    using base::register_number_equations;

public:
    //! \brief Constructor
    AqueousGasTransportEquation(
            uindex_t id_component,
            mesh::Mesh1DPtr the_mesh,
            LiquidGasAqueousVariableBox& variables,
            std::shared_ptr<BoundaryConditions> bcs
            );

    ~AqueousGasTransportEquation();


    //! \brief Compute the residuals inside 'element'
    void residuals_element_impl(
            index_t element,
            const Vector& displacement,
            const Vector& velocity,
            Eigen::Vector2d& element_residual,
            bool use_chemistry_rate
            );

    //! \brief Return the index of the equation for dof 'id_dof'
    index_t id_equation_impl(index_t id_dof);
    //! \brief Return the mesh
    mesh::Mesh1D* get_mesh_impl();

    //! \brief Called before computing the nodal residuals
    void pre_nodal_residual_hook_impl(index_t node, const Vector& displacement);
    //! \brief Called before computing the residuals
    void pre_residual_hook_impl(const Vector& displacement);
    //! \brief Called after computing the residuals
    void post_residual_hook_impl(const Vector& displacement,
                                 const Vector& residuals);

    //! \brief Compute the transport rates for main algorithm
    void compute_transport_rate(scalar_t dt, const Vector& displacement);

private:
    //! \brief Number the equations according the boundary conditions
    void number_equations();

    struct AqueousGasTransportEquationImpl;
    std::unique_ptr<AqueousGasTransportEquationImpl> m_impl; //!< The implementation
};

} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp

#endif // SPECMICP_REACTMICP_UNSATURATED_AQUEOUSPRESSUREEQUATION_HPP
