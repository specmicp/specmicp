/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

//! \file unsaturated/variables_sub.hpp
//! \brief base structures for the variables

#ifndef SPECMICP_REACTMICP_UNSATURATED_VARIABLESSUB_HPP
#define SPECMICP_REACTMICP_UNSATURATED_VARIABLESSUB_HPP

#include "specmicp_common/types.hpp"
#include "specmicp/adimensional/adimensional_system_solution.hpp"

#include "specmicp_common/compat.hpp"

#include "specmicp_common/physics/constants.hpp"
#include "specmicp_common/physics/units.hpp"

#include <vector>
#include <memory>

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace unsaturated {

//! \brief A simple variable
struct BaseVariable
{

    Vector variable;      //!< The variable

    //! \brief Return value of the variable
    scalar_t& operator() (index_t node) {
        return variable(node);
    }

    //! \brief Return value of the variable
    scalar_t operator() (index_t node) const {
        return variable(node);
    }

    //! \brief Return the size of the vector
    index_t size() const {
        return variable.rows();
    }

    //! \brief The variables are all set to 'value'
    void set_constant(scalar_t value) {
        variable.setConstant(value);
    }

    //! \brief Set the variables to zero
    void set_zero() {
        variable.setZero();
    }

    //! \brief Build a variable of size 'size'
    //!
    //! The varaibles are initialized to zero
    BaseVariable(index_t size):
        variable(Vector::Zero(size))
    {}
};

//! \brief A transient variable
//!
//! These type of variable store the rate of change of the variable (velocity)
//! and the value at the beginning of the timestep (predictor)
struct BaseTransientVariable:
        public BaseVariable
{
    Vector velocity;      //!< Rate of change of the variable
    Vector predictor;     //!< Predictor, value before first iteration

    //! \brief Constructor
    //!
    //! \param size number of dofs
    BaseTransientVariable(index_t size):
        BaseVariable(size),
        velocity(Vector::Zero(size)),
        predictor(Vector::Zero(size))
    {}

    //! \brief Update the variable
    void update(scalar_t dt) {
        variable = predictor + dt*velocity;
    }
};

//! \brief A main variable, used to solve the governing equations
//!
//! These variables also store the chemistry and transport rates for the
//!  coupling
struct MainVariable:
        public BaseTransientVariable
{
    //! \brief Transport fluxes of the corresponding governing equations
    Vector transport_fluxes;
    //! \brief Chemical exchange rate
    Vector chemistry_rate;


    //! \brief Constructor
    //!
    //! \param size number of dofs
    MainVariable(index_t size):
        BaseTransientVariable(size),
        transport_fluxes(Vector::Zero(size)),
        chemistry_rate(Vector::Zero(size))
    {}

    //! \brief Reset the variables
    //!
    //! This function is called if the solver failed and must be restarted
    void reset() {
        transport_fluxes.setZero();
        chemistry_rate.setZero();
        velocity.setZero();
        variable = predictor;
    }
};

//! \brief List of main variables
struct ListMainVariable
{
    // Variables are stored as pointer so we can skip components
    // that do not exist
    // But still access their variables through their index
    using value_type = std::unique_ptr<MainVariable>; //!< Type of a variable
    using vector_type = std::vector<value_type>; //!< Type of a vector of variables
    using iterator = vector_type::iterator; //!< Type of an iterator on the vector of variables
    using const_iterator = vector_type::const_iterator; //!< Type of a const iterator on the vector of variables

    vector_type variables; //!< List of main variables

    //! \brief Return the main variables for water component
    MainVariable& water() {
        return *variables[0];
    }

    //! \brief Return the main variables for an aqueous component
    MainVariable& aqueous_component(index_t aq_component) {
        specmicp_assert(variables[aq_component] != nullptr);
        return *variables[aq_component];
    }

    //! \brief Return the main variables for a component
    MainVariable& component(index_t component) {
        specmicp_assert(variables[component] != nullptr);
        return *variables[component];
    }

    //! \brief Return the main variables for ;component'
    MainVariable* operator() (index_t component) {
        return variables[component].get();
    }

    // Implementations of the following methods
    // is in variables.cpp

    //! \brief Initialize the variables
    ListMainVariable(index_t nb_vars, index_t nb_nodes);
    //! \brief Initialize some variables
    //!
    //! Only initialize values given by 'to_init'
    ListMainVariable(index_t nb_vars, index_t nb_nodes, std::vector<bool> to_init);

    //! \brief Reset the variables
    void reset();

    //! \brief Hard reset, erase all info for a component;
    void hard_reset(index_t component, index_t nb_nodes);
};

//! \brief A secondary variable
//!
//! A secodnary variables is a parameter needed in governing equations but not
//! the main variable
struct SecondaryVariable:
        public BaseVariable
{
    //! \brief Constructor
    //!
    //! \param size number of dofs (= number of nodes)
    SecondaryVariable(index_t size):
        BaseVariable(size)
    {}

};

//! \brief A secondary transient variable
//!
//! e.g. the porosity
struct SecondaryTransientVariable:
        public BaseTransientVariable
{

    //! \brief constructor
    //!
    //! \param size number of dofs
    SecondaryTransientVariable(index_t size):
        BaseTransientVariable(size)
    {}
};

//! \brief The chemistry solutions
struct ChemistrySolutions
{
    std::vector<AdimensionalSystemSolution> solutions; //!< The set of chemistry solutions

    //! \brief Constructor
    //!
    //! \param size the number of nodes
    ChemistrySolutions(index_t size):
        solutions(size)
    {}

    //! \brief Return a solution
    AdimensionalSystemSolution& solution(index_t node) {
        return solutions[node];
    }

    //! \brief Return a const solution
    const AdimensionalSystemSolution& solution(index_t node) const {
        return solutions[node];
    }

    //! \brief Set a solution
    void update_solution(
            index_t node,
            const AdimensionalSystemSolution& new_solution
            ) {
        solutions[node] = new_solution;
    }
};

//! \brief List of constants used in the problems
struct ConstantBox
{
    //! \brief R*T
    //!
    //! R : ideal gas constant
    //! T : temperature
    scalar_t rt {constants::gas_constant*units::celsius(25.0)};
    //! \brief  Viscosity of liquid water
    scalar_t viscosity_liquid_water {constants::water_viscosity};
    //! \brief Total pressure
    scalar_t total_pressure {1.01325e5};

    //! \brief Scale the constants using
    void scale(const units::UnitsSet& units_set);
};




} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp

#endif // SPECMICP_REACTMICP_UNSATURATED_VARIABLESSUB_HPP
