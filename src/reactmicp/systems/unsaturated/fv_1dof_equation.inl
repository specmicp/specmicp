/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

//! \file unsaturated/fv_1dof_equation.inl
//! \brief Implementation of 1dof equation for unsaturated system

#include "fv_1dof_equation.hpp" // for syntaxing coloring parsing
#include "dfpm/types.hpp"

#include <iostream>

namespace specmicp {
namespace reactmicp {
namespace systems {
namespace unsaturated {

template<typename Derived>
void FV1DOFEquation<Derived>::compute_jacobian(
        Vector& displacement,
        Vector& velocity,
        Eigen::SparseMatrix<scalar_t>& jacobian,
        scalar_t alphadt
        )
{
    mesh::Mesh1D* m_mesh = get_mesh();
    dfpm::list_triplet_t jacob;
    const index_t estimation = 3*get_neq();
    jacob.reserve(estimation);
    // assume relative variables are set
    for (index_t element: m_mesh->range_elements())
    {
        Eigen::Vector2d element_residual_orig;
        residuals_element(element, displacement, velocity, element_residual_orig, false);

        for (index_t enodec=0; enodec<2; ++enodec)
        {
            const index_t nodec = m_mesh->get_node(element, enodec);
            if (not (id_equation(nodec) > no_equation)) continue;

            const scalar_t tmp_d = displacement(nodec);
            const scalar_t tmp_v = velocity(nodec);

            scalar_t h = eps_jacobian*std::abs(tmp_v);
            if (h<eps_jacobian) h = eps_jacobian;
            velocity(nodec) = tmp_v + h;
            h = velocity(nodec) - tmp_v;

            displacement(nodec) = tmp_d + alphadt*h;

            pre_nodal_residual_hook(nodec, displacement);

            Eigen::Vector2d element_residual;
            residuals_element(element, displacement, velocity, element_residual, false);

            displacement(nodec) = tmp_d;
            velocity(nodec) = tmp_v;
            pre_nodal_residual_hook(nodec, displacement);

            for (index_t enoder=0; enoder<2; ++enoder)
            {
                const index_t noder = m_mesh->get_node(element, enoder);
                if (not (id_equation(noder) > no_equation)) continue;

                jacob.push_back(dfpm::triplet_t(
                              id_equation(noder),
                              id_equation(nodec),
                              (element_residual(enoder) - element_residual_orig(enoder))/h
                              ));
            }
        }
    }
    jacobian = Eigen::SparseMatrix<scalar_t>(get_neq(), get_neq());
    jacobian.setFromTriplets(jacob.begin(), jacob.end());
}


template<typename Derived>
void FV1DOFEquation<Derived>::compute_residuals(
        const Vector& displacement,
        const Vector& velocity,
        Vector& residuals,
        bool use_chemistry_rate
        )
{
    mesh::Mesh1D* m_mesh = get_mesh();
    residuals.setZero(get_neq());

    pre_residual_hook(displacement);

    Eigen::Vector2d element_residual;
    for(index_t element: m_mesh->range_elements())
    {
        residuals_element(element, displacement, velocity, element_residual, use_chemistry_rate);

        const index_t node_0 = m_mesh->get_node(element, 0);
        if (id_equation(node_0) > no_equation)
        {
            residuals(id_equation(node_0)) += element_residual(0);
        }
        const index_t node_1 = m_mesh->get_node(element, 1);
        if (id_equation(node_1) > no_equation)
        {
            residuals(id_equation(node_1)) += element_residual(1);
        }

    }

    post_residual_hook(displacement, residuals);
}

template<typename Derived>
void FV1DOFEquation<Derived>::update_solution(
        const Vector& update,
        scalar_t lambda,
        scalar_t alpha_dt,
        Vector& predictor,
        Vector& displacement,
        Vector& velocity
        )
{
    for (index_t node: get_mesh()->range_nodes())
    {
        if (id_equation(node) > no_equation)
        {
            velocity(node) += lambda*update(id_equation(node));
        }
    }
    displacement = predictor + alpha_dt*velocity;
    derived()->compute_transport_rate(alpha_dt, displacement);


}

} //end namespace unsaturated
} //end namespace systems
} //end namespace reactmicp
} //end namespace specmicp
