/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "eqcurve_extractor.hpp"

#include <iostream>

namespace specmicp {
namespace reactmicp {
namespace eqcurve {


index_t  EquilibriumCurveExtractor::find_point(scalar_t x)
{
    index_t index;
    const index_t size = m_eqcurve.rows();
    index_t lower_index = 0;
    index_t upper_index = size+1;

    while (upper_index - lower_index > 1)
    {
        index = (upper_index + lower_index) /2;
        if ((x >= xs_offset(index)) == is_increasing())
            lower_index = index;
        else
            upper_index = index;
    }
    if (x == xs_offset(1)) index = 0;
    else if (x == xs_offset(size)) index = size-1;
    else index = std::max((specmicp::index_t) 0, lower_index-1);

    return index;
}


scalar_t EquilibriumCurveExtractor::interpolate(index_t index, scalar_t x, index_t col)
{
    // limit
    if (is_increasing())
    {
        if (x <= m_eqcurve(first(), 0))
            return m_eqcurve(first(), col);
        else if (x >= m_eqcurve(last(), 0))
            return m_eqcurve(last(), col);
    }
    else
    {
        if (x >= m_eqcurve(first(), 0))
            return m_eqcurve(first(), col);
        else if (x <= m_eqcurve(last(), 0))
            return m_eqcurve(last(), col);
    }
    scalar_t y;

    const scalar_t diff = slope(index, col);
    y = m_eqcurve(index, col) + diff*(m_eqcurve(index+1,0)-x);

    return y;
}

scalar_t EquilibriumCurveExtractor::slope(index_t index, index_t col)
{
    if (index == last()) return (m_eqcurve(index-1,col)-m_eqcurve(index,col))/(m_eqcurve(index-1,0)-m_eqcurve(index,0));
    return (m_eqcurve(index+1,col)-m_eqcurve(index,col))/(m_eqcurve(index+1,0)-m_eqcurve(index,0));
}

} // end namespace eqcurve
} // end namespace reactmicp
} // end namespace specmicp
