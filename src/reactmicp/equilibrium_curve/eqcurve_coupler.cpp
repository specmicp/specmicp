/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#include "eqcurve_coupler.hpp"

#include "dfpm/meshes/mesh1d.hpp"
#include "dfpm/1dtransport/diffusion_parameters.hpp"

#include "dfpm/solver/parabolic_driver.hpp"

namespace specmicp {
namespace reactmicp {
namespace eqcurve {

EquilibriumCurveCoupler::EquilibriumCurveCoupler(Matrix& eq_curve,
                        mesh::Mesh1DPtr the_mesh
                        , dfpmsolver::ParabolicDriverOptions options):
    m_eqcurve(eq_curve),
    m_mesh(the_mesh),
    m_param(std::make_shared<dfpm::SaturatedDiffusion1DParameters>(m_mesh->nb_nodes())),
    m_aqueous_concentrations(the_mesh->nb_nodes()),
    m_solid_concentrations(the_mesh->nb_nodes()),
    m_transport_program(the_mesh, m_param, {0,}),
    m_transport_solver(m_transport_program)
{

    m_transport_solver.get_options() = options;

    index_t last = m_eqcurve.last();
    m_aqueous_concentrations(0) = m_eqcurve.totaq_concentration(last);
    m_solid_concentrations(0) = m_eqcurve.totsolid_concentration(last);
    m_param->porosity(0) = m_eqcurve.porosity(last);
    m_param->diffusion_coefficient(0) = m_eqcurve.diffusion_coefficient(last);

    index_t first = m_eqcurve.first();
    for (index_t node=1; node<the_mesh->nb_nodes(); ++node)
    {
        m_aqueous_concentrations(node) = m_eqcurve.totaq_concentration(first);
        m_solid_concentrations(node) = m_eqcurve.totsolid_concentration(first);
        m_param->porosity(node) = m_eqcurve.porosity(first);
        m_param->diffusion_coefficient(node) = m_eqcurve.diffusion_coefficient(first);
    }
}

void EquilibriumCurveCoupler::run_step(scalar_t timestep)
{
    dfpmsolver::ParabolicDriverReturnCode retcode = m_transport_solver.solve_timestep(timestep, m_aqueous_concentrations);
    if (retcode <= dfpmsolver::ParabolicDriverReturnCode::NotConvergedYet)
    {
        throw std::runtime_error("Cannot solve the transport problem, retcode : "+std::to_string((int) retcode));
    }

    chemistry_step();
}

void EquilibriumCurveCoupler::chemistry_step()
{
    for (index_t node=1; node<m_mesh->nb_nodes(); ++node)
    {
        index_t index = m_eqcurve.find_point(m_solid_concentrations(node));
        //std::cout << index << std::endl;
        scalar_t ceq = m_eqcurve.totaq_concentration(index);
        //std::cout << m_solid_concentrations(node) << " - diff : " << m_eqcurve.porosity(index)*(ceq-m_aqueous_concentrations(node)) << std::endl;
        m_solid_concentrations(node) -= std::min( m_solid_concentrations(node),
                                                  m_eqcurve.porosity(index)*(ceq-m_aqueous_concentrations(node)));
        m_aqueous_concentrations(node) = ceq;


        // ###FIXME -> locate node using previous guess
        index = m_eqcurve.find_point(m_solid_concentrations(node));
        m_param->porosity(node) = m_eqcurve.porosity(index);
        m_param->diffusion_coefficient(node)= m_eqcurve.diffusion_coefficient(index);

    }
}


} // end namespace eqcurve
} // end namespace reactmicp
} // end namespace specmicp
