/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_EQUILIBRIUMCURVE_CHEMISTRY_HPP
#define SPECMICP_REACTMICP_EQUILIBRIUMCURVE_CHEMISTRY_HPP

#include "specmicp_common/types.hpp"
#include "specmicp/adimensional/equilibrium_curve.hpp"
#include "specmicp_common/physics/units.hpp"

//! \file reactmicp/equilibrium_curve/chemistry.hpp
//! \brief Compute the equilibrium curve

namespace specmicp {
namespace reactmicp {

namespace eqcurve {


//! \brief Compute the equilibrium curve
class SPECMICP_DLL_PUBLIC EquilibriumCurveSpeciation:
        public EquilibriumCurve,
        public units::UnitBaseClass
{
public:
    //! \brief Constructor
    EquilibriumCurveSpeciation(
            RawDatabasePtr thedatabase,
            AdimensionalSystemConstraints constraints,
            index_t id_component,
            AdimensionalSystemSolverOptions options
            ):
        EquilibriumCurve(thedatabase, constraints),
        UnitBaseClass(options.units_set),
        m_data(thedatabase),
        m_idc(id_component)
    {
        solver_options() = options;
    }

    //! \brief Post Processing of a timestep
    void output();

    //! \brief Preprocessing of a timestep
    void update_problem();

    //! \brief Return the equilibrium curve
    //!
    //! \return a matrix N_timestep*4 (Solid, Aqueous, Porosity, Diffusivity)
    //!
    //! Diffusion coefficient is cement specific
    // ## FIXME
    Matrix get_equilibrium_curve(scalar_t end_total_concentration, scalar_t delta);

    //! \brief Pre-Processing, return step perturbation
    Vector get_perturbation();

    //! \brief brief error message
    void error_handling(std::string msg) const;

private:
    RawDatabasePtr m_data;  //!< the database

    index_t m_idc;          //!< Index of the controlling component

    Matrix m_eqcurve;       //!< The equilibrium curve

    index_t m_cnt;          //!< Number of timestep
    scalar_t m_delta;       //!< Timestep duration
};

} // end namespace eqcurve
} // end namespace reactmicp
} // end namespace specmicp

#endif // SPECMICP_REACTMICP_EQUILIBRIUMCURVE_CHEMISTRY_HPP
