/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */


#include "timestepper.hpp"

#include "reactive_transport_solver_structs.hpp"

namespace specmicp {
namespace reactmicp {
namespace solver {

scalar_t Timestepper::next_timestep(
        scalar_t dt_done,
        ReactiveTransportReturnCode return_code,
        index_t nb_iterations
        )
{
    if (return_code <= ReactiveTransportReturnCode::NotConvergedYet)
    {
        return get_options().decrease_failure*dt_done;
    }

    // previous timestep is correct
    m_total += dt_done;
    m_average.add_point(nb_iterations);

    scalar_t proposed_dt = dt_done;

    // If the error is minimized we increase the timestep
    if (return_code == ReactiveTransportReturnCode::ErrorMinimized)
    {
        proposed_dt *= get_options().increase_error_minimization;
    }
    else
    // Increase or decrease the timestep to reach the number of iteration target range
    {
        if (m_average.current_value() <= get_options().iteration_lower_target)
        {
            proposed_dt *= get_options().increase_factor;
        }
        else if (m_average.current_value() > get_options().iteration_upper_target)
        {
            proposed_dt *= get_options().decrease_factor;
        }
    }
    // Check that the total target is not exceeded
    if (m_total + proposed_dt > m_total_target)
    {
        proposed_dt = m_total_target - m_total;
    }

    // Check that the timestep is inside the bounds
    if (proposed_dt < get_options().lower_bound)
    {
        proposed_dt = get_options().lower_bound;
    }
    else if (proposed_dt > get_options().upper_bound)
    {
        proposed_dt = get_options().upper_bound;
    }

    return proposed_dt;

}

} // end namespace solver
} // end namespace reactmicp
} // end namespace specmicp
