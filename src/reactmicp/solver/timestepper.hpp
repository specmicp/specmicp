/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_SOLVER_TIMESTEPPER_HPP
#define SPECMICP_REACTMICP_SOLVER_TIMESTEPPER_HPP

//! \file reactmicp/solver/timestepper.hpp
//! \brief Manage the timestep duration in the reactive transport solver

#include "specmicp_common/moving_average.hpp"
#include "specmicp_common/options_handler.hpp"

#include "timestepper_structs.hpp"

namespace specmicp {
namespace reactmicp {
namespace solver {

// forward declaration
enum class ReactiveTransportReturnCode;


//! \brief Adaptative timestepper for the reactive transport solver
//!
//! The adaptative timestep algorithm selects the timestep depending on the
//! number of fix-point iterations. An average of this number is used to smooth
//! its value and take into account several timesteps. The average used is an
//! exponential moving average.
//!
//! This class can be used directly by the user, or indirectly through the
//! ReactMiCP 'runner' specmicp::reactmicp::solver::ReactiveTransportRunner.
//!
//! \sa specmicp::reactmicp::solver::ReactiveTransportRunner
//! \sa specmicp::utils::ExponentialMovingAverage
class SPECMICP_DLL_PUBLIC Timestepper:
        public OptionsHandler<TimestepperOptions>
{
public:
    //! \brief Construct a stepper
    //!
    //! \param dt_lower_bound lower_bound for the timestep
    //! \param dt_upper_bound upper_bound for the timestep
    //! \param total_target total target time
    //! \param init_iterations initial iterations
    Timestepper(scalar_t dt_lower_bound,
                scalar_t dt_upper_bound,
                scalar_t total_target,
                scalar_t init_iterations
                ):
        OptionsHandler(dt_lower_bound, dt_upper_bound),
        m_total(0),
        m_total_target(total_target),
        m_average(0.5, init_iterations)
    {
        m_average.set_alpha(get_options().alpha_average);
    }

    //! \brief Return the total time
    scalar_t get_total() const {return m_total;}
    //! \brief Return the total target time
    scalar_t get_total_target() const {return m_total_target;}
    //! \brief Set the total target time
    void set_total_target(scalar_t total_target) {m_total_target = total_target;}

    //! \brief Set the average parameter
    //!
    //! \param alpha parameter of the exponential moving average
    void set_average_parameter(scalar_t alpha) {
        get_options().alpha_average = alpha;
        m_average.set_alpha(alpha);
    }

    //! \brief Obtain the next timestep
    //!
    //! \param dt_done duration of the previous timestep
    //! \param return_code return code of the previous timestep
    //! \param nb_iterations number of fix-point iterations of the previous timestep
    //! \return a timestep duration
    scalar_t next_timestep(
            scalar_t dt_done,
            ReactiveTransportReturnCode return_code,
            index_t nb_iterations
            );

private:
    scalar_t m_total; //!< The current time
    scalar_t m_total_target; //!< Target value

    utils::ExponentialMovingAverage m_average; //!< average number of iterations
};

} // end namespace solver
} // end namespace reactmicp
} // end namespace specmicp

#endif // SPECMICP_REACTMICP_SOLVER_TIMESTEPPER_HPP
