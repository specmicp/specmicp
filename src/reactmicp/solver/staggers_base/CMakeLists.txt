set( reactmicp_solver_staggers_headers
    variables_base.hpp
    transport_stagger_base.hpp
    chemistry_stagger_base.hpp
    upscaling_stagger_base.hpp
    stagger_structs.hpp
    decl.inl
    staggers_base.hpp

)

add_to_reactmicp_headers_list(reactmicp_solver_staggers_headers)

INSTALL(FILES ${reactmicp_solver_staggers_headers}
    DESTINATION ${INCLUDE_INSTALL_DIR}/reactmicp/solver/staggers_base
)

