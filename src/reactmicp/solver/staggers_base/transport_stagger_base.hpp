/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */

#ifndef SPECMICP_REACTMICP_SOLVER_TRANSPORTSTAGGERBASE_HPP
#define SPECMICP_REACTMICP_SOLVER_TRANSPORTSTAGGERBASE_HPP

//! \file transport_stagger_base.hpp The base class for the transport stagger

// following file include main data types and forward declaration needed for a stagger
#include "decl.inl"

namespace specmicp {
namespace reactmicp {
namespace solver {

//! \brief The base class for a transport stagger
//!
class TransportStaggerBase
{
public:
    virtual ~TransportStaggerBase() {}

    //! \brief Initialize the stagger at the beginning of the computation
    //!
    //! \param var raw pointer to the variables
    virtual void initialize(VariablesBase * const var) {}

    //! \brief Initialize the stagger at the beginning of the computation
    //!
    //! \param var shared_ptr to the variables
    void initialize(std::shared_ptr<VariablesBase> var) {
        return initialize(var.get());
    }

    //! \brief Initialize the stagger at the beginning of an iteration
    //!
    //! This is where the first residual may be computed, the predictor saved, ...
    //! \param dt the duration of the timestep
    //! \param var raw pointer to the variables
    virtual void initialize_timestep(
            scalar_t dt,
            VariablesBase * const var
            ) = 0;

    //! \brief Solve the equation for the timetep
    //!
    //! \param var raw pointer to the variables
    virtual StaggerReturnCode restart_timestep(
            VariablesBase * const var
            ) = 0;

    //! \brief Compute the residuals norm
    //!
    //! \param var raw pointer to the variables
    virtual scalar_t get_residual(
            VariablesBase * const var
            ) = 0;

    //! \brief Compute the residuals norm
    //!
    //! \param var raw pointer to the variables
    virtual scalar_t get_residual_0(
            VariablesBase * const var
            ) = 0;

    //! \brief Obtain the norm of the step size
    //!
    //! This is used to check if the algorithm has reach a stationary points.
    //! It should look like : return main_variables.norm()
    //!
    //! \param var shared_ptr to the variables
    virtual scalar_t get_update(
            VariablesBase * const var
            ) = 0;

    //! \brief Print debug information when timestep has failed
    virtual void print_debug_information(
            VariablesBase * const var
            ) {}
};

} // end namespace solver
} // end namespace reactmicp
} // end namespace specmicp

#endif // SPECMICP_REACTMICP_SOLVER_TRANSPORTSTAGGERBASE_HPP
