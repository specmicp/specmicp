#include "catch.hpp"

#include "specmicp_common/eigen/value_checker.hpp"

using namespace specmicp;
using namespace value_checker;

specmicp_create_value_predicate(GreaterThan0, (value > 0.0));
specmicp_create_value_predicate(LessThan5, (value < 5.0));

specmicp_strict_lower_bound_predicate(GreaterThan5, 5.0);
specmicp_strict_upper_bound_predicate(LowerThan0, 0.0);



TEST_CASE("Value checker", "checker") {

    SECTION("True for all") {
        TrueForAll<GreaterThan0> checker;

        Vector test1(4);
        test1 << 1, 2, 3, 4;
        CHECK(checker.check(test1) == IsValidVector::good);

        Vector test2(4);
        test2 << 1, -2, 3, 4;
        CHECK(checker.check(test2) == IsValidVector::error);
    }

    SECTION("True for at least one") {
        TrueForAtLeastOne<IsNonZero> checker;

        Vector test_good(4);
        test_good << 0, 0, 1, 0;
        CHECK(checker.check(test_good) == IsValidVector::good);

        Vector test_good_2(4);
        test_good << 2, 1, 1, 4;
        CHECK(checker.check(test_good_2) == IsValidVector::good);

        Vector test_bad(4);
        test_bad << 0.0, 0.0, 0.0, 0.0;
        CHECK(checker.check(test_bad) == IsValidVector::error);

    }

    SECTION("Checker composer") {
        Composer<TrueForAll<GreaterThan0>,
                 TrueForAll<LessThan5,IsValidVector::critical>> checker;

        Vector test_good(4);
        test_good << 1, 2, 3, 4;
        CHECK(checker.check(test_good) == IsValidVector::good);

        Vector test_bad_1(4);
        test_bad_1 << 1, 2, 3, 6;
        CHECK(checker.check(test_bad_1) == IsValidVector::critical);

        Vector test_bad_2(4);
        test_bad_2 << 1, 2, -1, 6;
        CHECK(checker.check(test_bad_2) == IsValidVector::critical);

        Vector test_bad_3(4);
        test_bad_3 << 1, 2, -1, 2;
        CHECK(checker.check(test_bad_3) == IsValidVector::error);
    }

    SECTION("Predicate composer - And") {
        TrueForAll<PredicateAnd<GreaterThan0,LessThan5>> checker_and;

        Vector test_good(4);
        test_good << 1, 2, 3, 4;
        CHECK(checker_and.check(test_good) == IsValidVector::good);


        Vector test_bad_1(4);
        test_bad_1 << 1, 2, 3, 6;
        CHECK(checker_and.check(test_bad_1) == IsValidVector::error);

        Vector test_bad_2(4);
        test_bad_2 << 1, 2, -1, 6;
        CHECK(checker_and.check(test_bad_2) == IsValidVector::error);

        Vector test_bad_3(4);
        test_bad_3 << 1, 2, -1, 2;
        CHECK(checker_and.check(test_bad_3) == IsValidVector::error);

   }

   SECTION("Predicate composer - Or/Xor") {
        TrueForAll<PredicateOr<LowerThan0,GreaterThan5>> checker_or;

        Vector test_good_or(4);
        test_good_or << -1, -2, 6, 7;
        CHECK(checker_or.check(test_good_or) == IsValidVector::good);

        Vector test_bad_or(4);
        test_bad_or << -1, 3, 6, 7;
        CHECK(checker_or.check(test_bad_or) == IsValidVector::error);


        TrueForAll<PredicateXOr<GreaterThan0,LessThan5>> checker_xor;
        CHECK(checker_xor.check(test_good_or) == IsValidVector::good);
        CHECK(checker_xor.check(test_bad_or) == IsValidVector::error);
    }


   SECTION("Predicate composer - Not") {
        TrueForAll<PredicateNot<GreaterThan0>> checker_not;

        Vector test_good_not(4);
        test_good_not << -1, -2, -3, -4;
        CHECK(checker_not.check(test_good_not) == IsValidVector::good);

        Vector test_bad_not(4);
        test_bad_not << -1, -2, 3, -4;
        CHECK(checker_not.check(test_bad_not) == IsValidVector::error);
    }

}
