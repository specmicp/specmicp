# Catch test
#===========

#common

# mock test library
add_library(test_to_load SHARED EXCLUDE_FROM_ALL
    test_to_load.cpp
)
set_target_properties(test_to_load PROPERTIES PREFIX "")


add_custom_target(test_pasic_plugin_incl
    SOURCES test_basic_plugin_mockobject.hpp
)

add_library(test_basic_plugin SHARED EXCLUDE_FROM_ALL
    test_basic_plugin.cpp
)
set_target_properties(test_basic_plugin PROPERTIES PREFIX "")
target_link_libraries(test_basic_plugin specmicp_common_static)


set(SPECMICP_COMMON_TEST_FILES
    test_common.cpp

    all_io_files.cpp
    butchertableau.cpp
    cli.cpp
    condition_number.cpp
    dynamic_loader.cpp
    embeddedrungekutta.cpp
    laws.cpp
    micpsolver.cpp
    misc.cpp
    ncp_functions.cpp
    plugin_manager.cpp
    range_iterator.cpp
    units.cpp
    value_checker.cpp
    vector_checker.cpp
    sparse_solvers.cpp
    yaml.cpp
)

set_source_files_properties(
    misc.cpp
    PROPERTIES
    COMPILE_DEFINITIONS "CURRENT_DIRECTORY=\"${CMAKE_CURRENT_BINARY_DIR}\""
    )

if (HDF5_FOUND)
    list(APPEND SPECMICP_COMMON_TEST_FILES
        hdf5_all.cpp
    )
    include_directories(${HDF5_INCLUDE_DIRS})
    set_source_files_properties(${COMMON_TEST_DIR}/hdf5_all.cpp
         PROPERTIES COMPILE_DEFINITIONS HDF5_DEFINITIONS)
endif()

add_catch_test(NAME common SOURCES
    ${SPECMICP_COMMON_TEST_FILES}
    LINK_LIBRARIES specmicp_common_static
    DEPENDS test_to_load test_basic_plugin
)
