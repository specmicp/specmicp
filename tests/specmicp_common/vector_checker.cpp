#include "specmicp_common/eigen/vector_checker.hpp"

#include <catch.hpp>

using namespace specmicp;
using namespace specmicp::vector_checker;

TEST_CASE("Vector checker")
{
    Vector x(10);
    x << 0, 1, 2, 3, 4, 5, 6, 7, 8, 9;

    Vector null_vector(10);
    null_vector.setZero();

    Vector notfinite(3);
    notfinite << 0, 1, - INFINITY;

    SECTION("all") {
        CHECK((all(x) >= 0.0));

        CHECK_FALSE((all(x) >= 5.0));
    }
    SECTION("any") {
        CHECK((any(x) >= 0.0));

        CHECK((any(x) >= 5.0));
    }
    SECTION("is_finite") {
        CHECK((is_finite(x)));
        CHECK_FALSE((is_finite(notfinite)));
    }
    SECTION("is_bounded") {
        CHECK_FALSE((is_bounded(notfinite, 0.0, 1.0)));
        CHECK((is_bounded(x, 0.0, 10.0)));
        CHECK((is_bounded(x, 0.0, 10.0) && (any(x) > 0.0)));
        CHECK((is_lower_bounded(x, 0.0)));
        CHECK((is_upper_bounded(x, 10.0)));
        CHECK_FALSE((is_lower_bounded(x, 5.0)));
        CHECK_FALSE((is_upper_bounded(x, 5.0)));
    }

    SECTION("And") {
        CHECK((is_finite(x) && all(x) >= 0.0));
        CHECK(((all(x) >= 0.0) && (any(x) != 0.0)));
        CHECK(((all(x) >= 0.0) && (all(x) < 10.0)));
        CHECK_FALSE(((all(x) >= 1.0) && (any(x) != 0.0)));
        CHECK_FALSE(((any(x) != 0.0) && (all(x) >= 1.0)));
        CHECK_FALSE(((all(null_vector) >= 0.0) && (any(null_vector) != 0.0)));
    }

    SECTION("Or") {
        CHECK(((all(x) >= 10.0) || (all(x) <= 10.0)));
        CHECK(((all(x) <= 10.0) || (all(x) >= 10.0)));
    }

    SECTION("long combination") {
        CHECK(((all(x) >= 10.0) || ((all(x) >= 0.0) && (all(x) < 10.0))));
        CHECK(((all(x) >= 10.0) || (all(x) >= 0.0) && (all(x) < 10.0)));
        CHECK((all(x) >= 10.0 || all(x) >= 0.0 && all(x) < 10.0));
        CHECK((all(x) >= 10.0 || (all(x) >= 0.0 && all(x) < 10.0)));
    }

}
