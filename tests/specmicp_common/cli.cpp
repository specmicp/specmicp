#include "catch.hpp"

#include "specmicp_common/cli/parser.hpp"

using namespace specmicp;
using namespace cli;


namespace specmicp {
namespace cli {
    using option_value_t = std::pair<std::string, std::string>;
    extern option_value_t split_long_option(const std::string& opts);
}
}

TEST_CASE("options parsing", "[options],[cli]") {
    SECTION("split_long_options") {
        option_value_t pair;
        pair = split_long_option("--giraffe");

        CHECK(pair.first == "giraffe");
        CHECK(pair.second == "");

        pair = split_long_option("--rhino=ceros");
        CHECK(pair.first == "rhino");
        CHECK(pair.second == "ceros");

        pair = split_long_option("--hippopotamus=");
        CHECK(pair.first == "hippopotamus");
        CHECK(pair.second == "");
    }

    SECTION("Add options") {
        CommandLineParser parser;

        parser.add_option('d', "database",
                          ValueType::string,
                          "Path to the database");

        parser.add_option('r', "restart", false,
                          "Restart the problem"
                          );

        std::vector<std::string> args = {"--database", "/plop/plop", };

        auto retcode = parser.parse(args);
        REQUIRE(retcode == 0);

        CHECK(parser.get_option<ValueType::boolean>("restart") == false);
        CHECK(parser.get_option<ValueType::string>("database") ==
              "/plop/plop");
    }

    SECTION("Add options 2") {
        CommandLineParser parser;

        parser.add_option('d', "database",
                          ValueType::string,
                          "Path to the database");

        parser.add_option('r', "restart", false,
                          "Restart the problem"
                          );

        parser.add_option('a', "abort", false,
                          "Abort the computation if true"
                          );



        std::vector<std::string> args = {"-r", "-d", "plip", "-a"};
        auto retcode = parser.parse(args);
        REQUIRE(retcode == 0);
        CHECK(parser.get_option<ValueType::boolean>("restart") == true);
        CHECK(parser.get_option<ValueType::string>("database") ==
              "plip");
        CHECK(parser.get_option<ValueType::boolean>("abort") == true);

    }

    SECTION("Add options + pos") {
        CommandLineParser parser;

        parser.add_option('r', "restart", false,
                          "Restart the problem"
                          );

        parser.add_option('a', "abort", false,
                          "Abort the computation if true"
                          );

        parser.add_pos_argument("filename",
                                ValueType::string,
                                "path to the name"
                                );

        std::vector<std::string> args = {"-ar", "ploup"};
        auto retcode = parser.parse(args);
        REQUIRE(retcode == 0);
        CHECK(parser.get_option<ValueType::boolean>("restart") == true);
        CHECK(parser.get_option<ValueType::boolean>("abort") == true);
        CHECK(parser.get_pos_argument<ValueType::string>("filename") == "ploup");
    }


    SECTION("Add options - errors") {
        CommandLineParser parser;

        parser.add_option('d', "database",
                          ValueType::string,
                          "Path to the database");

        parser.add_pos_argument("filename",
                                ValueType::string,
                                "path to the name"
                                );

        parser.add_option('r', "restart", false,
                          "Restart the problem"
                          );

        std::vector<std::string> args = {"-a"};
        REQUIRE_THROWS_AS(parser.parse(args), std::runtime_error);

        args = {"-ra", "ploup"};
        REQUIRE_THROWS_AS(parser.parse(args), std::runtime_error);

        args = {"-r", "ploup", "plip"};
        REQUIRE_THROWS_AS(parser.parse(args), std::runtime_error);

    }

    SECTION("Help message") {

        CommandLineParser parser;

        parser.add_option('d', "database",
                          ValueType::string,
                          "Path to the database");

        parser.add_pos_argument("filename",
                                ValueType::string,
                                "path to the name"
                                );

        parser.add_option('r', "restart", false,
                          "Restart the problem"
                          );

        parser.register_program_name("test");
        parser.set_help_message(
                    "This is the help message for this help program.");
        std::vector<std::string> args = {"-h"};
        parser.parse(args);
    }

}
