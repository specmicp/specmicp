#include <catch.hpp>

#include "specmicp_common/io/yaml.hpp"
#include "specmicp_common/io/safe_config.hpp"

#include <iostream>

using namespace specmicp;
using namespace specmicp::io;

const char* yaml_input =
R"(
---
section1:
    {key1: value1, key2: 2, key3: true}

section2:
    key4: value4
)";

TEST_CASE("YAML RO", "[yaml],[io]") {
    SECTION("Parsing") {
        auto file = RORichYAMLNode::parse_yaml_string(yaml_input);

        CHECK(file.check("section1"));
        CHECK(file.check("section2"));
        CHECK(not file.check("section4"));

        file.check_mandatory("section1");
        RORichYAMLNode section1 = file["section1"];
        CHECK_THROWS(file["section4"]);

        const YAML::Node& node = section1.get_raw_node();
        CHECK(node.IsSequence() == false);
        CHECK(node.IsMap() == true);
        std::string test = section1.get_mandatory<std::string>("key1");
        CHECK(test == "value1");
        CHECK(section1.get_mandatory<int>("key2") == 2);
        CHECK(section1.get_mandatory<bool>("key3") == true);
        CHECK(section1.get_optional<std::string>("keyopt", "opt") == "opt");

        RORichYAMLNode section2 = file["section2"];
        CHECK(section2.get_optional<std::string>("key4", "foo") == "value4");
    }
}

const char* test_config =
R"(
---
vars:
    int:
        a: 1
        b: 2
        c: 4
    float:
        x: 1.0
        y: 2.5

section1:
    key1: attr1
    key2: 3*a+b
    key3: 3.0/b+y
    key4: [1, 2, 3, 4]
    key5: [1, 2.0, 3, 2+y, 5.4]
)";

TEST_CASE("SafeConfig", "[yaml],[io],[config]")
{
    SECTION("Init")
    {
        std::cout << "Next warning about unread key 'Section 1' is OK : " << std::endl;
        REQUIRE_NOTHROW(YAMLConfigFile::load_from_string(test_config, "test string"));
    }

    SECTION("SafeConfig")
    {
        auto fh = YAMLConfigFile::load_from_string(test_config, "test string");
        fh.has_section("section1");

        YAMLConfigHandle sec1 = fh.get_section("section1");
        CHECK(sec1.get_required_attribute<std::string>("key1") == "attr1");
        CHECK(sec1.get_required_attribute<scalar_t>("key3") == 4.0);
        CHECK(sec1.get_optional_attribute<index_t>("key2", 0) == 5);
        CHECK((sec1.list_to_vector<index_t>("key4") == std::vector<index_t>({1, 2, 3, 4})) );
        CHECK((sec1.list_to_vector<scalar_t>("key5") == std::vector<scalar_t>({1, 2.0, 3, 4.5, 5.4})) );
    }
}
