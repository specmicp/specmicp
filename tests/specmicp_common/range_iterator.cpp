#include "catch.hpp"

#include "specmicp_common/range_iterator.hpp"

TEST_CASE("RangeIterator", "[iterator],[types]") {
    SECTION("Simple test") {
        int current = 0;
        auto range = specmicp::RangeIterator<int>(0, 6);
        for (auto it=range.begin(); it!=range.end(); ++it) {
            CHECK(*it == current);
            ++current;
        }
        CHECK(current == 6);

        current = 0;
        for (auto val: specmicp::RangeIterator<int>(0, 6)) {
            CHECK(val == current);
            ++current;
        }
        CHECK(current == 6);
    }
}
