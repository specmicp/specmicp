/*-------------------------------------------------------

 - Module : tests/micpsolver
 - File : test_micpsolver.hpp
 - Author : Fabien Georget

Copyright (c) 2014, Fabien Georget <fabieng@princeton.edu>, Princeton University
All rights reserved.

Redistribution and use in source and binary forms, with or without
modification, are permitted provided that the following conditions are met:
    * Redistributions of source code must retain the above copyright
      notice, this list of conditions and the following disclaimer.
    * Redistributions in binary form must reproduce the above copyright
      notice, this list of conditions and the following disclaimer in the
      documentation and/or other materials provided with the distribution.
    * Neither the name of the Princeton University nor the
      names of its contributors may be used to endorse or promote products
      derived from this software without specific prior written permission.

THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

---------------------------------------------------------*/

#include "catch.hpp"
#include "specmicp_common/micpsolver/micpsolver.hpp"
#include "specmicp_common/micpsolver/micpprog.hpp"
#include "specmicp_common/log.hpp"


#include "specmicp_common/micpsolver/boxviprog.hpp"


using namespace specmicp;
using namespace specmicp::micpsolver;


#include "specmicp_common/micpsolver/ncp_function.hpp"

using specmicp::micpsolver::fisher_burmeister;
using specmicp::micpsolver::penalized_fisher_burmeister;

TEST_CASE("NCP functions") {
    SECTION("fisher Burmeister") {
        CHECK(fisher_burmeister(0.0, 0.0) == 0.0);
        CHECK(fisher_burmeister(0.0, 1.0) == 0.0);
        CHECK(fisher_burmeister(0.0, 3.4) == 0.0);
        CHECK(std::abs(fisher_burmeister(1.0, 1.0)-std::sqrt(2)+2) == Approx(0.0));
        CHECK(std::abs(fisher_burmeister(2.0, 1.0)-std::sqrt(5)+3) == Approx(0.0));
    }

    SECTION("Penalized Fischer Burmeister") {
        CHECK(penalized_fisher_burmeister(0.0, 0.0, 0.5) == 0.0);
        CHECK(penalized_fisher_burmeister(0.0, 1.0, 0.3) == 0.0);
        CHECK(penalized_fisher_burmeister(0.0, 3.4, 0.5) == 0.0);
        CHECK(penalized_fisher_burmeister(8.4, 0.0, 0.2) == 0.0);
        CHECK(std::abs(fisher_burmeister(1.0, 1.0)-penalized_fisher_burmeister(1.0, 1.0, 1.0)) == Approx(0.0));
        CHECK(std::abs(penalized_fisher_burmeister(2.0, 1.0, 0.5)-0.5*(std::sqrt(5)-3)+0.5*2.0) == Approx(0.0));
    }
}

class TestLinearProgram: public MiCPProg<TestLinearProgram>
{
public:
    //! Return the number of variables
    int total_variables() {return 3;}
    int nb_free_variables() {return 2;}
    int nb_complementarity_variables() {return 1;}

    //! Return the residual (R(X))
    void get_residuals(const Vector& x,
                       Vector& residual) {
        assert(residual.rows() == 3);
        residual << 1 + x(0) + x(2), 1 + 1*x(1) + x(2), -x(0) - x(1) +x(2);
    }

    //! Return the jacobian (J(x))
    void get_jacobian(const Vector& x,
                      Matrix& jacobian)
    {
        assert(jacobian.cols() == 3);
        assert(jacobian.rows() == 3);
        jacobian << 1, 0, 1, 0, 1, 1, -1, -1, 1;
    }
};

class TestNonLinearProgram: public MiCPProg<TestNonLinearProgram>
{
public:
    //! Return the number of variables
    int total_variables() {return 3;}
    int nb_free_variables() {return 2;}
    int nb_complementarity_variables() {return 1;}

    //! Return the residual (R(X))
    void get_residuals(const Vector& x,
                       Vector& residual) {
        assert(residual.rows() == 3);
        residual << -1 + x(0)*x(0) + x(2), 1 + 1*x(1) + x(2)*x(2), -x(0) - x(1) +x(2);
    }

    //! Return the jacobian (J(x))
    void get_jacobian(const Vector& x,
                      Matrix& jacobian)
    {
        assert(jacobian.cols() == 3);
        assert(jacobian.rows() == 3);
        jacobian << 2*x(0), 0, 1, 0, 1, 2*x(2), -1, -1, 1;
    }
};

/*
 * S. P. Dirkse and M. C. Ferris.
 *  MCPLIB: a collection of nonlinear mixed complementarity problems.
 * Optimization Methods and Software, 5(4):319-345, 1995.
 *
 */
class TestKojimaProgram: public MiCPProg<TestKojimaProgram>
{
public:
    //! Return the number of variables
    int total_variables() {return 4;}
    int nb_free_variables() {return 0;}
    int nb_complementarity_variables() {return 4;}

    //! Return the residual (R(X))
    void get_residuals(const Vector& x,
                       Vector& residual) {
        assert(residual.rows() == 4);
        residual << 3*x(0)*x(0)+2*x(0)*x(1)+2*x(1)*x(1)+x(2)+3*x(3)-6,
                2*x(0)*x(0)+x(1)*x(1)+x(0)+10*x(2) +2*x(3)-2,
                3*x(0)+x(0)*x(1)+2*x(1)*x(1)+2*x(2)+9*x(3)-9,
                x(0)*x(0)+3*x(1)*x(1)+2*x(2)+3*x(3)-3
                ;
    }

    //! Return the jacobian (J(x))
    void get_jacobian(Vector& x,
                      Matrix& jacobian)
    {
        assert(jacobian.cols() == 4);
        assert(jacobian.rows() == 4);
        const int neq = total_variables();
        Vector res(total_variables());
        Vector perturbed_res(total_variables());

        get_residuals(x, res);
        for (int j=0; j<neq; ++j)
        {
            double h = 1e-8*std::abs(x(j));
            //h = std::copysign(h, x(j));
            if (h==0) h = 1e-8;
            double tmp = x(j);
            x(j) += h;
            h = x(j) - tmp;
            get_residuals(x, perturbed_res);
            for (int i=0; i<neq; ++i)
            {
                jacobian(i, j) = (perturbed_res(i) - res(i))/h;
            }
            x(j) = tmp;
        }
        return;
    }
};


TEST_CASE("MiCPSolver")
{
    SECTION("linear program 0")
    {
        std::shared_ptr<TestLinearProgram> ptrprog  = std::make_shared<TestLinearProgram>();
        MiCPSolver<TestLinearProgram> solver(ptrprog);

        Eigen::VectorXd x(3);
        x << -1.5, -2 , 0;
        MiCPSolverReturnCode ret = solver.solve(x);
        CHECK(ret == MiCPSolverReturnCode::ResidualMinimized);
        CHECK(std::abs(x(0)+1) == Approx(0.0));
        CHECK(std::abs(x(1)+1) == Approx(0.0));
        CHECK(std::abs(x(2)) == Approx(0.0));
    }

    SECTION("linear program 10")
    {
        std::shared_ptr<TestLinearProgram> ptrprog  = std::make_shared<TestLinearProgram>();
        MiCPSolver<TestLinearProgram> solver(ptrprog);

        Eigen::VectorXd x(3);
        x << -1.5, -2 , 10;
        MiCPSolverReturnCode ret = solver.solve(x);
        CHECK(ret == MiCPSolverReturnCode::ResidualMinimized);
        CHECK(std::abs(x(0)+1) == Approx(0.0).margin(1e-8));
        CHECK(std::abs(x(1)+1) == Approx(0.0).margin(1e-8));
        CHECK(std::abs(x(2)) == Approx(0.0).margin(1e-8));
    }

    SECTION("non linear program 0")
    {
        std::shared_ptr<TestNonLinearProgram> ptrprog  = std::make_shared<TestNonLinearProgram>();
        MiCPSolver<TestNonLinearProgram> solver(ptrprog);

        Eigen::VectorXd x(3);
        x << -1.5, -2 , 0;
        MiCPSolverReturnCode ret = solver.solve(x);
        CHECK(ret == MiCPSolverReturnCode::ResidualMinimized);
        CHECK(std::abs(x(0)+1) == Approx(0.0).margin(1e-8));
        CHECK(std::abs(x(1)+1) == Approx(0.0).margin(1e-8));
        CHECK(std::abs(x(2)) == Approx(0.0).margin(1e-8));

    }

    SECTION("non linear program 5")
    {
        std::shared_ptr<TestNonLinearProgram> ptrprog  = std::make_shared<TestNonLinearProgram>();
        MiCPSolver<TestNonLinearProgram> solver(ptrprog);

        Eigen::VectorXd x(3);
        x << -1.5, -2 , 5;
        MiCPSolverReturnCode ret = solver.solve(x);
        CHECK(ret == MiCPSolverReturnCode::ResidualMinimized);
        CHECK(std::abs(x(0)+1) == Approx(0.0).margin(1e-8));
        CHECK(std::abs(x(1)+1) == Approx(0.0).margin(1e-8));
        CHECK(std::abs(x(2)) == Approx(0.0).margin(1e-8));
    }

    SECTION("non linear program : Kojima")
    {
        std::shared_ptr<TestKojimaProgram> ptrprog  = std::make_shared<TestKojimaProgram>();
        MiCPSolver<TestKojimaProgram> solver(ptrprog);

        Eigen::VectorXd x(4);
        x << 0.9, 0.1 , 2.9, 0.1;
        MiCPSolverReturnCode ret = solver.solve(x);
        CHECK(ret == MiCPSolverReturnCode::ResidualMinimized);
        CHECK(std::abs(x(0)-1) == Approx(0.0).margin(1e-8));
        CHECK(std::abs(x(1)) == Approx(0.0).margin(1e-8));
        CHECK(std::abs(x(2)-3) == Approx(0.0).margin(1e-8));
        CHECK(std::abs(x(3)) == Approx(0.0).margin(1e-8));


    }

//    void test_kojima_program_min()
//    {
//        std::shared_ptr<TestKojimaProgram> ptrprog  = std::make_shared<TestKojimaProgram>();
//        MiCPSolver<TestKojimaProgram, NCPfunction::min> solver(ptrprog);

//        Eigen::VectorXd x(4);
//        x << 0.9, 0.1 , 2.9, 0.1;
//        MiCPSolverReturnCode ret = solver.solve(x);
//        std::cout << x << std::endl;
//        TS_ASSERT_EQUALS(ret, MiCPSolverReturnCode::ResidualMinimized);
//        //TS_ASSERT_LESS_THAN_EQUALS(std::abs(x(0)+1), 1e-8);
//        //TS_ASSERT_LESS_THAN_EQUALS(std::abs(x(1)+1), 1e-8);
//        //TS_ASSERT_LESS_THAN_EQUALS(std::abs(x(2)), 1e-8);

//    }

}

// Box constrained VI problem

class Test1LinearVI: public BoxVIProg<Test1LinearVI>
{
public:
    //! Return the number of variables
    int total_variables() {return 3;}
    int nb_free_variables() {return 1;}
    int nb_complementarity_variables() {return 2;}

    //! Return the residual (R(X))
    void get_residuals(const Vector& x,
                       Vector& residual) {
        assert(residual.rows() == 3);
        residual <<
            x(0) - 3.0*x(1) - 2.0*x(2) - 2.0,
            3.0*x(0) - 3.0*x(1) - x(2) + 2.0,
            -x(0) + 2.0*x(1) + 3.0*x(2) + 1.0
        ;
    }

    //! Return the jacobian (J(x))
    void get_jacobian(const Vector& x,
                      Matrix& jacobian)
    {
        assert(jacobian.cols() == 3);
        assert(jacobian.rows() == 3);
        jacobian << 1.0, -3.0, -2.0,
                    3.0, -3.0, -1.0,
                    -1.0, +2.0, +3.0;
    }

    bool is_box_vi(index_t ideq, scalar_t& upper_bound) {
        if (ideq == 2) {
            upper_bound = 10.0;
            return true;
        }
        else {
            return false;
        }
    }


};

class Test2LinearVI: public BoxVIProg<Test2LinearVI>
{
public:
    //! Return the number of variables
    int total_variables() {return 3;}
    int nb_free_variables() {return 1;}
    int nb_complementarity_variables() {return 2;}

    //! Return the residual (R(X))
    void get_residuals(const Vector& x,
                       Vector& residual) {
        assert(residual.rows() == 3);
        residual <<
            x(0) + 2.0*x(1) + x(2) - 5.0,
            3.0*x(0) + x(1) - 3.0*x(2) - 3.0,
            x(0) + 2.0*x(1) + x(2) + 5.0;
        ;
    }

    //! Return the jacobian (J(x))
    void get_jacobian(const Vector& x,
                      Matrix& jacobian)
    {
        assert(jacobian.cols() == 3);
        assert(jacobian.rows() == 3);
        jacobian << 1.0, +2.0,  1.0,
                    3.0, +1.0, -3.0,
                    1.0, +2.0, +1.0;
    }

    bool is_box_vi(index_t ideq, scalar_t& upper_bound) {
        if (ideq == 2) {
            upper_bound = 10.0;
            return true;
        }
        else {
            return false;
        }
    }
};

class Test3LinearVI: public BoxVIProg<Test3LinearVI>
{
public:
    //! Return the number of variables
    int total_variables() {return 3;}
    int nb_free_variables() {return 1;}
    int nb_complementarity_variables() {return 2;}

    //! Return the residual (R(X))
    void get_residuals(const Vector& x,
                       Vector& residual) {
        assert(residual.rows() == 3);
        residual <<
            3.0*x(0) + 2.0*x(1) + x(2) - 5.0,
            x(0) + 3.0*x(1) - 2.0*x(2) + 6.0,
            2.0*x(0) - 3.0*x(1) + x(2) - 5.0;
        ;
    }

    //! Return the jacobian (J(x))
    void get_jacobian(const Vector& x,
                      Matrix& jacobian)
    {
        assert(jacobian.cols() == 3);
        assert(jacobian.rows() == 3);
        jacobian << 3.0, +2.0,  1.0,
                    1.0, +3.0, -2.0,
                    2.0, -3.0, +1.0;
    }

    bool is_box_vi(index_t ideq, scalar_t& upper_bound) {
        if (ideq == 2) {
            upper_bound = 4.0;
            return true;
        }
        else {
            return false;
        }
    }
};

class Test4NLVI: public BoxVIProg<Test4NLVI>
{
public:
    //! Return the number of variables
    int total_variables() {return 3;}
    int nb_free_variables() {return 1;}
    int nb_complementarity_variables() {return 2;}

    //! Return the residual (R(X))
    void get_residuals(const Vector& x,
                       Vector& residual) {
        assert(residual.rows() == 3);
        residual <<
            -x(0)*x(2)+3*x(1)*x(1)+x(2)+2,
            2*x(0)*x(0)+3*x(0)*x(1)*x(1)+2*x(0)*x(2),
            -4+x(0)*x(0)+x(1)*x(1)-3*x(2)*x(2)+3*x(0)*x(2);
    }

    //! Return the jacobian (J(x))
    void get_jacobian(const Vector& x,
                      Matrix& jacobian)
    {
        assert(jacobian.cols() == 3);
        assert(jacobian.rows() == 3);
        jacobian << -x(2), +6.0*x(1),  1.0-x(0),
                    4*x(0)+3*x(1)*x(1)+2*x(2), 6*x(0)*x(1), 2.0*x(0),
                    2*x(0)+3*x(2), 2*x(1), -6.0*x(2)+3*x(2);
    }

    bool is_box_vi(index_t ideq, scalar_t& upper_bound) {
        if (ideq == 2) {
            upper_bound = 10.0;
            return true;
        }
        else {
            return false;
        }
    }
};


TEST_CASE("BOX VI solver")
{
    SECTION("test 1 VI")
    {
        std::shared_ptr<Test1LinearVI> ptrprog  = std::make_shared<Test1LinearVI>();
        MiCPSolver<Test1LinearVI, ReformulationF::BoxVI> solver(ptrprog);

        Eigen::VectorXd x(3);
        x << 3, 2, 0;
        MiCPSolverReturnCode ret = solver.solve(x);
        CHECK(ret == MiCPSolverReturnCode::ResidualMinimized);
        CHECK(x(0) == Approx(4.0).epsilon(1e-8));
        CHECK(x(1) == Approx(0.0).epsilon(1e-8));
        CHECK(x(2) == Approx(1.0).epsilon(1e-8));
    }

    SECTION("test 2 VI")
    {
        std::shared_ptr<Test2LinearVI> ptrprog  = std::make_shared<Test2LinearVI>();
        MiCPSolver<Test2LinearVI, ReformulationF::BoxVI> solver(ptrprog);

        Eigen::VectorXd x(3);
        x << 3, 2, 2;
        MiCPSolverReturnCode ret = solver.solve(x);
        CHECK(ret == MiCPSolverReturnCode::ResidualMinimized);
        CHECK(x(0) == Approx(1.0/5.0).epsilon(1e-8));
        CHECK(x(1) == Approx(12.0/5.0).epsilon(1e-8));
        CHECK(x(2) == Approx(0.0).epsilon(1e-8));
    }

    SECTION("test 3 VI")
    {
        std::shared_ptr<Test3LinearVI> ptrprog  = std::make_shared<Test3LinearVI>();
        MiCPSolver<Test3LinearVI, ReformulationF::BoxVI> solver(ptrprog);

        Eigen::VectorXd x(3);
        x << 0.0, 0.0, 0.0;
        MiCPSolverReturnCode ret = solver.solve(x);
        CHECK(ret == MiCPSolverReturnCode::ResidualMinimized);
        CHECK(x(0) == Approx(-1.0/7.0).epsilon(1e-8));
        CHECK(x(1) == Approx(5.0/7.0).epsilon(1e-8));
        CHECK(x(2) == Approx(4.0).epsilon(1e-8));
    }


    SECTION("test 4 NL VI")
    {
        std::shared_ptr<Test4NLVI> ptrprog  = std::make_shared<Test4NLVI>();
        MiCPSolver<Test4NLVI, ReformulationF::BoxVI> solver(ptrprog);

        Eigen::VectorXd x(3);
        x << 1.0, 0.0, 2.0;
        MiCPSolverReturnCode ret = solver.solve(x);
        CHECK(ret == MiCPSolverReturnCode::ResidualMinimized);
        CHECK(x(0) == Approx(2.0).epsilon(1e-8));
        CHECK(x(1) == Approx(0.0).epsilon(1e-8));
        CHECK(x(2) == Approx(2.0).epsilon(1e-8));
    }
}
