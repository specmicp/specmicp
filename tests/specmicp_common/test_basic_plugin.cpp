#include "specmicp_common/plugins/plugin_interface.h"
#include "specmicp_common/plugins/plugin_base.hpp"
#include "specmicp_common/plugins/module_base.hpp"

#include "specmicp_common/compat.hpp"

#include "test_basic_plugin_mockobject.hpp"

#include <iostream>

class TestObject: public TestBaseObject
{
public:
    TestObject():
        TestBaseObject() {}

    double add(double a, double b) override {
        return a + b;
    }
};


class TestObject2: public TestBaseObject
{
public:
    TestObject2():
        TestBaseObject() {}

    double add(double a, double b) override {
        return a - b;
    }
};


class BasicPlugin: public specmicp::plugins::PluginBase
{
public:
    BasicPlugin():
        PluginBase()
    {
        set_api_version(0, 1, 0);
    }

    bool initialize(const specmicp::plugins::PluginManagerServices& services) override
    {
        auto test_module = specmicp::plugins::ModuleBase::create_module();
        auto retcode = services.register_module("test_module", std::move(test_module));
        if (not retcode) {
            return false;
        }

        auto factory = []() {return new TestObject();
            };
        retcode = services.register_object("test_module", "test_add", factory);
        if (not retcode) {
            return false;
        }

        auto factory2 = []() {return new TestObject2();
            };
        retcode = services.register_object("test_module", "test_substract", factory2);
        if (not retcode) {
            return false;
        }

        return true;
    }

};

SPECMICP_PLUGIN(BasicPlugin);


