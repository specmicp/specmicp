#include "catch.hpp"

#include "specmicp/adimensional_kinetics/kinetic_variables.hpp"

using namespace specmicp;
using namespace specmicp::kinetics;

TEST_CASE("kinetics variables", "[Specmicp,adim,kinetics,variables]")
{
    SECTION("Init")
    {
        Vector total_concentrations(4);
        total_concentrations << 1.0, 2.0, 3.0, 4.0;

        Vector mineral_concentrations(2);
        mineral_concentrations << 5, 6;

        AdimKineticVariables variables(total_concentrations, mineral_concentrations);

        CHECK(variables.total_concentration(0) == 1.0);
        CHECK(variables.total_concentration(1) == 2.0);
        CHECK(variables.total_concentration(2) == 3.0);
        CHECK(variables.total_concentration(3) == 4.0);

        CHECK(variables.concentration_mineral(0) == 5.0);
        CHECK(variables.concentration_mineral(1) == 6.0);

        CHECK(variables.rate_component(0) == 0.0);
    }
}
