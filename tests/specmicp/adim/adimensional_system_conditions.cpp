#include "catch.hpp"

#include "specmicp_common/log.hpp"
#include "specmicp/adimensional/adimensional_system_solver.hpp"
#include "specmicp/adimensional/adimensional_system_solution.hpp"
#include "specmicp/problem_solver/formulation.hpp"
#include "specmicp/problem_solver/dissolver.hpp"

#include "specmicp/adimensional/adimensional_system_solver.hpp"
#include "specmicp/adimensional/adimensional_system_solution_extractor.hpp"

#include "specmicp_database/database.hpp"

#include <iostream>


TEST_CASE("Fixed saturation", "[specmicp, conditions, water]") {

    specmicp::init_logger(&std::cerr, specmicp::logger::Error);

    SECTION("Fixed saturation")
    {
        specmicp::database::Database thedatabase(TEST_CEMDATA_PATH);
        std::map<std::string, std::string> swapping ({
                                                  {"H[+]","HO[-]"},
                                                  {"Si(OH)4", "SiO(OH)3[-]"}
                                                    });
        thedatabase.swap_components(swapping);
        thedatabase.remove_gas_phases();
        specmicp::RawDatabasePtr raw_data = thedatabase.get_database();
        thedatabase.remove_half_cell_reactions({"H2O", "HO[-]", "HCO3[-]"});

        specmicp::Formulation formulation;
        specmicp::scalar_t mult = 1.0;
        formulation.mass_solution = mult*0.156;
        formulation.amount_minerals = {{"C3S", mult*0.7}, {"C2S", mult*0.3}};
        formulation.add_aqueous_species("CO2", 2.0);

        specmicp::Vector total_concentrations = specmicp::Dissolver(raw_data).dissolve(formulation);
        specmicp::AdimensionalSystemConstraints constraints(total_concentrations);
        constraints.charge_keeper = 1;
        constraints.set_fixed_saturation(0.8);

        specmicp::AdimensionalSystemSolverOptions options;
        specmicp::AdimensionalSystemSolver solver(raw_data, constraints, options);

        specmicp::Vector x;
        solver.initialize_variables(x, 0.7, 2);

        auto perf = solver.solve(x, false);

        REQUIRE(perf.return_code >= specmicp::micpsolver::MiCPSolverReturnCode::Success);
        auto sol = solver.get_raw_solution(x);
        auto extr = specmicp::AdimensionalSystemSolutionExtractor(sol, raw_data, options.units_set);
        CHECK(extr.saturation_water() == Approx(0.8).epsilon(1e-6));

        constraints.set_fixed_saturation(0.7);
        solver = specmicp::AdimensionalSystemSolver(raw_data, constraints, sol, options);

        perf = solver.solve(x, false);

        REQUIRE(perf.return_code >= specmicp::micpsolver::MiCPSolverReturnCode::Success);
        auto sol2 = solver.get_raw_solution(x);
        auto extr2 = specmicp::AdimensionalSystemSolutionExtractor(sol2, raw_data, options.units_set);
        CHECK(extr2.saturation_water() == Approx(0.7).epsilon(1e-6));


    }

}


TEST_CASE("Fixed activity", "[specmicp, MiCP, program, adimensional, solver, conditions]") {

    specmicp::init_logger(&std::cerr, specmicp::logger::Error);

    SECTION("Thermocarbo - saturated ")
    {
        specmicp::database::Database thedatabase(TEST_CEMDATA_PATH);
        std::map<std::string, std::string> swapping ({
                                                  {"H[+]","HO[-]"},
                                                  {"Si(OH)4", "SiO(OH)3[-]"}
                                                    });
        thedatabase.swap_components(swapping);
        thedatabase.remove_gas_phases();
        specmicp::RawDatabasePtr raw_data = thedatabase.get_database();
        thedatabase.remove_half_cell_reactions({"H2O", "HO[-]", "HCO3[-]"});

        specmicp::Formulation formulation;
        specmicp::scalar_t mult = 1.0;
        formulation.mass_solution = mult*0.156;
        formulation.amount_minerals = {{"C3S", mult*0.7}, {"C2S", mult*0.3}};
        formulation.extra_components_to_keep = {"HCO3[-]", };

        specmicp::Vector total_concentrations = specmicp::Dissolver(raw_data).dissolve(formulation);
        specmicp::AdimensionalSystemConstraints constraints(total_concentrations);
        constraints.charge_keeper = 1;
        constraints.water_equation = specmicp::WaterEquationType::SaturatedSystem;

        specmicp::index_t id_h2o = raw_data->water_index();
        specmicp::index_t id_ho  = raw_data->get_id_component("HO[-]");
        specmicp::index_t id_co2 = raw_data->get_id_component("HCO3[-]");

        specmicp::AdimensionalSystemSolverOptions options;
        options.solver_options.maxstep = 20.0;
        options.solver_options.maxiter_maxstep = 100;
        options.solver_options.use_crashing = false;
        options.solver_options.use_scaling = true;
        options.solver_options.factor_descent_condition = -1;
        options.solver_options.factor_gradient_search_direction = 200;
        options.solver_options.condition_limit = -1;
        options.solver_options.set_tolerance(1e-10,1e-12);
        options.system_options.cutoff_total_concentration = 1e-16;

        specmicp::AdimensionalSystemSolver solver(raw_data, constraints, options);

        specmicp::Vector x;
        solver.initialize_variables(x, 0.8, 2);
        specmicp::scalar_t dh2co3 = 0.1;
        for (int k=0; k<40; ++k)
        {
            specmicp::AdimensionalSystemSolver solver(raw_data, constraints, options);

            specmicp::micpsolver::MiCPPerformance perf =  solver.solve(x);

            REQUIRE(perf.return_code >= specmicp::micpsolver::MiCPSolverReturnCode::NotConvergedYet);

            specmicp::AdimensionalSystemSolution solution = solver.get_raw_solution(x);
            specmicp::AdimensionalSystemSolutionExtractor sol(solution, raw_data, options.units_set);
            //std::cout << solution.main_variables(0) << std::endl;
            std::cout << sol.pH() << std::endl;

            constraints.total_concentrations(id_h2o) += mult*dh2co3;
            constraints.total_concentrations(id_ho)  -= mult*dh2co3;
            constraints.total_concentrations(id_co2) += mult*dh2co3;
        }
    }


    SECTION("Carbonate speciation - fixed activity") {
        specmicp::database::Database dbhandler(TEST_CEMDATA_PATH);

        std::vector<std::string> to_keep = {"H2O", "H[+]", "HCO3[-]", "Na[+]"};
        dbhandler.remove_gas_phases();
        std::string co2_gas = R"plop(
        [
        {
            "label": "CO2(g)",
            "composition": "CO2",
            "log_k": -1.468
        }
        ]
        )plop";
        dbhandler.remove_half_cell_reactions({"H2O", "H[+]", "HCO3[-]"});
        dbhandler.add_gas_phases(co2_gas);
        dbhandler.keep_only_components(to_keep);

        specmicp::RawDatabasePtr thedatabase = dbhandler.get_database();

        specmicp::index_t id_h = dbhandler.component_label_to_id("H[+]");
        specmicp::index_t id_na = dbhandler.component_label_to_id("Na[+]");
        specmicp::index_t id_hco3 = dbhandler.component_label_to_id("HCO3[-]");

        specmicp::index_t id_co2 = dbhandler.aqueous_label_to_id("CO2");
        specmicp::index_t id_co3 = dbhandler.aqueous_label_to_id("CO3[2-]");
        specmicp::index_t id_naco3 = dbhandler.aqueous_label_to_id("NaCO3[-]");
        specmicp::index_t id_nahco3 = dbhandler.aqueous_label_to_id("NaHCO3");
        specmicp::index_t id_co2g = dbhandler.gas_label_to_id("CO2(g)");

        specmicp::Vector total_concentrations(thedatabase->nb_component());
        total_concentrations(0) = 55;
        total_concentrations(id_hco3) = 0.1;

        specmicp::AdimensionalSystemConstraints constraints(total_concentrations);
        constraints.charge_keeper = id_na;
        constraints.enable_conservation_water();


        specmicp::AdimensionalSystemSolverOptions options;
        options.system_options.non_ideality = true;
        options.units_set.length = specmicp::units::LengthUnit::decimeter;
        options.solver_options.max_iter = 50;
        options.solver_options.maxstep = 10;
        options.solver_options.maxiter_maxstep = 100;
        options.solver_options.use_crashing = false;
        options.solver_options.use_scaling = false;
        options.solver_options.non_monotone_linesearch = false;
        options.solver_options.penalization_factor = 0.8;
        options.solver_options.factor_descent_condition = -1;
        options.solver_options.factor_gradient_search_direction = 10;
        options.solver_options.fvectol = 1e-6;

        specmicp::Vector x;

        constraints.add_fixed_activity_component(id_h, -4.0);
        specmicp::AdimensionalSystemSolver solver(thedatabase, constraints, options);
        solver.initialize_variables(x, 0.8, -2);

        specmicp::micpsolver::MiCPPerformance perf = solver.solve(x);
        REQUIRE((int) perf.return_code > 0 );
        specmicp::AdimensionalSystemSolution solution = solver.get_raw_solution(x);
        specmicp::AdimensionalSystemSolutionExtractor sol(solution, thedatabase, options.units_set);

        std::cout << 4.0 << " \t" << sol.pH() << " \t"
                  << sol.molality_component(id_hco3) << " \t"
                  << sol.molality_aqueous(id_co2) << " \t"
                  << sol.molality_aqueous(id_co3) << " \t"
                  << sol.molality_aqueous(id_naco3) << " \t"
                  << sol.molality_aqueous(id_nahco3) << "\t"
                  << sol.fugacity_gas(id_co2g)
                  << std::endl;
        for (double ph=4.5; ph<13; ph+=0.5)
        {
            constraints.fixed_activity_cs[0].log_value = -ph;
            solver = specmicp::AdimensionalSystemSolver(
                        thedatabase, constraints, options);

            perf =  solver.solve(x);
            REQUIRE((int) perf.return_code > 0 );
            solution = solver.get_raw_solution(x);

            specmicp::AdimensionalSystemSolutionExtractor sol(solution, thedatabase, options.units_set);

            std::cout << ph << "\t" << sol.pH() << " \t"
                      << sol.molality_component(id_hco3) << " \t"
                      << sol.molality_aqueous(id_co2) << " \t"
                      << sol.molality_aqueous(id_co3) << "\t"
                      << sol.molality_aqueous(id_naco3) << "\t"
                      << sol.molality_aqueous(id_nahco3) << "\t"
                      << sol.fugacity_gas(id_co2g)
                      << std::endl;
        }
        //std::cout << x << std::endl;
    }

    SECTION("Carbonate speciation - fixed molality") {
        specmicp::database::Database dbhandler(TEST_CEMDATA_PATH);

        std::vector<std::string> to_keep = {"H2O", "H[+]", "HCO3[-]", "Na[+]", "Cl[-]"};
        dbhandler.keep_only_components(to_keep);
        dbhandler.remove_gas_phases();
        dbhandler.remove_half_cell_reactions({"H2O", "H[+]", "HCO3[-]"});
        std::string co2_gas = R"plop(
        [
        {
            "label": "CO2(g)",
            "composition": "CO2",
            "log_k": -1.468
        }
        ]
        )plop";
        dbhandler.add_gas_phases(co2_gas);

        specmicp::RawDatabasePtr thedatabase = dbhandler.get_database();

        specmicp::index_t id_h = dbhandler.component_label_to_id("H[+]");
        specmicp::index_t id_na = dbhandler.component_label_to_id("Na[+]");
        specmicp::index_t id_cl = dbhandler.component_label_to_id("Cl[-]");
        specmicp::index_t id_hco3 = dbhandler.component_label_to_id("HCO3[-]");

        specmicp::index_t id_co2 = dbhandler.aqueous_label_to_id("CO2");
        specmicp::index_t id_co3 = dbhandler.aqueous_label_to_id("CO3[2-]");
        specmicp::index_t id_naco3 = dbhandler.aqueous_label_to_id("NaCO3[-]");
        specmicp::index_t id_nahco3 = dbhandler.aqueous_label_to_id("NaHCO3");
        specmicp::index_t id_co2g = dbhandler.gas_label_to_id("CO2(g)");

        specmicp::Vector total_concentrations(thedatabase->nb_component());
        total_concentrations(0) = 55;
        total_concentrations(id_hco3) = 0.1;

        specmicp::AdimensionalSystemConstraints constraints(total_concentrations);
        constraints.enable_conservation_water();


        specmicp::AdimensionalSystemSolverOptions options;
        options.system_options.non_ideality = true;
        options.units_set.length = specmicp::units::LengthUnit::decimeter;
        options.solver_options.max_iter = 50;
        options.solver_options.maxstep = 10;
        options.solver_options.maxiter_maxstep = 100;
        options.solver_options.use_crashing = false;
        options.solver_options.use_scaling = true;
        options.solver_options.non_monotone_linesearch = true;
        options.solver_options.penalization_factor = 0.8;
        options.solver_options.factor_descent_condition = -1;
        options.solver_options.factor_gradient_search_direction = 10;
        options.solver_options.fvectol = 1e-6;

        specmicp::Vector x;

        constraints.add_fixed_activity_component(id_h, -4.0);
        constraints.add_fixed_molality_component(id_na, -0.301);
        constraints.add_fixed_molality_component(id_cl, -0.301);
        //constraints.set_charge_keeper(id_h);
        specmicp::AdimensionalSystemSolver solver(thedatabase, constraints, options);
        solver.initialize_variables(x, 0.8, -2);

        specmicp::micpsolver::MiCPPerformance perf = solver.solve(x);
        REQUIRE((int) perf.return_code > 0 );
        specmicp::AdimensionalSystemSolution solution = solver.get_raw_solution(x);
        specmicp::AdimensionalSystemSolutionExtractor sol(solution, thedatabase, options.units_set);

        REQUIRE(sol.log_molality_component(id_na) == Approx(-0.301));
        REQUIRE(sol.log_molality_component(id_cl) == Approx(-0.301));

        std::cout << 4.0 << " \t" << sol.pH() << " \t"
                  << sol.molality_component(id_hco3) << " \t"
                  << sol.molality_aqueous(id_co2) << " \t"
                  << sol.molality_aqueous(id_co3) << " \t"
                  << sol.molality_aqueous(id_naco3) << " \t"
                  << sol.molality_aqueous(id_nahco3) << "\t"
                  << sol.fugacity_gas(id_co2g)
                  << std::endl;
        for (double ph=4.5; ph<13; ph+=0.5)
        {
            constraints.fixed_activity_cs[0].log_value = -ph;
            solver = specmicp::AdimensionalSystemSolver(
                        thedatabase, constraints, options);

            perf =  solver.solve(x);
            REQUIRE((int) perf.return_code > 0 );
            solution = solver.get_raw_solution(x);

            specmicp::AdimensionalSystemSolutionExtractor sol(solution, thedatabase, options.units_set);

            std::cout << ph << "\t" << sol.pH() << " \t"
                      << sol.molality_component(id_hco3) << " \t"
                      << sol.molality_aqueous(id_co2) << " \t"
                      << sol.molality_aqueous(id_co3) << "\t"
                      << sol.molality_aqueous(id_naco3) << "\t"
                      << sol.molality_aqueous(id_nahco3) << "\t"
                      << sol.fugacity_gas(id_co2g)
                      << std::endl;
        }
        //std::cout << x << std::endl;
    }



    SECTION("Carbonate speciation - fixed fugacity") {

        specmicp::database::Database dbhandler(TEST_CEMDATA_PATH);

        std::vector<std::string> to_keep = {"H2O", "H[+]", "HCO3[-]"};
        dbhandler.keep_only_components(to_keep);
        dbhandler.remove_gas_phases();
        dbhandler.remove_half_cell_reactions({"H2O", "H[+]", "HCO3[-]"});
        std::string co2_gas = R"plop(
        [
        {
            "label": "CO2(g)",
            "composition": "CO2",
            "log_k": -1.468
        }
        ]
        )plop";
        dbhandler.add_gas_phases(co2_gas);
        specmicp::RawDatabasePtr thedatabase = dbhandler.get_database();

        specmicp::index_t id_h = thedatabase->get_id_component("H[+]");
        specmicp::index_t id_hco3 = thedatabase->get_id_component("HCO3[-]");

        specmicp::index_t id_co2 = thedatabase->get_id_aqueous("CO2");
        specmicp::index_t id_co3 = thedatabase->get_id_aqueous("CO3[2-]");;
        specmicp::index_t id_co2g = thedatabase->get_id_gas("CO2(g)");
        CHECK(id_co2g == 0);

        specmicp::Vector total_concentrations(thedatabase->nb_component());
        total_concentrations(0) = 55;
        total_concentrations(id_hco3) = 0.1;

        specmicp::AdimensionalSystemConstraints constraints(total_concentrations);
        constraints.charge_keeper = id_h;
        constraints.enable_conservation_water();

        specmicp::AdimensionalSystemSolverOptions options;
        options.system_options.non_ideality = true;
        options.units_set.length = specmicp::units::LengthUnit::decimeter;
        options.solver_options.max_iter = 50;
        options.solver_options.maxstep = 20;
        options.solver_options.maxiter_maxstep = 10;
        options.solver_options.use_crashing = false;
        options.solver_options.use_scaling = true;
        options.solver_options.enable_non_monotone_linesearch();
        options.solver_options.penalization_factor = 0.8;
        options.solver_options.factor_descent_condition = -1;
        options.solver_options.fvectol = 1e-8;
        options.solver_options.condition_limit = -1;

        specmicp::Vector x;
        constraints.add_fixed_fugacity_gas(id_co2g, id_hco3, -5);
        specmicp::AdimensionalSystemSolver solver(thedatabase, constraints, options);
        solver.initialize_variables(x, 0.8, -2.0);

        specmicp::micpsolver::MiCPPerformance perf = solver.solve(x);
        REQUIRE((int) perf.return_code > 0 );
        specmicp::AdimensionalSystemSolution solution = solver.get_raw_solution(x);
        specmicp::AdimensionalSystemSolutionExtractor sol(solution, thedatabase, options.units_set);

        std::cout << -5 << " \t" << sol.pH() << " \t"
                  << sol.molality_component(id_hco3) << " \t"
                  << sol.molality_aqueous(id_co2) << " \t"
                  << sol.molality_aqueous(id_co3) << " \t"
                  << sol.fugacity_gas(id_co2g)
                  << std::endl;

        for (double lfug=-4.5; lfug<=-0.5; lfug+=0.25)
        {
            constraints.fixed_fugacity_cs[0].log_value = lfug;
            solver = specmicp::AdimensionalSystemSolver(
                        thedatabase, constraints, options);

            perf =  solver.solve(x);
            REQUIRE((int) perf.return_code > 0 );
            solution = solver.get_raw_solution(x);

            specmicp::AdimensionalSystemSolutionExtractor sol(solution, thedatabase, options.units_set);

            std::cout << lfug << "\t" << sol.pH() << " \t"
                      << sol.molality_component(id_hco3) << " \t"
                      << sol.molality_aqueous(id_co2) << " \t"
                      << sol.molality_aqueous(id_co3) << "\t"
                      << sol.fugacity_gas(id_co2g)
                      << std::endl;

        }


        //std::cout << x << std::endl;
    }
}
