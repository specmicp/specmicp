#include "catch.hpp"

#include "specmicp/adimensional/adimensional_system.hpp"
#include "specmicp/adimensional/adimensional_system_pcfm.hpp"
#include "specmicp/adimensional/adimensional_system_solver.hpp"

#include "specmicp_database/database.hpp"

specmicp::RawDatabasePtr get_pcfm_test_database()
{
    specmicp::database::Database thedatabase(TEST_CEMDATA_PATH);
    std::map<std::string, std::string> swapping ({
                                              {"H[+]","HO[-]"},
                                                });
    thedatabase.swap_components(swapping);

    std::vector<std::string> to_keep = {"H2O", "HO[-]", "Ca[2+]"};
    thedatabase.keep_only_components(to_keep);
    thedatabase.remove_solid_phases();

    return thedatabase.get_database();

}

// Test the Positive continuous fraction method

TEST_CASE("Positive continuous fraction method", "[specmicp, MiCP, program, adimensional, PCFM]") {

    specmicp::RawDatabasePtr the_database = get_pcfm_test_database();
    auto id_h2o = the_database->water_index();
    auto id_oh = the_database->get_id_component("HO[-]");
    auto id_ca = the_database->get_id_component("Ca[2+]");

    SECTION("PCFM") {

        specmicp::Vector total_concentration = specmicp::Vector::Zero(the_database->nb_component());
        total_concentration(id_h2o) = 55.5;
        total_concentration(id_oh) = 2e-3;
        total_concentration(id_ca) = 1e-3;
        specmicp::AdimensionalSystemConstraints constraints(total_concentration);

        constraints.disable_conservation_water();
        constraints.enable_surface_model(1.23456);
        //constraints.total_concentrations(2) = 0;

        std::shared_ptr<specmicp::AdimensionalSystem> ptrsystem =
                std::make_shared<specmicp::AdimensionalSystem>(the_database, constraints);

        specmicp::AdimensionalSystemSolver solver(the_database, constraints);

        specmicp::AdimensionalSystemPCFM pcfm_solver(ptrsystem);

        specmicp::Vector x;
        solver.initialize_variables(x, 1.0, -3.0, 0.0);

        specmicp::PCFMReturnCode retcode = pcfm_solver.solve(x);
        REQUIRE(retcode == specmicp::PCFMReturnCode::Success);


    }
}
