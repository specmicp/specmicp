#include "catch.hpp"

#include "specmicp_common/log.hpp"
#include "specmicp/adimensional/adimensional_system_solver.hpp"
#include "specmicp/adimensional/adimensional_system_solution.hpp"
#include "specmicp/adimensional/adimensional_system_solution_extractor.hpp"
#include "specmicp/problem_solver/formulation.hpp"
#include "specmicp/problem_solver/dissolver.hpp"

#include "specmicp_common/timer.hpp"

#include "specmicp_database/database.hpp"

#include <iostream>


TEST_CASE("BoxVI", "[Adimensional,BoxVI]")
{
    std::cerr.flush();
    specmicp::logger::ErrFile::stream() = &std::cerr;
    specmicp::stdlog::ReportLevel() = specmicp::logger::Warning;

    SECTION("Carboalu")
    {
        std::cout << "---------------------\n BOXVI\n ---------------------" << std::endl;



        specmicp::database::Database thedatabase(TEST_CEMDATA_PATH);
        std::map<std::string, std::string> swapping ({
                                                  {"H[+]","HO[-]"},
                                                  {"Si(OH)4", "SiO(OH)3[-]"},
                                                  {"Al[3+]","Al(OH)4[-]"},
                                                    });
        thedatabase.swap_components(swapping);
        //thedatabase.remove_half_cell_reactions();
        specmicp::RawDatabasePtr raw_data = thedatabase.get_database();
        thedatabase.remove_half_cell_reactions({"H2O", "HO[-]", "HCO3[-]"});

        specmicp::Formulation formulation;
        specmicp::scalar_t mult = 5e3;

        specmicp::scalar_t m_c3s = mult*0.6;
        specmicp::scalar_t m_c2s = mult*0.2;
        specmicp::scalar_t m_c3a = mult*0.10;
        specmicp::scalar_t m_gypsum = mult*0.10;
        specmicp::scalar_t wc = 0.8;
        specmicp::scalar_t m_water = wc*1e-3*(
                      m_c3s*(3*56.08+60.08)
                    + m_c2s*(2*56.06+60.08)
                    + m_c3a*(3*56.08+101.96)
                    + m_gypsum*(56.08+80.06+2*18.02)
                    );

        formulation.mass_solution = m_water;
        formulation.amount_minerals = {
            {"C3S", m_c3s},
            {"C2S", m_c2s},
            {"C3A", m_c3a},
            {"Gypsum", m_gypsum}
        };
        formulation.extra_components_to_keep = {"HCO3[-]", };
        formulation.minerals_to_keep = {
            "Portlandite",
            "CSH,jennite",
            "CSH,tobermorite",
            "SiO2(am)",
            "Calcite",
            "Al(OH)3(mic)",
            "Monosulfoaluminate",
            "Tricarboaluminate",
            "Monocarboaluminate",
            "Hemicarboaluminate",
            //"Straetlingite",
            "Gypsum",
            "Ettringite",
            //"Thaumasite"
        };
        thedatabase.remove_gas_phases();
        std::string co2_gas = R"plop(
        [
        {
            "label": "CO2(g)",
            "composition": "CO2",
            "log_k": -1.468
        }
        ]
        )plop";
        thedatabase.add_gas_phases(co2_gas);

        specmicp::Vector total_concentrations = specmicp::Dissolver(raw_data).dissolve(formulation);
        specmicp::index_t id_h2o = raw_data->get_id_component("H2O");
        specmicp::index_t id_ho  = raw_data->get_id_component("HO[-]");
        specmicp::index_t id_co2 = raw_data->get_id_component("HCO3[-]");

        specmicp::AdimensionalSystemConstraints constraints(total_concentrations);
        constraints.charge_keeper = 1;

        specmicp::AdimensionalSystemSolverOptions options;

        specmicp::Vector x;

        specmicp::scalar_t dh2co3 = mult*0.01;

        specmicp::uindex_t tot_iter = 0;

        specmicp::Timer timer_carboalu;
        timer_carboalu.start();

        specmicp::AdimensionalSystemSolver solver(raw_data, constraints, options);
        solver.initialize_variables(x, 0.5, -3.0);
        specmicp::micpsolver::MiCPPerformance perf =  solver.solve(x, false);

        REQUIRE((int) perf.return_code > (int) specmicp::micpsolver::MiCPSolverReturnCode::NotConvergedYet);



        specmicp::AdimensionalSystemSolution solution = solver.get_raw_solution(x);

        specmicp::index_t id_vi = raw_data->get_id_mineral("Monosulfoaluminate");
        auto extr = specmicp::AdimensionalSystemSolutionExtractor(solution, raw_data, options.units_set);
        std::cout << extr.volume_fraction_mineral(id_vi) << std::endl;

        constraints.set_mineral_upper_bound(id_vi, 0.9 * extr.volume_fraction_mineral(id_vi));


        solver = specmicp::AdimensionalSystemSolver(raw_data, constraints, solution, options);
        perf =  solver.solve(x, false);
        REQUIRE((int) perf.return_code > (int) specmicp::micpsolver::MiCPSolverReturnCode::NotConvergedYet);

        auto sol2 = solver.get_raw_solution(x);
        auto extr2 = specmicp::AdimensionalSystemSolutionExtractor(sol2, raw_data, options.units_set);
        std::cout << extr2.volume_fraction_mineral(id_vi) << std::endl;

        CHECK(extr2.volume_fraction_mineral(id_vi) <= constraints.mineral_constraints[0].param);
    }
}
