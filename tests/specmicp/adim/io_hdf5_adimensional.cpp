#include "catch.hpp"

#include <iostream>
#include "specmicp_common/log.hpp"
#include "specmicp/adimensional/adimensional_system.hpp"
#include "specmicp_common/micpsolver/micpsolver.hpp"

#include "specmicp/adimensional/adimensional_system_solver.hpp"

#include "specmicp/problem_solver/formulation.hpp"
#include "specmicp/problem_solver/dissolver.hpp"
#include "specmicp/adimensional/adimensional_system_solution_extractor.hpp"

#include "specmicp_database/database.hpp"

#include "specmicp/io/hdf5_adimensional.hpp"
#include "specmicp_common/io/hdf5/file.hpp"
#include "specmicp_common/io/hdf5/path.hpp"


using namespace specmicp;
using namespace specmicp::io;

static specmicp::RawDatabasePtr get_test_simple_database()
{
    specmicp::database::Database thedatabase(TEST_CEMDATA_PATH);
    std::map<std::string, std::string> swapping ({
                                              {"H[+]","HO[-]"},
                                                });
    thedatabase.swap_components(swapping);

    std::vector<std::string> to_keep = {"HO[-]", "Ca[2+]"};
    thedatabase.keep_only_components(to_keep);
    thedatabase.remove_half_cell_reactions(std::vector<std::string>({"H2O", "HO[-]",})) ;
    return thedatabase.get_database();

}

TEST_CASE("Adimensional_hdf5_saver", "[Adimensional],[io],[hdf5]") {

    specmicp::logger::ErrFile::stream() = &std::cerr;
    specmicp::stdlog::ReportLevel() = specmicp::logger::Error;

    specmicp::RawDatabasePtr thedatabase = get_test_simple_database();

    auto id_h2o = database::DataContainer::water_index();
    auto id_oh = thedatabase->get_id_component("HO[-]");
    auto id_ca = thedatabase->get_id_component("Ca[2+]");
    auto id_ch = thedatabase->get_id_mineral("Portlandite");


    Vector total_concentration = Vector::Zero(thedatabase->nb_component());
    total_concentration(id_h2o) = 0.03;
    total_concentration(id_oh) = 0.02;
    total_concentration(id_ca) = 0.01;
    specmicp::Vector x;
    specmicp::AdimensionalSystemConstraints constraints(total_concentration);

    specmicp::AdimensionalSystemSolver solver(thedatabase, constraints);
    solver.initialize_variables(x, 0.8, -2.0);

    solver.get_options().units_set.length = specmicp::units::LengthUnit::centimeter;
    solver.get_options().solver_options.maxstep = 10.0;
    solver.get_options().solver_options.maxiter_maxstep = 100;
    solver.get_options().solver_options.use_crashing = false;
    solver.get_options().solver_options.use_scaling = true;
    solver.get_options().solver_options.disable_descent_direction();
    solver.get_options().solver_options.factor_gradient_search_direction = 100;

    solver.solve(x);

SECTION("Save and read solution") {

    AdimensionalSystemSolution solution = solver.get_raw_solution(x);

    auto h5file = io::hdf5::File::open("test_specmicp_adim_solution.hdf5",
                         io::hdf5::OpenMode::CreateTruncate);

    save_adimensional_system_solution(h5file, "test_solution", solution);

    CHECK(h5file.has_link("test_solution"));
    AdimensionalSystemSolution readto = read_adimensional_system_solution(
                h5file,
                "test_solution"
                );

    CHECK(readto.is_valid);
    CHECK(readto.main_variables.rows() == solution.main_variables.rows());
    CHECK(readto.main_variables(0) == Approx(solution.main_variables(0)));
    CHECK(readto.main_variables(3) == Approx(solution.main_variables(3)));

    CHECK(readto.log_gamma.rows() == solution.log_gamma.rows());
    CHECK(readto.log_gamma(2) == Approx(solution.log_gamma(2)));
    CHECK(readto.secondary_molalities.rows() == solution.secondary_molalities.rows());
    CHECK(readto.secondary_molalities(1) == Approx(solution.secondary_molalities(1)));

    CHECK(readto.ionic_strength == Approx(solution.ionic_strength));
    CHECK(readto.inert_volume_fraction == Approx(solution.inert_volume_fraction));

}

}
