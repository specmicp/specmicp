#include "catch.hpp"

#include "specmicp/problem_solver/reactant_box.hpp"
#include "specmicp_common/physics/units.hpp"

#include "specmicp_database/database.hpp"

#include <map>
#include <iostream>

static specmicp::RawDatabasePtr get_test_database()
{
    specmicp::database::Database thedatabase(TEST_CEMDATA_PATH);
    std::map<std::string, std::string> swapping ({
                                              {"H[+]","HO[-]"},
                                                });
    thedatabase.swap_components(swapping);

    thedatabase.remove_half_cell_reactions(std::vector<std::string>({"H2O", "HO[-]",})) ;

    return thedatabase.get_database();

}

TEST_CASE("Reactant box", "[formulation]")
{
    specmicp::RawDatabasePtr raw_data = get_test_database();

    SECTION("test 1")
    {

        // mol/m^3
        specmicp::ReactantBox formulation(raw_data, specmicp::units::SI_units);

        formulation.set_solution(100, "kg/m^3");
        formulation.add_aqueous_species("NaCl", 0.5, "mol/kg");
        formulation.add_solid_phase("Portlandite", 200, "kg/m^3");
        formulation.add_component(raw_data->get_label_component_from_element("K"));

        specmicp::Vector total_concentration = formulation.get_total_concentration(true);

        CHECK(raw_data->get_id_component_from_element("C")   == -1);
        CHECK(raw_data->get_id_component_from_element("Cl")  != -1);
        CHECK(raw_data->get_id_component_from_element("K")   != -1);

        CHECK(total_concentration[0]
                == Approx(100/raw_data->molar_mass_basis(0, specmicp::units::SI_units)));
        CHECK(total_concentration[raw_data->get_id_component_from_element("Na")]
                == Approx(0.5*100));
        CHECK(total_concentration[raw_data->get_id_component_from_element("Cl")]
                == Approx(0.5*100));

        CHECK(total_concentration[raw_data->get_id_component_from_element("Ca")]
                == Approx(200/raw_data->molar_mass_mineral(
                              raw_data->get_id_mineral("Portlandite"),
                              specmicp::units::SI_units)
                          )
                );

        // mmol/dm^3
        specmicp::units::UnitsSet dmmol; // 1 mmol/dm^3 = 1 mol/m^3
        dmmol.length   = specmicp::units::LengthUnit::decimeter;
        dmmol.quantity = specmicp::units::QuantityUnit::millimoles;

        specmicp::ReactantBox formulation2(raw_data, dmmol);

        formulation2.set_solution(100, "kg/m^3");
        formulation2.add_aqueous_species("NaCl", 0.5, "mol/kg");
        formulation2.add_solid_phase("Portlandite", 200, "kg/m^3");
        formulation2.add_component(raw_data->get_label_component_from_element("K"));

        specmicp::Vector total_concentration2 = formulation2.get_total_concentration();

        for (specmicp::index_t id: raw_data->range_component()) {
            CHECK(total_concentration(id) == Approx(total_concentration2(id)).epsilon(1e-8));
        }


        // mol/dm^3
        specmicp::units::UnitsSet dmol;
        dmol.length   = specmicp::units::LengthUnit::decimeter;
        dmol.quantity = specmicp::units::QuantityUnit::moles;

        specmicp::ReactantBox formulation3(raw_data, dmol);

        formulation3.set_solution(100, "kg/m^3");
        formulation3.add_aqueous_species("NaCl", 0.5, "mol/kg");
        formulation3.add_solid_phase("Portlandite", 200, "kg/m^3");
        formulation3.add_component(raw_data->get_label_component_from_element("K"));

        specmicp::Vector total_concentration3 = formulation3.get_total_concentration();

        for (specmicp::index_t id: raw_data->range_component()) {
            CHECK(1e-3*total_concentration(id) == Approx(total_concentration3(id)).epsilon(1e-8));
        }

        // mol/cm^3
        specmicp::units::UnitsSet cmol;
        cmol.length   = specmicp::units::LengthUnit::centimeter;
        cmol.quantity = specmicp::units::QuantityUnit::moles;

        specmicp::ReactantBox formulation4(raw_data, cmol);

        formulation4.set_solution(100, "kg/m^3");
        formulation4.add_aqueous_species("NaCl", 0.5, "mol/kg");
        formulation4.add_solid_phase("Portlandite", 200, "kg/m^3");
        formulation4.add_component(raw_data->get_label_component_from_element("K"));

        specmicp::Vector total_concentration4= formulation4.get_total_concentration();

        for (specmicp::index_t id: raw_data->range_component()) {
            CHECK(1e-6*total_concentration(id) == Approx(total_concentration4(id)).epsilon(1e-8));
        }

    }
}
