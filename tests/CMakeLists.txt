################## Tests ########################################


add_library(Catch::catch2 INTERFACE IMPORTED)
set_property(TARGET Catch::catch2 PROPERTY
    INTERFACE_INCLUDE_DIRECTORIES ${CMAKE_CURRENT_SOURCE_DIR}/catch/)

set(PROJECT_TEST_DIR ${CMAKE_CURRENT_SOURCE_DIR})
include_directories(${PROJECT_TEST_DIR})

add_custom_target(check COMMAND ${CMAKE_CTEST_COMMAND}) # make check is also valid

function(add_catch_test)
    set(options "")
    set(oneValueArgs NAME)
    set(multiValueArgs SOURCES LINK_LIBRARIES DEPENDS)
    cmake_parse_arguments(ARGS "${options}" "${oneValueArgs}" "${multiValueArgs}" ${ARGN} )
    set(TARGETNAME test_${ARGS_NAME})
    add_executable(${TARGETNAME} EXCLUDE_FROM_ALL ${ARGS_SOURCES} )
    target_link_libraries(${TARGETNAME} Catch::catch2)
    if(ARGS_DEPENDS)
        add_dependencies(${TARGETNAME} ${ARGS_DEPENDS})
    endif()
    if(ARGS_LINK_LIBRARIES)
        target_link_libraries(${TARGETNAME} ${ARGS_LINK_LIBRARIES})
    endif()
    add_test(NAME ${ARGS_NAME}_build COMMAND "${CMAKE_COMMAND}" --build "${PROJECT_BINARY_DIR}" --target ${TARGETNAME})
    add_test(NAME ${ARGS_NAME} COMMAND ${TARGETNAME})
    set_tests_properties(${ARGS_NAME} PROPERTIES DEPENDS ${ARGS_NAME}_build)
endfunction()




add_subdirectory( specmicp_common   )
add_subdirectory( specmicp_database )
add_subdirectory( specmicp          )
add_subdirectory( dfpm              )
add_subdirectory( reactmicp         )
