#include <catch.hpp>

#include "dfpm/meshes/mesh1d.hpp"
#include "dfpm/io/configuration.hpp"
#include "specmicp_common/io/safe_config.hpp"

TEST_CASE("Configuration", "[meshes],[io],[configuration]") {
    SECTION("Uniform mesh")
    {
        const char * conf_str =
        R"(
        mesh:
            type: uniform_mesh
            uniform_mesh:
                dx: 1.0
                section: 2.0
                nb_nodes: 50
        )";

        auto conf = specmicp::io::YAMLConfigFile::load_from_string(conf_str);
        auto the_mesh =  specmicp::io::configure_mesh(conf.get_section("mesh"));

        REQUIRE(the_mesh->nb_nodes()     == 50);
        CHECK(the_mesh->get_dx(0)        == 1.0);
        CHECK(the_mesh->get_position(0)  == 0.0);
        CHECK(the_mesh->get_position(1)  == 1.0);
        CHECK(the_mesh->get_position(49) == 49.0);
        CHECK(the_mesh->get_face_area(0) == 2.0);
    }

    SECTION("Ramp mesh") {

        const char * conf_str =
        R"(
        mesh:
            type: ramp_mesh
            ramp_mesh:
                min_dx: 1.0
                max_dx: 5.0
                section: 2.0
                length_ramp: 10
                length_plateau: 50

        )";

        auto conf = specmicp::io::YAMLConfigFile::load_from_string(conf_str);
        auto the_mesh = specmicp::io::configure_mesh(conf.get_section("mesh"));

        REQUIRE(the_mesh->get_dx(0) == 1.0);
        REQUIRE(the_mesh->get_dx(the_mesh->nb_nodes()-2) == 5.0);
    }

    SECTION("Uniform axisymmetric mesh") {
        const char * conf_str =
        R"(
        mesh:
            type: uniform_axisymmetric
            uniform_axisymmetric:
                radius: 100.0
                nb_nodes: 10
                height: 3.0

        )";

        auto conf = specmicp::io::YAMLConfigFile::load_from_string(conf_str);
        auto the_mesh = specmicp::io::configure_mesh(conf.get_section("mesh"));

        REQUIRE(the_mesh->nb_nodes() == 10);
        CHECK(the_mesh->get_dx(0) == Approx(100.0/9.0));
        CHECK(the_mesh->get_position(0) == 100.0);
        CHECK(the_mesh->get_position(9) == Approx(0.0));
        double sum = 0.0;
        for (auto e: the_mesh->range_elements())
        {
            sum += the_mesh->get_volume_element(e);
        }
        CHECK(sum == Approx(M_PI*std::pow(100.0, 2)*3.0));
    }


}
