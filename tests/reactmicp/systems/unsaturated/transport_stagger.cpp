#include "catch.hpp"

#include "reactmicp/systems/unsaturated/boundary_conditions.hpp"
#include "reactmicp/systems/unsaturated/variables.hpp"
#include "reactmicp/systems/unsaturated/variables_box.hpp"
#include "reactmicp/systems/unsaturated/transport_stagger.hpp"
#include "reactmicp/systems/unsaturated/variables_interface.hpp"

#include "specmicp_database/database.hpp"
#include "dfpm/meshes/mesh1d.hpp"

#include "dfpm/solver/parabolic_driver.hpp"

#include "reactmicp/io/reactive_transport.hpp"

#include "specmicp_common/log.hpp"
#include <iostream>

using namespace specmicp;
using namespace specmicp::mesh;
using namespace specmicp::reactmicp::systems::unsaturated;

static scalar_t identity(index_t _, scalar_t saturation) {
    return saturation;
}

static scalar_t one_minus(index_t _, scalar_t saturation) {
    return 1.0 - saturation;
}

static scalar_t decreasing_linear(index_t _, scalar_t saturation) {
    return 10*(1-saturation);
}


static specmicp::database::RawDatabasePtr get_database()
{
    static database::RawDatabasePtr raw_data {nullptr};
    if (raw_data == nullptr)
    {
        specmicp::database::Database thedatabase(TEST_CEMDATA_PATH);
        thedatabase.keep_only_components({"H2O", "H[+]", "Ca[2+]", "Si(OH)4", "HCO3[-]"});
        std::map<std::string, std::string> swap = {{"HCO3[-]", "CO2"},};
        thedatabase.swap_components(swap);
        raw_data = thedatabase.get_database();
        raw_data->freeze_db();
    }
    return raw_data;
}

TEST_CASE("Transport stagger", "[transport][stagger]") {
    init_logger(&std::cout, logger::Warning);

    SECTION("Initialization") {
        std::cout << "\n ------------- \n Transport stagger \n ------------- \n";

        index_t nb_nodes = 10;
        scalar_t dx = 0.1;
        scalar_t cross_section = 1.0;

        auto the_mesh = uniform_mesh1d({dx, nb_nodes, cross_section});
        auto raw_data = get_database();

        index_t id_co2 = raw_data->get_id_component_from_element("C");

        VariablesInterface vars_interface(
                    the_mesh,
                    raw_data,
                    {0, id_co2}
                    );

        Vector sat_init(nb_nodes);
        sat_init.setConstant(0.71);
        sat_init(0) = 0.0;
        sat_init(1) = 0.6;
        vars_interface.set_liquid_saturation(sat_init);

        Vector pres_0_init(nb_nodes);
        pres_0_init.setConstant(1.0);
        pres_0_init(0) = 0.5;
        vars_interface.set_partial_pressure(0, pres_0_init);

        Vector pres_5_init(nb_nodes);
        pres_5_init.setConstant(1.0);
        pres_5_init(0) = 0.5;
        vars_interface.set_partial_pressure(id_co2, pres_5_init);

        Vector aq_concentration(nb_nodes);
        aq_concentration.setConstant(1.0);
        aq_concentration(1) = 0.5;
        aq_concentration(0) = 0.0;

        for (index_t aqcomp: raw_data->range_aqueous_component())
        {
            vars_interface.set_aqueous_concentration(aqcomp, aq_concentration);
        }

        vars_interface.set_porosity(0.3);
        vars_interface.set_water_aqueous_concentration(1.0);
        vars_interface.set_liquid_permeability(1e-4);
        vars_interface.set_liquid_diffusivity(1.0);
        vars_interface.set_resistance_gas_diffusivity(1.0);
        vars_interface.set_binary_gas_diffusivity(0, 1.0);

        vars_interface.set_capillary_pressure_model(decreasing_linear);
        vars_interface.set_relative_liquid_diffusivity_model(identity);
        vars_interface.set_relative_liquid_permeability_model(identity);
        vars_interface.set_relative_gas_diffusivity_model(one_minus);

        auto bcs = BoundaryConditions::make(the_mesh->nb_nodes(), raw_data->nb_component());
        bcs->add_fixed_node(0);
        bcs->add_gas_node(0);

        UnsaturatedVariablesPtr vars = vars_interface.get_variables();
        UnsaturatedTransportStagger stagger(vars, bcs, UnsaturatedTransportStaggerOptions() );

        stagger.initialize_timestep(0.01, vars.get());
        scalar_t res_0 = stagger.get_residual_0(vars.get());

        std::cout << "res_0 : " << res_0 << std::endl;

        auto retcode = stagger.restart_timestep(vars.get());
        REQUIRE(static_cast<int>(retcode) > 0);
        std::cout << "Retcode : " << (int) retcode << std::endl;

        scalar_t res = stagger.get_residual(vars.get());
        std::cout << "res : " << res << std::endl;
        //REQUIRE(res < 1e-6);

        scalar_t sat1 = vars_interface.get_liquid_saturation().norm();
        scalar_t presw1 = vars_interface.get_partial_pressure(0).norm();
        scalar_t presc1 = vars_interface.get_partial_pressure(id_co2).norm();
        scalar_t aqc1 = vars_interface.get_aqueous_concentration(2).norm();


        retcode = stagger.restart_timestep(vars.get());
        std::cout << "Retcode : " << (int) retcode << std::endl;

        res = stagger.get_residual(vars.get());
        std::cout << "res : " << res << std::endl;

        scalar_t sat2 = vars_interface.get_liquid_saturation().norm();
        scalar_t presw2 = vars_interface.get_partial_pressure(0).norm();
        scalar_t presc2 = vars_interface.get_partial_pressure(id_co2).norm();
        scalar_t aqc2 = vars_interface.get_aqueous_concentration(2).norm();

        REQUIRE(sat1 == Approx(sat2));
        REQUIRE(presw1 == Approx(presw2));
        REQUIRE(presc1 == Approx(presc2));
        REQUIRE(aqc1 == Approx(aqc2));


        for (int i=0; i<100; ++i) {

            stagger.initialize_timestep(0.01, vars.get());
            scalar_t res_0 = stagger.get_residual_0(vars.get());
            retcode = stagger.restart_timestep(vars.get());
            REQUIRE(static_cast<int>(retcode) > 0);
        }

        std::cout << vars->get_liquid_saturation().variable << "\n - \n";
        std::cout << vars->get_aqueous_concentration(2).variable << "\n - \n";
        std::cout << vars->get_aqueous_concentration(3).variable << "\n - \n";
        std::cout << vars->get_pressure_main_variables(0).variable << "\n - \n";
        std::cout << vars->get_pressure_main_variables(id_co2).variable
                  << "\n -" << std::endl;

    }
}
