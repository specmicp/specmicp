#include "catch.hpp"

#include "specmicp_database/yaml_reader.hpp"
#include "specmicp_database/switch_basis.hpp"

#include "str_database.hpp"

#include "specmicp_common/io/hdf5/file.hpp"
#include "specmicp_database/io/hdf5_database.hpp"

#include <iostream>
#include <sstream>

using namespace specmicp;
using namespace specmicp::database;
using namespace specmicp::io;

TEST_CASE("HDF5 saver", "[hdf5],[io]")
{
    std::istringstream input(good_test_database);
    DataReaderYaml reader(input);

    auto data = reader.get_database();


    SECTION("labels output") {
        auto file = hdf5::File::open("test_database_labels.hf5", hdf5::OpenMode::CreateTruncate);

        save_database_labels(file, "database", data);
    }
}
