#include "catch.hpp"

#include "specmicp_database/species/species.hpp"
#include "specmicp_database/species/component_list.hpp"
#include "specmicp_database/species/aqueous_list.hpp"
#include "specmicp_database/species/mineral_list.hpp"

#include <iostream>

using namespace specmicp;
using namespace specmicp::database;

TEST_CASE("WrapperSpecies") {
    SECTION("VectorSpeciesWrapper") {
        VectorSpeciesWrapper wrap(3);
        wrap.set_value(0, 1);
        wrap.set_value(1, 2);
        wrap.set_value(2, 3);

        CHECK(wrap(0) == 1);
        CHECK(wrap.get_value(1) == 2);

        wrap.move_erase(2, 1);
        wrap.resize(2);

        CHECK(wrap(1) == 3);
        Vector other(2);
        other << 1, 3;

        CHECK(wrap.dot(other) == 10);

        Matrix transformation(2, 2);
        transformation << 1, 0, 0, 1;

        wrap.transform(transformation);
        CHECK(wrap(0) == 1);
        CHECK(wrap(1) == 3);

        transformation << 2, 1, 0, 3;
        wrap.transform(transformation);
        CHECK(wrap(0) == 5);
        CHECK(wrap(1) == 9);
    }

    SECTION("MatrixSpeciesWrapper") {
        MatrixSpeciesWrapper wrap(3, 2);
        CHECK(wrap.size() == 3);
        wrap.set_value(0, 0, 1);
        wrap.set_value(0, 1, 2);

        wrap.set_value(1, 0, 3);

        CHECK(wrap(1, 1) == 0);
        CHECK(wrap(0, 1) == 2);

        wrap.move_erase(1, 0);
        wrap.resize(1);
        CHECK(wrap.size() == 1);
        CHECK(wrap(0, 0) == 3);
    }
}

TEST_CASE("DatabaseSpecies", "[Species]")
{
    SECTION("SpeciesList")
    {
        SpeciesList species_list(2);
        CHECK(species_list.is_valid() == false);
        REQUIRE(species_list.size() == 2);

        species_list.set_label(0, "HOP");
        species_list.set_label(1, "REHOP");
        species_list.set_valid();

        REQUIRE(species_list.size() == 2);
        CHECK(species_list.is_valid() == true);

        CHECK(species_list.get_label(0) == "HOP");
        CHECK(species_list.get_label(1) == "REHOP");

        CHECK(species_list.get_id("HOP") == 0);
        CHECK(species_list.get_id("REHOP") == 1);
        CHECK(species_list.get_id("OUHLALA") == no_species);

        species_list.move_erase(1, 0);
        CHECK(species_list.is_valid() == false);
        species_list.resize(1);
        species_list.set_valid();

        REQUIRE(species_list.size() == 1);


        CHECK(species_list.get_label(0) == "REHOP");
        CHECK(species_list.get_id("HOP") == no_species);
        CHECK(species_list.get_id("REHOP") == 0);
        CHECK(species_list.get_id("OUHLALA") == no_species);
        CHECK(species_list.is_valid() == true);

    }

    SECTION("ReactiveSpeciesList") {

        ReactiveSpeciesList slist(3, 3);

        slist.set_label(0, "S1");
        slist.set_nu_ji(0, 0, 1);
        slist.set_nu_ji(0, 1, 2);
        slist.set_nu_ji(0, 2, 3);
        slist.set_logk(0, -1);

        slist.set_label(1, "S2");
        slist.set_nu_ji(1, 0, 4);
        slist.set_nu_ji(1, 1, 5);
        slist.set_logk(1, -2);

        slist.set_label(2, "S3");
        slist.set_nu_ji(2, 0, 6);
        slist.set_nu_ji(2, 2, 7);
        slist.set_logk(2, -3);
        CHECK(slist.is_valid() == false);

        slist.set_valid();

        CHECK(slist.is_valid() == true);
        CHECK(slist.size() == 3);
        CHECK(slist.get_id("S3") == 2);
        CHECK(slist.get_label(1) == "S2");


        CHECK(slist.nu_ji(2, 0) == 6);
        CHECK(slist.nu_ji(1, 1) == 5);
        // check default value
        CHECK(slist.nu_ji(1, 2) == 0);
        CHECK(slist.nu_ji(0, 2) == 3);

        slist.move_erase(2, 1);
        slist.resize(2);
        CHECK(slist.is_valid() == false);

        slist.set_valid();

        CHECK(slist.is_valid() == true);
        CHECK(slist.size() == 2);

        CHECK(slist.get_label(0) == "S1");
        CHECK(slist.get_label(1) == "S3");

        CHECK(slist.get_id("S2") == no_species);
        CHECK(slist.nu_ji(1, 0) == 6);
        CHECK(slist.logk(1) == -3);
    }
}

TEST_CASE("ComponentList", "[database,species,component]") {

    ComponentList clist(3);
    clist.set_values(0, {"C1", 1.0, IonicModelValues{0, 1, 2}});
    clist.set_values(1, {"C2", 2.0, IonicModelValues{-1, 1, 2}});
    clist.set_values(2, {"C3", 3.0, IonicModelValues{2, 3, 4}});

    SECTION("Initialization") {

        CHECK(clist.get_label(1) == "C2");
        CHECK(clist.get_id("C3") == 2);
        CHECK(clist.get_id("NotHere") == -1);

        CHECK(clist.molar_mass(1) == 2.0);

        CHECK(clist.charge(0) == 0.0);
        CHECK(clist.charge(1) == -1.0);

        CHECK(clist.a_debye(2) == 3.0);
        CHECK(clist.b_debye(2) == 4.0);
    }

    SECTION("Move and erase") {
        clist.move_erase(2, 1);

        CHECK(clist.molar_mass(1) == 3.0);
        CHECK(clist.get_label(1) == "C3");
        CHECK(clist.get_id("C2") == no_species);
        CHECK(clist.charge(0) == 0.0);
        CHECK(clist.charge(1) == 2.0);
        CHECK(clist.a_debye(1) == 3.0);
        CHECK(clist.b_debye(1) == 4.0);
    }

    SECTION("Molar mass wrapper") {
        Vector stoech(3);
        stoech << 1.0, 2.0, 3.0;

        auto molmass = clist.molar_mass_compounds(stoech);
        CHECK(molmass == 14.0);
    }
}

TEST_CASE("AqueousList", "[database,species,aqueous]") {
    AqueousList alist(3,2);

    alist.set_values(0, {"A1", -1, IonicModelValues{1, 2, 3}});
    alist.set_values(1, {"A2", -2, IonicModelValues{4, 5, 6}});
    alist.set_values(2, {"A3", -3, IonicModelValues{7, 8, 9}});

    alist.set_nu_ji(0, 0, 1);
    alist.set_nu_ji(0, 1, 2);
    alist.set_nu_ji(1, 0, 3);
    alist.set_nu_ji(1, 1, 4);
    alist.set_nu_ji(2, 0, 5);

    SECTION("Initialisation") {
        REQUIRE(alist.size() == 3);
        CHECK(alist.get_label(1) == "A2");
        CHECK(alist.get_id("A3") == 2);
        CHECK(alist.get_id("NotHere") == -1);

        CHECK(alist.logk(0) == -1);
        CHECK(alist.logk(1) == -2);
        CHECK(alist.logk(2) == -3);

        CHECK(alist.nu_ji(0, 0) == 1);
        CHECK(alist.nu_ji(0, 1) == 2);
        CHECK(alist.nu_ji(1, 0) == 3);
        CHECK(alist.nu_ji(1, 1) == 4);
        CHECK(alist.nu_ji(2, 0) == 5);
        CHECK(alist.nu_ji(2, 1) == 0);

    }

    SECTION("Move erase") {
        alist.move_erase(2, 1);
        alist.resize(2);

        CHECK(alist.size() == 2);

        CHECK(alist.get_id("A3") == 1);
        CHECK(alist.get_id("A2") == -1);

        CHECK(alist.logk(1) == -3);
        CHECK(alist.nu_ji(1, 0) == 5);
        CHECK(alist.nu_ji(1, 1) == 0);
    }

    SECTION("Canonicalization") {
        alist.canonicalize(2, 1, 1.0);

        CHECK(alist.nu_ji(2, 0) == 8.0);
        CHECK(alist.nu_ji(2, 1) == 4.0);

        CHECK(alist.logk(2) == -5.0);
    }
}

TEST_CASE("MineralList", "[Minerals,species]") {

    MineralList mlist(3, 3);
    mlist.set_values(0, {"M1", -1.0, 10.0});
    mlist.set_values(1, {"M2", -2.0, 20.0});
    mlist.set_values(2, {"M3", -3.0, 30.0});

    Matrix stoech_coefficient(3, 3);
    stoech_coefficient <<
         1, 2, 3,
         4, 5, 6,
         7, 8, 9;

    mlist.set_nu_matrix(stoech_coefficient);

    mlist.set_valid();

    SECTION("Initialization") {
        CHECK(mlist.is_valid() == true);

        CHECK(mlist.get_label(2) == "M3");
        CHECK(mlist.get_id("M2") == 1);
        CHECK(mlist.get_id("NotHere") == no_species);

        CHECK(mlist.nu_ji(2, 2) == 9);
        CHECK(mlist.nu_ji(1, 2) == 6);

        CHECK(mlist.logk(2) == -3);
        CHECK(mlist.logk(1) == -2);

        CHECK(mlist.molar_volume(0) == 10.0);
        CHECK(mlist.molar_volume(1) == 20.0);
    }

    SECTION("Move Erase") {
        mlist.move_erase(2, 1);
        CHECK(mlist.is_valid() == false);

        mlist.resize(2);
        mlist.set_valid();

        CHECK(mlist.is_valid() == true);

        CHECK(mlist.nu_ji(1, 2) == 9);

        CHECK(mlist.logk(1) == -3);
        CHECK(mlist.logk(0) == -1);

        CHECK(mlist.molar_volume(0) == 10.0);
        CHECK(mlist.molar_volume(1) == 30.0);
    }

    SECTION("Move erase to") {
        MineralList mlist2(3, 3);
        mlist2.set_values(0, {"MK1", -4.0, 40.0});
        mlist2.set_values(1, {"MK2", -5.0, 50.0});
        mlist2.set_values(2, {"MK3", -6.0, 60.0});

        Matrix stoech_coefficient2(3, 3);
        stoech_coefficient2 <<
             -1, -2, -3,
             -4, -5, -6,
             -7, -8, -9;

        mlist2.set_nu_matrix(stoech_coefficient2);

        mlist2.set_valid();
        mlist2.resize(4);

        mlist.move_erase_to(2, mlist2, 3);
        mlist.resize(2);

        CHECK(mlist.is_valid() == false);
        CHECK(mlist2.is_valid() == false);

        mlist.set_valid();
        mlist2.set_valid();

        CHECK(mlist.is_valid() == true);
        CHECK(mlist2.is_valid() == true);

        CHECK(mlist.size() == 2);
        CHECK(mlist2.size() == 4);

        CHECK(mlist2.get_id("M3") == 3);
        CHECK(mlist.get_id("M3") == no_species);

        CHECK(mlist2.nu_ji(3, 2) == 9);
        CHECK(mlist2.molar_volume(3) == 30);
        CHECK(mlist2.logk(3) == -3);

    }

    SECTION("Canonicalization") {
        AqueousList alist(3,3);

        alist.set_values(0, {"A1", -1.0, IonicModelValues{1, 2, 3}});
        alist.set_values(1, {"A2", -2.0, IonicModelValues{4, 5, 6}});
        alist.set_values(2, {"A3", -3.0, IonicModelValues{7, 8, 9}});


        Matrix acoeff(3, 3);
        acoeff << 0, 1, 2,
                  3, 4, 5,
                  6, 7, 8;

        alist.set_nu_matrix(acoeff);

        mlist.canonicalize(0, alist, 0, 1.0);
        CHECK(mlist.nu_ji(0, 0) == 1.0);
        CHECK(mlist.nu_ji(0, 1) == 3.0);
        CHECK(mlist.nu_ji(0, 2) == 5.0);
        CHECK(mlist.logk(0) == -2.0);

        mlist.canonicalize(1, alist, 1, -2.0);
        CHECK(mlist.nu_ji(1, 0) == 4.0-2*3.0);
        CHECK(mlist.nu_ji(1, 1) == 5.0-2*4.0);
        CHECK(mlist.nu_ji(1, 2) == 6.0-2*5.0);
        CHECK(mlist.logk(1) == -2.0+2.0*2.0);

    }
}
