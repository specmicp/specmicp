#include <catch.hpp>

#include "specmicp_common/io/safe_config.hpp"
#include "specmicp_database/io/configuration.hpp"
#include "specmicp_database/database.hpp"

TEST_CASE("database configuration", "[database],[io],[configuration]")
{
    SECTION("Configuration")
    {
        auto conf = specmicp::io::YAMLConfigFile::load("db_test.yaml");

        specmicp::RawDatabasePtr db = specmicp::io::configure_database(
                    conf.get_section("database"));

        REQUIRE(db->get_id_component("H[+]") == specmicp::no_species);
        REQUIRE(db->get_id_component("HO[-]") != specmicp::no_species);

        REQUIRE(db->get_id_component("Al[3+]") == specmicp::no_species);

        REQUIRE(db->nb_gas() == 1);

        REQUIRE(db->get_label_gas(0) == "CO2(g)");

        REQUIRE(db->nb_mineral() == 7);
        REQUIRE(db->get_id_mineral("SiO2_mod") != specmicp::no_species);
    }
}
