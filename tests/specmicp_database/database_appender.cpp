#include "catch.hpp"

#include "specmicp_database/yaml_reader.hpp"
#include "specmicp_database/appender.hpp"
#include "str_database.hpp"
#include "specmicp_common/timer.hpp"

#include <iostream>
#include <sstream>

using namespace specmicp;
using namespace specmicp::database;

static std::string mineral_appendix_database = R"plop(
[
{
        "label": "MA1",
        "composition": "2 A2[+], A3[-], C1[-]",
        "log_k": -10,
        "molar_volume": 30.0
},
{
        "label": "MKA1",
        "composition": "4C3, 3A1",
        "log_k": -20,
        "molar_volume": 50.0,
        "flag_kinetic": 1
}
]
)plop";


static std::string gas_appendix_database = R"plop(
[
{
        "label": "GA1",
        "composition": "A1",
        "log_k": -6
}
]
)plop";


static std::string sorbed_appendix_database = R"plop(
[
{
        "label": "SA1",
        "composition": "A1",
        "log_k": -5,
        "nb_sites_occupied": 2
}
]
)plop";

static std::string compounds_appendix_database = R"plop(
[
{
        "label": "CompA1",
        "composition": "6 H2O, C3"
}
]
)plop";

TEST_CASE("DatabaseAppender", "[Database],[Appender],[Minerals]") {

    std::istringstream input(good_test_database);
    DataReaderYaml reader(input);

    auto data = reader.get_database();

    SECTION("Append minerals") {
        size_t orig_hash = data->get_hash();

        DataAppender appender(data);
        appender.add_minerals(mineral_appendix_database, false);

        CHECK(data->is_valid());
        CHECK(data->get_hash() != orig_hash);
        CHECK(data->nb_mineral() == 3);
        CHECK(data->nb_mineral_kinetic() == 2);

        CHECK(data->get_id_mineral("MA1") == 2);
        CHECK(data->get_id_mineral_kinetic("MKA1") == 1);

        CHECK(data->nu_mineral(2, 3) == 4.0);
        CHECK(data->molar_volume_mineral(2) == 30.0*1e-6);

        CHECK(data->molar_mass_mineral_kinetic(1, units::CMG_units) == 31.0);
    }

    SECTION("Append gas") {
        size_t orig_hash = data->get_hash();
        DataAppender appender(data);
        appender.add_gas(gas_appendix_database, false);

        CHECK(data->is_valid());
        CHECK(data->get_hash() != orig_hash);
        CHECK(data->nb_gas() == 2);
        CHECK(data->get_id_gas("GA1") == 1);
        CHECK(data->nu_gas(1, 2) == 1);
        CHECK(data->nu_gas(1, 3) == 1);
    }

    SECTION("Append sorbed") {
        size_t orig_hash = data->get_hash();
        DataAppender appender(data);
        appender.add_sorbed(sorbed_appendix_database, false);

        CHECK(data->is_valid());
        CHECK(data->get_hash() != orig_hash);
        CHECK(data->nb_sorbed() == 2);
        CHECK(data->get_id_sorbed("SA1") == 1);
        CHECK(data->nu_sorbed(1, 2) == 1);
        CHECK(data->nu_sorbed(1, 3) == 1);
        CHECK(data->nb_sorption_sites(1) == 2);

    }

    SECTION("Append compounds") {
        size_t orig_hash = data->get_hash();
        DataAppender appender(data);
        appender.add_compounds(compounds_appendix_database, false);

        CHECK(data->is_valid());
        CHECK(data->get_hash() != orig_hash);
        CHECK(data->nb_compounds() == 2);
        CHECK(data->get_id_compound("CompA1") == 1);
        CHECK(data->nu_compound(1, 0) == 6);
        CHECK(data->nu_compound(1, 4) == 1);

    }
}
