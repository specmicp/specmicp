# ReactMiCP     {#reactmicp_file}


 ReactMiCP is a reactive transport solver with the following features :

- Sequential Iterative Algorithm [SIA] algorithm
- Flexible, allow user-defined system, equations, models, extensible...
- adaptive timestep
- 
- 
 
Two systems are currently defined :
- saturated system
- unsaturated system

The user can use or extend these systems, or define new ones.

The next files presents the solver in depth :

- @subpage reactmicp_governing_file
- @subpage reactmicp_solver_file
- [Adaptive timestep]()
- [Saturated system]()
- [Unsaturated system]()
- [Defining a new system]()

