SpecMiCP / ReacMiCP {#mainpage}
===================

[TOC]

Documentation {#documentation}
=============

- [Database API](@ref database)
- [Specmicp API](@ref specmicp_api_file) 
- [ReactMiCP] (@ref reactmicp_file)

Obtaining the code {#code}
==================

The code is accessible through [bitbucket Git repository][1]. 
Requirements and installation documentation are available in the `INSTALL` 
file.



[1]: https://bitbucket.org/specmicp/specmicp
