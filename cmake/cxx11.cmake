# check for a c++11 compiler flag

# to set the flag, set the variable CXX11_FLAG

include(CheckCXXCompilerFlag)

if(NOT CXX11_FLAG)
    check_cxx_compiler_flag("-std=c++11" HAS_CXX11)
    if(NOT HAS_CXX11)
        message(FATAL_ERROR "A c++11 compatible compiler is necessary")
    else()
        set(CXX11_FLAG "-std=c++11" CACHE INTERNAL "The C++11 flag")
    endif()
endif()
