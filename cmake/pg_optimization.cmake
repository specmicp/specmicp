# Profile guided optimization
# ============================

# To compile for training : cmake . -DSPECMICP_PROFILE_GENERATE
# To compile using the generated profiles : cmake . -DSPECMICP_PROFILE_USE

# functiom set_pgo_flag(<files>)
# ---------------------------
# Append the correct pgo compile flags to <files>


# set the generate flag
macro( _pg_generate flags )
     set( flags "-fprofile-generate" )
     if ( SPECMICP_USE_OPENMP AND OPENMP_FOUND )
         set(flags  " -fprofile-generate -fprofile-correction" )
     endif()
endmacro( _pg_generate )

# set the use flag
macro( _pg_use flags )
    set( flags "-fprofile-use;-Wno-error=coverage-mismatch" )
    if ( SPECMICP_USE_OPENMP AND OPENMP_FOUND )
          set( flags " -fprofile-use -Wno-error=coverage-mismatch -fprofile-correction" )
     endif()
endmacro( _pg_use )

# set the correct flag
function( set_pgo_flag )
    if( CMAKE_COMPILER_IS_GNUCXX )
        if( SPECMICP_PROFILE_GENERATE )
            _pg_generate( flags )
        elseif( SPECMICP_PROFILE_USE )
            _pg_use( flags )
        endif()
        set_property( SOURCE ${ARGN} APPEND_STRING PROPERTY COMPILE_FLAGS ${flags} )
    endif()
endfunction( set_pgo_flag )

function(target_link_profile target)
    if( CMAKE_COMPILER_IS_GNUCXX )
        if( SPECMICP_PROFILE_GENERATE )
            target_link_libraries(${target} PUBLIC gcov)
        endif()
    endif()
endfunction(target_link_profile)
