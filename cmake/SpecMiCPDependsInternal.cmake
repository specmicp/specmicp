# This file contains internal dependancy that are not needed by projects using
# the library

# OpenMP
#-------
# not required but recommended in release mode
if(SPECMICP_USE_OPENMP)
    find_package(OpenMP)
    if (OPENMP_FOUND)
        set(SPECMICP_HAVE_OPENMP ON CACHE BOOL "True if SpecMiCP uses openmp")
    endif(OPENMP_FOUND)
endif()


# libc functions required
# -----------------------

# note : most of this functions are required by specmicp_common/filesystem.cpp


# just a friendly warning
if(NOT UNIX)
    message(WARNING "Not tested on non linux platform ! Probably won't work")
endif()


include(CheckIncludeFile)
include(CheckFunctionExists)

# just a quick macro to check if a required include exist
macro(check_required_include name var)
    CHECK_INCLUDE_FILE( ${name}  ${var} )
    if (NOT ${var})
        message(SEND_ERROR "Missing required include ${name}")
    endif()
endmacro(check_required_include)

macro(check_required_function name var)
    CHECK_FUNCTION_EXISTS( ${name}  ${var} )
    if (NOT ${var})
        message(SEND_ERROR "Missing required function ${name}")
    endif()
endmacro(check_required_function)


check_required_include( "string.h"   HAVE_STRING_H   )
check_required_include( "dirent.h"   HAVE_DIRENT_H   )
check_required_include( "unistd.h"   HAVE_UNISTD_H   )
check_required_include( "sys/stat.h" HAVE_SYS_STAT_H )
check_required_include( "limits.h"   HAVE_LIMITS_H   )
check_required_include( "stdlib.h"   HAVE_STDLIB_H   )

check_required_include("time.h" HAVE_NOTSYS_TIME_H )
check_required_function("strptime" HAVE_STRPTIME )

# These includes are optional
CHECK_INCLUDE_FILE( "sys/time.h" HAVE_TIME_H )
CHECK_INCLUDE_FILE( "sys/resource.h" HAVE_RESOURCE_H )
if (${HAVE_TIME_H} AND ${HAVE_RESOURCE_H})
    CHECK_FUNCTION_EXISTS("getrusage" SPECMICP_HAVE_GETRUSAGE)
endif()

CHECK_FUNCTION_EXISTS( "secure_getenv" SPECMICP_HAVE_SECURE_GETENV )
