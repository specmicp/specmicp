# SanitizerBuild
# ===============
#
# Define a new build type using the "-fsanitize=" options
# It can be activated the normal way : -DCMAKE_BUILD_TYPE=Sanitizer
#
# author : Fabien Georget
# This is part of the SpecMiCP build system


#This are the flags that may be included
set(ADDRESS_SANITIZER_FLAG "-fsanitize=address")

# test if the flags exists
include(CheckCXXCompilerFlag)
set(CMAKE_REQUIRED_FLAGS "-Werror ${ADDRESS_SANITIZER_FLAG}")
check_cxx_compiler_flag("${ADDRESS_SANITIZER_FLAG}" HAVE_ADDRESS_SANITIZER)
unset(CMAKE_REQUIRED_FLAGS)

if (NOT HAVE_ADDRESS_SANITIZER)
    message(WARNING "The address sanitizer does not exist")
else()

    set(SANITIZER_EXTRA_WARNINGS "-Wextra"
        CACHE STRING "Extra warnings for the sanitizer build")
     set(FLAGS_ASANITIZER "${FLAGS_ASANITIZER} -O2 -g -Wall -pedantic ${SANTIZER_EXTRA_WARNINGS} ${ADDRESS_SANITIZER_FLAG} -fno-omit-frame-pointer")

# -Wfloat-conversion -Winline -Weffc++ -Wold-style-cast -Woverloaded-virtual

    set( CMAKE_CXX_FLAGS_ASANITIZER "${FLAGS_ASANITIZER}" CACHE STRING
        "Flags used by the C++ compiler during sanitizer builds."
        FORCE )
    set( CMAKE_C_FLAGS_ASANITIZER "${FLAGS_ASANITIZER}" CACHE STRING
        "Flags used by the C compiler during sanitizer builds."
        FORCE )

    set( CMAKE_EXE_LINKER_FLAGS_SANITIZER
        "${FLAGS_ASANITIZER}" CACHE STRING
        "Flags used for linking binaries during SANITIZER builds."
        FORCE )
    set( CMAKE_SHARED_LINKER_FLAGS_ASANITIZER
        "${FLAGS_ASANITIZER}" CACHE STRING
        "Flags used by the shared libraries linker during SANITIZER builds."
        FORCE )
    set( CMAKE_STATIC_LINKER_FLAGS_ASANITIZER
        "" CACHE STRING
        "Flags used by the static libraries linker during SANITIZER builds."
        FORCE )

mark_as_advanced(
    CMAKE_CXX_FLAGS_ASANITIZER
    CMAKE_C_FLAGS_ASANITIZER
    CMAKE_EXE_LINKER_FLAGS_ASANITIZER
    CMAKE_SHARED_LINKER_FLAGS_ASANITIZER
    CMAKE_STATIC_LINKER_FLAGS_ASANITIZER
 )
    # Update the documentation string of CMAKE_BUILD_TYPE for GUIs
    set( CMAKE_BUILD_TYPE "${CMAKE_BUILD_TYPE}" CACHE STRING
        "Choose the type of build, options are: None Debug Release RelWithDebInfo MinSizeRel ASanitizer UBSanitizer."
        FORCE )

endif()
