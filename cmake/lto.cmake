# Link time optimisation
# =======================

# Ref : https://lists.launchpad.net/kicad-developers/msg17900.html

# The script need to find the GCC/llvm version of ar / nm / ranlib to access
# the LTO plugin.
# If they are not in PATH, their location can be given by the variable BINUTILS_DIR
# They also set the flags to use.

if( SPECMICP_LTO AND USE_LD_GOLD)

    check_cxx_compiler_flag( "-flto -fuse-ld=gold" HAVE_LTO_FLAG )
    check_cxx_compiler_flag( "-flto -ffat-lto-objects -fuse-ld=gold" HAVE_FAT_LTO_OBJECT )

    if (HAVE_LTO_FLAG)

        # It's necessary to find the gcc/llvm ar/nm/ranlib because we need the lto plugin
        if ("${CMAKE_CXX_COMPILER_ID}" MATCHES "GNU")
            find_program( LTO_AR NAMES gcc-ar HINTS ${BINUTILS_DIR} )
            find_program( LTO_NM NAMES gcc-nm HINTS ${BINUTILS_DIR} )
            find_program( LTO_RANLIB NAMES gcc-ranlib HINTS ${BINUTILS_DIR} )
            if ( HAVE_FLAT_LTO_OBJECT )
                if( SPECMICP_FAT_LTO )
                    set ( LTO_FLAGS "-flto -ffat-lto-objects" )
                else()
                    set ( LTO_FLAGS "-flto -fno-fat-lto-objects" )
                endif( SPECMICP_FAT_LTO )
            else()
                set( LTO_FLAGS "-flto" )
            endif( HAVE_FLAT_LTO_OBJECT )
        elseif ("${CMAKE_CXX_COMPILER_ID}" MATCHES "Clang")
            find_program( LTO_AR NAMES llvm-ar HINTS ${BINUTILS_DIR} )
            find_program( LTO_NM NAMES llvm-nm HINTS ${BINUTILS_DIR} )
            find_program( LTO_RANLIB NAMES llvm-ranlib HINTS ${BINUTILS_DIR} )
            set ( LTO_FLAGS " -flto")
        endif()


        if( LTO_AR AND LTO_NM AND LTO_RANLIB )
            set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${LTO_FLAGS}")
            # use correct programs
            set( CMAKE_AR "${LTO_AR}" )
            set( CMAKE_NM "${LTO_NM}" )
            set( CMAKE_RANLIB "${LTO_RANLIB}" )
        else()
            message( WARNING "Binutils wrappers with LTO plugin could not be found. Disabling LTO." )
        endif()

        mark_as_advanced(
            LTO_AR
            LTO_NM
            LTO_RANLIB
            LTO_FLAGS
            )

    endif(HAVE_LTO_FLAG)

endif()
