/* =============================================================================

 Copyright (c) 2014 - 2016
 F. Georget <fabieng@princeton.edu> Princeton University
 All rights reserved.


Redistribution and use in source and binary forms, with or without modification,
are permitted provided that the following conditions are met:

    1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.

    2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

    3. Neither the name of the copyright holder nor the names of its
    contributors may be used to endorse or promote products derived from this
    software without specific prior written permission.

* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR
ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE. *

============================================================================= */
#include "specmicp/specmicp.hpp"

#include "specmicp_common/timer.hpp"

#include <iostream>

class AutomaticReactionPath: public specmicp::EquilibriumCurve
{
public:

    AutomaticReactionPath()
    {
        specmicp::database::Database thedatabase("../data/cemdata.yaml");
        std::map<std::string, std::string> swapping ({
                                                  {"H[+]","HO[-]"},
                                                  {"Si(OH)4", "SiO(OH)3[-]"},
                                                  {"Al[3+]","Al(OH)4[-]"},
                                                   {"Fe[2+]", "Fe(OH)4[-]"},
                                                    });
        thedatabase.swap_components(swapping);
        thedatabase.remove_half_cell_reactions(std::vector<std::string>({"SO4[2-]"}));
        thedatabase.remove_gas_phases();
        specmicp::RawDatabasePtr raw_data = thedatabase.get_database();
        set_database(raw_data);


        specmicp::Formulation formulation;
        specmicp::scalar_t mult = 6e3;

        specmicp::scalar_t m_c3s = mult*0.6;
        specmicp::scalar_t m_c2s = mult*0.2;
        specmicp::scalar_t m_c3a = mult*0.10;
        specmicp::scalar_t m_c4af = mult*0.05;
        specmicp::scalar_t m_gypsum = mult*0.05;
        specmicp::scalar_t wc = 0.8;
        specmicp::scalar_t m_water = wc*1e-3*(
                      m_c3s*(3*56.08+60.08)
                    + m_c2s*(2*56.06+60.08)
                    + m_c3a*(3*56.08+101.96)
                    + m_c4af*(4*56.08+101.96+159.07)
                    + m_gypsum*(56.08+80.06+2*18.02)
                    );

        formulation.mass_solution = m_water;
        formulation.amount_minerals = {
            {"C3S", m_c3s},
            {"C2S", m_c2s},
            {"C3A", m_c3a},
            {"C4AF", m_c4af},
            {"Gypsum", m_gypsum}
        };
        formulation.extra_components_to_keep = {"HCO3[-]", };
        formulation.minerals_to_keep = {
            "Portlandite",
            "CSH,jennite",
            "CSH,tobermorite",
            "SiO2,am",
            "Calcite",
            "Al(OH)3,am",
            "C3AH6",
            "C4AH13",
            "C2AH8",
            "CAH10",
            "Monosulfoaluminate",
            "Tricarboaluminate",
            "Monocarboaluminate",
            "Hemicarboaluminate",
            //"Straetlingite",
            "Gypsum",
            "Ettringite",
            "Thaumasite",
            "C3FH6",
            "C4FH13",
            "C2FH8",
            "Fe(OH)3(mic)",
            "Fe-Ettringite",
            "Fe-Monosulfate",
            "Fe-Monocarbonate",
            "Fe-Hemicarbonate"
        };


        specmicp::Vector total_concentrations = specmicp::Dissolver(raw_data).dissolve(formulation);

        id_h2o = thedatabase.component_label_to_id("H2O");
        id_ho  = thedatabase.component_label_to_id("HO[-]");
        id_hco3 = thedatabase.component_label_to_id("HCO3[-]");
        id_co2g = thedatabase.gas_label_to_id("CO2(g)");
        id_ca = thedatabase.component_label_to_id("Ca[2+]");

        constraints() = specmicp::AdimensionalSystemConstraints(total_concentrations);
        constraints().charge_keeper = id_ho;

        specmicp::AdimensionalSystemSolverOptions& options = solver_options();
        options.solver_options.maxstep = 50.0;
        options.solver_options.max_iter = 200;
        options.solver_options.maxiter_maxstep = 200;
        options.solver_options.use_crashing = false;
        options.solver_options.use_scaling = true;
        options.solver_options.non_monotone_linesearch = true;
        options.solver_options.factor_descent_condition = -1;
        options.solver_options.factor_gradient_search_direction = 300;
        options.solver_options.projection_min_variable = 1e-9;
        options.solver_options.fvectol = 1e-10;
        options.solver_options.steptol = 1e-16;
        options.system_options.non_ideality_tolerance = 1e-14;
        options.system_options.scaling_electron = 1e10;

        specmicp::Vector x;
        specmicp::AdimensionalSystemSolver solver(raw_data, constraints(), solver_options());
        solver.initialize_variables(x, 0.5, {
                                        {"Ca[2+]", -2.0},
                                        {"HO[-]", -1.4},
                                        {"Al(OH)4[-]", -4},
                                        {"Fe(OH)4[-]", -4},
                                        {"SiO(OH)3[-]", -6.0},
                                        {"SO4[2-]", -6.2}
                                    });
        specmicp::micpsolver::MiCPPerformance perf = solver.solve(x);
        if (perf.return_code <= specmicp::micpsolver::MiCPSolverReturnCode::NotConvergedYet)
        {
            error_handling("Failed to solve first problem, return code : "
                           + std::to_string((int) perf.return_code)
                           + ".");
        }
        solution_vector() = x;
        initialize_solution(solver.get_raw_solution(x));

//        std::cout << x << std::endl;
//        std::cout << current_solution().secondary_molalities << std::endl;

        std::cout << "Buffering \t" << "porosity \t pH \t Eh";
        for (specmicp::index_t mineral: raw_data->range_mineral())
        {
            std::cout << "\t" << raw_data->minerals.get_label(mineral);
        }
        std::cout << std::endl;

        output();

    }

    void output()
    {
        specmicp::AdimensionalSystemSolutionExtractor sol(current_solution(), database(), solver_options().units_set);
        std::cout <<  constraints().total_concentrations(id_hco3)/constraints().total_concentrations(id_ca)
                   << "\t" << sol.porosity()
                   << "\t" << sol.pH()
                   << "\t" << sol.Eh();
        for (specmicp::index_t mineral: database()->range_mineral())
        {
            std::cout << "\t" << sol.volume_fraction_mineral(mineral);
        }
        std::cout << std::endl;
    }

    void update_problem()
    {
        constraints().total_concentrations(id_hco3) += 100;
        constraints().total_concentrations(id_ho) -= 100;
        constraints().total_concentrations(id_h2o) += 100;
    }

private:
    specmicp::index_t id_h2o;
    specmicp::index_t id_ho;
    specmicp::index_t id_hco3;
    specmicp::index_t id_co2g;
    specmicp::index_t id_ca;
};

int main()
{
    specmicp::logger::ErrFile::stream() = &std::cerr;
    specmicp::stdlog::ReportLevel() = specmicp::logger::Warning;

    specmicp::Timer timer;

    AutomaticReactionPath test_automatic;

    for (int i=0; i<180; ++i)
    {
        test_automatic.run_step();
    }
    timer.stop();

    std::cout << "Execution time : " << timer.elapsed_time() << "s" << std::endl;

    return EXIT_SUCCESS;
}
