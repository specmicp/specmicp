####################################################
#                                                  #
#           SpecMiCP : CMakeLists.txt              #
#                                                  #
####################################################

project(specmicp)
cmake_minimum_required(VERSION 3.3)

set(SPECMICP_VERSION 0.0.4)

# CMake Options
# =============

# cmake modules
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} ${PROJECT_SOURCE_DIR}/cmake)
include(SpecMiCPOptions)

# External Package
# ================

include(SpecMiCPDependsInternal)
include(SpecMiCPDepends)


# Build Mode
# ==========

# sanitizer
# ---------
# to check memory, undefined behavior and all...
include(ASanitizer)
include(UBSanitizer)

# compilation flags
# ============================================================================

# require C++11 standard by default
# use C++14 if possible
# note : these cmake features require CMake >= 3.2
if("cxx_std_11" IN_LIST CMAKE_CXX_KNOWN_FEATURES)
    # cxx_11_std is only available in cmake >= 3.8
    if("cxx_std_14" IN_LIST CMAKE_CXX_COMPILE_FEATURES)
        set(SPECMICP_CXX_STANDARD 14 CACHE STRING "CXX standard version")
        set(SPECMICP_CXX_FEATURES "cxx_std_14" CACHE STRING "CXX interface features")
    else()
        set(SPECMICP_CXX_STANDARD 11 CACHE STRING "CXX standard version")
        set(SPECMICP_CXX_FEATURES "cxx_std_11" CACHE STRING "CXX interface features")
    endif()
else()
    set(SPECMICP_CXX_STANDARD 11 CACHE STRING "CXX standard version")
    set(CMAKE_CXX_STANDARD ${SPECMICP_CXX_STANDARD})
    set(CMAKE_CXX_STANDARD_REQUIRED ON)
endif()

macro (spc_set_cxx_version_interface target)
    if(DEFINED SPECMICP_CXX_FEATURES)
        set_property(TARGET ${target} PROPERTY INTERFACE_COMPILE_FEATURES ${SPECMICP_CXX_FEATURES})
    endif()
endmacro()

# Some more flags
# Check corresponding cmake files for more detail

# Gold Linker
# ------------
include(gold_linker)
# PGO optimization
# ----------------
include(pg_optimization)
# Symbols visibility
# ------------------
include(visibility_flag)
# Link time optimization
# ----------------------
include(lto)

# set the flags
# -------------
if (OPENMP_FOUND)
    set( CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    set( CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${OpenMP_CXX_FLAGS}")
endif()
# let's be pedantic in DEBUG mode, it's always fun
set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -pedantic")

# always best to check what we do...
message(STATUS "C++ flags : ${CMAKE_CXX_FLAGS}")


# Directories
# -----------
include(SpecMiCPDirs)

#                   CPP API : SpecMiCP / ReactMiCP
#########################################################################

# the main source directory - the c++ api
set(SPECMICP_CPP_API ${CMAKE_CURRENT_SOURCE_DIR}/src)

# export names
set(SPECMICP_TARGET SpecMiCPTargets)
set(SPECMICP_CONFIG SpecMiCPConfig)


include_directories( ${SPECMICP_CPP_API} )
add_subdirectory(    ${SPECMICP_CPP_API} )

#export
export(EXPORT ${SPECMICP_TARGET}  NAMESPACE "SpecMiCP::" FILE ${SPECMICP_TARGET}.cmake)
install(EXPORT ${SPECMICP_TARGET} NAMESPACE "SpecMiCP::" DESTINATION ${SHARE_INSTALL_DIR}/cmake)

# the necessary libraries to link to
set( SPECMICP_LIBS  # just speciation solver
    specmicp specmicp_database specmicp_common
)
set( REACTMICP_LIBS # reactive transport solver
    reactmicp dfpm ${SPECMICP_LIBS}
)
# static versions
set( SPECMICP_STATIC_LIBS
    specmicp_static specmicp_database_static specmicp_common_static
)
set( REACTMICP_STATIC_LIBS
    reactmicp_static dfpm_static ${SPECMICP_STATIC_LIBS}
)


#                        Databases
#########################################################################

add_subdirectory( data )

#                       Documentation
#########################################################################

# "common" documentation
# -----------------------
set(
    DOCS_LIST
    ${CMAKE_CURRENT_SOURCE_DIR}/README.md
    ${CMAKE_CURRENT_SOURCE_DIR}/INSTALL
    ${CMAKE_CURRENT_SOURCE_DIR}/COPYING
)

add_custom_target(docs SOURCES ${DOCS_LIST})
install(FILES ${DOCS_LIST}
    DESTINATION ${SHARE_INSTALL_DIR}
)

# scripts
# --------

add_subdirectory( scripts )

# Doxygen documentation
# ---------------------
add_subdirectory( doc )

#                          Tests
#########################################################################

if( SPECMICP_TEST )
    enable_testing(true)
endif()
add_subdirectory( tests )

#                        Examples
#########################################################################
add_subdirectory( examples )

#                       Benchmark
########################################################################
if (SPECMICP_BENCHMARK)
    add_subdirectory( benchmark )
endif()

#                       Docker
########################################################################

add_subdirectory( docker )

#                    Package Config File
########################################################################

include(CMakePackageConfigHelpers)

configure_package_config_file(cmake/${SPECMICP_CONFIG}.cmake.in
  ${CMAKE_CURRENT_BINARY_DIR}/${SPECMICP_CONFIG}.cmake
  INSTALL_DESTINATION ${SHARE_INSTALL_DIR}/cmake
  PATH_VARS INCLUDE_INSTALL_DIR SHARE_INSTALL_DIR)

write_basic_package_version_file(
  ${CMAKE_CURRENT_BINARY_DIR}/SpecMiCPConfigVersion.cmake
  VERSION ${SPECMICP_VERSION}
  COMPATIBILITY SameMajorVersion )
install(FILES ${CMAKE_CURRENT_BINARY_DIR}/${SPECMICP_CONFIG}.cmake
              ${CMAKE_CURRENT_BINARY_DIR}/SpecMiCPConfigVersion.cmake
              ${CMAKE_CURRENT_SOURCE_DIR}/cmake/SpecMiCPDepends.cmake
              ${CMAKE_CURRENT_SOURCE_DIR}/cmake/FindEigen3Unsupported.cmake
        DESTINATION ${SHARE_INSTALL_DIR}/cmake )
