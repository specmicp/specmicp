#!/bin/bash

git clone https://bitbucket.org/specmicp/specmicp
cd specmicp
mkdir build && cd build
cmake ../
make && ctest
